﻿using System.Collections;
using System.Collections.Generic;
using Codebase.SceneLogic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameDevSettingsData", menuName = "ScriptableObjects/GameDevSettingsData", order = 2)]
public class GameDevSettingsData : ScriptableObject
{
    public bool SkipSplash;
    public bool SkipMenu;
    public bool SkipShotAnimation;
    //public bool SkipGame;
    //public bool SKipCredits;
}
