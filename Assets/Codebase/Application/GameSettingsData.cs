﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsData", menuName = "ScriptableObjects/GameSettingsData", order = 2)]
public class GameSettingsData : ScriptableObject
{
    [SerializeField] private bool AudioMute, ControlInverseY, GraphicsAA, GraphicsCA, GraphicsVignette, GraphicsSSR, GraphicsBloom, ControlHoldLook;
    [SerializeField] private float VolumeMaster, VolumeMusic, VolumeEffect;

    private Dictionary<SettingOfGame, bool> lookUpBool;
    private Dictionary<SettingOfGame, float> lookUpFloat;

    public bool GetBoolSetting(SettingOfGame settingOfGame)
    {
        ImportFieldsToDictionary();
        if (lookUpBool.ContainsKey(settingOfGame))
            return lookUpBool[settingOfGame];
        
        LogError(settingOfGame);
        return false;
    }

    public void SetBoolSetting(SettingOfGame settingOfGame, bool value)
    {
        if (lookUpBool.ContainsKey(settingOfGame))
        {
            lookUpBool[settingOfGame] = value;
            ExportDictionaryToFields();
        }
        else LogError(settingOfGame);
    }

    public float GetFloatSetting(SettingOfGame settingOfGame)
    {
        ImportFieldsToDictionary();
        if (lookUpFloat.ContainsKey(settingOfGame))
            return lookUpFloat[settingOfGame];
        
        LogError(settingOfGame);
        return 0f;
    }

    public void SetFloatSetting(SettingOfGame settingOfGame, float value)
    {
        if (lookUpFloat.ContainsKey(settingOfGame))
        {
            lookUpFloat[settingOfGame] = value;
            ExportDictionaryToFields();
        }
        else LogError(settingOfGame);
    }

    private void LogError(SettingOfGame setting)
    {
        Debug.LogError($"{setting} not found",this);
    }

    private void ImportFieldsToDictionary()
    {
        lookUpBool = new Dictionary<SettingOfGame, bool>
        {
            {SettingOfGame.AudioMute, AudioMute},
            {SettingOfGame.GraphicsSSAO, GraphicsAA},
            {SettingOfGame.GraphicsCA, GraphicsCA},
            {SettingOfGame.GraphicsSSR, GraphicsSSR},
            {SettingOfGame.GraphicsVignette, GraphicsVignette},
            {SettingOfGame.GraphicsBloom, GraphicsBloom},
            {SettingOfGame.ControlHoldLook, ControlHoldLook},
            {SettingOfGame.ControlInverseY, ControlInverseY}
        };
        
        lookUpFloat = new Dictionary<SettingOfGame, float>
        {
            {SettingOfGame.VolumeMaster, VolumeMaster},
            {SettingOfGame.VolumeMusic, VolumeMusic},
            {SettingOfGame.VolumeEffect, VolumeEffect}
        };
    }

    private void ExportDictionaryToFields()
    {
        AudioMute = lookUpBool[SettingOfGame.AudioMute];
        GraphicsAA = lookUpBool[SettingOfGame.GraphicsSSAO];
        GraphicsCA = lookUpBool[SettingOfGame.GraphicsCA];
        GraphicsSSR = lookUpBool[SettingOfGame.GraphicsSSR];
        GraphicsVignette = lookUpBool[SettingOfGame.GraphicsVignette];
        GraphicsBloom = lookUpBool[SettingOfGame.GraphicsBloom];
        ControlHoldLook = lookUpBool[SettingOfGame.ControlHoldLook];
        ControlInverseY = lookUpBool[SettingOfGame.ControlInverseY];
        
        VolumeMaster = lookUpFloat[SettingOfGame.VolumeMaster];
        VolumeMusic = lookUpFloat[SettingOfGame.VolumeMusic];
        VolumeEffect = lookUpFloat[SettingOfGame.VolumeEffect];
    }
}
