﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using Codebase.Application;
using UnityEngine;

public class GameSettingsLoader : MonoBehaviour
{
	[SerializeField] private GameSettingsData buildSettings = default;
	[SerializeField] private bool enableInEditor;
	private IGameSettingsMediator gameSettingsMediator;
	private IGameStateMachine gameStateMachine;
	
	protected virtual void Awake()
	{
		gameSettingsMediator = FindObjectOfType<GameSettingsMediator>();
		gameStateMachine = FindObjectOfType<GameStateMachine>();
	}

	private void OnEnable()
	{
		gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged; 
	}

	private void GameStateMachine_OnStateChanged(StateOfGame state)
	{
		if ((!Application.isEditor || enableInEditor) && state == StateOfGame.Booting)
		{
			if (buildSettings != null && gameSettingsMediator != null)
				Export();
			else Debug.LogWarning("loader not setup properly", this);
		}
	}
	
	private void OnDisable()
	{
		gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged; 
	}

	protected virtual void Start()
    {
	  //   if (buildSettings != null && gameSettingsMediator != null)
			// StartCoroutine(ExportLate());
	  //   else Debug.LogWarning("loader not setup properly", this);
    }

    private IEnumerator ExportLate()
    {
	    yield return null;
	    yield return null;
	    yield return null;

	    Export();
    }

    private void Export()
    {
	    ExportBool(SettingOfGame.GraphicsBloom, 
		    SettingOfGame.GraphicsVignette, 
		    SettingOfGame.GraphicsCA, 
		    SettingOfGame.GraphicsSSR, 
		    SettingOfGame.GraphicsSSAO,
		    SettingOfGame.ControlHoldLook,
		    SettingOfGame.ControlInverseY,
		    SettingOfGame.AudioMute);
	    
	    ExportFloat(SettingOfGame.VolumeEffect,
		    SettingOfGame.VolumeMaster,
		    SettingOfGame.VolumeMusic);
    }

    private void ExportBool(params SettingOfGame[] settings)
    {
	    foreach (var setting in settings)
	    {
		    gameSettingsMediator.SetState(setting, buildSettings.GetBoolSetting(setting));
	    }
    }
    
    private void ExportFloat(params SettingOfGame[] settings)
    {
	    foreach (var setting in settings)
	    {
		    gameSettingsMediator.SetState(setting, buildSettings.GetFloatSetting(setting));
	    }
    }
}
