﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingsMediator : MonoBehaviour, IGameSettingsMediator
{
	[SerializeField] private GameSettingsData gameSettingsData = default;
	public event Action<SettingOfGame, bool> OnSettingBoolStateChanged;
	public event Action<SettingOfGame, float> OnSettingFloatStateChanged;

	private readonly Dictionary<ElementOfMenu, SettingOfGame> menuSettingLookup = new Dictionary<ElementOfMenu, SettingOfGame>()
	{
		{ElementOfMenu.AudioMute, SettingOfGame.AudioMute},
		{ElementOfMenu.VolumeMaster, SettingOfGame.VolumeMaster},
		{ElementOfMenu.VolumeMusic, SettingOfGame.VolumeMusic},
		{ElementOfMenu.VolumeEffect, SettingOfGame.VolumeEffect},
		{ElementOfMenu.GraphicsSSAO, SettingOfGame.GraphicsSSAO},
		{ElementOfMenu.GraphicsVignette, SettingOfGame.GraphicsVignette},
		{ElementOfMenu.GraphicsBloom, SettingOfGame.GraphicsBloom},
		{ElementOfMenu.GraphicsCA, SettingOfGame.GraphicsCA},
		{ElementOfMenu.GraphicsSSR, SettingOfGame.GraphicsSSR},
		{ElementOfMenu.ControlHoldLook, SettingOfGame.ControlHoldLook},
		{ElementOfMenu.ControlInverseY, SettingOfGame.ControlInverseY}
	};

	private IMenuElementManager menuElementManager;

	private SettingOfGame SettingOfMenuElement(ElementOfMenu element)
	{
		if (menuSettingLookup.ContainsKey(element))
			return menuSettingLookup[element];

		return SettingOfGame.Nothing;
	}
	
	public bool GetBoolState(SettingOfGame setting)
	{
		return gameSettingsData.GetBoolSetting(setting);
	}

	public bool GetBoolState(ElementOfMenu element)
	{
		return gameSettingsData.GetBoolSetting(SettingOfMenuElement(element));
	}
	
	public float GetFloatState(SettingOfGame setting)
	{
		return gameSettingsData.GetFloatSetting(setting);
	}
	
	public float GetFloatState(ElementOfMenu element)
	{
		return gameSettingsData.GetFloatSetting(SettingOfMenuElement(element));
	}

	public void SetState(SettingOfGame setting, bool value)
	{
		gameSettingsData.SetBoolSetting(setting,value);
		OnSettingBoolStateChanged?.Invoke(setting,value);
	}
	
	public void SetState(SettingOfGame setting, float value)
	{
		gameSettingsData.SetFloatSetting(setting,value);
		OnSettingFloatStateChanged?.Invoke(setting,value);
	}
	
	protected virtual void Awake()
	{
		menuElementManager = MenuElementManager.Instance;
	}

	protected void OnEnable()
	{
		menuElementManager.OnElementBoolStateChanged += MenuElementManager_OnElementBoolStateChanged;
		menuElementManager.OnElementFloatStateChanged += MenuElementManager_OnElementFloatStateChanged;
	}

	private void MenuElementManager_OnElementFloatStateChanged(ElementOfMenu element, float value)
	{

		if (menuSettingLookup.ContainsKey(element))
		{
			gameSettingsData.SetFloatSetting(menuSettingLookup[element],value);
			OnSettingFloatStateChanged?.Invoke(menuSettingLookup[element],value);
		}
	}

	private void MenuElementManager_OnElementBoolStateChanged(ElementOfMenu element, bool value)
	{
		if (menuSettingLookup.ContainsKey(element))
		{
			gameSettingsData.SetBoolSetting(menuSettingLookup[element],value);
			OnSettingBoolStateChanged?.Invoke(menuSettingLookup[element],value);
		}
	}
	
	protected void OnDisable()
	{
		menuElementManager.OnElementBoolStateChanged -= MenuElementManager_OnElementBoolStateChanged;
		menuElementManager.OnElementFloatStateChanged -= MenuElementManager_OnElementFloatStateChanged;
	}
}
