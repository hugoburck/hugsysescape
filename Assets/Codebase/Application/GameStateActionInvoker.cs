﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class GameStateActionInvoker : MonoBehaviour
{
	[SerializeField] private StateOfGame state = default;
	[SerializeField] private GameAction action = default;
	private IGameStateMachine gameStateMachine;

	protected virtual void Awake()
	{
		gameStateMachine = FindObjectOfType<GameStateMachine>();
	}

	private void OnEnable()
	{
		gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
	}

	private void GameStateMachine_OnStateChanged(StateOfGame state)
	{
		if (state == this.state && action != null)
			action.Action();
	}
	
	private void OnDisable()
	{
		gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
	}

	protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }
}
