﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using UnityEngine;

public class GameStateMachine : MonoBehaviour, IGameStateMachine
{
    public event Action<StateOfGame> OnStateChanged;
    [SerializeField] private StateOfGame backingState = default;
    [SerializeField] private GameDevSettingsData gameDevSettingsData = default;
    private IGameTransitionManager gameTransitionManager;
    private IShotAnimationManager shotAnimationManager;

    public StateOfGame State
    {
        get => backingState;
        private set
        {
            if (backingState == value)
                return;
            
            PreviousChangedState = backingState;
            backingState = value;
            OnStateChanged?.Invoke(backingState);
        }
    }

    public StateOfGame PreviousChangedState { get; private set; }

    private void Awake()
    {
        ServiceLocator.Instance.Register<IGameStateMachine>(this);
        gameTransitionManager = GameTransitionManager.Instance;
        shotAnimationManager = RoomIntroManager.Instance;
    }

    private void Start()
    {
        StartCoroutine(BootGame());
    }

    private IEnumerator BootGame()
    {
        yield return null;
        State = StateOfGame.Booting;
    }

    private void OnEnable()
    {
        gameTransitionManager.OnTransitioned += GameTransitionManager_OnTransitioned;
    }

    private void GameTransitionManager_OnTransitioned(StateOfGame stateOfGame)
    {
        if (State == StateOfGame.Booting)
            State = SkipState();
        else
            State = stateOfGame;
    }

    private StateOfGame SkipState()
    {
        if (Application.isEditor && gameDevSettingsData != null)
        {
            if (!gameDevSettingsData.SkipSplash)
                return StateOfGame.Splash;
            
            if (!gameDevSettingsData.SkipMenu)
                return StateOfGame.Menu;
            
            return StateOfGame.Game;
        }
        
        return StateOfGame.Splash;
    }

    private void OnDisable()
    {
        gameTransitionManager.OnTransitioned -= GameTransitionManager_OnTransitioned;
    }
}

    
