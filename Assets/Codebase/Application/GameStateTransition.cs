﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Audio;
using Codebase.Fade;

public class GameStateTransition : MonoBehaviourSingleton<GameStateTransition>
{
	public event Action OnTransitionStart;
	public event Action<StateOfGame> OnTransitionEnd;
	private IImageFader imageFader;
	private IAudioMusicPlayer audioMusicPlayer;
	
	public void Transition(StateOfGame state, float seconds = 3)
	{
		StartCoroutine(TransitionRoutine(state, seconds));
	}
	
	public bool IsInTransition { get; private set; }
	
	private IEnumerator TransitionRoutine(StateOfGame state, float seconds)
	{
		IsInTransition = true;
		OnTransitionStart?.Invoke();
		imageFader.FadeIn(seconds);
		audioMusicPlayer.StopCurrentMusic(seconds);
		while (imageFader.IsInTransition)
			yield return null;

		IsInTransition = false;
		OnTransitionEnd?.Invoke(state);
	}
	
	protected virtual void Awake()
	{
        ServiceLocator.Instance.Register<GameStateTransition>(this);
        StartCoroutine(LateAwake());
	}

	private IEnumerator LateAwake()
	{
		yield return null;
		imageFader = ServiceLocator.Instance.Resolve(imageFader);
		audioMusicPlayer = ServiceLocator.Instance.Resolve(audioMusicPlayer);
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }
}
