﻿using System;
using System.Collections;
using Codebase.Application;
using UnityEngine;

public abstract class GameTransition : MonoBehaviour
{
	public event Action<StateOfGame> OnTransitionEnd;
	public event Action OnTransitionStart;
	private IGameTransitionManager gameTransitionManager;

	protected void FireOnTransitionEnd(StateOfGame state)
	{
		OnTransitionEnd?.Invoke(state);
	}

	protected void FireOnTransitionStart()
	{
		OnTransitionStart?.Invoke();
	}
	
	protected virtual void Awake()
	{
		gameTransitionManager = GameTransitionManager.Instance;
	}

	protected virtual void OnEnable()
	{
		gameTransitionManager.Register(this);
	}

	protected virtual void OnDisable()
	{
		gameTransitionManager.Unregister(this);
	}
}