﻿using System;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Helpers;
using UnityEngine;

public class GameTransitionManager : Singleton<GameTransitionManager>, IGameTransitionManager
{
    public event Action<StateOfGame> OnTransitioned;
    public event Action OnTransitioning;
    private List<GameTransition> gameTransitions;

    public GameTransitionManager()
    {
        gameTransitions = new List<GameTransition>();
    }
    
    public void Register(GameTransition gameTransition)
    {
        if (!gameTransitions.Contains(gameTransition))
        {
            gameTransitions.Add(gameTransition);
            gameTransition.OnTransitionEnd += GameTransitionOnTransitionEnd;
            gameTransition.OnTransitionStart += GameTransitionOnTransitionStart;
        }
    }

    public void Unregister(GameTransition gameTransition)
    {
        if (gameTransitions.Contains(gameTransition))
        {
            gameTransitions.Remove(gameTransition);
            gameTransition.OnTransitionEnd -= GameTransitionOnTransitionEnd;
            gameTransition.OnTransitionStart -= GameTransitionOnTransitionStart;
        }
    }

    private void GameTransitionOnTransitionEnd(StateOfGame state)
    {
        OnTransitioned?.Invoke(state);
    }

    private void GameTransitionOnTransitionStart()
    {
        OnTransitioning?.Invoke();
    }
}