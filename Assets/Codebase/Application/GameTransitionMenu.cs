﻿public abstract class GameTransitionMenu : GameTransition
{
    private IMenuElementManager menuElementManager;

    protected override void Awake()
    {
        base.Awake();
        menuElementManager = MenuElementManager.Instance;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        menuElementManager.OnElementUsed += MenuElementManager_OnElementUsed;
    }

    protected abstract void MenuElementManager_OnElementUsed(ElementOfMenu element, float value);

    protected override void OnDisable()
    {
        base.OnDisable();
        menuElementManager.OnElementUsed -= MenuElementManager_OnElementUsed;
    }
}