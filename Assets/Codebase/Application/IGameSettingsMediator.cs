﻿using System;

public interface IGameSettingsMediator
{
    event Action<SettingOfGame, bool> OnSettingBoolStateChanged;
    event Action<SettingOfGame, float> OnSettingFloatStateChanged;
    bool GetBoolState(SettingOfGame setting);
    bool GetBoolState(ElementOfMenu element);
    float GetFloatState(SettingOfGame setting);
    float GetFloatState(ElementOfMenu element);
    void SetState(SettingOfGame setting, bool value);
    void SetState(SettingOfGame setting, float value);
}