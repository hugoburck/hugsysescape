﻿using System;

namespace Codebase.Application
{
    public interface IGameStateMachine
    {
        event Action<StateOfGame> OnStateChanged;
        StateOfGame State { get; }
        StateOfGame PreviousChangedState { get; }
    }
}