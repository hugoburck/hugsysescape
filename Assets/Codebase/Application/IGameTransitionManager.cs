﻿using System;
using Codebase.Application;

public interface IGameTransitionManager
{
    event Action<StateOfGame> OnTransitioned;
    event Action OnTransitioning;
    void Register(GameTransition gameTransition);
    void Unregister(GameTransition gameTransition);
}