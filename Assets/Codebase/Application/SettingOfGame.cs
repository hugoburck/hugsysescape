﻿public enum SettingOfGame
{
    Nothing = 0,
    AudioMute,
    ControlInverseY,
    ControlHoldLook,
    GraphicsSSAO,
    GraphicsCA,
    GraphicsSSR,
    GraphicsVignette,
    GraphicsBloom,
    VolumeMaster,
    VolumeMusic,
    VolumeEffect
}