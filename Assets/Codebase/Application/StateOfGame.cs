﻿namespace Codebase.Application
{
    public enum StateOfGame
    {
        Idle,
        Booting,
        Splash,
        Menu,
        Ending, //unused
        Credits, //unused
        Game,
        Quit
    }
}
