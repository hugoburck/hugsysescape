﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class TransitionBoot : GameTransition
{
    public uint seconds = 1;
    private IGameStateMachine gameStateMachine;
    
    // Start is called before the first frame update
    private void Start()
    {
        gameStateMachine = ServiceLocator.Instance.Resolve(gameStateMachine);
        OnEnable();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (gameStateMachine != null)
            gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
    }

    private void GameStateMachine_OnStateChanged(StateOfGame state)
    {
        if (state == StateOfGame.Booting)
            StartCoroutine(StartRoutine());
    }
    
    protected override void OnDisable()
    {
        base.OnDisable();
        if (gameStateMachine != null)
            gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
    }

    private IEnumerator StartRoutine()
    {
        FireOnTransitionStart();

        yield return new WaitForSeconds(seconds);
        
        FireOnTransitionEnd(StateOfGame.Splash);
    }
}
