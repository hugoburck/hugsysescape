﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Audio;
using Codebase.Fade;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class TransitionNewGame : GameTransitionMenu
{
	[SerializeField] private float FadeOutSeconds = 3f;
	private IImageFader imageFader;
	private IAudioMusicPlayer audioMusicPlayer;


	public void Load()
	{
		StartCoroutine(LoadRoutine());
	}

	private IEnumerator LoadRoutine()
	{
		FireOnTransitionStart();
		audioMusicPlayer.StopCurrentMusic(FadeOutSeconds);
		imageFader.FadeIn(FadeOutSeconds);
		while (imageFader.IsInTransition)
			yield return null;
		
		FireOnTransitionEnd(StateOfGame.Game);
	}

	protected virtual void Start()
	{
		audioMusicPlayer = ServiceLocator.Instance.Resolve(audioMusicPlayer);
		imageFader = ServiceLocator.Instance.Resolve(imageFader);
	}

	protected override void MenuElementManager_OnElementUsed(ElementOfMenu element, float value)
	{
		if (element == ElementOfMenu.GameNew)
		{
			Load();
		}
	}
	
}
