﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Fade;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class TransitionQuitDesktop : GameTransitionMenu
{
    private IImageFader imageFader;
    private IMenuElementManager menuElementManager;
    
    public void Quit()
    {
        if (!Application.isPlaying) 
            return;
        
        StartCoroutine(QuitRoutine());
    }

    private IEnumerator QuitRoutine()
    {
        FireOnTransitionStart();
        imageFader.FadeIn(2);
        while (imageFader.IsInTransition)
            yield return null;
        
        FireOnTransitionEnd(StateOfGame.Quit);
        if (Application.isEditor)
            ExitPlaying();
        else Application.Quit();
    }

    private void ExitPlaying()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#endif
    }

    protected override void MenuElementManager_OnElementUsed(ElementOfMenu element, float value)
    {
        if (element == ElementOfMenu.QuitToDesktop)
            Quit();
    }

    private void Start()
    {
        imageFader = ServiceLocator.Instance.Resolve(imageFader);
    }
}
