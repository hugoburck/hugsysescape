﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Audio;
using Codebase.Fade;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class TransitionQuitMenu : GameTransitionMenu
{
	private IImageFader imageFader;
	private IAudioMusicPlayer audioMusicPlayer;
	
	public void Quit()
	{
		StartCoroutine(LoadRoutine());
	}

	private IEnumerator LoadRoutine()
	{
		FireOnTransitionStart();
		imageFader.FadeIn(3);
		audioMusicPlayer.StopCurrentMusic(3);
		while (imageFader.IsInTransition)
			yield return null;
		
		FireOnTransitionEnd(StateOfGame.Menu);
	}

	protected virtual void Start()
	{
		imageFader = ServiceLocator.Instance.Resolve(imageFader);
		audioMusicPlayer = ServiceLocator.Instance.Resolve(audioMusicPlayer);
	}

	protected override void MenuElementManager_OnElementUsed(ElementOfMenu element, float value)
	{
		if (element == ElementOfMenu.QuitToMenu)
			Quit();
	}
}
