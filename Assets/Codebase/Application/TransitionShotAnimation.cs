﻿using Codebase.Application;

public class TransitionShotAnimation : GameTransition
{
    private IShotAnimationManager shotAnimationManager;

    protected override void Awake()
    {
        base.Awake();
        shotAnimationManager = RoomIntroManager.Instance;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        shotAnimationManager.OnEnd += ShotAnimationManager_OnEnd;
    }

    private void ShotAnimationManager_OnEnd()
    {
        if (shotAnimationManager.CurrentSequence == 1)
        {
            FireOnTransitionEnd(StateOfGame.Credits);
        }
    }
}