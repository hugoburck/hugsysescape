﻿using Codebase.Application;

public class TransitionSplash : GameTransition
{
    private ISplashNodeRunnerManager splashNodeRunnerManager;

    protected override void Awake()
    {
        base.Awake();
        splashNodeRunnerManager = SplashNodeRunnerManager.Instance;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        splashNodeRunnerManager.OnEnd += SplashNodeRunner_OnFinished;
    }

    private void SplashNodeRunner_OnFinished(StateOfGame stateOfGame)
    {
        if (stateOfGame == StateOfGame.Splash)
            FireOnTransitionEnd(StateOfGame.Menu);
        
        if (stateOfGame == StateOfGame.Credits)
            FireOnTransitionEnd(StateOfGame.Menu);
    }
    
    protected override void OnDisable()
    {
        base.OnDisable();
        splashNodeRunnerManager.OnEnd -= SplashNodeRunner_OnFinished;
    }
}