﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAudioEffectPlayer
{
	void Play();
	void Stop();
}

public class AudioEffectPlayer : MonoBehaviour, IAudioEffectPlayer
{
	[SerializeField] private AudioSource audioSource = default;

	public void Play()
	{
		if (audioSource != null)
			audioSource.Play();
	}

	public void Stop()
	{
		if (audioSource != null)
			audioSource.Stop();
	}


	protected virtual void Awake()
	{
		if (audioSource == null)
			GetComponent<AudioSource>();

		InitializationChecker.CheckImmediate(this, audioSource);
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }
}
