﻿using System;
using Codebase.Application;
using Codebase.PlayerControl;
using Codebase.SceneLogic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Codebase.Audio
{
	public sealed class AudioMusicPlayFromState : MonoBehaviour
	{
		[SerializeField] private AudioMusicPlayer audioMusicPlayer = default;
		private IGameStateMachine gameStateMachine;
		private IPlayerStateMachine playerStateMachine;
		private ILoadAlphaRoom loadAlphaRoom;
		private IShotAnimationStateMachine shotAnimationStateMachine;
		private int count;
		
		private void Awake()
		{
			gameStateMachine = FindObjectOfType<GameStateMachine>();
			playerStateMachine = FindObjectOfType<PlayerStateMachine>();
			loadAlphaRoom = FindObjectOfType<LoadAlphaRoom>();
			shotAnimationStateMachine = FindObjectOfType<ShotAnimationStateMachine>();
		}

		private void OnEnable()
		{
			gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
			playerStateMachine.OnChanged += PlayerStateMachine_OnChanged;
			loadAlphaRoom.OnLoaded += LoadAlphaRoom_OnLoaded;
			shotAnimationStateMachine.OnStateChanged += ShotAnimationStateMachine_OnStateChanged;
		}

		private void ShotAnimationStateMachine_OnStateChanged(StateOfShotAnimation state)
		{
			PlayAnimationMusic(state);
		}

		private void LoadAlphaRoom_OnLoaded(int sceneIndex)
		{
			if (shotAnimationStateMachine.State != StateOfShotAnimation.Idle)
				return;
				
			PlayLevelMusic(sceneIndex);
		}

		private void PlayerStateMachine_OnChanged(StateOfPlayer state)
		{
			if (gameStateMachine.State != StateOfGame.Game)
				return;

			if (state == StateOfPlayer.Exploring && playerStateMachine.PreviousChangedState == StateOfPlayer.Animating)
			{
				PlayLevelMusic(loadAlphaRoom.CurrentSceneIndex);
			}
		}

		private void GameStateMachine_OnStateChanged(StateOfGame state)
		{
			switch (state)
			{
				case StateOfGame.Menu :
					audioMusicPlayer.Play(TrackOfMusic.Menu, 3);
					break;
				case StateOfGame.Game :
					PlayLevelMusic(loadAlphaRoom.CurrentSceneIndex);
					break;
				case StateOfGame.Credits :
					//audioMusicPlayer.Play(TrackOfMusic.Credits,3);
					audioMusicPlayer.Play(TrackOfJingle.Uncover);
					break;
			}
		}

		private void PlayLevelMusic(int sceneIndex) //TODO must work with scene enum
		{
			if (sceneIndex == 0)
				audioMusicPlayer.Play(TrackOfMusic.GameA, 3);

			if (sceneIndex == 3)
				audioMusicPlayer.Play(TrackOfMusic.GameB, 3);
			
			if (sceneIndex == 5)
				audioMusicPlayer.Play(TrackOfMusic.GameC, 3);
			
			if (sceneIndex == 7)
				audioMusicPlayer.Play(TrackOfMusic.GameD, 3);
		}

		private void PlayAnimationMusic(StateOfShotAnimation state)
		{
			switch (state)
			{
				case StateOfShotAnimation.Intro:
					audioMusicPlayer.Play(TrackOfMusic.Intro, 1);
					break;
				case StateOfShotAnimation.Outro:
					audioMusicPlayer.Play(TrackOfMusic.Outro, 1);
					break;
				default :
					break;
			}
		}

		private void OnDisable()
		{
			gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
			playerStateMachine.OnChanged -= PlayerStateMachine_OnChanged;
			loadAlphaRoom.OnLoaded -= LoadAlphaRoom_OnLoaded;
			shotAnimationStateMachine.OnStateChanged -= ShotAnimationStateMachine_OnStateChanged;
		}
	}
}
