﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Codebase.Audio
{
	public interface IAudioMusicPlayer
	{
		void Play(TrackOfMusic track, float fadeSeconds = 0f);
		void Play(TrackOfJingle track);
		void StopCurrentMusic(float fadeSeconds = 0f);
		void Register<TControl,TTrack>(TControl control) where TControl : AudioSourceControl<TTrack>;
	}

	public sealed class AudioMusicPlayer : MonoBehaviour, IAudioMusicPlayer
	{
		//public event Action OnPlay, OnStop;
		private readonly Dictionary<TrackOfMusic, AudioSourceControlMusic> musicControls = new Dictionary<TrackOfMusic, AudioSourceControlMusic>();
		private readonly Dictionary<TrackOfJingle, AudioSourceControlJingle> jingleControls = new Dictionary<TrackOfJingle, AudioSourceControlJingle>();
		private int count;
		private TrackOfMusic currentMusicPlaying;
		
		public void Play(TrackOfMusic track, float fadeSeconds = 0f)
		{
			StopCurrentMusic(fadeSeconds);
			if (!musicControls.ContainsKey(track))
			{
				Debug.LogWarning($"Unlisted track {track} was requested",this);
				return;
			}
				
			musicControls[track].Play(fadeSeconds);
			currentMusicPlaying = track;
		}
		
		public void Play(TrackOfJingle track)
		{
			if (!jingleControls.ContainsKey(track))
			{
				Debug.LogWarning($"Unlisted track {track} was requested",this);
				return;
			}
			
			jingleControls[track].Play(1);
		}

		public void StopCurrentMusic(float fadeSeconds = 0f)
		{
			if (musicControls.ContainsKey(currentMusicPlaying))
				musicControls[currentMusicPlaying].Stop(fadeSeconds);
		}

		public void Register<TControl,TTrack>(TControl control) where TControl : AudioSourceControl<TTrack>
		{
			AddData<TControl,TTrack>(control);
		}

		private void Awake()
		{
			ServiceLocator.Instance.Register<IAudioMusicPlayer>(this);
		}

		private void Start()
		{
			FindSources<AudioSourceControlMusic,TrackOfMusic>();
			FindSources<AudioSourceControlJingle,TrackOfJingle>();
		}

		private void FindSources<TControl,TTrack>() where TControl : AudioSourceControl<TTrack>
		{
			var sourceBehaviors = GetComponentsInChildren<TControl>();
			foreach (var behavior in sourceBehaviors)
			{
				AddData<TControl,TTrack>(behavior);
			}
		}

		private void AddData<TControl,TTrack>(TControl control) where TControl : AudioSourceControl<TTrack>
		{

			var track = control.GetData.Item1;
			var source = control.GetData.Item2;
			if (source == null)
				return;
				
			if (track is TrackOfMusic music)
				musicControls.Add(music,control as AudioSourceControlMusic);
			
			if (track is TrackOfJingle jingle)
				jingleControls.Add(jingle,control as AudioSourceControlJingle);
			
			count++;
		}
	}
}