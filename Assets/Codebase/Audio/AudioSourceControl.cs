﻿using System;
using System.Collections;
using Codebase.Helpers.Extensions;
using UnityEngine;
using UnityEngine.Serialization;

namespace Codebase.Audio
{
	public abstract class AudioSourceControl<T> : MonoBehaviour
	{
		[SerializeField] private T track = default;
		[SerializeField] private AudioSource audioSource = default;
		[SerializeField] private float volume;
		private float volumeDynamic;

		public Tuple<T, AudioSource> GetData => new Tuple<T, AudioSource>(track,audioSource);

		public void Play(float fadeSeconds = 0f)
		{
			StopAllCoroutines();
			fadeSeconds = clampSeconds(fadeSeconds);
			StartCoroutine(PlayFadeIn(fadeSeconds));
		}

		public void Stop(float fadeSeconds = 0f)
		{
			StopAllCoroutines();
			fadeSeconds = clampSeconds(fadeSeconds);
			StartCoroutine(FadeOutStop(fadeSeconds));
		}

		private float clampSeconds(float seconds)
		{
			return Mathf.Clamp(seconds, Mathf.Epsilon, float.MaxValue);
		}

		private IEnumerator PlayFadeIn(float seconds)
		{
			audioSource.Play();
			while (volumeDynamic < this.volume)
			{
				audioSource.volume = Mathf.Clamp(volumeDynamic += Time.deltaTime / seconds * this.volume, 0, this.volume);
				yield return null;
			}
		}

		private IEnumerator FadeOutStop(float seconds)
		{
			while (volumeDynamic > 0f)
			{
				audioSource.volume = Mathf.Clamp(volumeDynamic -= Time.deltaTime / seconds * this.volume, 0, this.volume);
				yield return null;
			}
			
			audioSource.Stop();
		}

		private void Awake()
		{
			if (audioSource == null)
				audioSource = GetComponent<AudioSource>();
			
			InitializationChecker.CheckImmediate(this, audioSource);
		}

		private void Start()
		{
			if (volume.IsEqualTo(0f))
				volume = audioSource.volume;

			audioSource.volume = 0f;
		}
	}
}
