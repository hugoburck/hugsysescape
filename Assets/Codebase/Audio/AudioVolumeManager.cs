﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioVolumeManager : MonoBehaviour
{
	[SerializeField] private AudioMixer audioMixer = default;
	private IGameSettingsMediator gameSettingsMediator;
	
	
	protected virtual void Awake()
	{
		gameSettingsMediator = FindObjectOfType<GameSettingsMediator>();
		InitializationChecker.CheckImmediate(this, audioMixer);
	}

	protected void OnEnable()
	{
		gameSettingsMediator.OnSettingBoolStateChanged += GameSettingsMediator_OnSettingBoolStateChanged;
		gameSettingsMediator.OnSettingFloatStateChanged += GameSettingsMediator_OnSettingFloatStateChanged;
	}

	private void GameSettingsMediator_OnSettingFloatStateChanged(SettingOfGame setting, float state)
	{
		state = state * 80 - 80;
		if (setting == SettingOfGame.VolumeMaster)
		{
			bool mute = gameSettingsMediator.GetBoolState(SettingOfGame.AudioMute);
			audioMixer.SetFloat("VolumeMaster", mute? -80 : state);
		}
		
		if (setting == SettingOfGame.VolumeMusic)
		{
			audioMixer.SetFloat("VolumeMusic", state);
		}
		
		if (setting == SettingOfGame.VolumeEffect)
		{
			audioMixer.SetFloat("VolumeEffects", state);
			audioMixer.SetFloat("VolumeJingle", state);
		}
	}

	private void GameSettingsMediator_OnSettingBoolStateChanged(SettingOfGame element, bool state)
	{
		if (element == SettingOfGame.AudioMute)
		{
			float volumeMasterSlider = gameSettingsMediator.GetFloatState(SettingOfGame.VolumeMaster);
			audioMixer.SetFloat("VolumeMaster", state ? -80f : volumeMasterSlider);
		}
	}
	
	protected void OnDisable()
	{
		gameSettingsMediator.OnSettingBoolStateChanged -= GameSettingsMediator_OnSettingBoolStateChanged;
		gameSettingsMediator.OnSettingFloatStateChanged -= GameSettingsMediator_OnSettingFloatStateChanged;
	}

	protected virtual void Start()
	{
		FetchBoolFromMediator(SettingOfGame.AudioMute);
		FetchFloatFromMediator(SettingOfGame.VolumeMaster);
		FetchFloatFromMediator(SettingOfGame.VolumeMusic);
		FetchFloatFromMediator(SettingOfGame.VolumeEffect);
	}

	private void FetchBoolFromMediator(SettingOfGame element)
	{
		GameSettingsMediator_OnSettingBoolStateChanged(element, gameSettingsMediator.GetBoolState(element));
	}
	
	private void FetchFloatFromMediator(SettingOfGame element)
	{
		GameSettingsMediator_OnSettingFloatStateChanged(element, gameSettingsMediator.GetFloatState(element));
	}
}
