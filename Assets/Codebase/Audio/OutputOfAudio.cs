﻿public enum OutputOfAudio
{
    None,
    Music,
    Jingle,
    Effect
}
