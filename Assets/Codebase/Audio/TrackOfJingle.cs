﻿namespace Codebase.Audio
{
    public enum TrackOfJingle
    {
        None = 0,
        PuzzleSolved,
        Uncover
    }
}