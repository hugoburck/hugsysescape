﻿namespace Codebase.Audio
{
    public enum TrackOfMusic
    {
        None = 0,
        Menu,
        Intro,
        GameA,
        GameB,
        GameC,
        GameD,
        Outro,
        Credits,
    }
}