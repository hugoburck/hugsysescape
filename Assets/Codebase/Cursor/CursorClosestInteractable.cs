﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class CursorClosestInteractable
{
    public float Distance { get; private set; }
    public Interactable GetClosest(List<Interactable> clickables,ICursorPosition cursorPosition,Camera camera,float maxRadius)
    {
        float closestDistance = Mathf.Infinity;
        Interactable closest = null;
        foreach (var clickable in clickables)
        {
            Vector3 interactableScreenPoint = closestScreenPoint(clickable,cursorPosition,camera,maxRadius);
            float distance = Vector3.Distance(cursorPosition.Position, interactableScreenPoint);
            if (distance > maxRadius)
                continue;
            if (distance < closestDistance)
            {
                Distance = closestDistance = distance;
                closest = clickable;
            }
        }
        return closest;
    }

    private Vector3 closestScreenPoint(Interactable clickable,ICursorPosition cursorPosition,Camera camera,float maxRadius)
    {
        if (clickable.GetWorldPoints == null)
            return camera.WorldToScreenPoint(clickable.transform.position);
        
        float closestDistance = Mathf.Infinity;
        Vector3 closestScreenPoint = Vector3.positiveInfinity;

        foreach (var point in clickable.GetWorldPoints.Points)
        {
            Vector3 screenPoint = camera.WorldToScreenPoint(point);
            float distance = Vector3.Distance(cursorPosition.Position, screenPoint);
            if (distance > maxRadius)
                continue;
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestScreenPoint = screenPoint;
            }
        }

        return closestScreenPoint;

    }
}

