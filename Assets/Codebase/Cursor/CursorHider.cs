﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Cursor;
using UnityEngine;
using UnityEngine.Serialization;

public class CursorHider : MonoBehaviour, ICursorHider
{
    [SerializeField] private bool visibleHardware = default;
    [SerializeField] private Canvas canvas = default;
    public event Action<bool> OnVisibility;
    private IGameStateMachine gameStateMachine;
    private IShotAnimationManager shotAnimationManager;
    private IShotAnimationStateMachine shotAnimationStateMachine;
    private IGameTransitionManager gameTransitionManager;

    public bool IsHiding => canvas.enabled == false;

    private void Awake()
    {
        ServiceLocator.Instance.Register<ICursorHider>(this);
        gameStateMachine = FindObjectOfType<GameStateMachine>();
        shotAnimationStateMachine = FindObjectOfType<ShotAnimationStateMachine>();
        gameTransitionManager = GameTransitionManager.Instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = visibleHardware;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        Cursor.visible = visibleHardware;
    }

    private void CanvasEnable()
    { 
        canvas.enabled = true;
        OnVisibility?.Invoke(canvas.enabled);
    }
    private void CanvasDisable()
    {
        canvas.enabled = false;
        OnVisibility?.Invoke(canvas.enabled);
    }

    private void OnEnable()
    {
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
        shotAnimationStateMachine.OnStateChanged += ShotAnimationStateMachine_OnStateChanged;
        gameTransitionManager.OnTransitioning += GameTransitionManager_OnTransitioning;
    }

    private void GameTransitionManager_OnTransitioning()
    {
        CanvasDisable();
    }

    private void ShotAnimationStateMachine_OnStateChanged(StateOfShotAnimation state)
    {
        if (state == StateOfShotAnimation.Idle && !IsGameInStateWithoutCursor)
            CanvasEnable();
        else CanvasDisable();
    }

    private void GameStateMachine_OnStateChanged(StateOfGame state)
    {
        CanvasEnable();
        
        if (IsGameStateWithoutCursor(state))
            CanvasDisable();
    }

    private void OnDisable()
    {
        gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
        gameTransitionManager.OnTransitioning -= GameTransitionManager_OnTransitioning;
    }

    private bool IsGameInStateWithoutCursor
    {
        get
        {
            if (gameStateMachine == null)
            {
                Debug.LogError("gamestate service not available");
                return false;
            }

            return IsGameStateWithoutCursor(gameStateMachine.State);
        }
    }

    private bool IsGameStateWithoutCursor(StateOfGame state)
    {
        return state == StateOfGame.Splash ||
            state == StateOfGame.Credits ||
            state == StateOfGame.Booting;
    }

}
