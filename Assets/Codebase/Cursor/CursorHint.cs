﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorHint : MonoBehaviour
{
    public CursorPointer Pointer;
    public Image Image;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Color color = Color.white;
        color.a = Pointer.IsNothingInRange ? 0f : 1 - (Pointer.closestDistance / Pointer.PixelRadius);
        Image.color = color;
    }
}
