﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorIcon : MonoBehaviour
{
    public Image Image;
    public StyleOfCursorIcon Mode;
    private Color original;

    private CursorPointer Pointer => CursorPointer.Instance;

    // Start is called before the first frame update
    void Start()
    {
        Image = GetComponent<Image>();
        original = Image.color;
    }

    // Update is called once per frame
    void Update()
    {
        Image.color = setColor(original, Color.white, 0f);

        if (NoHovering)
        {
            if (this.Mode == interactableMode(Pointer.closestClickable))
            {
                Image.color = setColor(original, Color.white, CalculateAlpha/2);
            }
        }
        else
        {
            if (this.Mode == interactableMode(Interactable.CurrentClickableMouseOver))
            {
                Image.color = setColor(original,Color.white, 1f);
            }
        }
        Image.enabled = Image.color.a > 0f;
    }

    private Color setColor(Color color, Color multiplier,float alpha)
    {
        color.a = alpha;
        return color * multiplier;
    }

    private float CalculateAlpha => Pointer.IsNothingInRange? 0f : 1 - (Pointer.closestDistance / Pointer.PixelRadius);

    private StyleOfCursorIcon interactableMode(Interactable interactable)
    {
        if (interactable is Inspectable) return StyleOfCursorIcon.Inspect;
        if (interactable is ReceiverPiece) return StyleOfCursorIcon.Inquiry;
        if (interactable is PuzzlePiece) return StyleOfCursorIcon.Action; //PuzzlePiece is supertype
        return StyleOfCursorIcon.None;
    }

    private bool NoHovering { get => Interactable.CurrentClickableMouseOver == null; }
}