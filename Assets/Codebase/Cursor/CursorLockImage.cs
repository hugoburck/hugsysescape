﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.PlayerControl;
using UnityEngine;
using UnityEngine.UI;

public class CursorLockImage : MonoBehaviour
{
	[SerializeField] private GameObject locker = default;
	[SerializeField] private GameObject pointer = default;
	private IGameStateMachine gameFsm;
	private IPlayerStateMachine playerFsm;
	private ICursorLocker cursorLocker;
	
	protected virtual void Awake()
	{
		gameFsm = FindObjectOfType<GameStateMachine>();
		playerFsm = FindObjectOfType<PlayerStateMachine>();
		cursorLocker = FindObjectOfType<CursorLocker>();
	}

	private void OnEnable()
	{
		cursorLocker.OnCursorLockState += CursorLocker_OnCursorLockState;
	}

	private void CursorLocker_OnCursorLockState(bool isLocked)
	{
		if (gameFsm.State == StateOfGame.Game)
		{
			locker.SetActive(isLocked);
		}
		else
		{
			locker.SetActive(false);
		}

		pointer.SetActive(!locker.activeInHierarchy);
	}

	protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }
}
