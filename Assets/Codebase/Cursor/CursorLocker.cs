﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Codebase.Application;
using Codebase.Cursor;
using Codebase.InventoryControl;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

public class CursorLocker : MonoBehaviour, ICursorLocker
{
	[SerializeField] private CursorHider _cursorHider = default;
	public event Action<bool> OnCursorLockState;
	private IGameStateMachine gameStateMachine;
	private IPlayerStateMachine playerStateMachine;
	private ICursorHider cursorHider;
	private IDeviceInput deviceInput;
	private IGameSettingsMediator gameSettingsMediator;
	private bool lookHold;

	public bool IsCursorLocked => Cursor.lockState == CursorLockMode.Locked;

	protected virtual void Awake()
	{
		cursorHider = _cursorHider != null ? _cursorHider : FindObjectOfType<CursorHider>();
		gameStateMachine = FindObjectOfType<GameStateMachine>();
		playerStateMachine = FindObjectOfType<PlayerStateMachine>();
		deviceInput = FindObjectOfType<DeviceInput>();
		gameSettingsMediator = FindObjectOfType<GameSettingsMediator>();
		InitializationChecker.CheckImmediate(this, (CursorHider)cursorHider);
	}

    protected virtual void Start()
    {
	    lookHold = gameSettingsMediator.GetBoolState(SettingOfGame.ControlHoldLook);
    }

    private void LockCursor()
    {
	    Cursor.lockState = CursorLockMode.Locked;
	    OnCursorLockState?.Invoke(IsCursorLocked);
    }

    private void UnlockCursor()
    {
	    Cursor.lockState = CursorLockMode.None;
	    OnCursorLockState?.Invoke(IsCursorLocked);
    }

    private void ToggleLock()
    {
	    if (Cursor.lockState == CursorLockMode.Locked)
		    UnlockCursor();
	    else
		    LockCursor();
    }
    
    private void OnEnable()
    {
	    cursorHider.OnVisibility += CursorHider_OnVisibility;
	    gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
	    playerStateMachine.OnChanged += PlayerStateMachine_OnChanged;
	    deviceInput.OnEnterInputEvent += DeviceInput_OnEnterInputEvent;
	    deviceInput.OnExitInputEvent += DeviceInput_OnExitInputEvent;
	    gameSettingsMediator.OnSettingBoolStateChanged += GameSettingsMediator_OnSettingBoolStateChanged;
    }

    private void GameSettingsMediator_OnSettingBoolStateChanged(SettingOfGame setting, bool value)
    {
	    if (setting == SettingOfGame.ControlHoldLook)
			lookHold = value;
    }

    private void DeviceInput_OnEnterInputEvent(ActionOfPlayer action, float intensity)
    {
	    if (!(gameStateMachine.State == StateOfGame.Game &&
	          playerStateMachine.State == StateOfPlayer.Exploring))
		    return;
	    
	    if (action == ActionOfPlayer.Inventory)
	    {
		    if (lookHold)
			    LockCursor();
		    else ToggleLock();
	    }
    }

    private void DeviceInput_OnExitInputEvent(ActionOfPlayer action, float value)
    {
	    if (action == ActionOfPlayer.Inventory && lookHold)
		    UnlockCursor();
    }

    private void PlayerStateMachine_OnChanged(StateOfPlayer state)
    {
	    if (gameStateMachine.State != StateOfGame.Game)
		    return;
	    
	    if (state == StateOfPlayer.OpenMenu ||
	        state == StateOfPlayer.InspectingItem ||
	        state == StateOfPlayer.InspectingPuzzle)
		    UnlockCursor();
	    else
	    {
		    LockCursor();
	    } 
    }

    private void GameStateMachine_OnStateChanged(StateOfGame stateMode)
    {
	    if (stateMode == StateOfGame.Menu)
		    UnlockCursor();
	    else if (stateMode == StateOfGame.Game)
		    LockCursor();
    }

    private void CursorHider_OnVisibility(bool visible)
    {
	    if (!visible)
		    LockCursor();
    }

    private void OnDisable()
    {
	    cursorHider.OnVisibility -= CursorHider_OnVisibility;
	    gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
	    gameSettingsMediator.OnSettingBoolStateChanged -= GameSettingsMediator_OnSettingBoolStateChanged;
    }
}
