﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CursorPointer : MonoBehaviourSingleton<CursorPointer>
{
    [Range(0,256)]
    public float closestDistance;
    private string closestName;
    public Interactable closestClickable;
    public int PixelRadius = 256;
    public RectTransform CanvasRect;
    private ICursorPosition cursorPosition;
    private List<Interactable> interactables = new List<Interactable>();
    private List<Interactable> clickables = new List<Interactable>();
    private CursorClosestInteractable cursorClosestInteractable;
    private Camera cam;

    public CursorPointer()
    {
        cursorPosition = new InputMousePosition();
        cursorClosestInteractable = new CursorClosestInteractable();
    }

    public CursorPointer Inject(ICursorPosition cursorPosition)
    {
        this.cursorPosition = cursorPosition;
        return this;
    }

    public bool IsNothingInRange
    {
        get => closestClickable == null;
    }

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        interactables = Interactable.Instances;
        cam = PlayerCamera.Instance.Camera;
    }

    // Update is called once per frame
    void Update()
    {
        clickables.Clear();
        for (int i = 0; i < interactables.Count; i++)
        {
            if (interactables[i].IsClickable)
                clickables.Add(interactables[i]);
        }
        //clickables = interactables.Where((i) => i.IsClickable).ToList(); //linq in update thread?

        closestClickable = cursorClosestInteractable.GetClosest(clickables, cursorPosition, cam, PixelRadius);
        if (closestClickable != null)
            closestDistance = cursorClosestInteractable.Distance;
        PlaceImage();
    }

    private void PlaceImage()
    {
        var rect = GetComponent<RectTransform>();
        if (rect == null || CanvasRect == null)
            throw new MissingComponentException();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(CanvasRect, cursorPosition.Position, null, out Vector2 localPoint);
        rect.anchoredPosition = localPoint;
    }
}
