﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CursorRaycast : MonoBehaviour, ICursorRaycast
{
    public event Action<Transform,Collider> OnHitChanged;
    public Transform CurrentHitTransform { get; private set; }
    public Collider CurrentHitCollider { get; private set; }
    public LayerMask LayerMask;
    public bool Closest = true;
    public bool MouseOver { get; private set; }
    public Transform HitTrans;

    private Transform oldHitTrans;
    [SerializeField] private Camera cam = default;
    private ICursorPosition cursorPosition;
    private IRaycast raycast;

    public CursorRaycast()
    {
        cursorPosition = new InputMousePosition();
        raycast = new PhysicsRaycast();
    }

    public CursorRaycast Inject(Camera cam, ICursorPosition cursorPos, IRaycast raycast)
    {
        this.cam = cam;
        this.cursorPosition = cursorPos;
        this.raycast = raycast;
        return this;
    }

    public bool IsHit(Transform transform)
    {
        return transform == CurrentHitTransform;
    }

    private Transform Shoot()
    {
        if (cam == null)
        {
            Debug.Log("Camera missing", this);
            return null;
        }
        
        Ray ray = cam.ScreenPointToRay(cursorPosition.Position);
        return Closest ? Hit(ray) : AnyHit(ray);
    }

    private Transform Hit(Ray ray)
    {
        return raycast.GetTransform(ray, Mathf.Infinity, LayerMask);
        //Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, LayerMask);
        //return hit.transform;
    }

    private Transform AnyHit(Ray ray) //not ready for use
    {
        RaycastHit[] hits = raycast.GetHits(ray, Mathf.Infinity, LayerMask);
        foreach (var hit in hits)
            if (hit.transform != null)
                return hit.transform;
        
        return null;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (cam == null)
            cam = Camera.main;
    }

    // Update is called once per frame
    void Awake()
    {
        ServiceLocator.Instance.Register<ICursorRaycast>(this);
    }

    private void OnEnable()
    {
        FrameGuid.OnNewFrame += FrameGuidInstance_OnNewFrame;
    }

    private void OnDisable()
    {
        FrameGuid.OnNewFrame -= FrameGuidInstance_OnNewFrame;
    }

    private void FrameGuidInstance_OnNewFrame()
    {
        HandleFrame();
    }

    private Collider GetCollider(Transform trans)
    {
        return trans == null ? null : trans.GetComponent<Collider>();
    }

    public void HandleFrame()
    {
        CurrentHitTransform = Shoot();
        if (oldHitTrans != CurrentHitTransform)
        {
            CurrentHitCollider = GetCollider(CurrentHitTransform);
            HitTrans = CurrentHitTransform;
            OnHitChanged?.Invoke(CurrentHitTransform,CurrentHitCollider);
        }
        
        oldHitTrans = CurrentHitTransform;
    }
}
