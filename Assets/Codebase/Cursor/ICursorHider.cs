﻿using System;

namespace Codebase.Cursor
{
    public interface ICursorHider
    {
        event Action<bool> OnVisibility;
        bool IsHiding { get; }
    }
}