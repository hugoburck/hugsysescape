﻿using System;

public interface ICursorLocker
{
    event Action<bool> OnCursorLockState;
    bool IsCursorLocked { get; }
}