﻿using UnityEngine;
public interface ICursorPosition
{
    Vector2 Position { get; }
}

