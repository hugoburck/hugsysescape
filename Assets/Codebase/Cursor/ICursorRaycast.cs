﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICursorRaycast
{
    event Action<Transform,Collider> OnHitChanged;
    Transform CurrentHitTransform { get; }
    Collider CurrentHitCollider { get; }
}
