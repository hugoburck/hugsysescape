﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public interface IRaycast
{
    RaycastHit GetHit(Ray ray, float range, LayerMask layerMask);
    RaycastHit[] GetHits(Ray ray, float range, LayerMask layerMask);
    Transform GetTransform(Ray ray, float range, LayerMask layerMask);
    Transform[] GetTransforms(Ray ray, float range, LayerMask layerMask);
}

