﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMousePosition : ICursorPosition
{
    public Vector2 Position
    {
        get
        {
            return Input.mousePosition;
        }
    }
}
