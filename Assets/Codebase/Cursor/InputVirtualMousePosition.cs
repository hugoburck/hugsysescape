﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

public class InputVirtualMousePosition : MonoBehaviourSingleton<InputVirtualMousePosition>, ICursorPosition
{
	public Vector2 Position { get; private set; }
	private IDeviceInput deviceInput;
	public Vector2 _pos;
	

	protected virtual void Awake()
	{
		deviceInput = DeviceInput.Instance;
	}

	protected virtual void OnEnable()
    {
        deviceInput.OnInputEvent += DeviceInput_OnInputEvent;
    }

	private void DeviceInput_OnInputEvent(ActionOfPlayer pa, float i)
	{
		Vector2 delta = Vector2.zero;
		Vector2 posClamp = Position;
		if (pa == ActionOfPlayer.LookHeading)
			delta.x = i;

		if (pa == ActionOfPlayer.LookPitch)
			delta.y = -i;
		
		posClamp += delta;
		posClamp.x = Mathf.Clamp( posClamp.x,0, Screen.width);
		posClamp.y = Mathf.Clamp( posClamp.y,0, Screen.height);
		Position = _pos = posClamp;

	}

	protected virtual void OnDisable()
    {
	    deviceInput.OnInputEvent -= DeviceInput_OnInputEvent;
    }
}
