﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class PhysicsRaycast : IRaycast
{
    public RaycastHit GetHit(Ray ray, float range, LayerMask layerMask)
    {
        Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, layerMask);
        return hit;
    }

    public RaycastHit[] GetHits(Ray ray, float range, LayerMask layerMask)
    {
        RaycastHit[] hits = Physics.RaycastAll(ray,range,layerMask);
        return hits;
    }

    public Transform GetTransform(Ray ray, float range, LayerMask layerMask)
    {
        return GetHit(ray, range, layerMask).transform;
    }

    public Transform[] GetTransforms(Ray ray, float range, LayerMask layerMask)
    {
        RaycastHit[] hits = GetHits(ray, range, layerMask);
        Transform[] transforms = new Transform[hits.Length];
        for (int i = 0; i < hits.Length; i++)
        {
            transforms[i] = hits[i].transform;
        }
        return transforms;
    }
}

