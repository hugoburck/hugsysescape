﻿public enum StyleOfCursorIcon 
{ 
    None,
    Action,
    Inspect,
    Inquiry
}
