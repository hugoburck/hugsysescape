﻿public enum CommandOfFader
{
    Idle,
    FadeIn,
    FadeOut
}