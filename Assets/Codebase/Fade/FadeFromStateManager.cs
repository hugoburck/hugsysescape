﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Fade;
using Codebase.PlayerControl;
using UnityEngine;

/// <summary>
/// Issues fade commands based on states from statemachines
/// </summary>
public class FadeFromStateManager : MonoBehaviour
{
	private IImageFader imageFader;
	private IPlayerStateMachine playerStateMachine;
	private IGameStateMachine gameStateMachine;
	
	protected virtual void Awake()
	{
        
    }

    protected virtual void Start()
    {
	    imageFader = ServiceLocator.Instance.Resolve(imageFader);
	    playerStateMachine = ServiceLocator.Instance.Resolve(playerStateMachine);
	    gameStateMachine = ServiceLocator.Instance.Resolve(gameStateMachine);
    }

    protected void OnEnable()
    {
	    StartCoroutine(LateOnEnable());
    }

    private IEnumerator LateOnEnable()
    {
	    yield return null;
	    gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
    }

    private void GameStateMachine_OnStateChanged(StateOfGame state)
    {
	    if (state == StateOfGame.Menu)
		    imageFader.FadeOut(3, true);
    }

    protected void OnDisable()
    {
	    gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
    }

    protected virtual void Update()
    {
        
    }
}
