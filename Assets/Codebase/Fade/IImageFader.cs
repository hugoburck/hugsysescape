﻿using System;
using System.Collections;
using UnityEngine;

namespace Codebase.Fade
{
    public interface IImageFader
    {
        event Action<StateOfFader> OnStateChanged;
        bool IsInTransition { get; }
        void SetColorForNextFadeIn(Color color);
        void FadeIn(float seconds = 0.5f, bool fromStart = false);
        void FadeOut(float seconds = 0.5f, bool fromStart = false);
        IEnumerator WaitForFade();
    }
}