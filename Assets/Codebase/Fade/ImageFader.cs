﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using Codebase.Helpers.Extensions;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

public class ImageFader : MonoBehaviour, IImageFader
{
    [SerializeField] private Image image = default;
    public event Action<StateOfFader> OnStateChanged;
    private Color color, colorDefault, colorChange;
    private float seconds = 0.5f;
    private StateOfFader backingStateOfFader;
    private float counter;
    private bool changeColor;
    
    //public static ImageFader Instance { get; private set; }
    
    public StateOfFader StateOfFader
    {
        get => backingStateOfFader;
        set
        {
            if (backingStateOfFader == value) 
                return;
            
            backingStateOfFader = value;
            OnStateChanged?.Invoke(backingStateOfFader);
        }
    }

    public bool IsInTransition => StateOfFader == StateOfFader.FadingIn || StateOfFader == StateOfFader.FadingOut;

    public void SetColorForNextFadeIn(Color color)
    {
        changeColor = true;
        colorChange = color;
    }

    public void FadeIn(float seconds = 0.5f, bool fromStart = false)
    {
        if (changeColor)
            this.color = colorChange;

        
        if (fromStart)
            counter = 0f;
        
        this.seconds = seconds;
        StopAllCoroutines();
        StartCoroutine(FadeInRoutine());
    }

    public void FadeOut(float seconds = 0.5f, bool fromStart = false)
    {
        if (fromStart)
            counter = 1f;
        
        this.seconds = seconds;
        StopAllCoroutines();
        StartCoroutine(FadeOutRoutine());
    }

    public IEnumerator WaitForFade()
    {
        while (IsInTransition)
            yield return null;
    }

    private IEnumerator FadeInRoutine()
    {
        StateOfFader = StateOfFader.FadingIn;
        while (true)
        {
            AddToAlphaClamped(ref color, Increment);
            ApplyColor();
            if (IsFadedIn)
                break;
            
            yield return null;
        }

        StateOfFader = StateOfFader.FadedIn;
    }

    private IEnumerator FadeOutRoutine()
    {
        StateOfFader = StateOfFader.FadingOut;
        while (true)
        {
            AddToAlphaClamped(ref color, -Increment);
            ApplyColor();
            if (IsFadedOut)
                break;
            
            yield return null;
        }

        ResetColor();
        StateOfFader = StateOfFader.FadedOut;
    }

    private bool IsFadedIn => color.a.IsEqualTo(1);
    private bool IsFadedOut => color.a.IsEqualTo(0);

    private void AddToAlphaClamped(ref Color color, float increment)
    {
        counter += increment;
        counter = Mathf.Clamp01(counter);
        color.a = counter.CosineDamper();
    }

    private void ResetColor()
    {
        changeColor = false;
        this.color = colorDefault;
    }

    private void ApplyColor()
    {
        image.color = color;
    }

    private float Increment => Time.deltaTime / Mathf.Max(Mathf.Epsilon, seconds);

    private void Awake()
    {
        // if (Instance == null)
        //     Instance  = this;
        // else Debug.LogWarning("Another instance started here",this);

        InitializationChecker.CheckImmediate(this, image);
        ServiceLocator.Instance.Register<IImageFader>(this);
    }

    private void Start()
    {
        colorDefault = this.color;
    }
}
