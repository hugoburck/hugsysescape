﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using UnityEngine;

public class ImageFaderCommand : MonoBehaviour
{
    //[SerializeField] private ImageFader imageFader;
    private IImageFader imageFader;

    public bool FadeIn, FadeOut;

    public StateOfFader state;

    private void Start()
    {
        imageFader = ServiceLocator.Instance.Resolve(imageFader);
    }
    
    void OnEnable()
    {
        StartCoroutine(OnEnableDelay());
    }
    
    private IEnumerator OnEnableDelay()
    {
        yield return null;
        imageFader.OnStateChanged += ImageFader_OnStateChanged;

    }
    
    void OnDisable()
    {
        imageFader.OnStateChanged -= ImageFader_OnStateChanged;
    }

    private void ImageFader_OnStateChanged(StateOfFader state)
    {
        this.state = state;
    }

    // Update is called once per frame
    void Update()
    {
        if (FadeIn)
            imageFader.FadeIn();
        if (FadeOut)
            imageFader.FadeOut();

        FadeIn = FadeOut = false;
    }
}
