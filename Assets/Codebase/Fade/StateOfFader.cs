﻿public enum StateOfFader
{
    Idle,
    FadingIn,
    FadedIn,
    FadingOut,
    FadedOut
}
