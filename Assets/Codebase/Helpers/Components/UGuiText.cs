﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;


class UGuiText : MonoBehaviour, IGuiText
{
    public Text UnityGuiText = default;

    public string Text 
    {
        get
        {
            if (initialized)
                return UnityGuiText.text;
            else 
                return string.Empty;
        }
        set
        {
            if (initialized)
                UnityGuiText.text = value;
        } 
    }

    private bool initialized 
    {
        get 
        {
            if (UnityGuiText != null)
                return true;
            else
            {
                Debug.LogError("Not initialized", this);
                return false;
            }
        } 
    }
}

