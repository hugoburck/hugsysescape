﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

/// <summary>
/// Calls predefined private methods used by MonoBehaviours via Reflection.
/// Used exclusively for simulating framecalls while testing within the EditModeTests assembly.
/// </summary>
public static class CallMonoBehaviourReflectionMethod
{
    private const string 
        awake = "Awake",
        update = "Update",
        start = "Start";

    public static void Update<T>(T instance) where T : MonoBehaviour
    {
        Call(instance, update, null);
    }

    public static void Start<T>(T instance) where T : MonoBehaviour
    {
        Call(instance, start, null);
    }

    public static void Awake<T>(T instance) where T : MonoBehaviour
    {
        Call(instance, awake, null);
    }

    private static void Call<T>(T instance, string method, params object[] arguments) where T : MonoBehaviour
    {
        MethodInfo methodInfo = instance.GetType().GetTypeInfo().GetDeclaredMethod(method);
        if (methodInfo == null)
            Debug.LogError($"{method} not found");
        else
            methodInfo.Invoke(instance, arguments);
    }
}