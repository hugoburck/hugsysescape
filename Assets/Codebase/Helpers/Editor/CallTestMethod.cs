﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

/// <summary>
/// Calls a method defined by ITest on implementing classes.
/// Used exclusively for kickstarting classes while testing within the EditModeTests assembly.
/// </summary>
class CallTestMethod
{
    private const string
       testMethodName = "_test";

    public static void Call<T>(T instance) where T : ITest
    {
        MethodInfo methodInfo = instance.GetType().GetTypeInfo().GetDeclaredMethod(testMethodName);
        if (methodInfo == null)
            Debug.LogError($"{testMethodName} not found");
        else
            methodInfo.Invoke(instance, null);
    }
}

