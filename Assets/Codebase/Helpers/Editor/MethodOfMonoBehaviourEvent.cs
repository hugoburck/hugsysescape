﻿public enum MethodOfMonoBehaviourEvent
{
    Awake,
    OnEnable,
    Start,
    Update,
    OnDisable
}