﻿using System;
using UnityEngine;

namespace Codebase.Helpers.Extensions
{
    public static class ArithmeticExtensions
    {
        public static int DistanceTo(this int nra, int nrb)
        {
            return Math.Abs(nra - nrb);
        }
    
        public static float DistanceTo(this float nra, float nrb)
        {
            return Math.Abs(nra - nrb);
        }
    
        public static bool SmallerThan(this int a, int b)
        {
            return a < b;
        }

        public static bool SmallerThan(this float a, float b)
        {
            return a < b;
        }
    
        public static bool BiggerThan(this int a, int b)
        {
            return a > b;
        }

        public static bool BiggerThan(this float a, float b)
        {
            return a > b;
        }

        public static bool IsEqualTo(this float a, float b, int decimalPrecision = 5) //TODO this is comparison, not arithmetic
        {
            int multiplier = (int)Math.Pow(10, decimalPrecision);
            if (multiplier <= 0)
                multiplier = 1;
            return (int)(a * multiplier) == (int)(b * multiplier);
        }
        
        public static float CosineDamper(this float number)
        {
            return Mathf.Cos(number * Mathf.PI - Mathf.PI) / 2 + 0.5f;
        }
    }
}
