﻿using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Codebase.Helpers.Extensions
{
    public static class ColorExtensions //Extension might be slow with the switch
    {
        private enum ColorSlotMode{R,G,B,A}
        
        public static Color CopyWithR(this Color color, float value)
        {
            return SetColor(color, value, ColorSlotMode.R);
        }

        private static Color SetColor(Color color, float value, ColorSlotMode mode)
        {
            switch(mode)
            {
                case ColorSlotMode.R:
                    color.r = value;
                    break;
                case ColorSlotMode.G:
                    color.g = value;
                    break;
                case ColorSlotMode.B:
                    color.b = value;
                    break;
                case ColorSlotMode.A:
                    color.a = value;
                    break;
                default:
                    Debug.LogWarning($"Unhandled case: {mode}");
                    break;
            }

            return color;
        }
    }
}