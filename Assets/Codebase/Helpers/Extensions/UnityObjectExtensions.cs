﻿namespace Codebase.Helpers.Extensions
{
    public static class UnityObjectExtensions
    {
        public static void DestroySmart(this UnityEngine.Object obj)
        {
#if UNITY_EDITOR
            UnityEngine.Object.DestroyImmediate(obj);
#else
            UnityEngine.Object.Destroy(obj);
#endif
        }
    } 
}

