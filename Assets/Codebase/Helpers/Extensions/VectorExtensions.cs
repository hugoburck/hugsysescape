﻿using UnityEngine;

namespace Codebase.Helpers.Extensions
{
    public static class VectorExtensions //Extension might be slow with the switch
    {
        public static void SetX(this ref Vector3 vector, float value)
        {
            vector = SetAxis(vector, value, AxisOfTransform.X);
        }
        
        public static void SetY(this ref Vector3 vector, float value)
        {
            vector = SetAxis(vector, value, AxisOfTransform.Y);
        }
        
        public static void SetZ(this ref Vector3 vector, float value)
        {
            vector = SetAxis(vector, value, AxisOfTransform.Z);
        }
        
        public static Vector3 CopyWithX(this Vector3 vector, float value)
        {
            return SetAxis(vector, value, AxisOfTransform.X);
        }
        
        public static Vector3 CopyWithY(this Vector3 vector, float value)
        {
            return SetAxis(vector, value, AxisOfTransform.Y);
        }
        
        public static Vector3 CopyWithZ(this Vector3 vector, float value)
        {
            return SetAxis(vector, value, AxisOfTransform.Z);
        }

        private static Vector3 SetAxis(Vector3 vector, float value, AxisOfTransform axis)
        {
            switch (axis)
            {
                case AxisOfTransform.X:
                    vector.x = value;
                    break;
                case AxisOfTransform.Y:
                    vector.y = value;
                    break;
                case AxisOfTransform.Z:
                    vector.z = value;
                    break;
            }

            return vector;
        }
    }
}