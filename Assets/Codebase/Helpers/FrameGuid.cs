﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

///<summary>Creates a Guid for a frame before all other MonoBehaviours are called.
///<para>With the Guid you can let scripts check if they're called more than once per frame.
///</para>
///</summary>
[ExecuteInEditMode]
public class FrameGuid : MonoBehaviourSingleton<FrameGuid>
{
    public static event Action OnNewFrame;
    public static Guid Current { get; private set; }

    public static bool IsDifferentTo(Guid guid)
    {
        return guid != Current;
    }

    // Update is called once per frame
    void Update()
    {
        Current = Guid.NewGuid();
        OnNewFrame?.Invoke();
    }

    private void Start()
    {
        //Debug.Log("Starting", this);
    }
}
