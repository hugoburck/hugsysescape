﻿using System;

public interface ICheckInitialized
{
    bool CheckSuccess { get; }
    bool CheckFail { get; }
}

