﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializationChecker : ICheckInitialized
{
    public const string ErrorMessage = "Missing components here",
        MissingMessage = "No components given to check here";
    
    private Object[] components;
    private Object owner;

    public InitializationChecker (Object owner, params Object[] components)
    {
        this.owner = owner;
        this.components = components;
    }

    public static InitializationChecker Factory(Object owner, params Object[] components)
    {
        return new InitializationChecker(owner, components);
    }

    /// <summary>Checks if all supplied components are not null.
    /// <para>First entry must be the owner, second entry must be one or more components.</para>
    /// </summary>
    public static bool CheckImmediate(Object owner, params Object[] components)
    {
        if (components == null || components.Length == 0)
        {
            Debug.LogError(MissingMessage + $" at {owner.name}",owner);
            return false;
        }
        
        for (int i = 0; i < components.Length; i++)
        {
            if (components[i] == null)
            {
                Debug.LogError(ErrorMessage, owner);
                return false;
            }
        }
        return true;
    }

    public void SetComponents(Object owner, params Object[] components)
    {
        this.owner = owner;
        this.components = components;
    }

    public bool CheckSuccess => CheckImmediate(owner, components);

    public bool CheckFail => !CheckSuccess;

    public bool SetAndCheckComponents(Object owner, params Object[] components)
    {
        SetComponents(owner, components);
        return CheckSuccess;
    }
}
