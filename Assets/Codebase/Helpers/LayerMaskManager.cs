﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Wrapper for LayerMask to make it work with the Layer Enum for this project.
/// Prevents spelling mistakes when acquiring layers.
/// Enum entries cannot contain whitespace unlike the actual layers, so underscores will be internally replaced with a space if no match was initially found.
/// </summary>
[ExecuteInEditMode]
public class LayerMaskManager : MonoBehaviour
{
    private const int invalidLayer = -1;
    private const string invalidatedName = "_notfound", whitespace = " ", underscore = "_";

    public static int GetMask(params UnitOfLayer[] layers)
    {
        string[] names = new string[layers.Length];
        for (int i = 0; i < names.Length; i++)       
            names[i] = Name(layers[i]);        
        return LayerMask.GetMask(names);
    }

    public static int GetLayer(UnitOfLayer unitOfLayer)
    {
        return LayerMask.NameToLayer(Name(unitOfLayer));
    }

    // Start is called before the first frame update
    void Start()
    {
        ValidateLayerEnum();
    }

    private static string Name(UnitOfLayer unitOfLayer)
    {
        string name = unitOfLayer.ToString();
        if (ValidLayer(name))
            return name;

        name = name.Replace(underscore, whitespace);
        if (ValidLayer(name))
            return name;
        else 
            return invalidatedName;
    }

    private static bool ValidLayer(string name)
    {
        return LayerMask.NameToLayer(name) != invalidLayer;
    }

    private void ValidateLayerEnum()
    {
        //Array layers = Enum.GetValues(typeof(Layer));
        foreach (UnitOfLayer layer in Enum.GetValues(typeof(UnitOfLayer)))
        {
            if (Name(layer) == invalidatedName)
                Debug.Log($"{layer.ToString()} couldn't be converted to bitmask!", this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
