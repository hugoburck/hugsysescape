﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    private static object padLock = false;
    private static bool exiting = false;

    // Start is called before the first frame update
    public static T Instance
    {
        get
        {
            lock (padLock)
            {
                if (exiting) return null;
                if (instance != null) return instance;
                instance = FindObjectOfType<T>();
                if (instance != null) return instance;
                Debug.LogWarning($"Instance of {typeof(T).Name} wasn't found when requested");
                return instance = CreateNewInstance();
            }
        }
    }

    private static T CreateNewInstance()
    {
        GameObject singleton = new GameObject();
        singleton.name = typeof(T).Name + "Singleton";
        singleton.transform.parent = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects()[0].transform;
        singleton.transform.SetParent(null);
        Debug.Log("Creating new Singleton here",singleton.gameObject);
        Object.DontDestroyOnLoad(singleton);
        return singleton.AddComponent<T>();
    }

    private void OnDestroy()
    {
        exiting = true;
    }

    private void OnApplicationQuit()
    {
        exiting = true;
    }
}
