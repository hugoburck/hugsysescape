﻿using UnityEngine;

public static class PrefabInfo
{
    public static float LargestBoundsExtent(GameObject prefab) //to class
    {
        GameObject clone = Object.Instantiate(prefab,Vector3.zero, Quaternion.identity);
        MeshRenderer[] filters = clone.GetComponentsInChildren<MeshRenderer>();
        Bounds boundsMerged = default;
        for (int i = 0; i < filters.Length; i++)
        {
            boundsMerged.Encapsulate(filters[i].bounds);
        }
        float largestExtent = default;
        for (int i = 0; i < 3; i++)
        {
            float extent = boundsMerged.extents[i];
            if (extent > largestExtent)
                largestExtent = extent;
        }
        Object.Destroy(clone);
        return largestExtent;
    }
}