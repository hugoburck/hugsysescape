﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObject //WIP, untested, do not use
{
    private static T instance;
    private static object padLock = false;
    private static bool exiting = false;

    // Start is called before the first frame update
    public static T Instance
    {
        get
        {
            lock (padLock)
            {
                if (exiting) return null;
                if (instance != null) return instance;
#if Unity_Editor
                Debug.Log($"t:{typeof(T).Name}");
                string[] guids = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(T).Name}");
                if (guids != null && guids.Length > 0)
                {
                    string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[0]);
                    instance = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
                }
                if (instance != null) return instance;
#endif
                return instance = CreateNewInstance();
            }
        }
    }

    private static string creationPath
    {
        get
        {
            return $"Assets/Data/ScriptableObjectSingleton_{typeof(T).Name}.asset";
        } 
    }

    private static T CreateNewInstance()
    {
        ScriptableObject instance = ScriptableObject.CreateInstance(typeof(T));
#if Unity_Editor
        UnityEditor.AssetDatabase.CreateAsset(instance, creationPath);
        UnityEditor.AssetDatabase.SaveAssets();
        return UnityEditor.AssetDatabase.LoadAssetAtPath<T>(creationPath) as T;
#endif
        return instance as T;
    }

    private void OnDestroy()
    {
        exiting = true;
    }

    private void OnApplicationQuit()
    {
        exiting = true;
    }
}
