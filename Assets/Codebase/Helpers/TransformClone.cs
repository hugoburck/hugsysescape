﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformClone //should be extention methods
{
    public static void CloneLocal(Transform source, Transform target)
    {
        target.localPosition = source.localPosition;
        target.localRotation = source.localRotation;
        target.localScale = source.localScale;
    }

    /// <summary>
    /// This will not clone the world scale as it is lossy and readonly.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    /// <returns></returns>
    public static void CloneWorld(Transform source, Transform target)
    {
        target.position = source.position;
        target.rotation = source.rotation;
        target.localScale = source.localScale;
    }
}
