﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Trigger<T>
{
    protected T old = default;

    public Trigger()
    {
        old = default;
    }
    public Trigger(T initial)
    {
        old = initial;
    }

    public bool OnChange(T current)
    {
        return RunStoreOld(SpecializedOnChange, current);
    }

    public bool OnUp(T current)
    {
        return RunStoreOld(SpecializedOnUp, current);
    }

    public bool OnDown(T current)
    {
        return RunStoreOld(SpecializedOnDown, current);
    }

    protected abstract bool SpecializedOnUp(T current);
    protected abstract bool SpecializedOnDown(T current);
    protected abstract bool SpecializedOnChange(T current);

    private bool RunStoreOld(Func<T,bool> specialized, T current)
    {
        bool state = specialized(current);
        old = current;
        return state;
    }
}
