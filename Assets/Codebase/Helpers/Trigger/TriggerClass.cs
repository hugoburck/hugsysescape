﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerClass<T> : Trigger<T> where T : class
{
    protected override bool SpecializedOnChange(T current)
    {
        return old != current;
    }

    protected override bool SpecializedOnDown(T current)
    {
        return old != null & current == null;
    }

    protected override bool SpecializedOnUp(T current)
    {
        return old == null & current != null;
    }
}
