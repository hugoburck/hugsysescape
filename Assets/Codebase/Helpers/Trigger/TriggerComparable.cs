﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerComparable<T> : Trigger<T> where T : System.IComparable
{
    protected override bool SpecializedOnChange(T current)
    {
        return current.CompareTo(old) != 0;
    }

    protected override bool SpecializedOnDown(T current)
    {
        return current.CompareTo(old) < 0;
    }

    protected override bool SpecializedOnUp(T current)
    {
        return current.CompareTo(old) > 0;
    }
}
