﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerInt : Trigger<int>
{
    protected override bool SpecializedOnChange(int current)
    {
        return current != old;
    }

    protected override bool SpecializedOnDown(int current)
    {
        return current < old;
    }

    protected override bool SpecializedOnUp(int current)
    {
        return current > old;
    }
}
