﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNull<T> : TriggerClass<T> where T : class
{
    protected override bool SpecializedOnChange(T current)
    {
        return IsDifferent(current, old) || IsDifferent(old, current);
    }

    private bool IsDifferent(T isNull, T isNotNull)
    {
        return isNull == null && isNotNull != null;
    }
}
