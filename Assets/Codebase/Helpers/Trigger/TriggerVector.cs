﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerVector : Trigger<Vector3>
{
    protected override bool SpecializedOnChange(Vector3 current)
    {
        return !current.Equals(old);
    }

    protected override bool SpecializedOnDown(Vector3 current)
    {
        return current.magnitude < old.magnitude;
    }

    protected override bool SpecializedOnUp(Vector3 current)
    {
        return current.magnitude > old.magnitude;
    }
}
