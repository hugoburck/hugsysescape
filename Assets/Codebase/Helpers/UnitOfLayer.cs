﻿public enum UnitOfLayer
{
    Default,
    TransparentFX,
    Ignore_Raycast,
    Water,
    UI,
    PostProcessing,
    Colliders,
    Player,
    Disabled,
    Inventory,
    Raycast
}
