﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Helpers
{
    public static class UnityObjectHelper
    {
        /// <summary>
        /// Uses DestroyImmediate in the Editor (testing) and Destroy otherwise.
        /// </summary>
        /// <param name="obj"></param>
        public static void DestroySmart(UnityEngine.Object obj)
        {
#if UNITY_EDITOR
            UnityEngine.Object.DestroyImmediate(obj);
#else
                UnityEngine.Object.Destroy(obj);
#endif
        }
    }
}
