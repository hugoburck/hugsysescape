﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MaterialRemapping : AssetPostprocessor
{
    private void OnPreprocessModel()
    {
        ModelImporter modelImporter = assetImporter as ModelImporter;
        bool allRemapped = modelImporter.SearchAndRemapMaterials(ModelImporterMaterialName.BasedOnTextureName,
            ModelImporterMaterialSearch.RecursiveUp);
        
        if (!allRemapped)
            Debug.Log($"Not all materials remapped at {modelImporter.name}");
    }
}
