﻿public enum ActionOfInstructionTrigger
{
    Nothing = 0,
    Spawn,
    Destroy
}