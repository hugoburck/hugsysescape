﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "InstructionData_", menuName = "ScriptableObjects/InstructionData", order = 3)]
public class InstructionData : ScriptableObject
{
    [SerializeField] private string title = "title";
    [SerializeField] private Sprite sprite = default;
    [Multiline(3)]
    [SerializeField] private string explanation = "explanation";

    public string GetTitle => title;
    public string GetExplanation => explanation;
    public Sprite GetSprite => sprite;
}
