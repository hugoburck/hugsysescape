﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InstructionTrigger : MonoBehaviour
{
	[SerializeField] protected InstructionData data = default;
	[SerializeField] private ActionOfInstructionTrigger action = default;
	protected InstructionViewManager viewManager;
	
	protected virtual void Awake()
	{
		if (data == null)
			Debug.LogError("No data specified",this);
		
		if (action == ActionOfInstructionTrigger.Nothing)
			Debug.LogError("No action specified",this);
		
		viewManager = GameObject.FindObjectOfType<InstructionViewManager>();
	}

    protected void SendData()
    {
	    this.enabled = false;
	    
	    switch (action)
	    {
		    case ActionOfInstructionTrigger.Spawn:
			    viewManager.Spawn(data);
			    return;
		    case ActionOfInstructionTrigger.Destroy:
			    viewManager.Destroy(data);
			    return;
	    }

	    
    }
}