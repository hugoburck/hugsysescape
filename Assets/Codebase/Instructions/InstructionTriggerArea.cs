﻿using UnityEngine;

public class InstructionTriggerArea : InstructionTrigger
{
    [SerializeField] private AccessArea area = default;
    private bool triggered = false;

    private void Start()
    {
        InitializationChecker.CheckImmediate(this, area);
    }

    private void Update()
    {
        if (triggered == false && area.IsWithin)
        {
            triggered = true;
            SendData();
        }
    }
}