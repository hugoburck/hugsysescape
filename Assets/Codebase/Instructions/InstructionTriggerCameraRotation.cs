﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Serialization;

public class InstructionTriggerCameraRotation : InstructionTriggerMoveDistance
{
	private CameraCharacterRotation cameraCharacterRotation;

	protected override void Awake()
	{
        base.Awake();
        cameraCharacterRotation = FindObjectOfType<CameraCharacterRotation>();
	}

	private void OnEnable()
	{
		cameraCharacterRotation.OnRotation += CalculationOnMovementHandler;
	}
	
	private void OnDisable()
	{
		cameraCharacterRotation.OnRotation -= CalculationOnMovementHandler;
	}
}
