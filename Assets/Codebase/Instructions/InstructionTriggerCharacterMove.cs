﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class InstructionTriggerCharacterMove : InstructionTriggerMoveDistance
{
	private MoveCharacter moveCharacter;
	
	protected override void Awake()
	{
        base.Awake();
        moveCharacter = FindObjectOfType<MoveCharacter>();
	}

	private void OnEnable()
	{
		moveCharacter.OnMovement += CalculationOnMovementHandler;
	}
	
	private void OnDisable()
	{
		moveCharacter.OnMovement -= CalculationOnMovementHandler;
	}
}
