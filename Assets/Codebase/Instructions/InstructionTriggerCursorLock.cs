﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl;
using UnityEngine;

public class InstructionTriggerCursorLock : InstructionTrigger
{
	private IPlayerStateMachine playerFsm;
	private ICursorLocker cursorLocker;
	
	protected override void Awake()
	{
       base.Awake();
       playerFsm = FindObjectOfType<PlayerStateMachine>();
       cursorLocker = FindObjectOfType<CursorLocker>();
	}

	private void OnEnable()
	{
		cursorLocker.OnCursorLockState += CursorLocker_OnCursorLockState;
	}

	private void CursorLocker_OnCursorLockState(bool state)
	{
		if (playerFsm.State == StateOfPlayer.Exploring && state)
			SendData();
	}
	
	private void OnDisable()
	{
		cursorLocker.OnCursorLockState -= CursorLocker_OnCursorLockState;
	}
}
