﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InstructionTriggerInspection : InstructionTrigger
{
	[SerializeField] protected Inspectable inspectable = default;
	
	private void OnEnable()
    {
	    Inspectable.OnInspectionChangedEvent += Inspectable_OnInspectionChangedEvent;
    }

	protected abstract void Inspectable_OnInspectionChangedEvent(Inspectable inspectable);

    private void OnDisable()
    {
	    Inspectable.OnInspectionChangedEvent -= Inspectable_OnInspectionChangedEvent;
    }
}