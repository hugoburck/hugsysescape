﻿public class InstructionTriggerInspectionEnter : InstructionTriggerInspection
{
    protected override void Inspectable_OnInspectionChangedEvent(Inspectable inspectable)
    {
        if (inspectable == this.inspectable)
        {
            SendData();
        }
    }
}