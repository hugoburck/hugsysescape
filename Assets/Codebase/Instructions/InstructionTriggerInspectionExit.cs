﻿public class InstructionTriggerInspectionExit : InstructionTriggerInspection
{
    protected override void Inspectable_OnInspectionChangedEvent(Inspectable inspectable)
    {
        if (inspectable == null)
        {
            SendData();
        }
    }
}