﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl;
using UnityEngine;

public abstract class InstructionTriggerItemInspect : InstructionTrigger
{
	private IPlayerStateMachine playerFsm;
	
	protected override void Awake()
	{
        base.Awake();
        playerFsm = FindObjectOfType<PlayerStateMachine>();
	}

	private void OnEnable()
	{
		playerFsm.OnChanged += PlayerFsm_OnChanged;
	}

	protected abstract void PlayerFsm_OnChanged(StateOfPlayer state);

	private void OnDisable()
	{
		playerFsm.OnChanged -= PlayerFsm_OnChanged;
	}
}