﻿public class InstructionTriggerItemInspectEnter : InstructionTriggerItemInspect
{
    protected override void PlayerFsm_OnChanged(StateOfPlayer state)
    {
        if (state == StateOfPlayer.InspectingItem)
            SendData();
    }
}    