﻿public class InstructionTriggerItemInspectExit : InstructionTriggerItemInspect
{
    private StateOfPlayer oldState;
    protected override void PlayerFsm_OnChanged(StateOfPlayer state)
    {
        if (oldState == StateOfPlayer.InspectingItem)
            SendData();
		
        oldState = state;
    }
}