﻿using UnityEngine;

public class InstructionTriggerKey : InstructionTrigger
{
	[SerializeField] private KeyCode key = default;

	private void Update()
	{
		if (Input.GetKeyDown(key))
			SendData();
	}
}