﻿using UnityEngine;

public abstract class InstructionTriggerMoveDistance : InstructionTrigger
{
    [SerializeField] private float travelDistance;
    private float distance;
	
    protected void CalculationOnMovementHandler(Vector3 movement)
    {
        if (!viewManager.HasThisInstructionSpawned(data))
            return;
		
        distance += Mathf.Abs(Vector3.Magnitude(movement));
        if (distance > travelDistance)
            SendData();
    }
}