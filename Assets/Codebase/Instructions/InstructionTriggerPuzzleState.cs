﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionTriggerPuzzleState : InstructionTrigger
{
	[SerializeField] private PuzzleToState puzzleToState = default;
	[SerializeField] private StateOfPuzzlePiece state = default;
	
	protected void Start()
	{
		InitializationChecker.CheckImmediate(this, puzzleToState);
	}

	private void OnEnable()
	{
		puzzleToState.OnStateChanged += PuzzleToState_OnStateChanged;
	}

	private void PuzzleToState_OnStateChanged(StateOfPuzzlePiece state)
	{
		if (state == this.state)
			SendData();
	}
	
	private void OnDisable()
	{
		puzzleToState.OnStateChanged -= PuzzleToState_OnStateChanged;
	}
}
