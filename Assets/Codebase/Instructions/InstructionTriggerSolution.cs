﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionTriggerSolution : InstructionTrigger
{
	private SolutionChecker solutionChecker;
	
	protected override void Awake()
	{
        base.Awake();
        solutionChecker = FindObjectOfType<SolutionChecker>();
	}

	private void OnEnable()
	{
		SolutionChecker.OnSolutionCorrect += SolutionChecker_OnSolutionCorrect;
	}

	private void SolutionChecker_OnSolutionCorrect(bool state)
	{
		if (state)
			SendData();
	}
	
	private void OnDisable()
	{
		SolutionChecker.OnSolutionCorrect -= SolutionChecker_OnSolutionCorrect;
	}
}
