﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class InstructionView : MonoBehaviour
{
	[SerializeField] private Image image = default;
	[SerializeField] private TextMeshProUGUI title = default;
	[SerializeField] private TextMeshProUGUI explanation = default;
	private InstructionData data;

	public void Construct(InstructionData instructionData)
	{
		data = instructionData;
		image.sprite = data.GetSprite;
		title.text = data.GetTitle;
		explanation.text = data.GetExplanation;
		gameObject.name = $"{nameof(InstructionView)}_{data.GetTitle}";
	}

	public bool IsSameData(InstructionData data)
	{
		return this.data == data;
	}
}
