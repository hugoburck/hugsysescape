﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class InstructionViewManager : MonoBehaviour
{
	[SerializeField] private GameObject prefab = default;
	[SerializeField] private Transform parent = default;

	private List<InstructionView> views = new List<InstructionView>();

	public InstructionView Spawn(InstructionData instructionData)
	{
		if (prefab == null)
		{
			Debug.LogError("Manager not setup properly",this);
			return null;
		}
		
		foreach (var instructionView in views)
		{
			if (instructionView.IsSameData(instructionData))
			{
				Debug.Log($"{instructionData} dupe being spawned",this);
			}
		}
		
		GameObject instance = GameObject.Instantiate(prefab, parent);
		instance.transform.SetAsFirstSibling();
		InstructionView view = instance.GetComponent<InstructionView>();
		if (view == null)
		{
			Debug.LogError("View not setup properly",this);
			return null;
		}
		
		view.Construct(instructionData);
		views.Add(view);

		return view;
	}

	public void Destroy(InstructionData instructionData)
	{
		List<InstructionView> removals = views.Where((v) => v.IsSameData(instructionData)).ToList();

		foreach (var view in removals)
		{
			views.Remove(view);
			Destroy(view.gameObject);
		}
	}

	public void Flush()
	{
		foreach (var view in views)
		{
			Destroy(view.gameObject);
		}
		
		views.Clear();
	}

	public bool HasThisInstructionSpawned(InstructionData instructionData)
	{
		foreach (var view in views)
		{
			if (view.IsSameData(instructionData))
				return true;
		}

		return false;
	}
}
