﻿using System;

public interface IShotAnimationManager
{
    int CurrentSequence { get; }
    bool IsPlaying { get; }
    void PlayFromStart(int sequence);
    event Action OnStart, OnEnd;
}