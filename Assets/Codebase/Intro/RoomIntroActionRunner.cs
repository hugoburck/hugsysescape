﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class RoomIntroActionRunner : MonoBehaviour
{
    [SerializeField] private GameAction StartAction = default, EndAction = default;
    private IShotAnimationManager shotAnimationManager;

    private void Awake()
    {
        shotAnimationManager = FindObjectOfType<RoomIntroManager>();
    }

    protected virtual void OnEnable()
    {
        shotAnimationManager.OnStart += RoomIntroManager_OnStart;
        shotAnimationManager.OnEnd += RoomIntroManager_OnEnd;
    }

    private void RoomIntroManager_OnStart()
    {
        if (StartAction != null)
            StartAction.Action();
    }

    private void RoomIntroManager_OnEnd()
    {
        if (EndAction != null)
            EndAction.Action();
    }
    
    protected virtual void OnDisable()
    {
        shotAnimationManager.OnStart -= RoomIntroManager_OnEnd;
        shotAnimationManager.OnEnd -= RoomIntroManager_OnEnd;
    }
}
