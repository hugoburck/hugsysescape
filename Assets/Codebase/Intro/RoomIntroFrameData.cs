﻿using System;
using UnityEngine;

[Serializable]
public struct RoomIntroFrameData : IRoomIntroData
{
#pragma warning disable 0649
    [SerializeField] private Vector2 position;
    [SerializeField] private Vector2 scale;
#pragma warning restore 0649

    public Vector2 Position => position;
    public Vector2 Scale => scale;
}