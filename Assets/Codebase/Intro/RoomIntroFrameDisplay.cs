﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomIntroFrameDisplay : MonoBehaviour, IRoomIntroDisplay
{
    [SerializeField] private Image image = default;
    [SerializeField] private RoomIntroFrameSequence[] sequences = default;
    
    public bool IsNodeDone { get; private set; }
    public int SequenceCount => sequences?.Length ?? 0;
    public int NodeCount(int sequence) => !RoomIntroManager.ValidIndex(sequences, sequence) ? 0 : sequences[sequence].NodeCount;

    private RoomIntroManager manager;
    public void Inject(RoomIntroManager manager)
    {
        this.manager = manager;
    }

    private void Awake()
    {
        InitializationChecker.CheckImmediate(this, image);
    }

    public void Play(int sequence, int node)
    {
        StartCoroutine(Animate(sequence, node));
    }

    public void Reset()
    {
        StopAllCoroutines();
    }

    private IEnumerator Animate(int sequence, int node)
    {
        IsNodeDone = false;
        image.rectTransform.anchoredPosition = sequences[sequence][node].Position * manager.ReferenceResolution;
        image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, sequences[sequence][node].Scale.x * manager.ReferenceResolution.x);
        image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, sequences[sequence][node].Scale.y * manager.ReferenceResolution.x); //screen horizontal
        yield return new WaitForSeconds(RoomIntroManager.Seconds);
        
        IsNodeDone = true;
    }

    // private Vector2 ScreenDimension => new Vector2(Screen.width, Screen.height);
    //
    // private Vector2 Position(int index)
    // {
    //     if (RoomIntroManager.ValidIndex(positions, index))
    //         return positions[index];
    //     
    //     Debug.LogError("No position retrieved", this);
    //     return Vector2.zero;
    // }
    //
    // private Vector2 Size(int index)
    // {
    //     if (RoomIntroManager.ValidIndex(sizes, index))
    //         return sizes[index];
    //     
    //     Debug.LogError("No size retrieved", this);
    //     return Vector2.zero;
    // }
}
