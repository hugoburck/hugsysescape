﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Codebase.Audio;
using Codebase.Fade;
using UnityEngine;
using UnityEngine.UI;

public class RoomIntroManager : MonoBehaviourSingleton<RoomIntroManager>, IShotAnimationManager
{
    [SerializeField] private Canvas canvas = default;
    [SerializeField] private CanvasScaler canvasScaler = default;
    [SerializeField] private RoomIntroMessageDisplay messageDisplay = default;
    [SerializeField] private RoomIntroShotDisplay shotDisplay = default;
    [SerializeField] private RoomIntroFrameDisplay frameDisplay = default;
    [SerializeField] private GameDevSettingsData gameDevSettingsData = default;
    public event Action OnStart, OnEnd;
    public const float Seconds = 3;
    private int nodes = 3, curNode = 0;
    private IImageFader imageFader;
    private IAudioMusicPlayer audioMusicPlayer;

    public int CurrentSequence { get; private set; }
    
    public bool IsPlaying { get; private set; }

    public void PlayFromStart(int sequence)
    {
        if (Application.isEditor && gameDevSettingsData != null && gameDevSettingsData.SkipShotAnimation)
            return;
        
        if (!IsValid(sequence))
        {
            Debug.LogError("No intro sequence defined", this);
            return;
        }

        nodes = MinimumNodesIn(sequence);
        
        CurrentSequence = sequence;
        IsPlaying = true;
        OnStart?.Invoke();
        Reset();
        Play();
    }

    public Vector2 ReferenceResolution => canvasScaler.referenceResolution;

    private bool IsValid(int sequence)
    {
        return sequence < frameDisplay.SequenceCount &&
               sequence < messageDisplay.SequenceCount &&
               sequence < shotDisplay.SequenceCount;
    }

    private int MinimumNodesIn(int sequence)
    {
        int[] counts = 
        {
            frameDisplay.NodeCount(sequence),
            messageDisplay.NodeCount(sequence),
            shotDisplay.NodeCount(sequence)
        };

        return counts.Min();
    }
    

    private void Play()
    {
        SetSequencesActive(true);
        imageFader.FadeOut(1);
        messageDisplay.Play(CurrentSequence, curNode);
        shotDisplay.Play(CurrentSequence, curNode);
        frameDisplay.Play(CurrentSequence, curNode);
        StartCoroutine(WaitForSequenceNode());
    }
    

    private void SetSequencesActive(bool state)
    {
        canvas.gameObject.SetActive(state);
        messageDisplay.gameObject.SetActive(state);
        shotDisplay.gameObject.SetActive(state);
        frameDisplay.gameObject.SetActive(state);
    }
    
    public static bool ValidIndex<T>(List<T> list, int index)
    {
        return !(list == null || list.Count == 0 || index < 0 || index >= list.Count);
    }
    
    public static bool ValidIndex<T>(T[] list, int index) //TODO: promote to helper class
    {
        return !(list == null || list.Length == 0 || index < 0 || index >= list.Length);
    }

    private void Awake()
    {
        InitializationChecker.CheckImmediate(this,
            messageDisplay, shotDisplay, frameDisplay, canvas, canvasScaler);
        
        ServiceLocator.Instance.Register<IShotAnimationManager>(this);
    }

    private void Start()
    {
        messageDisplay.Inject(this);
        frameDisplay.Inject(this);
        imageFader = ServiceLocator.Instance.Resolve(imageFader);
        audioMusicPlayer = ServiceLocator.Instance.Resolve(audioMusicPlayer);
    }

    private void Reset()
    {
        curNode = 0;
        messageDisplay.Reset();
        shotDisplay.Reset();
        frameDisplay.Reset();
        StopAllCoroutines();
    }

    private void End()
    {
        SetSequencesActive(false);
        IsPlaying = false;
        imageFader.FadeOut(1);
        OnEnd?.Invoke();
    }

    private bool OnLastNode => curNode == nodes - 1;

    private IEnumerator WaitForSequenceNode()
    {
        while (!messageDisplay.IsNodeDone && !shotDisplay.IsNodeDone && !frameDisplay.IsNodeDone)
            yield return null;

        imageFader.FadeIn(1);
        if (OnLastNode)
            audioMusicPlayer.StopCurrentMusic(1f);
        
        yield return StartCoroutine(imageFader.WaitForFade());

        if (++curNode < nodes)
            Play();
        else End();
    }
}
