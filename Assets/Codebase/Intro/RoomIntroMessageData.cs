﻿using System;
using UnityEngine;

[Serializable]
public struct RoomIntroMessageData : IRoomIntroData
{
#pragma warning disable 0649
    [SerializeField] private string message;
    [SerializeField] private Vector2 position;
#pragma warning restore 0649

    public string Message => message;
    public Vector2 Position => position;
}