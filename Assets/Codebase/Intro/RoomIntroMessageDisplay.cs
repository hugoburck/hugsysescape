﻿using System.Collections;
using TMPro;
using UnityEngine;

public class RoomIntroMessageDisplay : MonoBehaviour, IRoomIntroDisplay
{
    [SerializeField] private TextMeshProUGUI tmpUGUI = default;
    [SerializeField] private RoomIntroMessageSequence[] sequences = default;

    public bool IsNodeDone { get; private set; }
    public int SequenceCount => sequences?.Length ?? 0;
    public int NodeCount(int sequence) => !RoomIntroManager.ValidIndex(sequences, sequence) ? 0 : sequences[sequence].NodeCount;
    
    private RoomIntroManager manager;
    public void Inject(RoomIntroManager manager)
    {
        this.manager = manager;
    }

    private void Awake()
    {
        InitializationChecker.CheckImmediate(this, tmpUGUI);
    }

    public void Play(int sequence, int node)
    {
        StartCoroutine(Animate(sequence,node));
    }

    public void Reset()
    {
        tmpUGUI.text = string.Empty;
        StopAllCoroutines();
    }

    private IEnumerator Animate(int sequence, int node)
    {
        IsNodeDone = false;
        tmpUGUI.text = sequences[sequence][node].Message;
        tmpUGUI.rectTransform.anchoredPosition = sequences[sequence][node].Position * manager.ReferenceResolution;
        yield return new WaitForSeconds(RoomIntroManager.Seconds);
        IsNodeDone = true;
    }

    // private Vector2 ScreenDimension => new Vector2(Screen.width, Screen.height);
    //
    // private string Message(int index)
    // {
    //     if (RoomIntroManager.ValidIndex(messages, index)) 
    //         return messages[index];
    //     
    //     Debug.LogError("No message retrieved", this);
    //     return "Error";
    // }
    //
    // private Vector2 Position(int index)
    // {
    //     if (RoomIntroManager.ValidIndex(positions, index))
    //         return positions[index];
    //     
    //     Debug.LogError("No position retrieved", this);
    //     return Vector2.zero;
    // }
    

}