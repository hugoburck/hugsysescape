﻿using System;
using UnityEngine;

[Serializable]
public class RoomIntroShotData : IRoomIntroData
{
    [SerializeField] private Transform position = default;
    [SerializeField] private Transform target = default;
    [SerializeField] private float fov = 60;

    public Transform Position => position;
    public Transform Target => target;
    public float FOV => fov;
}