﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomIntroShotDisplay : MonoBehaviour, IRoomIntroDisplay
{
    [SerializeField] private CameraAnimatedPlacement cameraAnimatedPlacement = default;
    [SerializeField] private RoomIntroShotSequence[] sequences = default;

    public bool IsNodeDone { get; private set; }
    public int SequenceCount => sequences?.Length ?? 0;
    public int NodeCount(int sequence) => !RoomIntroManager.ValidIndex(sequences, sequence) ? 0 : sequences[sequence].NodeCount;

    public void Play(int sequence, int index)
    {
        StartCoroutine(Animate(sequence, index));
    }

    public void Reset()
    {
        StopAllCoroutines();
    }

    private void Awake()
    {
        cameraAnimatedPlacement = FindObjectOfType<CameraAnimatedPlacement>();
        InitializationChecker.CheckImmediate(this, cameraAnimatedPlacement);
    }

    private IEnumerator Animate(int sequence, int node)
    {
        IsNodeDone = false;
        Vector3 pos = Vector3.zero;
        Quaternion rot = new Quaternion();
        float fov = 60;

        pos = sequences[sequence][node].Position.position;
        rot = Quaternion.LookRotation(sequences[sequence][node].Target.position - pos);
        fov = sequences[sequence][node].FOV;
        cameraAnimatedPlacement.Place(pos,rot,fov);
        yield return new WaitForSeconds(RoomIntroManager.Seconds);
        IsNodeDone = true;
    }
}
