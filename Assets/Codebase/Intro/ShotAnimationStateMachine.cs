﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public interface IShotAnimationStateMachine
{
    event Action<StateOfShotAnimation> OnStateChanged;
    StateOfShotAnimation State { get; }
}

public class ShotAnimationStateMachine : MonoBehaviour, IShotAnimationStateMachine
{
    public event Action<StateOfShotAnimation> OnStateChanged;
    public StateOfShotAnimation backing;
    private IShotAnimationManager roomIntroManager;
    
    public StateOfShotAnimation State
    {
        get => backing;
        
        private set
        {
            if (value == backing) 
                return;
            
            backing = value;
            OnStateChanged?.Invoke(backing);
        } 
    }

    private void Awake()
    {
        ServiceLocator.Instance.Register<IShotAnimationStateMachine>(this);
    }

    private void Start()
    {
        roomIntroManager = ServiceLocator.Instance.Resolve(roomIntroManager);
        Subscribe();
    }

    private void OnEnable()
    {
        Subscribe();
    }

    private void Subscribe()
    {
        if (roomIntroManager != null)
        {
            roomIntroManager.OnStart -= RoomIntroManager_OnStart;
            roomIntroManager.OnEnd -= RoomIntroManager_OnEnd;
            roomIntroManager.OnStart += RoomIntroManager_OnStart;
            roomIntroManager.OnEnd += RoomIntroManager_OnEnd;
        }
    }

    private void RoomIntroManager_OnStart()
    {
        if (roomIntroManager.CurrentSequence == 0)
            State = StateOfShotAnimation.Intro;

        if (roomIntroManager.CurrentSequence == 1)
            State = StateOfShotAnimation.Outro;
    }

    private void RoomIntroManager_OnEnd()
    {
        State = StateOfShotAnimation.Idle;
    }

    private void OnDisable()
    {
        if (roomIntroManager != null)
        {
            roomIntroManager.OnStart -= RoomIntroManager_OnStart;
            roomIntroManager.OnEnd -= RoomIntroManager_OnEnd;
        }
    }
}
