﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateOfShotAnimation
{
    Idle,
    Intro,
    Outro
}
