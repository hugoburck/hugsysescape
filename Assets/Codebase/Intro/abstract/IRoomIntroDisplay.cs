﻿public interface IRoomIntroDisplay
{
    bool IsNodeDone { get; }
    int SequenceCount { get; }
    int NodeCount(int sequence);
}