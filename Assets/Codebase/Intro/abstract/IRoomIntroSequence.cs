﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRoomIntroSequence
{
    int NodeCount { get; }
}
