﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RoomIntroSequence<T> : MonoBehaviour where T : IRoomIntroData, new()
{
    [SerializeField] private T[] datas = default;
    
    public T this[int index] => GetData(index);
    
    public int NodeCount => datas?.Length ?? 0;

    private T GetData(int index)
    {
        if ( index < 0 || index >= datas.Length)
            return new T();
        
        return datas[index];
    }
    
}
