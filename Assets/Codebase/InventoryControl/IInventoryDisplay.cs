﻿using System;

namespace Codebase.InventoryControl
{
    public interface IInventoryDisplay
    {
        bool IsInventoryDisplayed { get; }
        event Action<bool> OnDisplayChanged;
    }
}