﻿using System.Collections;
using System.Collections.Generic;
using Codebase.InventoryControl;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
public class InventoryCanvasControl : MonoBehaviour //TODO remove this class
{
    private static Canvas canvas;
    //private IInventoryDisplay inventoryDisplay;

    public static bool IsCanvasEnabled => canvas.enabled;

    // Start is called before the first frame update
    void Awake()
    {
        canvas = GetComponent<Canvas>();
        //inventoryDisplay = FindObjectOfType<InventoryDisplay>();
    }

    private void OnEnable()
    {
        //inventoryDisplay.OnDisplayChanged += InventoryDisplay_OnDisplayChanged;
    }

    private void InventoryDisplay_OnDisplayChanged(bool state)
    {
        canvas.enabled = state;
    }

    private void OnDisable()
    {
        //inventoryDisplay.OnDisplayChanged -= InventoryDisplay_OnDisplayChanged;
    }
}
