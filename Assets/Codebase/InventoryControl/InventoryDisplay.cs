﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using Codebase.InventoryControl;
using Codebase.PlayerControl.DeviceInput;

public class InventoryDisplay : MonoBehaviourSingleton<InventoryDisplay>, IInventoryDisplay
{
    [SerializeField] private InventoryCanvasControl inventoryCanvasControl = default;
    public bool IsInventoryDisplayed { get; private set; }
    public event Action<bool> OnDisplayChanged;
    private IDeviceInput deviceInput;

    private bool ToggleInventory() => IsInventoryDisplayed = !IsInventoryDisplayed;

    private void Awake()
    {
        InitializationChecker.CheckImmediate(this, inventoryCanvasControl);
    }

    private void Start()
    {
        IsInventoryDisplayed = InventoryCanvasControl.IsCanvasEnabled;
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
    }

    // private void OnEnable()
    // {
    //     StartCoroutine(SubServices());
    // }
    //
    // private IEnumerator SubServices()
    // {
    //     yield return null;
    //     deviceInput.OnExitInputEvent += DeviceInput_ExitInputEvent;
    // }
    //
    // private void DeviceInput_ExitInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    // {
    //     return;
    //     
    //     if (actionOfPlayer == ActionOfPlayer.Inventory)
    //     {
    //         OnDisplayChanged?.Invoke(ToggleInventory());
    //     }
    // }
    //
    // private void OnDisable()
    // {
    //     deviceInput.OnExitInputEvent -= DeviceInput_ExitInputEvent;
    // }
}
