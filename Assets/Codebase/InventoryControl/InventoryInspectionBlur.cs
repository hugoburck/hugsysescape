﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryInspectionBlur : MonoBehaviour
{
    [SerializeField]
    private Image image;
    [SerializeField]
    private InventoryItemViewer itemViewer;

    private void Awake()
    {
        if (image == null)
            image = GetComponent<Image>();
        if (itemViewer == null)
            itemViewer = FindObjectOfType<InventoryItemViewer>();
        InitializationChecker.CheckImmediate(this, image, itemViewer);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        image.enabled = InventoryItemViewer.IsInspectingItem;
    }
}
