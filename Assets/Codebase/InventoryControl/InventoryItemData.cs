﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;


[CreateAssetMenu(fileName = "InventoryItemData", menuName = "ScriptableObjects/InventoryItemData_", order = 1)]
public class InventoryItemData : ScriptableObject
{
    [SerializeField] private GameObject prefab = default;
    [SerializeField] private Vector3 viewRotation = default;
    [SerializeField] private Vector3 inspectRotation = default;

    public GameObject Prefab => prefab;
    public Vector3 ViewRotation => viewRotation;
    public Vector3 InspectRotation => inspectRotation;
}
