﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using UnityEngine;
using Helpers;
using UnityEngine.Serialization;

public interface IInventoryItemDrag
{
    bool IsDragging { get; }
}

public class InventoryItemDrag : MonoBehaviour, IInventoryItemDrag
{
    private ICursorPosition cursorPosition;
    [SerializeField] private Camera inventoryCamera = default;
    private GameObject prefab;
    private const float depth = 0.1f, dragDistance = 16f;
    private Vector2 clickedPosition;
    private TriggerComparable<bool> triggerDistance;
    private Vector3 dragPosStart;
    private float seconds = 0.2f;
    private bool solutionCorrect;
    

    public InventoryItemDrag()
    {
        cursorPosition = new InputMousePosition();
        triggerDistance = new TriggerComparable<bool>();
    }
    
    public bool IsDragging { get; private set; }

    private void Awake()
    {
        InitializationChecker.CheckImmediate(this, inventoryCamera);
        ServiceLocator.Instance.Register<IInventoryItemDrag>(this);
    }

    private void Start()
    {

    }

    private void OnEnable()
    {
        InventoryItemView.OnItemDrag += InventoryItemViewOnOnItemDrag;
        InventoryItemView.OnItemDrop += InventoryItemViewOnOnItemDrop;
        SolutionChecker.OnSolutionCorrect += SolutionChecker_OnSolutionCorrect;
    }

    private void SolutionChecker_OnSolutionCorrect(bool correct)
    {
        if (prefab != null)
        {
            IsDragging = false;
            solutionCorrect = correct;
        }
    }

    private void InventoryItemViewOnOnItemDrop(InventoryItemData itemData)
    {

    }

    private void InventoryItemViewOnOnItemDrag(InventoryItemData itemData)
    {
        ResetDistanceDelta();
        SpawnPrefab(itemData);
        dragAnimation = Animate();
        StartCoroutine(dragAnimation);
    }

    private IEnumerator dragAnimation;

    private void OnDisable()
    {
        InventoryItemView.OnItemDrag -= InventoryItemViewOnOnItemDrag;
        InventoryItemView.OnItemDrop -= InventoryItemViewOnOnItemDrop;
        SolutionChecker.OnSolutionCorrect -= SolutionChecker_OnSolutionCorrect;
    }

    private void SpawnPrefab(InventoryItemData itemData)
    {
        prefab = Instantiate(itemData.Prefab, transform);
        prefab.transform.localRotation = Quaternion.Euler(itemData.ViewRotation);
        prefab.transform.localScale = Vector3.one * InventoryViewFactory.PrefabSize * (1/PrefabInfo.LargestBoundsExtent(itemData.Prefab));
        SetLayer(UnitOfLayer.Disabled);
        var colliders = prefab.GetComponentsInChildren<Collider>();
        if (colliders != null && colliders.Length > 0)
            foreach (var col in colliders)
                col.enabled = false;
    }

    private IEnumerator Animate()
    {
        GameObject prefab = this.prefab;
        IsDragging = true;
        dragPosStart = PositionFromCursor(cursorPosition.Position);
        
        while (IsDragging)
        {
            prefab.transform.position = PositionFromCursor(cursorPosition.Position);
        
            if (triggerDistance.OnUp(GetDistanceDragged > dragDistance))
                SetLayer(UnitOfLayer.Inventory);

            yield return null;
        }

        if (solutionCorrect)
        {
            Destroy(prefab);
            yield break;
        }
        
        Vector3 dragPosCur = prefab.transform.position;
        float counter = 0f;
        while (counter < 1f)
        {
            counter += Time.deltaTime * (1/seconds);
            prefab.transform.position = Vector3.Lerp(dragPosCur,dragPosStart, counter.CosineDamper());
            yield return null;
        }
        
        Destroy(prefab);
    }

    private Vector3 PositionFromCursor(Vector3 pos)
    {
        pos.z = depth;
        return inventoryCamera.ScreenToWorldPoint(pos);
    }

    private float GetDistanceDragged => Vector2.Distance(cursorPosition.Position , clickedPosition);

    private void ResetDistanceDelta()
    {
        clickedPosition = cursorPosition.Position;
    }

    private void SetLayer(UnitOfLayer unitOfLayer)
    {
        foreach (var trans in prefab.GetComponentsInChildren<Transform>())
        {
            trans.gameObject.layer = LayerMaskManager.GetLayer(unitOfLayer);
        }
    }
}
