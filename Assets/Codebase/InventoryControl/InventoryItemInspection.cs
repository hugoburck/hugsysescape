﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemInspection : MonoBehaviour
{
    public InventoryItemData ItemData { get; private set; }
    public static InventoryItemInspection CurrentMouseOver = null;
    public static bool HasMouseOver => CurrentMouseOver != null;
    private bool mouseDrag;

    public InventoryItemInspection Initialize(InventoryItemData itemData)
    {
        ItemData = itemData;
        return this;
    }

    public void SetLayer(UnitOfLayer unitOfLayer)
    {
        foreach (var trans in this.GetComponentsInChildren<Transform>())
        {
            trans.gameObject.layer = LayerMaskManager.GetLayer(unitOfLayer);
        }
    }

    public void EnableGobj()
    {
        this.gameObject.SetActive(true);
    }
    
    public void DisableGobj()
    {
        this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        //DeviceInput.OnInputEvent += DeviceInput_InputEvent;
        //DeviceInput.OnExitInputEvent += DeviceInput_ExitInputEvent;
    }

    private void DeviceInput_ExitInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (actionOfPlayer == ActionOfPlayer.Use)
            mouseDrag = false;
    }

    private void DeviceInput_InputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (mouseDrag)
        {
            if (actionOfPlayer == ActionOfPlayer.LookHeading)
                rotateInspection(0, -intensity);
            if (actionOfPlayer == ActionOfPlayer.LookPitch)
                rotateInspection(-intensity, 0);
        }
    }

    private void rotateInspection(float heading,float pitch)
    {
        transform.Rotate(0f, pitch, heading, Space.World);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnMouseDrag()
    {
        mouseDrag = true;
    }

    private void OnMouseEnter()
    {
        CurrentMouseOver = this;
    }

    private void OnMouseExit()
    {
        if (CurrentMouseOver == this)
            CurrentMouseOver = null;
    }
}
