﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItemInspector : MonoBehaviour
{
    public InventoryItemData CurrentItemData;

    private void OnEnable()
    {
        InventoryItemView.OnItemDrop += InventoryItemView_OnItemDrop;
    }

    private void OnDisable()
    {
        InventoryItemView.OnItemDrop -= InventoryItemView_OnItemDrop;
    }

    private void InventoryItemView_OnItemDrop(InventoryItemData itemData)
    {
        if (itemData == InventoryItemView.CurrentMouseOver.ItemData)
        {
            if (CurrentItemData == itemData)
                DestroyInspection();
            else
            {
                SpawnInspection(itemData);
            }
        }
    }

    private void DestroyInspection()
    {
        Debug.Log("Uninspect");
        CurrentItemData = null;
    }

    private void SpawnInspection(InventoryItemData itemData)
    {
        Debug.Log("Inspect");
        CurrentItemData = itemData;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
