﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.InventoryControl;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

public class InventoryItemView : MonoBehaviour
{
    public static InventoryItemView CurrentMouseOver;
    public static bool HasMouseOver { get => CurrentMouseOver != null; }
    public static event Action<InventoryItemData> OnItemDrop;
    public static event Action<InventoryItemData> OnItemDrag;
    public InventoryItemData ItemData { get => _data; private set => _data = value; }
    private InventoryItemData _data; //inspection only
    private bool mouseDrag;
    private IDeviceInput deviceInput;
    private InventoryViewCollision viewCollision;


    public InventoryItemView(InventoryItemData itemData)
    {
        ItemData = itemData;
    }

    public InventoryItemView Initialize(InventoryItemData itemData, InventoryViewCollision viewCollision)
    {
        if (itemData != null)
            ItemData = itemData;
        else Debug.LogError("No itemdata found",this);

        if (viewCollision != null)
        {
            this.viewCollision = viewCollision;
            viewCollision.OnMouseOver += ViewCollision_OnMouseOver;
        }
        else Debug.LogError("No collision found",this);
        
        return this;
    }

    private void ViewCollision_OnMouseOver(bool state)
    {
        if (state && CurrentMouseOver == null) 
            CurrentMouseOver = this;
        
        if (!state && CurrentMouseOver == this) 
            CurrentMouseOver = null;
    }

    public bool IsDraggingItem => ItemData != null;
    

    // Start is called before the first frame update
    void Start()
    {
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
    }

    private void OnEnable()
    {
        StartCoroutine(SubServices());
    }
    
    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnEnterInputEvent += DeviceInput_EnterInputEvent;
        deviceInput.OnExitInputEvent += DeviceInput_ExitInputEvent;
    }

    private void InventoryDisplay_OnInventoryDisplayChanged(bool state)
    {
        int layer = LayerMaskManager.GetLayer(state ? UnitOfLayer.Inventory : UnitOfLayer.Disabled);
        foreach (Transform transform in GetComponentsInChildren<Transform>())
            transform.gameObject.layer = layer;
    }

    private void OnDisable()
    {
        deviceInput.OnEnterInputEvent -= DeviceInput_EnterInputEvent;
        deviceInput.OnExitInputEvent -= DeviceInput_ExitInputEvent;
    }

    private void DeviceInput_ExitInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (actionOfPlayer == ActionOfPlayer.Use)
        {
            if (mouseDrag)
            {
                OnItemDrop?.Invoke(ItemData);
            }
            mouseDrag = false;
        }
    }

    private void DeviceInput_EnterInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (actionOfPlayer == ActionOfPlayer.Use &&
            viewCollision.MouseOver)
        {
            mouseDrag = true;
            OnItemDrag?.Invoke(ItemData);
        }
    }

    private void OnDestroy()
    {
        viewCollision.OnMouseOver -= ViewCollision_OnMouseOver;
    }
}
