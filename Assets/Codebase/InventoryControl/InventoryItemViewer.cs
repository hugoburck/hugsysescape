﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Codebase.InventoryControl;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;

public class InventoryItemViewer : MonoBehaviour
{
    [SerializeField]
    public List<InventoryItemView> itemViews = new List<InventoryItemView>();
    public List<InventoryItemInspection> itemInspections = new List<InventoryItemInspection>();
    public static InventoryItemInspection CurrentInspected;
    public GameObject Background;
    public Transform InspectionParent;
    public static event Action<InventoryItemData> OnEnterInspection;
    public static event Action OnExitInspection;
    public static event Action<InventoryItemInspection> OnInspectionChanged;
    
    private Vector3 posalign = new Vector3(0.08f, 0.0f, 0.1f); //TODO temporary, must be improved
    private const int maxItems = 5;
    private IDeviceInput deviceInput;
    private IPlayerStateMachine playerStateMachine;

    public static bool IsInspectingItem => CurrentInspected != null;

    private void Awake()
    {
        deviceInput = FindObjectOfType<DeviceInput>();
    }

    private void Start()
    {
        playerStateMachine = ServiceLocator.Instance.Resolve(playerStateMachine);
    }

    private void OnEnable()
    {
        InventoryRepoData.OnAddedItemEvent += InventoryRepoData_OnAddedItemEvent;
        InventoryRepoData.OnRemovedItemEvent += InventoryRepoData_OnRemovedItemEvent;
        InventoryItemView.OnItemDrop += InventoryItemView_OnItemDrop;
        deviceInput.OnEnterInputEvent += DeviceInput_OnEnterInputEvent;
    }

    private void DeviceInput_OnEnterInputEvent(ActionOfPlayer pa, float i)
    {
        if (IsInspectingItem && pa == ActionOfPlayer.Cancel)
            HideInspection(CurrentInspected);
    }

    private void InventoryRepoData_OnAddedItemEvent(InventoryItemData itemData)
    {
        itemViews.Add(InventoryViewFactory.View(itemData, transform, Background));
        var inspection = InventoryViewFactory.Inspection(itemData, InspectionParent);
        itemInspections.Add(inspection);
        AlignItems(itemViews);
        if (playerStateMachine.State == StateOfPlayer.InspectingItem)
            ShowInspection(inspection);
    }

    private void InventoryRepoData_OnRemovedItemEvent(InventoryItemData itemData)
    {
        var view = itemViews.Find(v => v.ItemData == itemData);
        var inspection = itemInspections.Find(i => i.ItemData == itemData);
        Destroy(view.gameObject);
        Destroy(inspection.gameObject);
        itemViews.Remove(view);
        itemInspections.Remove(inspection);
    }

    private void InventoryItemView_OnItemDrop(InventoryItemData itemData)
    {
        if (InventoryItemView.CurrentMouseOver != null)
        {
            SetLayerOnAllInspections(UnitOfLayer.Disabled);
            if (itemData == InventoryItemView.CurrentMouseOver.ItemData)
            {
                InventoryItemInspection inspection = itemInspections.Find(i => i.ItemData == itemData);
                if (IsInspectingItem && CurrentInspected.ItemData == itemData)
                    HideInspection(inspection);
                else
                    ShowInspection(inspection);
            }
        }
    }

    private void OnDisable()
    {
        InventoryRepoData.OnAddedItemEvent -= InventoryRepoData_OnAddedItemEvent;
        InventoryRepoData.OnRemovedItemEvent -= InventoryRepoData_OnRemovedItemEvent;
        InventoryItemView.OnItemDrop -= InventoryItemView_OnItemDrop;
        deviceInput.OnEnterInputEvent -= DeviceInput_OnEnterInputEvent;
    }

    private void ShowInspection(InventoryItemInspection inspection)
    {
        CurrentInspected = inspection;
        inspection.SetLayer(UnitOfLayer.Default);
        OnEnterInspection?.Invoke(inspection.ItemData);
        OnInspectionChanged?.Invoke(CurrentInspected);
    }

    private void HideInspection(InventoryItemInspection inspection)
    {
        if (inspection == null)
            return;
        
        inspection.SetLayer(UnitOfLayer.Disabled);
        if (CurrentInspected == inspection) 
            CurrentInspected = null;
        
        OnExitInspection?.Invoke();
        OnInspectionChanged?.Invoke(CurrentInspected);
    }

    private void SetLayerOnAllInspections(UnitOfLayer unitOfLayer)
    {
        foreach (var inspection in itemInspections)
        {
            inspection.SetLayer(unitOfLayer);
        }
    }

    private void AlignItems(List<InventoryItemView> itemViews)
    {
        float spacing = 0.1f / maxItems;
        float centerOffset = (spacing * (maxItems -1)) / 2;
        float rowOffset = spacing * (maxItems);
        for (int i = 0; i < itemViews.Count; i++)
        {
            Vector3 align = Vector3.zero;
            int currentRow = i / maxItems;
            align.x = -spacing * currentRow;
            align.y = -spacing * i;
            align.y += centerOffset;
            align.y += rowOffset * currentRow;
            itemViews[i].transform.localPosition = align + posalign;
        }
    }
}
