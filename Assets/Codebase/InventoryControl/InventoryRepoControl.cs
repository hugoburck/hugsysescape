﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class InventoryRepoControl : MonoBehaviour
{
    [SerializeField]
    private InventoryRepoData repoData = default;

    public void ClearItems()
    {
        repoData.ClearItems();
    }

    public void AddItem(InventoryItemData itemData)
    {
        repoData.AddItem(itemData);
    }
    
    public void RemoveItem(InventoryItemData itemData)
    {
        repoData.RemoveItem(itemData);
    }

    // Start is called before the first frame update
    private void Start()
    {
        if(!Application.isPlaying)
            ClearItems();
        //load savegame
    }

    void OnEnable()
    {
        ItemPiece.OnCollectedEvent += AddCollectedItem;
        SolutionChecker.OnSolutionItem += SolutionChecker_OnSolutionItem;
        SolutionChecker.OnSolutionReward += AddCollectedItem;
    }

    private void SolutionChecker_OnSolutionItem(InventoryItemData itemData)
    {
        RemoveItem(itemData);
    }

    void OnDisable()
    {
        ItemPiece.OnCollectedEvent -= AddCollectedItem;
        SolutionChecker.OnSolutionItem -= SolutionChecker_OnSolutionItem;
        SolutionChecker.OnSolutionReward -= AddCollectedItem;
    }

    private void AddCollectedItem(InventoryItemData itemData)
    {
        AddItem(itemData);
    }
}
