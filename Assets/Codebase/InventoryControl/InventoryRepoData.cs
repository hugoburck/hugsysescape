﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[CreateAssetMenu(fileName = "InventoryRepoData", menuName = "ScriptableObjects/InventoryRepoData_", order = 1)]
public class InventoryRepoData : ScriptableObject
{
    [SerializeField]
    private List<InventoryItemData> ItemsData = new List<InventoryItemData>();
    public static event Action<InventoryItemData> OnAddedItemEvent;
    public static event Action<InventoryItemData> OnRemovedItemEvent;

    public bool AddItem(InventoryItemData itemData)
    {
        bool isFreshItem = !ItemsData.Contains(itemData);
        if (isFreshItem) 
        {
            ItemsData.Add(itemData);
            OnAddedItemEvent?.Invoke(itemData);
        }
        else
            Debug.Log("Item was already in inventory");
        return isFreshItem;
    }

    public bool RemoveItem(InventoryItemData itemData)
    {
        bool isExcistingItem = ItemsData.Contains(itemData);
        if (isExcistingItem)
        {
            ItemsData.Remove(itemData);
            OnRemovedItemEvent?.Invoke(itemData);
        }
        else
            Debug.Log("Item was not in inventory");
        return isExcistingItem;
    }

    public void ClearItems()
    {
        for (int i = ItemsData.Count - 1; i > -1; i--)
        {
            RemoveItem(ItemsData[i]);
        }
        ItemsData.Clear();
    }
}
