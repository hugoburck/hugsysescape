﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryViewCollision : MonoBehaviour
{
	public bool MouseOver { get; private set; }
	public event Action<bool> OnMouseOver;
    
    private void OnMouseEnter()
    {
	    MouseOver = true;
	    OnMouseOver?.Invoke(MouseOver);
    }

    private void OnMouseExit()
    {
	    MouseOver = false;
	    OnMouseOver?.Invoke(MouseOver);
    }
}
