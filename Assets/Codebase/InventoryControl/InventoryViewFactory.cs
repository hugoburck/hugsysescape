﻿using UnityEngine;
using System.Collections.Generic;
using Codebase.InventoryControl;

public static class InventoryViewFactory
{
    public const float ViewSize = 0.02f, PrefabSize = 0.005f;
    
    public static InventoryItemView View(InventoryItemData itemData,Transform parent,GameObject background)
    {
        GameObject view = new GameObject(itemData.name,typeof(MeshCollider));
        view.transform.parent = parent;
        if (!background.TryGetComponent(out MeshCollider _))
            view.GetComponent<MeshCollider>().sharedMesh = background.GetComponent<MeshCollider>().sharedMesh;
        
        GameObject bg = Object.Instantiate(background, view.transform);
        GameObject prefab = Object.Instantiate(itemData.Prefab, view.transform);
        foreach (Transform transform in view.GetComponentsInChildren<Transform>())
            transform.gameObject.layer = LayerMaskManager.GetLayer(UnitOfLayer.Inventory);

        bg.transform.localPosition = Vector3.forward * 0.001f;
        prefab.transform.localPosition = Vector3.zero;

        prefab.transform.localRotation = Quaternion.Euler(itemData.ViewRotation);

        bg.transform.localScale = Vector3.one * ViewSize;
        prefab.transform.localScale = Vector3.one * PrefabSize * (1/PrefabInfo.LargestBoundsExtent(itemData.Prefab));

        foreach (var collider in prefab.GetComponentsInChildren<Collider>())
        {
            collider.enabled = false;
        }
        
        return view.AddComponent<InventoryItemView>().Initialize(itemData, bg.GetComponent<InventoryViewCollision>());
    }

    public static InventoryItemInspection Inspection(InventoryItemData itemData,Transform parent)
    {
        GameObject prefab = Object.Instantiate(itemData.Prefab, parent);
        prefab.transform.localPosition = Vector3.zero;
        prefab.transform.localRotation = Quaternion.Euler(itemData.InspectRotation);
        foreach (Transform transform in prefab.GetComponentsInChildren<Transform>())
            transform.gameObject.layer = LayerMaskManager.GetLayer(UnitOfLayer.Disabled);

        if (!prefab.TryGetComponent(out Collider _))
        {
            Debug.Log("This inspection has no collider, adding sphere");
            var spherecol = prefab.AddComponent<SphereCollider>();
            spherecol.radius = 0.05f;
        }

        return prefab.AddComponent<InventoryItemInspection>().Initialize(itemData);
    }
    
}

