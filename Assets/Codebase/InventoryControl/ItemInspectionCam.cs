﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using UnityEngine;
using UnityEngine.Serialization;

public class ItemInspectionCam : MonoBehaviour
{
    private IPlayerCameraControl playerCameraControl;

    public Vector3 Position => transform.position;
    public Quaternion Rotation => transform.rotation;
    public float Fov { get; private set; }

    private void Awake()
    {
        playerCameraControl = PlayerCamera.Instance;
    }

    private void Start()
    {
        Fov = playerCameraControl.Fov;
    }

    private void OnEnable()
    {
        InventoryItemViewer.OnInspectionChanged += InventoryItemViewerOnOnInspectionChanged;
    }

    private void InventoryItemViewerOnOnInspectionChanged(InventoryItemInspection inspection)
    {
        if (!InventoryItemViewer.IsInspectingItem)
            return;
        
        transform.localPosition = transform.localPosition.CopyWithZ(DistanceToPrefab(inspection));
    }

    private float DistanceToPrefab(InventoryItemInspection inspection) =>
        -PrefabInfo.LargestBoundsExtent(inspection.ItemData.Prefab) * 4;
}
