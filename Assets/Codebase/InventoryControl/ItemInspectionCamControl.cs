﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Xml;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

public class ItemInspectionCamControl : MonoBehaviour
{
    [SerializeField] private Transform pivot = default;
    private bool mouseDrag;
    private IInventoryItemDrag inventoryItemDrag;
    private IDeviceInput deviceInput;
    private Vector3 startRotation = new Vector3(45, 0, 0);

    public bool IsRotating => mouseDrag;

    private void Awake()
    {
        if (pivot == null)
            pivot = GetComponent<Transform>();
    }

    private void Start()
    {
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
        inventoryItemDrag = ServiceLocator.Instance.Resolve(inventoryItemDrag);
    }

    private void OnEnable()
    {
        StartCoroutine(SubServices());
        InventoryItemViewer.OnEnterInspection += InventoryItemViewer_OnEnterInspection;
    }

    private void InventoryItemViewer_OnEnterInspection(InventoryItemData itemData)
    {
        pivot.localRotation = Quaternion.Euler(startRotation);
    }

    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnInputEvent += DeviceInput_InputEvent;
        deviceInput.OnExitInputEvent += DeviceInput_ExitInputEvent;
    }

    private void OnDisable()
    {
        InventoryItemViewer.OnEnterInspection -= InventoryItemViewer_OnEnterInspection;
        deviceInput.OnInputEvent -= DeviceInput_InputEvent;
        deviceInput.OnExitInputEvent -= DeviceInput_ExitInputEvent;
    }

    private void DeviceInput_ExitInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (actionOfPlayer == ActionOfPlayer.Use)
            mouseDrag = false;
    }

    private void DeviceInput_InputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (inventoryItemDrag.IsDragging)
            return;
        
        if (actionOfPlayer == ActionOfPlayer.Use)
        {
            mouseDrag = true;
            return;
        }
        
        if (mouseDrag)
        {
            if (actionOfPlayer == ActionOfPlayer.LookHeading)
                rotateInspection(0, intensity);
            if (actionOfPlayer == ActionOfPlayer.LookPitch)
                rotateInspection(intensity, 0);
        }
    }

    private void rotateInspection(float heading,float pitch)
    {
        // transform.rotation = Quaternion.AngleAxis(heading, Vector3.up);
        // transform.rotation = Quaternion.AngleAxis(pitch, Vector3.right);
        // transform.Rotate(heading, pitch, 0f, Space.Self);
        // transform.RotateAround(this.transform.position,Vector3.up,pitch);
        // transform.RotateAround(this.transform.position,Vector3.right,heading);
        Vector3 euler = pivot.rotation.eulerAngles + new Vector3(heading, pitch, 0f);
        pivot.rotation = Quaternion.Euler(euler);
    }
}
