﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Mesh;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
public class EntryLocator : MonoBehaviour
{
    [SerializeField]
    private Image image;

    [SerializeField] 
    private MeshScreenBounds meshScreenBounds = default;
    [SerializeField] 
    private PositionToScreen posToScreen = default;

    private PuzzlePiece selectedPuzzlePiece;
    private MeshFilter puzzleMeshFilter;

    public float Scale = 2f;
    
    
    private RectTransform canvasRect,locatorRect;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        locatorRect = GetComponent<RectTransform>();
        InitializationChecker.CheckImmediate(this,meshScreenBounds, posToScreen);
        image.enabled = false;
    }

    private void OnEnable()
    {
        EntryMono.OnSelectedEntryChanged += EntryMonoOnOnSelectedEntryChanged;
    }

    private void OnDisable()
    {
        EntryMono.OnSelectedEntryChanged -= EntryMonoOnOnSelectedEntryChanged;
    }

    private void EntryMonoOnOnSelectedEntryChanged(EntryMono entry)
    {
        if (entry == null)
        {
            image.enabled = false;
            selectedPuzzlePiece = null;
            
        }
        else
        {            
            image.enabled = true;
            selectedPuzzlePiece = entry.messageUnit.piece;
            if (selectedPuzzlePiece.HasReadableMeshCollider)
            {
                puzzleMeshFilter = selectedPuzzlePiece.GetMeshFilterFromMeshCollider;
            }
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        if (selectedPuzzlePiece != null)
        {
            if (selectedPuzzlePiece.HasReadableMeshCollider)
                SetupLocatorRect(meshScreenBounds.Center(puzzleMeshFilter),meshScreenBounds.Dimension(puzzleMeshFilter));
            else
            {
                SetupLocatorRect(posToScreen.Center(selectedPuzzlePiece.transform),new Vector2(20,20));
            }
        }
    }

    private void SetupLocatorRect(Vector2 center,Vector2 dimension)
    {
        locatorRect.anchoredPosition = GetLocalRectanglePoint(center);
        locatorRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, dimension.x * Scale);
        locatorRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, dimension.y * Scale);
        locatorRect.ForceUpdateRectTransforms();
    }

    private Vector2 GetLocalRectanglePoint(Vector2 screenPoint)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, screenPoint, null,
            out Vector2 localPoint);
        return localPoint;
    }

    private Vector2 entryScreenPos(EntryMono entry)
    {
        return PlayerCamera.Instance.Camera.WorldToScreenPoint(entry.messageUnit.piece.transform.position);
    }
}
