﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class EntryManager
{
    private static List<EntryMono> allEntries = new List<EntryMono>();
    public static EntryMono SelectedEntry;

    public static bool EntriesContains(EntryMono entryMono)
    {
        return allEntries.Contains(entryMono);
    }

    public static int EntriesCount => allEntries.Count;

    public static void Add(EntryMono entryMono)
    {
        allEntries.Add(entryMono);
    }

    public static void Remove(MessageUnit messageUnit)
    {
        for (int i = 0; i < allEntries.Count; i++)
        {
            if (messageUnit.Equals(allEntries[i].messageUnit))
            {
                UnityObjectHelper.DestroySmart(allEntries[i].gameObject);
            }
        }
    }

    public static void Remove(EntryMono entryMono)
    {
        allEntries.Remove(entryMono);
    }

    public static void ResetOthers(EntryMono caller)
    {
        for (int i = 0; i < allEntries.Count; i++)
        {
            if (allEntries[i] == caller)
                continue;
            allEntries[i].ResetState();
        }
    }
}

