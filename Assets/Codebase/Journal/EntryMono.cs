﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class EntryMono : MonoBehaviour
{
    public static event Action<EntryMono> OnSelectedEntryChanged;
    public MessageUnit messageUnit;
    private IGuiText guiText;
    [SerializeField]
    private Button uguiButton = default;
    [SerializeField]
    private UguiButtonEventHandler uguiButtonEventHandler = default;
    [SerializeField]
    private Image selection = default;
    private bool selected;
    private ICheckInitialized initChecker;

    public EntryMono Inject(ICheckInitialized checkInitialized, UguiButtonEventHandler uguiButtonEventHandler, IGuiText guiText)
    {
        initChecker = checkInitialized;
        this.uguiButtonEventHandler = uguiButtonEventHandler;
        this.guiText = guiText;
        return this;
    }

    public void Initialize(MessageUnit messageUnit)
    {
        if (initChecker.CheckFail)
        {
            Debug.LogError("Prefab not setup correctly", this);
            return;
        }

        EntryManager.Add(this);
        uguiButtonEventHandler.OnButtonClick += UguiButtonEventHandler_OnButtonClick;
        this.messageUnit = messageUnit;
        guiText.Text = this.messageUnit.Message;
    }
    

    public void ResetState()
    {
        selection.enabled = selected = false;
    }

    private void Awake()
    {
        if (guiText == null)
            guiText = GetComponent<IGuiText>();
        if (initChecker == null)
            initChecker = new InitializationChecker(this, uguiButton, uguiButtonEventHandler, selection);
    }

    private void Start()
    {
        
    }

    private void UguiButtonEventHandler_OnButtonClick()
    {
        EntryManager.ResetOthers(this);
        selected = !selected;
        selection.enabled = selected;
        if (selected)
        {
            EntryManager.SelectedEntry = this;
            
        }
        else if (EntryManager.SelectedEntry == this)
        {
            EntryManager.SelectedEntry = null;
        }
        OnSelectedEntryChanged?.Invoke(EntryManager.SelectedEntry);
    }

    private void OnDestroy()
    {
        ResetState();
        EntryManager.Remove(this);
    }
}
