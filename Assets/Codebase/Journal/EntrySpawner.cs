﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntrySpawner : MonoBehaviour //also destroys
{
    [SerializeField]
    private GameObject EntryPrefab = default;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        MessageRepoData.OnMessageAdded += MessageRepoData_OnMessageAdded;
        MessageRepoData.OnMessageRemoved += MessageRepoData_OnMessageRemoved;
    }

    private void OnDisable()
    {
        MessageRepoData.OnMessageAdded -= MessageRepoData_OnMessageAdded;
        MessageRepoData.OnMessageRemoved -= MessageRepoData_OnMessageRemoved;
    }

    private void MessageRepoData_OnMessageAdded(MessageUnit messageUnit)
    {
        if (EntryPrefab == null)
        {
            Debug.LogError("Prefab not found", this);
            return;
        }
        GameObject instance = Instantiate(EntryPrefab, this.transform);
        EntryMono entry = instance.GetComponent<EntryMono>();
        if (entry == null)
        {
            Debug.LogError("EntryPrefab not setup correctly", this);
        }
        entry.Initialize(messageUnit);

    }

    private void MessageRepoData_OnMessageRemoved(MessageUnit messageUnit)
    {
        EntryManager.Remove(messageUnit);
    }

}
