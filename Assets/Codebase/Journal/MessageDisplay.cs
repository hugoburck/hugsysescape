﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class MessageDisplay : MonoBehaviourSingleton<MessageDisplay>
{
    public const string ErrorMessage = "No repo found";
    public TextMeshProUGUI TMP;
    private string message;
    private float counter;

    // Start is called before the first frame update
    void Start()
    {
        TMP = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        PieceMessages.OnMessagePush += PuzzlePieceOnMessagePush;
    }

    private void PuzzlePieceOnMessagePush(PuzzlePiece piece, string message)
    {
        if (message == string.Empty)
            return;
        this.message = message;
        counter = 1f;
    }
    
    private void OnDisable()
    {
        PieceMessages.OnMessagePush -= PuzzlePieceOnMessagePush;
    }

    // Update is called once per frame
    void Update()
    {
        TMP.enabled = counter != 0f;
        if (counter == 0f)
            return;
        TMP.text = message;
        counter -= Time.deltaTime;
        counter = Mathf.Clamp(counter, 0, 1);
    }
}
