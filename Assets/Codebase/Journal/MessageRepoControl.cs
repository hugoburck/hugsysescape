﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

public class MessageRepoControl : MonoBehaviourSingleton<MessageRepoControl>
{
    [SerializeField]
    private MessageRepoData messageRepoData = default;

    public static bool ContainsMessage(PuzzlePiece piece, string message)
    {
        return Instance.messageRepoData.ContainsMessage(message, piece);
    }

    public void AddMessage(string message, PuzzlePiece piece)
    {
        messageRepoData.AddMessage(message, piece);
    }

    public void StrikeMessage(string message, PuzzlePiece piece)
    {
        messageRepoData.StrikeMessage(message, piece);
    }

    private void OnEnable()
    {
        PieceMessages.OnMessagePush += PuzzlePieceOnMessagePush;
        PieceMessages.OnMessagePull += PuzzlePieceOnMessagePull;
    }

    private void PuzzlePieceOnMessagePull(PuzzlePiece piece, string message)
    {
        StrikeMessage(message,piece);
    }

    private void PuzzlePieceOnMessagePush(PuzzlePiece piece, string message)
    {
        AddMessage(message,piece);
    }

    private void OnDisable()
    {
        PieceMessages.OnMessagePush -= PuzzlePieceOnMessagePush;
        PieceMessages.OnMessagePull -= PuzzlePieceOnMessagePull;
    }

    private void Start()
    {
        messageRepoData.ResetMessages(); //load from savedata
    }
}
