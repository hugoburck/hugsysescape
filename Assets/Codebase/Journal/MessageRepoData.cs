﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "MessageRepoData", menuName = "ScriptableObjects/MessageRepoData", order = 3)]
public class MessageRepoData : ScriptableObject//Singleton<MessageRepoData>
{
    public static event Action<MessageUnit> OnMessageAdded;
    public static event Action<MessageUnit> OnMessageRemoved;
    [SerializeField]
    private List<MessageUnit> Messages = default;

    public void AddMessage(string message,PuzzlePiece piece)
    {
        if (message == String.Empty)
            return;
        bool exists = false;
        for (int i = 0; i < Messages.Count; i++)
        {
            if (Messages[i].IsSameMessage(message, piece))
                exists = true;
        }
        if (!exists)
        {
            MessageUnit unit = new MessageUnit(message, piece);
            Messages.Add(unit);
            OnMessageAdded?.Invoke(unit);
        }
    }

    public void StrikeMessage(string message, PuzzlePiece piece) //now removes, changed later
    {
        if (message == string.Empty)
            return;
        int index = -1;
        for (int i = 0; i < Messages.Count; i++)
        {
            if (Messages[i].IsSameMessage(message, piece))
            {
                index = i;
                break;
            }
        }

        if (index >= 0)
        {
            MessageUnit unit = Messages[index];
            Messages.RemoveAt(index);
            OnMessageRemoved?.Invoke(unit);
        }
    }

    public void ResetMessages()
    {
        Messages = new List<MessageUnit>();
    }

    public bool ContainsMessage(string message, PuzzlePiece piece)
    {
        foreach (var unit in Messages)
        {
            if (unit.IsSameMessage(message, piece))
                return true;
        }

        return false;
    }
}
