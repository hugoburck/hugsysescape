﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct MessageUnit
{
    public string Message; //readonly
    public PuzzlePiece piece; //readonly

    public MessageUnit(string message, PuzzlePiece piece)
    {
        this.Message = message;
        this.piece = piece;
    }

    public static MessageUnit Factory(string message, PuzzlePiece piece)
    {
        return new MessageUnit(message, piece);
    }

    public bool IsSameMessage(string message,PuzzlePiece piece)
    {
        return Message == message && this.piece == piece;
    }
}
