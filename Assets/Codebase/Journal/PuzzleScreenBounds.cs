﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PuzzleScreenBounds : MonoBehaviour
{
    public PuzzlePiece PuzzlePiece;

    [SerializeField] private Camera cam = default;
    private List<Vector3> screenPoints = new List<Vector3>();
    private Vector2 horiRange,vertRange,center,dimension;
    private Guid guid;

    public Vector2 HorizontalRange => horiRange;
    public Vector2 VerticalRange => vertRange;

    public Vector2 Center
    {
        get
        {
            UpdateOnNewFrame();
            return center;
        }
    }

    public Vector2 Dimension
    {
        get
        {
            UpdateOnNewFrame();
            return dimension;
        }
    }

    private void GetScreenPoints()
    {
        screenPoints.Clear();
        var worldPoints = PuzzlePiece.GetWorldPoints;
        if (worldPoints != null)
            foreach (var worldPoint in worldPoints.Points)
            {
                screenPoints.Add(cam.WorldToScreenPoint(worldPoint));
            }
        else
        {
            screenPoints.Add(cam.WorldToScreenPoint(PuzzlePiece.transform.position));
        }
    }

    private void UpdateOnNewFrame()
    {
        if (guid == FrameGuid.Current) 
            return;
        GetScreenBounds();
        guid = FrameGuid.Current;
    }


    private void GetScreenBounds()
    {
        GetScreenPoints();
        horiRange.x = Mathf.Infinity;
        horiRange.y = Mathf.NegativeInfinity;
        vertRange.x = Mathf.Infinity;
        vertRange.y = Mathf.NegativeInfinity;
        
        foreach (var screenPoint in screenPoints)
        {
            horiRange.x = screenPoint.x < horiRange.x ? screenPoint.x : horiRange.x;
            horiRange.y = screenPoint.x > horiRange.y ? screenPoint.x : horiRange.y;
            vertRange.x = screenPoint.y < vertRange.x ? screenPoint.y : vertRange.x;
            vertRange.y = screenPoint.y > vertRange.y ? screenPoint.y : vertRange.y;
        }

        dimension.x = horiRange.y - horiRange.x;
        dimension.y = vertRange.y - vertRange.x;

        center.x = horiRange.x + dimension.x / 2;
        center.y = vertRange.x + dimension.y / 2;
    }
}
