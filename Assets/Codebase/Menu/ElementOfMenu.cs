﻿public enum ElementOfMenu
{
    Nothing = 0,
    GameNew,
    GameContinue,
    MenuOptions,
    QuitToDesktop,
    QuitToMenu,
    MenuMain,
    GraphicsSSAO,
    ControlInverseY,
    AudioMute,
    VolumeMaster,
    VolumeMusic,
    VolumeEffect,
    GameResume,
    Prefab,
    GraphicsVignette,
    GraphicsCA,
    GraphicsBloom,
    GraphicsSSR,
    ControlHoldLook
}