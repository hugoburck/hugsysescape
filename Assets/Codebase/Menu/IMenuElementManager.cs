﻿using System;

public interface IMenuElementManager
{
    event Action<ElementOfMenu, float> OnElementUsed;
    event Action<ElementOfMenu, bool> OnElementBoolStateChanged;
    event Action<ElementOfMenu, float> OnElementFloatStateChanged;
    void Register(MenuElement menuElement);
    void Unregister(MenuElement menuElement);
}