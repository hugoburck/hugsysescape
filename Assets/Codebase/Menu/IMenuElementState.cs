﻿using System;

public interface IMenuElementState<T>
{
    T State { get; }
    event Action<ElementOfMenu, T> OnStateChanged;
}