﻿using System;

public interface IMenuFsm
{
    event Action<StateOfMenu> OnStateChanged;
    StateOfMenu State { get; }
}