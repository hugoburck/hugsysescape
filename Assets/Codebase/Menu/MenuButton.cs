﻿using System.Collections;
using System.Dynamic;
using Codebase.Ugui;
using UnityEngine;

[RequireComponent(typeof(UguiSelectableEventHandler))]
public class MenuButton : MenuElement
{
	private UguiSelectableEventHandler selectableEventHandler;
	
	protected override void Awake()
	{
		base.Awake();
		selectableEventHandler = GetComponent<UguiSelectableEventHandler>();
		selectableEventHandler.OnClick += ButtonEventHandler_OnButtonClick;
	}

	private void ButtonEventHandler_OnButtonClick()
	{
		base.Invoke(element,1);
	}
}