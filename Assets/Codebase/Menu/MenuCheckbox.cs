﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Ugui;
using UnityEngine;

[RequireComponent(typeof(UguiSelectableEventHandler))]
public class MenuCheckbox : MenuElement, IMenuElementStateBool
{
	[SerializeField] private bool state = default;
	private UguiSelectableEventHandler selectableEventHandler;

	public event Action<ElementOfMenu, bool> OnStateChanged;
	public bool State => state;


	protected override void Awake()
	{
		base.Awake();
		selectableEventHandler = GetComponent<UguiSelectableEventHandler>();
		selectableEventHandler.OnClick += SelectableEventHandler_OnClick;
    }

	private void SelectableEventHandler_OnClick()
	{
		state = !state;
		base.Invoke(element,state ? 1 : 0);
		OnStateChanged?.Invoke(element,state);
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		state = gameSettingsMediator.GetBoolState(element);
	}

	protected virtual void Start()
    {
        
    }

	protected virtual void Update()
    {
        
    }
}
