﻿using System;
using UnityEngine;

public abstract class MenuElement : MonoBehaviour
{
    [SerializeField] protected ElementOfMenu element = default;
    public event Action<ElementOfMenu, float> OnElementUsed;
    protected IGameSettingsMediator gameSettingsMediator;
    private IMenuElementManager manager;

    protected void Invoke(ElementOfMenu element, float value)
    {
        OnElementUsed?.Invoke(element,value);
    }

    protected virtual void Awake()
    {
        manager = MenuElementManager.Instance;
        gameSettingsMediator = FindObjectOfType<GameSettingsMediator>();
    }

    protected virtual void OnEnable()
    {
        manager.Register(this);
    }

    protected virtual void OnDisable()
    {
        manager.Unregister(this);
    }
}