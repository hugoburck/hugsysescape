﻿using System;
using System.Collections.Generic;
using Codebase.Helpers;
using UnityEngine;

public class MenuElementManager : Singleton<MenuElementManager>, IMenuElementManager
{
    public event Action<ElementOfMenu, float> OnElementUsed;
    public event Action<ElementOfMenu, bool> OnElementBoolStateChanged;
    public event Action<ElementOfMenu, float> OnElementFloatStateChanged;
    private List<MenuElement> menuElements;
    private List<IMenuElementStateBool> boolStateElements;
    private List<IMenuElementStateFloat> floatStateElements;

    public MenuElementManager()
    {
        menuElements = new List<MenuElement>();
        boolStateElements = new List<IMenuElementStateBool>();
        floatStateElements = new List<IMenuElementStateFloat>();
    }

    public void Register(MenuElement menuElement)
    {
        if (!menuElements.Contains(menuElement))
        {
            menuElements.Add(menuElement);
            menuElement.OnElementUsed += MenuElement_OnElementUsed;
        }
        
        if (menuElement is IMenuElementStateBool boolElement)
            Register(boolElement);
        
        if (menuElement is IMenuElementStateFloat floatElement)
            Register(floatElement);
            
    }
    
    private void Register(IMenuElementStateBool boolStateHolder)
    {
        if (!boolStateElements.Contains(boolStateHolder))
        {
            boolStateElements.Add(boolStateHolder);
            boolStateHolder.OnStateChanged += BoolStateHolder_OnStateChanged;
        }
    }
    
    private void Register(IMenuElementStateFloat floatStateBoolHolder)
    {
        if (!floatStateElements.Contains(floatStateBoolHolder))
        {
            floatStateElements.Add(floatStateBoolHolder);
            floatStateBoolHolder.OnStateChanged += FloatStateHolder_OnStateChanged;
        }
    }

    private void MenuElement_OnElementUsed(ElementOfMenu element, float value)
    {
        if (element == ElementOfMenu.Prefab)
        {
            Debug.Log("An element has no type id"); //TODO should be more descriptive. Event should have source object
        }
        OnElementUsed?.Invoke(element,value);
    }

    private void BoolStateHolder_OnStateChanged(ElementOfMenu element, bool state)
    {
        OnElementBoolStateChanged?.Invoke(element,state);
    }

    private void FloatStateHolder_OnStateChanged(ElementOfMenu element, float state)
    {
        OnElementFloatStateChanged?.Invoke(element,state);
    }

    public void Unregister(MenuElement menuElement)
    {
        if (menuElements.Contains(menuElement))
        {
            menuElements.Remove(menuElement);
            menuElement.OnElementUsed -= MenuElement_OnElementUsed;
        }
        
        if (menuElement is IMenuElementStateBool boolStateElement)
            Unregister(boolStateElement);
        
        if (menuElement is IMenuElementStateFloat floatStateElement)
            Unregister(floatStateElement);
    }

    private void Unregister(IMenuElementStateBool element)
    {
        if (boolStateElements.Contains(element))
        {
            boolStateElements.Remove(element);
            element.OnStateChanged -= BoolStateHolder_OnStateChanged;
        }
    }

    private void Unregister(IMenuElementStateFloat element)
    {
        if (floatStateElements.Contains(element))
        {
            floatStateElements.Remove(element);
            element.OnStateChanged -= FloatStateHolder_OnStateChanged;
        }
    }
}