﻿using UnityEngine;

public class MenuElementManagerBehaviour : MonoBehaviour
{
    private IMenuElementManager manager;

    private void Awake()
    {
        manager = MenuElementManager.Instance;
    }

    private void OnEnable()
    {
        manager.OnElementUsed += Manager_OnElementUsed;
    }

    private void Manager_OnElementUsed(ElementOfMenu element, float value)
    {
        Debug.Log($"{element} = {value}");
    }
    
    private void OnDisable()
    {
        manager.OnElementUsed -= Manager_OnElementUsed;
    }
}