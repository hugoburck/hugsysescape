﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuElementVisibility : MonoBehaviour
{
	[SerializeField] private GameObject Target = default;
	[SerializeField] private StateOfMenu[] VisibleStates = default;
	private IMenuFsm menuFsm;
	
	protected virtual void Awake()
	{
		menuFsm = FindObjectOfType<MenuFsm>();
	}

    protected virtual void Start()
    {
        menuFsm.OnStateChanged += MenuFsmOnStateChanged;
    }

    protected void OnEnable()
    {
	    MenuFsmOnStateChanged(menuFsm.State);
    }

    private void MenuFsmOnStateChanged(StateOfMenu state)
    {
	    if (VisibleStates == null || Target == null) 
		    return;

	    bool isVisible = false;
	    foreach (var visibleState in VisibleStates)
	    {
		    if (state == visibleState)
			    isVisible = true;
	    }
	    
	    Target.SetActive(isVisible);
    }

    protected void OnDestroy()
    {
	    menuFsm.OnStateChanged -= MenuFsmOnStateChanged;
    }
}
