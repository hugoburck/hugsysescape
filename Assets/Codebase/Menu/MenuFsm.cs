﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using UnityEngine;

public class MenuFsm : MonoBehaviour, IMenuFsm
{
	[SerializeField] private string stateName;
	public event Action<StateOfMenu> OnStateChanged;

	private StateOfMenu backingState = default;
	private IGameStateMachine gameStateMachine;
	private IMenuElementManager menuElementManager;
	
	public StateOfMenu State
	{
		get => backingState;
		private set
		{
			if (value != backingState)
			{
				backingState = value;
				SetName();
				OnStateChanged?.Invoke(backingState);
			}
		}
	}

	protected virtual void Awake()
	{
		gameStateMachine = FindObjectOfType<GameStateMachine>();
		menuElementManager = MenuElementManager.Instance;
	}

	protected virtual void Start()
	{
		SetName();
	}

    protected virtual void OnEnable()
    {
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
        menuElementManager.OnElementUsed += MenuElementManager_OnElementUsed;
    }

    private void MenuElementManager_OnElementUsed(ElementOfMenu element, float value)
    {
	    switch (element)
	    {
		    case ElementOfMenu.MenuMain:
			    State = StateOfMenu.Main;
			    break;
		    case ElementOfMenu.MenuOptions:
			    State = StateOfMenu.Options;
			    break;
			case ElementOfMenu.GameResume:
				State = StateOfMenu.StandBy;
				break;
	    }
    }

    private void GameStateMachine_OnStateChanged(StateOfGame stateOfGame)
    {
	    State = StateOfMenu.Idle;
	    if (stateOfGame == StateOfGame.Menu)
		    State = StateOfMenu.Main;

	    if (stateOfGame == StateOfGame.Game)
		    State = StateOfMenu.StandBy;

    }
    
    protected virtual void OnDisable()
    {
	    gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
	    menuElementManager.OnElementUsed -= MenuElementManager_OnElementUsed;
    }
    
    private void SetName() => stateName = backingState.ToString();
}
