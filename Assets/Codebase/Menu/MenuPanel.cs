﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour
{
	[SerializeField] private MenuToggle toggle = default;
	private List<GameObject> children;

	protected virtual void Awake()
	{
		SetupChildren();
	}

	private void SetupChildren()
	{
		var transforms = GetComponentsInChildren<Transform>();
		children = new List<GameObject>();
		foreach (var child in transforms)
		{
			if (child.gameObject != this.transform.gameObject)
				children.Add(child.gameObject);
		}
	}

	private void SetActiveChildren(bool state)
	{
		foreach (var child in children)
		{
			child.SetActive(state);
		}
	}

	private void OnEnable()
	{
		toggle.OnElementUsed += Toggle_OnElementUsed;
	}

	private void Toggle_OnElementUsed(ElementOfMenu element, float value)
	{
		SetActiveChildren(value != 0f);
	}
	
	private void OnDisable()
	{
		toggle.OnElementUsed -= Toggle_OnElementUsed;
	}
}
