﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class MenuSlider : MenuElement, IMenuElementStateFloat
{
	private Slider slider;

	public event Action<ElementOfMenu, float> OnStateChanged;
	public float State => slider.value;

	protected override void Awake()
	{
		base.Awake();
		slider = GetComponent<Slider>();
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		slider.onValueChanged.AddListener(Slider_OnValueChanged);
		slider.value = gameSettingsMediator.GetFloatState(element);
	}

	private void Slider_OnValueChanged(float range)
	{
		OnStateChanged?.Invoke(element,range);
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		slider.onValueChanged.RemoveListener(Slider_OnValueChanged);
	}
}
