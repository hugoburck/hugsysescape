﻿using System;
using System.Collections;
using Codebase.Application;
using Codebase.Ugui;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

[RequireComponent(typeof(UguiSelectableEventHandler))]
public class MenuToggle : MenuElement, IMenuElementStateBool
{
    [SerializeField] private bool isDefault = default;
    [SerializeField] private MenuToggleGroup group = default;
    [SerializeField] private bool state = default;
    private UguiSelectableEventHandler selectableEventHandler;
    private bool oneTime;

    public event Action<ElementOfMenu, bool> OnStateChanged;
    public bool State => state;


    protected override void Awake()
    {
        base.Awake();
        selectableEventHandler = GetComponent<UguiSelectableEventHandler>();
        selectableEventHandler.OnClick += SelectableEventHandler_OnClick;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        
        if (group != null)
            group.Register(this);
        
        group.OnToggleSelected += Group_OnToggleSelected;
        //oneTime = true;
        StartCoroutine(SetDefaultState());
    }

    private IEnumerator SetDefaultState()
    {
        yield return null;
        if (isDefault)
            SelectToggle();
    }

    private void Group_OnToggleSelected(MenuToggle toggle)
    {
        state = toggle == this;
        base.Invoke(element,state ? 1 : 0);
        OnStateChanged?.Invoke(element, state);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        
        group.OnToggleSelected -= Group_OnToggleSelected;
        if (group != null)
            group.Unregister(this);
    }

    private void Update()
    {
        // if (isDefault && oneTime)
        // {
        //     SelectToggle();
        //     oneTime = false;
        // }
    }

    private void SelectableEventHandler_OnClick()
    {
        SelectToggle();
    }

    private void SelectToggle()
    {
        group.Select(this);
    }
}

