﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuToggleGroup : MonoBehaviour
{
	public event Action<MenuToggle> OnToggleSelected;
	[SerializeField] private List<MenuToggle> toggles;

	public bool Register(MenuToggle toggle)
	{
		if (!toggles.Contains(toggle))
		{
			toggles.Add(toggle);
			return true;
		}
		
		return false;
	}
	
	public bool Unregister(MenuToggle toggle)
	{
		if (toggles.Contains(toggle))
		{
			toggles.Remove(toggle);
			return true;
		}
		
		return false;
	}

	public void Select(MenuToggle toggle)
	{
		if (toggle.State)
			return;
		
		OnToggleSelected?.Invoke(toggle);
	}
	
	protected virtual void Awake()
	{
		toggles = new List<MenuToggle>();
    }
}
