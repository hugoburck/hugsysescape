﻿public enum StateOfMenu
{
    Idle = 0,
    Main,
    Options,
    GameStart,
    GameContinue,
    GameQuitStart,
    GameQuitDesktop,
    StandBy
}