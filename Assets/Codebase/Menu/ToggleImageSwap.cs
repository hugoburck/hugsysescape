﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleImageSwap : MonoBehaviour
{
	[SerializeField] private GameObject on = default, off = default;
	private MenuElement element;
	private IMenuElementStateBool elementStateBool;
	
	protected virtual void Awake()
	{
		element = GetComponentInParent<MenuElement>();
		if (element is IMenuElementStateBool elementBool)
			elementStateBool = elementBool;
	}

	protected virtual void OnEnable()
	{
		elementStateBool.OnStateChanged += ElementStateBool_OnStateChanged;
		StartCoroutine(StateFromParent());
	}

	private void ElementStateBool_OnStateChanged(ElementOfMenu _, bool state)
	{
		ChildrenActivity(state);
	}

	private IEnumerator StateFromParent()
	{
		yield return null;
		yield return null;
		yield return null;
		ChildrenActivity(elementStateBool.State);
	}

	private void ChildrenActivity(bool state)
	{
		if (on != null)
			on.SetActive(state);
		
		if (off != null)
			off.SetActive(!state);
	}

	protected virtual void OnDisable()
	{
		elementStateBool.OnStateChanged -= ElementStateBool_OnStateChanged;
	}
}
