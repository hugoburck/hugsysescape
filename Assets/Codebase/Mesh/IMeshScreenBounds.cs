﻿using UnityEngine;

namespace Codebase.Mesh
{
    public interface IMeshScreenBounds
    {
        MeshFilter MeshFilter { get; set; }
        Vector2 Center(MeshFilter meshFilter);
        Vector2 Dimension(MeshFilter meshFilter);
    }
}