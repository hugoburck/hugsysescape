﻿using System;
using UnityEngine;

namespace Codebase.Mesh
{
    public class MeshScreenBounds : MonoBehaviour, IMeshScreenBounds
    {

        [SerializeField] 
        private WorldToScreenPoints worldToScreenPoints = default;
        private MeshWorldPoints meshWorldPoints;
        private Vector2 horiRange,vertRange,center,dimension;
        private Guid guid;

        public MeshScreenBounds()
        {
            meshWorldPoints = new MeshWorldPoints();
        }

        public MeshFilter MeshFilter { get; set; }

        public Vector2 Center(MeshFilter meshFilter)
        {
            UpdateOnNewFrame(meshFilter);
                return center;
    }

        public Vector2 Dimension(MeshFilter meshFilter)
        {
            UpdateOnNewFrame(meshFilter);
                return dimension;
        }
        
        private void UpdateOnNewFrame(MeshFilter meshFilter)
        {
            if (guid == FrameGuid.Current) 
                return;
            GetScreenBounds(worldToScreenPoints.ScreenPoints(meshWorldPoints.Points(meshFilter)));
            guid = FrameGuid.Current;
        }
        
        private void GetScreenBounds(Vector3[] screenPoints)
        {
            horiRange.x = Mathf.Infinity;
            horiRange.y = Mathf.NegativeInfinity;
            vertRange.x = Mathf.Infinity;
            vertRange.y = Mathf.NegativeInfinity;
        
            foreach (var screenPoint in screenPoints)
            {
                horiRange.x = screenPoint.x < horiRange.x ? screenPoint.x : horiRange.x;
                horiRange.y = screenPoint.x > horiRange.y ? screenPoint.x : horiRange.y;
                vertRange.x = screenPoint.y < vertRange.x ? screenPoint.y : vertRange.x;
                vertRange.y = screenPoint.y > vertRange.y ? screenPoint.y : vertRange.y;
            }

            dimension.x = horiRange.y - horiRange.x;
            dimension.y = vertRange.y - vertRange.x;

            center.x = horiRange.x + dimension.x / 2;
            center.y = vertRange.x + dimension.y / 2;
        }
    }
}