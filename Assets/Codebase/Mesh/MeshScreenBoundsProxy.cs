﻿using System;
using UnityEngine;

namespace Codebase.Mesh
{
    public class MeshScreenBoundsProxy : MonoBehaviour,IMeshScreenBounds
    {
        [SerializeField]
        public MeshScreenBounds meshScreenBounds;

        public MeshFilter MeshFilter
        {
            get => meshScreenBounds.MeshFilter;
            set => meshScreenBounds.MeshFilter = value;
        }

        public Vector2 Center(MeshFilter meshFilter)
        {
            return meshScreenBounds.Center(meshFilter);
        }

        public Vector2 Dimension(MeshFilter meshFilter)
        {
            return meshScreenBounds.Dimension(meshFilter);
        }

        private void Awake()
        {
            InitializationChecker.CheckImmediate(this,meshScreenBounds);
        }
    }
}