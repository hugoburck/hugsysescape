﻿using UnityEngine;

namespace Codebase.Mesh
{
    public class MeshWorldPoints
    {
        private Vector3[] points;

        public Vector3[] Points(MeshFilter meshFilter)
        {
            if (points == null || points.Length == 0)
                Recalculate(meshFilter);
            return points;
        }

        public void Recalculate(MeshFilter meshFilter)
        {
            UnityEngine.Mesh mesh = meshFilter.sharedMesh;
            Vector3[] vertices = mesh.vertices;
            int vertexCount = mesh.vertexCount;
        
            points = new Vector3[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                points[i] = meshFilter.transform.TransformPoint(vertices[i]);
            }   
        }
    }
}