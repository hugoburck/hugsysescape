﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Mesh;
using UnityEngine;

public class PositionToScreen : MonoBehaviour
{
    [SerializeField] private WorldToScreenPoints worldToScreenPoints = default;

    private void Awake()
    {
        worldToScreenPoints = worldToScreenPoints != null ? worldToScreenPoints : GetComponent<WorldToScreenPoints>();
    }

    public Vector2 Center(Transform transform)
    {
        return Center(transform.position);
    }

    public Vector2 Center(Vector3 position)
    {
        return worldToScreenPoints.ScreenPoint(position);
    }
}
