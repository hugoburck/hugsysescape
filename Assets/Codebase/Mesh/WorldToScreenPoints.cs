﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Codebase.Mesh
{
    public class WorldToScreenPoints : MonoBehaviour
    {
        [SerializeField] private Camera cam = default;
        private Vector3[] screenPoints;
        
        public Vector3[] ScreenPoints(Vector3[] worldPoints, Camera cam = null)
        {
            cam = cam == null ? this.cam : cam;
            screenPoints = new Vector3[worldPoints.Length];

            for (int i = 0; i < screenPoints.Length; i++)
            {
                screenPoints[i] = cam.WorldToScreenPoint(worldPoints[i]);
            }

            return screenPoints;
        }

        public Vector3 ScreenPoint(Vector3 worldPoint)
        {
            return cam.WorldToScreenPoint(worldPoint);
        }
    }
}