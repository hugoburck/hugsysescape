﻿public enum ActionOfPlayer
{
    Idle,
    WalkDepth,
    WalkStrafe,
    LookHeading,
    LookPitch,
    Sprint,
    Use,
    Crouch,
    Cancel,
    Inventory
}
