﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl;
using UnityEngine;

public class CameraAnimatedPlacement : MonoBehaviour
{
    public event Action<Vector3, Quaternion, float> OnPlacement;

    public void Place(Vector3 pos, Quaternion rot, float fov)
    {
        OnPlacement?.Invoke(pos,rot,fov);
    }
}
