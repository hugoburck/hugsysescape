﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Codebase.PlayerControl;

/// <summary>
/// Positions the camera from the Charactercontroller and crouchcontroller
/// </summary>
public class CameraCharacterPosition : MonoBehaviour
{
    public event Action<Vector3> OnPositionChanged;
    public Vector3 Position { get; private set; }

    private CharacterController characterController;
    private IPlayerStateMachine playerStateMachine;
    private IPlayerCharacter playerCharacter;

    private void Awake()
    {
        playerCharacter = PlayerCharacter.Instance;
        playerStateMachine = PlayerStateMachine.Instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        characterController = playerCharacter.CharacterController;
    }

    private void OnEnable()
    {
        playerCharacter.OnPositionChanged += PlayerCharacter_OnPositionChanged;
        
    }

    private void PlayerCharacter_OnPositionChanged(Vector3 characterPos)
    {
        if (playerStateMachine.State == StateOfPlayer.Exploring)
        {
            Position = SetWorldHeight(characterPos);
            OnPositionChanged?.Invoke(Position);
        }
    }

    private void OnDisable()
    {
        playerCharacter.OnPositionChanged -= PlayerCharacter_OnPositionChanged;
    }

    private Vector3 SetWorldHeight(Vector3 frompos)
    {
        Vector3 pos = frompos;
        pos.y += characterController.height;
        return pos;
    }

}
