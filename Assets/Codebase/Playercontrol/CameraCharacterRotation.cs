﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Codebase.Application;
using Codebase.InventoryControl;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;

[RequireComponent(typeof(Camera))]
public class CameraCharacterRotation : MonoBehaviour
{
    public Vector3 EulerRotation { get; private set; }
    public event Action<Vector3> OnRotation;
    private static CameraCharacterRotation rotcam;
    private IPlayerCameraControl playerCam;
    private IDeviceInput deviceInput;
    private IGameStateMachine gameStateMachine;
    private IPlayerStateMachine playerStateMachine;
    private IGameSettingsMediator gameSettingsMediator;

    public static CameraCharacterRotation AtMainCameraCharacterRotation
    {
        get
        {
            if (rotcam == null && Camera.main != null) 
                rotcam = Camera.main.GetComponent<CameraCharacterRotation>();
            if (rotcam == null) 
                throw new MissingComponentException("RotateCamera missing at Camera.main");
            
            return rotcam;
        }
    }

    public void SetEulerRotation(Vector3 angles)
    {
        EulerRotation = angles;
    }

    public Quaternion RotationWithoutPitch => Quaternion.Euler(new Vector3(0, EulerRotation.y, EulerRotation.z)); //has float inaccuracies according to test

    public Quaternion RotationWithPitch => Quaternion.Euler(EulerRotation); //has float inaccuracies according to test

    private Func<float,Vector3> lookPitchHandler;

    private void Awake()
    {
        gameStateMachine = FindObjectOfType<GameStateMachine>();
        playerStateMachine = FindObjectOfType<PlayerStateMachine>();
        playerCam = PlayerCamera.Instance;
        gameSettingsMediator = FindObjectOfType<GameSettingsMediator>();
    }

    // Start is called before the first frame update

    void Start()
    {
        CopyEulerFromPlayer();
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
        SetLookPitchResolve(gameSettingsMediator.GetBoolState(SettingOfGame.ControlInverseY));
    }

    private void SetLookPitchResolve(bool isInverse)
    {
        lookPitchHandler = isInverse ? (Func<float,Vector3>)LookPitchInverse : LookPitchRegular;
    }

    private Vector3 ClampedPitch(Vector3 eulerRotation)
    {
        float pitch = eulerRotation.x;
        pitch = Mathf.Clamp(pitch, -90, 90);
        eulerRotation.x = pitch;
        return eulerRotation;
    }

    public Vector3 Rotate(ActionOfPlayer actionOfPlayer, float axis, StateOfPlayer stateOfPlayer, bool isCursorLocked)
    {
        if (stateOfPlayer != StateOfPlayer.Exploring)
            return EulerRotation;

        if (isCursorLocked)
        {
            if (actionOfPlayer == ActionOfPlayer.LookHeading)
            {
                EulerRotation += Vector3.up * axis;
            }

            if (actionOfPlayer == ActionOfPlayer.LookPitch)
            {
                EulerRotation += lookPitchHandler(axis);
            }
        }
        
        return ClampedPitch(EulerRotation);
    }

    private Vector3 LookPitchRegular(float axis)
    {
        return Vector3.right * axis;
    }
    
    private Vector3 LookPitchInverse(float axis)
    {
        return Vector3.right * -axis;
    }

    private void OnEnable()
    {
        StartCoroutine(OnEnableLate());
        playerStateMachine.OnChanged += PlayerStateMachine_OnChanged;
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
        gameSettingsMediator.OnSettingBoolStateChanged += GameSettingsMediator_OnSettingBoolStateChanged;
    }

    private void GameSettingsMediator_OnSettingBoolStateChanged(SettingOfGame setting, bool value)
    {
        if (setting == SettingOfGame.ControlInverseY)
            SetLookPitchResolve(value);
    }

    private IEnumerator OnEnableLate()
    {
        yield return null;
        deviceInput.OnInputEvent += DeviceInput_OnInputEvent;
    }

    private void DeviceInput_OnInputEvent(ActionOfPlayer actionOfPlayer, float axis)
    {
        if (playerStateMachine.State != StateOfPlayer.Exploring)
            return;

        if (!(actionOfPlayer == ActionOfPlayer.LookHeading || actionOfPlayer == ActionOfPlayer.LookPitch))
            return;

        EulerRotation = Rotate(actionOfPlayer,axis,playerStateMachine.State, Cursor.lockState == CursorLockMode.Locked);
        OnRotation?.Invoke(EulerRotation);
    }

    private void GameStateMachine_OnStateChanged(StateOfGame stateMode)
    {
        // if (stateMode == GameStateMode.Game)
        //     CopyEulerFromPlayer();
    }

    private void PlayerStateMachine_OnChanged(StateOfPlayer state)
    {
        if (state == StateOfPlayer.Exploring)
            OnRotation?.Invoke(EulerRotation);
    }

    private void OnDisable()
    {
        deviceInput.OnInputEvent -= DeviceInput_OnInputEvent;
        playerStateMachine.OnChanged -= PlayerStateMachine_OnChanged;
        gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
        gameSettingsMediator.OnSettingBoolStateChanged -= GameSettingsMediator_OnSettingBoolStateChanged;
    }

    private void CopyEulerFromPlayer()
    {
        EulerRotation = playerCam.Camera.transform.rotation.eulerAngles;
        float x = EulerRotation.x;
        if (x > 90) 
            x -= 360;
        
        if (x < -90) 
            x += 360;

        Vector3 rot = EulerRotation;
        rot.x = x;
        EulerRotation = rot;
    }
}
