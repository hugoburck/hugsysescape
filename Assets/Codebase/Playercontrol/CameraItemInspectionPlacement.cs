﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl;
using UnityEngine;

public class CameraItemInspectionPlacement : MonoBehaviour
{
	private IPlayerStateMachine playerStateMachine;
	private ItemInspectionCam itemInspectionCam;
	private bool isActive;

	public Vector3 Position { get; private set; }
	public Quaternion Rotation { get; private set; }
	public float Fov { get; private set; }

	protected virtual void Awake()
	{
		playerStateMachine = PlayerStateMachine.Instance;
		itemInspectionCam = FindObjectOfType<ItemInspectionCam>();
	}

	protected virtual void OnEnable()
    {
		playerStateMachine.OnChanged += PlayerStateMachine_OnChanged;
    }

	private void PlayerStateMachine_OnChanged(StateOfPlayer state)
	{
		isActive = (state == StateOfPlayer.InspectingItem);
	}

	protected virtual void OnDisable()
	{
		playerStateMachine.OnChanged -= PlayerStateMachine_OnChanged;
	}

	protected virtual void Update()
	{
		if (!isActive)
			return;

		Position = itemInspectionCam.Position;
		Rotation = itemInspectionCam.Rotation;
		Fov = itemInspectionCam.Fov;
	}
}
