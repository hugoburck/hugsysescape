﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using UnityEngine;

public class CameraWorldInspectionPlacement : MonoBehaviour //TODO PlayerCamera doesn't switch correctly between this
{
	[SerializeField] private float seconds = 0.5f;
	[SerializeField] private AudioEffectPlayer _audioEffectPlayer = default;
	public event Action<bool> OnInspecting;
	private IPlayerCameraControl playerCameraControl;
	private IAudioEffectPlayer audioEffectPlayer;
	private Vector3 originPos;
	private Quaternion originRot;
	private float originFov;

	public Vector3 Position { get; private set; }
	public Quaternion Rotation { get; private set; }
	public float Fov { get; private set; }
	
	protected virtual void Awake()
	{
		playerCameraControl = PlayerCamera.Instance;

		if (_audioEffectPlayer != null)
			audioEffectPlayer = _audioEffectPlayer;
	}
    
    protected virtual void OnEnable()
    {
        Inspectable.OnInspectionChangedEvent += Inspectable_OnInspectionChangedEvent;
    }

    private void Inspectable_OnInspectionChangedEvent(Inspectable inspectable)
    {
	    if (Inspectable.IsFirstInspection)
	    {
		    StoreOrigin();
		    OnInspecting?.Invoke(true);
	    }

	    StopAllCoroutines();
        
	    if (inspectable != null)
	    {
		    StartCoroutine(LerpElements(
			    inspectable.InspectionView.CameraPosition, 
			    inspectable.InspectionView.CameraRotation, 
			    inspectable.InspectionView.CameraFOV));
	    }
	    else
	    {
		    StartCoroutine(LerpElements(originPos,originRot,originFov));
	    }

	    audioEffectPlayer?.Play();
    }

    protected void OnDisable()
    {
	    Inspectable.OnInspectionChangedEvent -= Inspectable_OnInspectionChangedEvent;
    }

    private IEnumerator LerpElements(Vector3 tarPos, Quaternion tarRot, float tarFov)
    {
	    float timer = 0;
	    Vector3 pos = playerCameraControl.Position;
	    Quaternion rot = playerCameraControl.Rotation;
	    float fov = playerCameraControl.Fov;
	    
	    while (timer < 1)
	    {
		    timer += Time.deltaTime / seconds;
		    Mathf.Clamp01(timer);
		    Position = Vector3.Lerp(pos, tarPos, timer.CosineDamper());
		    Rotation = Quaternion.Slerp(rot, tarRot, timer.CosineDamper());
		    Fov = Mathf.Lerp(fov, tarFov, timer.CosineDamper());
		    yield return null;
	    }
	    
	    if (Inspectable.IsNotInspecting)
		    OnInspecting?.Invoke(false);
    }

    private void StoreOrigin()
    {
	    originPos = Position = playerCameraControl.Position;
	    originRot = Rotation = playerCameraControl.Rotation;
	    originFov = Fov = playerCameraControl.Fov;
    }

    private void RestoreOrigin()
    {
	    Move(originPos);
	    Rotate(originRot);
	    SetFov(originFov);
    }

    private void Move(Vector3 pos)
    {
	    Position = pos;
    }

    private void Rotate(Quaternion rot)
    {
	    Rotation = rot;
    }

    private void SetFov(float fov)
    {
	    this.Fov = fov;
    }
}
