﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchCeilingCheck : MonoBehaviour, ICeilingCheck
{
    public LayerMask LayerMask;
    private const float thicknessPadding = 0.00f, heightOffset = 0.1f;


    private Vector3 CapsuleOffset(CharacterController controller)
    {
        return new Vector3(0, heightOffset + controller.radius,0);
    }

    private bool LowCeiling(float walkHeight, CharacterController controller)
    {
        Vector3 capsuleStart = controller.transform.position + CapsuleOffset(controller);
        Vector3 capsuleEnd = capsuleStart - CapsuleOffset(controller) + Vector3.up * walkHeight;
        return Physics.CheckCapsule(capsuleStart, capsuleEnd, controller.radius + thicknessPadding, LayerMask);
    }

    public bool HasEnoughHeadroom(float walkHeight, CharacterController controller)
    {
        return !LowCeiling(walkHeight, controller);
    }
}
