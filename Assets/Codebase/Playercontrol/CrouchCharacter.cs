﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;

//[RequireComponent(typeof(ICeilingCheck))]
public class CrouchCharacter : MonoBehaviour
{
    public event EventHandler<CrouchEventArgs> OnCrouchChanged;
    public float CrouchHeight = 0.5f;
    public float WalkHeight { get; private set; }
    private bool crouchRequest,crouching;
    private CharacterController controller;
    private CrouchEventArgs crouchEventArgs = new CrouchEventArgs();
    private ICeilingCheck ceilingCheck;
    private IDeviceInput deviceInput;
    private IPlayerStateMachine playerStateMachine;

    private readonly TriggerComparable<bool> triggerCrouching = new TriggerComparable<bool>();

    public CrouchCharacter Inject(ICeilingCheck ceilingCheck, CharacterController characterController)
    {
        this.ceilingCheck = ceilingCheck;
        this.controller = characterController;
        return this;
    }

    private void Awake()
    {
        ceilingCheck = GetComponent<ICeilingCheck>();
        playerStateMachine = FindObjectOfType<PlayerStateMachine>();
    }

    // Start is called before the first frame update
    void Start()
    {
        controller = PlayerCharacter.Instance.CharacterController;
        WalkHeight = controller.height;
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
    }

    // Update is called once per frame
    public void Update()
    {
        //Crouch();
    }

    private void Crouch()
    {
        if (playerStateMachine.State != StateOfPlayer.Exploring)
            return;

        if (crouchRequest)
        {
            crouchRequest = false;
            crouching = true;
        }
        else if (ceilingCheck.HasEnoughHeadroom(WalkHeight, controller))
        {
            crouching = false;
        }

        if (triggerCrouching.OnChange(crouching))
        {
            if (crouching)
                OnCrouchChanged?.Invoke(this, SetGetCrouchEventArgs(CrouchHeight));
            else
                OnCrouchChanged?.Invoke(this, SetGetCrouchEventArgs(WalkHeight));
        }
    }

    private void OnEnable()
    {
        StartCoroutine(SubServices());
    }

    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnEnterInputEvent += DeviceInput_OnEnterInputEvent;
    }

    private void OnDisable()
    {
        deviceInput.OnEnterInputEvent -= DeviceInput_OnEnterInputEvent;
    }

    private CrouchEventArgs SetGetCrouchEventArgs(float height)
    {
        crouchEventArgs.IsCrouching = crouching;
        crouchEventArgs.ControllerHeight = height;
        Vector3 center = controller.center;
        center.y = height / 2;
        crouchEventArgs.ControllerCenter = center;
        return crouchEventArgs;
    }

    public void DeviceInput_OnEnterInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (actionOfPlayer == ActionOfPlayer.Crouch && playerStateMachine.State == StateOfPlayer.Exploring)
        {
            crouchRequest = !crouching;
            Crouch();
        }
    }

}

public class CrouchEventArgs : EventArgs
{
    public bool IsCrouching { get; set; }
    public float ControllerHeight { get; set; }
    public Vector3 ControllerCenter { get; set; }
}