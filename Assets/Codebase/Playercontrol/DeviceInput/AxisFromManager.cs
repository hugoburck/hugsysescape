﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisFromManager : IAxisFromInput
{
    public float GetAxis(string axisName)
    {
        return Input.GetAxis(axisName);
    }
}
