﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Codebase.PlayerControl.DeviceInput;

public class DeviceInput : MonoBehaviourSingleton<DeviceInput>, IDeviceInput
{
    //public delegate void inputHandler(PlayerAction playerAction, float intensity);
    public event Action<ActionOfPlayer, float> OnInputEvent, OnEnterInputEvent, OnExitInputEvent; //might make additional specialized events
    public DeviceOfControl LastUsed { get; private set; }

    private IAxisFromInput axisFromInput;
    private readonly InputHandler[] inputHandlers;

    private readonly Dictionary<ActionOfPlayer, string> actionInputPairs = new Dictionary<ActionOfPlayer, string>
    {
        { ActionOfPlayer.WalkDepth, "MoveDepth" },
        { ActionOfPlayer.WalkStrafe, "MoveStrafe" },
        { ActionOfPlayer.LookHeading, "LookHeading" },
        { ActionOfPlayer.LookPitch, "LookPitch" },
        { ActionOfPlayer.Sprint, "Sprint" },
        { ActionOfPlayer.Crouch, "Crouch" },
        { ActionOfPlayer.Use, "Use" },
        { ActionOfPlayer.Cancel, "Cancel" },
        { ActionOfPlayer.Inventory, "Inventory" }
    };

    private const string keyboardMouseSuffix = "", gamePadSuffix = "Pad"; //KeyboardMouse is default in InputManager, no suffix. Maybe later

    public DeviceInput()
    {
        axisFromInput = new AxisFromManager();
        inputHandlers = new InputHandler[actionInputPairs.Count];
        for (int i = 0; i < inputHandlers.Length; i++)
        {
            inputHandlers[i] = new InputHandler(actionInputPairs.ElementAt(i).Value, actionInputPairs.ElementAt(i).Key, this);
        }
    }
    
    public void Inject(IAxisFromInput axisFromInput)
    {
        this.axisFromInput = axisFromInput;
    }

    public string ActionInputName(ActionOfPlayer actionOfPlayer)
    {
        if (actionInputPairs.TryGetValue(actionOfPlayer, out string name))
            return name;
        Debug.LogError("PlayerAction not found");
        return string.Empty;
    }

    private class InputHandler
    {
        private string nameInManager;
        private DeviceInput deviceInputContainer;

        public InputHandler(string nameInManager, ActionOfPlayer actionOfPlayer, DeviceInput deviceInput)
        {
            this.nameInManager = nameInManager;
            ActionOfPlayer = actionOfPlayer;
            deviceInputContainer = deviceInput;
        }

        public ActionOfPlayer ActionOfPlayer { get; private set; }
        public float CurrentAxis { get; private set; }
        public bool IsCurrentZero { get => CurrentAxis == 0; }
        private float StoredAxis { get; set; }
        private bool IsStoredZero { get => StoredAxis == 0; }
        public bool OnCurrentZero { get => !IsStoredZero && IsCurrentZero; }
        public bool OnCurrentNotZero { get => IsStoredZero && !IsCurrentZero; }
        public bool OnCurrentChanged { get => CurrentAxis != StoredAxis; }

        public void EvaluateAxis()
        {
            StoredAxis = CurrentAxis;
            CurrentAxis = MostActiveAxis(nameInManager);
        }

        public void StoreAxis()
        {
            StoredAxis = CurrentAxis;
        }

        private float MostActiveAxis(string inputName)
        {
            float axisKBM = deviceInputContainer.axisFromInput.GetAxis(KBMName(inputName));
            float axisPad = deviceInputContainer.axisFromInput.GetAxis(PadName(inputName));
            if (axisKBM == 0 && axisPad == 0) return 0;

            float axisKBMSquared = Mathf.Pow(axisKBM, 2f);
            float axisPadSquared = Mathf.Pow(axisPad, 2f);
            if (axisKBMSquared >= axisPadSquared)
            {
                deviceInputContainer.LastUsed = DeviceOfControl.KeyboardMouse;
                return axisKBM;
            }
            else
            {
                deviceInputContainer.LastUsed = DeviceOfControl.GamePad;
                return axisPad;
            }
        }

        private string PadName(string inputName)
        {
            return inputName + gamePadSuffix;
        }

        private string KBMName(string inputName)
        {
            return inputName + keyboardMouseSuffix;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        ServiceLocator.Instance.Register<IDeviceInput>(this);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var input in inputHandlers)
        {
            input.EvaluateAxis();
            if (!input.IsCurrentZero) OnInputEvent?.Invoke(input.ActionOfPlayer, input.CurrentAxis);
            if (input.OnCurrentNotZero) OnEnterInputEvent?.Invoke(input.ActionOfPlayer, input.CurrentAxis);
            if (input.OnCurrentZero) OnExitInputEvent?.Invoke(input.ActionOfPlayer, input.CurrentAxis);
        }
    }
}
