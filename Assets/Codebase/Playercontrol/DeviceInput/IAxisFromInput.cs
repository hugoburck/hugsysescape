﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAxisFromInput
{
    float GetAxis(string axisName);
}
