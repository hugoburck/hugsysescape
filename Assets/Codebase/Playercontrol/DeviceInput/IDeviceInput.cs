﻿using System;

namespace Codebase.PlayerControl.DeviceInput
{
    public interface IDeviceInput
    {
        event Action<ActionOfPlayer, float> OnInputEvent, OnEnterInputEvent, OnExitInputEvent;
        DeviceOfControl LastUsed { get; }
    }
}