﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IModifySpeed
{
    Vector3 Modify(Vector3 movement);
}
