﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerCameraControl
{
    Camera Camera { get; }
    Vector3 Position { get; }
    Vector3 PositionCharacter { get; }
    Quaternion Rotation { get; }
    float Fov { get; }
}
