﻿using System;
using UnityEngine;

namespace Codebase.PlayerControl
{
    public interface IPlayerCharacter
    {
        event Action<Vector3> OnPositionChanged;
        CharacterController CharacterController { get; }
        void MoveAbsolute(Vector3 position);
        Vector3 Position { get; }
    }
}