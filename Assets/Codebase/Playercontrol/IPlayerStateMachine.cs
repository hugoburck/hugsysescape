﻿using System;

namespace Codebase.PlayerControl
{
    public interface IPlayerStateMachine
    {
        StateOfPlayer State { get;  }
        event Action<StateOfPlayer> OnSet,OnChanged;
        StateOfPlayer PreviousChangedState { get; }
        StateOfPlayer PreviousSetState { get; }
    }
}