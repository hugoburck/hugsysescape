﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOverMaster : MonoBehaviour
{
    private List<MouseOverSlave> slaves;

    public void AddSlave(MouseOverSlave slave)
    {
        if (slaves == null)
            slaves = new List<MouseOverSlave>();
        if (slaves.Contains(slave))
            return;
        slaves.Add(slave);
    }

    public void RemoveSlave(MouseOverSlave slave)
    {
        if (!slaves.Contains(slave))
            return;
        slaves.Remove(slave);
        if (slaves.Count == 0)
            slaves = null;
    }

    public bool IsMouseOver
    {
        get
        {
            if (slaves == null) 
                return false;
            foreach (var slave  in slaves)
            {
                if (slave.IsMouseOver)
                    return true;
            }
            return false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
