﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class MouseOverSlave : MonoBehaviour
{
    public bool IsMouseOver { get; private set; }

    private void Register(MouseOverMaster master)
    {
        if (master != null)
            master.AddSlave(this);
    }

    private void Register()
    {
        var master = GetComponent<MouseOverMaster>();
        if (master == null) 
            master = GetComponentInParent<MouseOverMaster>();
        if (master != null)
            master.AddSlave(this);
        else
            Debug.Log("No master found", this);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        Collider collider = GetComponent<Collider>();
        if (collider == null)
            Debug.Log("No Collider found here", this);
        Register();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter()
    {
        IsMouseOver = true;
    }

    private void OnMouseExit()
    {
        IsMouseOver = false;
    }
}
