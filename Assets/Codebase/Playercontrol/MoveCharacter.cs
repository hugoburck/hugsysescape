﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;

public class MoveCharacter : MonoBehaviour
{
    public bool IgnorePitch;
    public event Action<Vector3> OnMovement;
    private static List<IModifySpeed> speedModifiers = new List<IModifySpeed>();
    private IDeviceInput deviceInput;
    private IPlayerStateMachine playerStateMachine;

    public static bool AddSpeedModifier(IModifySpeed modifier)
    {
        if (speedModifiers.Contains(modifier)) return false;
        speedModifiers.Add(modifier);
        return true;
    }

    public Vector3 Movement { get; private set; }

    public static bool RemoveSpeedModifier(IModifySpeed modifier)
    {
        if (!speedModifiers.Contains(modifier)) return false;
        speedModifiers.Remove(modifier);
        return true;
    }

    private Vector3 ApplyModifiers(Vector3 originalMovement)
    {
        Vector3 newMovement = originalMovement;
        foreach (var modifier in speedModifiers)
        {
            newMovement = modifier.Modify(newMovement);
        }
        return newMovement;
    }

    private GameObject _copy;
    private GameObject CopyGameObject(GameObject original) //promote to helper class
    {
        if (_copy != null) return _copy;
        _copy = new GameObject("CameraCopy");
        _copy.transform.parent = this.transform.parent;
        TransformClone.CloneLocal(original.transform, _copy.transform);
        return _copy;
    }

    private void DeviceInput_OnInputEvent(ActionOfPlayer actionOfPlayer,float axis) //makes a pitchless copy of the camera, could be better
    {
        if (playerStateMachine.State != StateOfPlayer.Exploring) 
            return;
        
        Movement = Move(actionOfPlayer,axis, playerStateMachine.State);
        if (Movement != Vector3.zero) 
            OnMovement?.Invoke(ApplyModifiers(Movement));
    }

    public Vector3 Move(ActionOfPlayer actionOfPlayer, float axis, StateOfPlayer stateOfPlayer) //TODO fix sloppy pitchless rotation logic
    {
        Movement = Vector3.zero;
        if (stateOfPlayer != StateOfPlayer.Exploring)
            return Movement;

        GameObject game = CopyGameObject(CameraCharacterRotation.AtMainCameraCharacterRotation.gameObject);
        game.transform.rotation = IgnorePitch ? CameraCharacterRotation.AtMainCameraCharacterRotation.RotationWithoutPitch : CameraCharacterRotation.AtMainCameraCharacterRotation.RotationWithPitch;

        Transform camTrans = game.transform;

        if (IgnorePitch) camTrans.rotation = CameraCharacterRotation.AtMainCameraCharacterRotation.RotationWithoutPitch;

        if (actionOfPlayer == ActionOfPlayer.WalkDepth)
        {
            Movement += camTrans.TransformDirection(Vector3.forward * axis);
        }

        if (actionOfPlayer == ActionOfPlayer.WalkStrafe)
        {
            Movement += camTrans.TransformDirection(Vector3.right * axis);
        }
        return Movement;
    }

    private void Awake()
    {
        playerStateMachine = FindObjectOfType<PlayerStateMachine>();
    }

    private void Start()
    {
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
    }

    private void OnEnable()
    {
        StartCoroutine(SubServices());
    }

    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnInputEvent += DeviceInput_OnInputEvent;
    }

    private void OnDisable()
    {
        deviceInput.OnInputEvent -= DeviceInput_OnInputEvent;
    }
}
