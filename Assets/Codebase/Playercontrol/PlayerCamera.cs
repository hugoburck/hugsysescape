﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Codebase.PlayerControl;

[RequireComponent(typeof(Camera))]
public class PlayerCamera : MonoBehaviourSingleton<PlayerCamera>, IPlayerCameraControl
{
    [SerializeField] private CameraCharacterRotation cameraCharacterRotation = default;
    [SerializeField] private CameraCharacterPosition cameraCharacterPosition = default;
    [SerializeField] private CameraAnimatedPlacement cameraAnimatedPlacement = default;
    [SerializeField] private CameraWorldInspectionPlacement cameraWorldInspectionPlacement = default;
    [SerializeField] private CameraItemInspectionPlacement cameraItemInspectionPlacement = default;

    public float DefaultFOV = 60f;

    private Vector3 previousPos;
    private Quaternion previousRotation;
    private float previousFOV;
    private IPlayerStateMachine playerStateMachine;
    private IEnumerator fromWorldInspection, fromItemInspection;

    public Camera Camera { get; private set; }

    public Vector3 Position => transform.position;
    public Vector3 PositionCharacter => cameraCharacterPosition.Position;

    public Quaternion Rotation => transform.rotation;

    public float Fov => Camera.fieldOfView;

    private void SetFOV(float fov)
    {
        Camera.fieldOfView = fov;
    }

    public void Rotate(Quaternion rotation)
    {
        Camera.transform.rotation = rotation;
    }

    private void Move(Vector3 position)
    {
        transform.position = position;
    }

    private void Awake()
    {
        Camera = GetComponent<Camera>();
        playerStateMachine = FindObjectOfType<PlayerStateMachine>();
        fromWorldInspection = GetFromWorldInspection();
        fromItemInspection = GetFromItemInspection();
        InitializationChecker.CheckImmediate(this, Camera, cameraCharacterRotation, cameraCharacterPosition, cameraAnimatedPlacement);

    }

    private void Start()
    {
        Camera.fieldOfView = DefaultFOV;
    }

    private void OnEnable()
    {
        playerStateMachine.OnChanged += PlayerStateMachine_OnChanged;
        PlayerStateMachine_OnChanged(playerStateMachine.State);
    }

    private void PlayerStateMachine_OnChanged(StateOfPlayer state)
    {
        if (state == StateOfPlayer.Exploring)
        {
            Move(cameraCharacterPosition.Position);
            Rotate(cameraCharacterRotation.RotationWithPitch);
            SetFOV(DefaultFOV);
            cameraCharacterRotation.OnRotation += CameraCharacterRotation_OnRotation;
            cameraCharacterPosition.OnPositionChanged += CameraCharacterPosition_OnPositionChanged;
        }
        else
        {
            cameraCharacterRotation.OnRotation -= CameraCharacterRotation_OnRotation;
            cameraCharacterPosition.OnPositionChanged -= CameraCharacterPosition_OnPositionChanged;
        }

        if (state == StateOfPlayer.Animating)
        {
            cameraAnimatedPlacement.OnPlacement += CameraAnimatedPlacement_OnPlacement;
        }
        else
        {
            cameraAnimatedPlacement.OnPlacement -= CameraAnimatedPlacement_OnPlacement;
        }
        
        if (state == StateOfPlayer.InspectingPuzzle)
            StartCoroutine(fromWorldInspection);
        else StopCoroutine(fromWorldInspection);
        
        if (state == StateOfPlayer.InspectingItem)
            StartCoroutine(fromItemInspection);
        else StopCoroutine(fromItemInspection);
            
    }

    private void OnDisable()
    {
        playerStateMachine.OnChanged -= PlayerStateMachine_OnChanged;
    }

    private void CameraAnimatedPlacement_OnPlacement(Vector3 pos, Quaternion rot, float fov)
    {
        SetFOV(fov);
        Rotate(rot);
        Move(pos);
    }

    private void CameraCharacterPosition_OnPositionChanged(Vector3 pos)
    {
        Move(pos);
    }

    private void CameraCharacterRotation_OnRotation(Vector3 eulerAngles)
    {
        Rotate(Quaternion.Euler(eulerAngles));
    }

    private IEnumerator GetFromWorldInspection()
    {
        while (true)
        {
            Move(cameraWorldInspectionPlacement.Position);
            Rotate(cameraWorldInspectionPlacement.Rotation);
            SetFOV(cameraWorldInspectionPlacement.Fov);
            yield return null;
        }
    }
    
    private IEnumerator GetFromItemInspection()
    {
        while (true)
        {
            Move(cameraItemInspectionPlacement.Position);
            Rotate(cameraItemInspectionPlacement.Rotation);
            SetFOV(cameraItemInspectionPlacement.Fov);
            yield return null;
        }
    }
}
