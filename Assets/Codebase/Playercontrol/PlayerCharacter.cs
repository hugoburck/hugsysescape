﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Codebase.PlayerControl;

[RequireComponent(typeof(MoveCharacter))]
[RequireComponent(typeof(CharacterController))]
public class PlayerCharacter : MonoBehaviourSingleton<PlayerCharacter>, IPlayerCharacter
{
    public event Action<Vector3> OnPositionChanged;
    public CharacterController CharacterController { get; private set; }
    [SerializeField]
    private MoveCharacter moveCharacter = default;
    [SerializeField]
    private CrouchCharacter crouchCharacter = default;

    private IPlayerCameraControl playerCamera;
    private IPlayerStateMachine playerStateMachine;

    private bool moveLock;

    public void MoveAbsolute(Vector3 position)
    {
        this.transform.position = position;
        StartCoroutine(BlockControllerInput());
        OnPositionChanged?.Invoke(transform.position);
    }

    public Vector3 Position => transform.position;

    //Teleporting doesn't work if the CharacterController gets moved
    private IEnumerator BlockControllerInput()
    {
        moveLock = true;
        yield return null;
        moveLock = false;
    }

    private void Awake()
    {
        if (CharacterController == null) 
            CharacterController = GetComponent<CharacterController>();

        playerCamera = PlayerCamera.Instance;
        playerStateMachine = PlayerStateMachine.Instance;
        
        if (moveCharacter == null)
            moveCharacter = GetComponent<MoveCharacter>();
        
        if (crouchCharacter == null)
            crouchCharacter = GetComponent<CrouchCharacter>();
        
        InitializationChecker.CheckImmediate(this, CharacterController, moveCharacter, crouchCharacter, playerCamera as PlayerCamera);
    }

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        playerStateMachine.OnChanged += PlayerStateMachine_OnChanged;
        PlayerStateMachine_OnChanged(playerStateMachine.State);
    }

    private void PlayerStateMachine_OnChanged(StateOfPlayer state)
    {
        if (state == StateOfPlayer.Exploring)
        {        
            moveCharacter.OnMovement += MoveCharacter_OnMovement;
            crouchCharacter.OnCrouchChanged += CrouchCharacter_OnCrouchChanged;
            MoveCharacter_OnMovement(Vector3.zero);
        }
        else
        {
            moveCharacter.OnMovement -= MoveCharacter_OnMovement;
            crouchCharacter.OnCrouchChanged -= CrouchCharacter_OnCrouchChanged;
        }
    }

    private void OnDisable()
    {
        playerStateMachine.OnChanged -= PlayerStateMachine_OnChanged;
    }

    private void MoveCharacter_OnMovement(Vector3 movement)
    {
        if (moveLock)
            return;
        
        CharacterController.SimpleMove(movement);
        OnPositionChanged?.Invoke(transform.position);
    }

    private void CrouchCharacter_OnCrouchChanged(object sender, CrouchEventArgs e)
    {
        CharacterController.height = e.ControllerHeight;
        CharacterController.center = e.ControllerCenter;
        OnPositionChanged?.Invoke(transform.position);
    }

}
