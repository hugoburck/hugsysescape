﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.PlayerControl;
using UnityEngine;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

public class PlayerStateMachine : MonoBehaviourSingleton<PlayerStateMachine>, IPlayerStateMachine
{
    public StateOfPlayer State { get; private set; }
    public event Action<StateOfPlayer> OnSet,OnChanged;
    private StateOfPlayer old, oldChanged;
    private CameraWorldInspectionPlacement cameraWorldInspectionPlacement;
    private IShotAnimationStateMachine shotAnimationStateMachine;
    private IGameStateMachine gameStateMachine;
    [SerializeField] private StateOfPlayer backing = default;

    public StateOfPlayer PreviousChangedState => oldChanged;
    public StateOfPlayer PreviousSetState => old;

    private void Set(StateOfPlayer state)
    {
        old = State;
        
        if (state != old)
        {
            oldChanged = State;
            State = state;
            OnChanged?.Invoke(state);
        }
        else
        {
            State = state;
            OnSet?.Invoke(state);
        }
        
        this.backing = state;
    }

    private void Awake()
    {
        ServiceLocator.Instance.Register<IPlayerStateMachine>(this);
        cameraWorldInspectionPlacement = FindObjectOfType<CameraWorldInspectionPlacement>();
        shotAnimationStateMachine = FindObjectOfType<ShotAnimationStateMachine>();
        gameStateMachine = FindObjectOfType<GameStateMachine>();
    }

    private void OnEnable()
    {
        cameraWorldInspectionPlacement.OnInspecting += CameraWorldInspectionPlacementOnOnInspecting;
        InventoryItemViewer.OnEnterInspection += InventoryItemViewer_OnEnterInspection;
        InventoryItemViewer.OnExitInspection += InventoryItemViewer_OnExitInspection;
        shotAnimationStateMachine.OnStateChanged += ShotAnimationStateMachine_OnStateChanged;
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
    }

    private void ShotAnimationStateMachine_OnStateChanged(StateOfShotAnimation state)
    {
        if (state == StateOfShotAnimation.Idle)
            Set(StateOfPlayer.Exploring);
        else Set(StateOfPlayer.Animating);
    }

    private void GameStateMachine_OnStateChanged(StateOfGame state)
    {
        if (state != StateOfGame.Game)
            Set(StateOfPlayer.Idle);
        else Set(StateOfPlayer.Exploring);
    }

    private void CameraWorldInspectionPlacementOnOnInspecting(bool isInspecting)
    {
        Set(isInspecting ? StateOfPlayer.InspectingPuzzle : StateOfPlayer.Exploring);
    }

    private void InventoryItemViewer_OnExitInspection()
    {
        Set(Inspectable.IsNotInspecting ? StateOfPlayer.Exploring : StateOfPlayer.InspectingPuzzle);
    }

    private void InventoryItemViewer_OnEnterInspection(InventoryItemData itemData)
    {
        Set(StateOfPlayer.InspectingItem);
    }

    private void OnDisable()
    {
        cameraWorldInspectionPlacement.OnInspecting -= CameraWorldInspectionPlacementOnOnInspecting;
        InventoryItemViewer.OnEnterInspection -= InventoryItemViewer_OnEnterInspection;
        InventoryItemViewer.OnExitInspection -= InventoryItemViewer_OnExitInspection;
        gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
    }
}
