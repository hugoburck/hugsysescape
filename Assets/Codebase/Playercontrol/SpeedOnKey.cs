﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

public class SpeedOnKey : MonoBehaviour , IModifySpeed
{
    private float axis;
    private IDeviceInput deviceInput;
    public Vector3 Modify(Vector3 movement)
    {
        if (axis == 0) return movement;
        movement *= axis + 1;
        axis = 0;
        return movement;
    }

    // Start is called before the first frame update
    void Start()
    {
        MoveCharacter.AddSpeedModifier(this);
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GetInput(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (actionOfPlayer == ActionOfPlayer.Sprint) axis = intensity;
    }

    private void OnEnable()
    {
        StartCoroutine(SubServices());
    }

    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnInputEvent += GetInput;
    }

    private void OnDisable()
    {
        deviceInput.OnInputEvent -= GetInput;
    }
}
