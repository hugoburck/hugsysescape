﻿public enum StateOfPlayer
{
    Idle,
    Exploring,
    InspectingPuzzle,
    InspectingItem,
    Animating,
    OpenMenu
}

