﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportOnSceneLoad : MonoBehaviour
{
    [SerializeField] private TeleportPlayer teleportPlayer = default;
    
    // Start is called before the first frame update
    void Awake()
    {
        InitializationChecker.CheckImmediate(this,teleportPlayer);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SceneManagerOnsceneLoaded;
    }

    private void SceneManagerOnsceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (HasSceneTeleport(scene, out GameObject gobj))
            teleportPlayer.Teleport(gobj.transform.position,gobj.transform.rotation);
    }

    private bool HasSceneTeleport(Scene scene, out GameObject gobj)
    {
        gobj = null;
        foreach (var rootGobj in scene.GetRootGameObjects())
        {
            if (rootGobj.CompareTag("Teleport"))
            {
                gobj = rootGobj;
                return true;
            }
        }

        return false;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManagerOnsceneLoaded;
    }
}
