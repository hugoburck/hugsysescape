﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlayer : MonoBehaviour
{
    [SerializeField] private PlayerCharacter playerCharacter = default;
    [SerializeField] private PlayerCamera playerCamera = default;
    [SerializeField] private CameraCharacterRotation cameraCharacterRotation = default;

    private void Awake()
    {
        if (playerCharacter == null)
            playerCharacter = PlayerCharacter.Instance;

        if (playerCamera == null)
            playerCamera = PlayerCamera.Instance;

        InitializationChecker.CheckImmediate(this, playerCamera, playerCharacter, cameraCharacterRotation);
    }

    private void Start()
    {
        
    }

    public void Teleport(Vector3 position, Quaternion rotation)
    {
        playerCharacter.MoveAbsolute(position);
        playerCamera.Rotate(rotation);
        cameraCharacterRotation.SetEulerRotation(rotation.eulerAngles);
        Debug.Log(position);
    }

    public void Teleport(Transform transform)
    {
        Teleport(transform.position,transform.rotation);
    }
}
