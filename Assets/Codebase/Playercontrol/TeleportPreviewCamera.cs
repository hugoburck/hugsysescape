﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[ExecuteInEditMode]
public class TeleportPreviewCamera : MonoBehaviour
{
    //TODO: preview camera needs to be ready before the scene load call is made
    //I might need to build a generic behaviour enabler. This class is very specific
    
    [SerializeField] private Camera cam = default;

    private void Awake()
    {
        if (cam == null)
            cam = GetComponent<Camera>();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        cam.enabled = !Application.isPlaying;
    }
}
