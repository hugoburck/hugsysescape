﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TeleportSceneTarget : MonoBehaviour
{
    //You can get this component or get the tag this component sets.
    //I don't know which method is faster.
    //I might need to build a TagManager like the LayerManager.
    
    private const string tagName = "Teleport";
    
    void Awake()
    {
        if (!Application.isPlaying)
            gameObject.tag = tagName;
    }
}
