﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.Serialization;

/// <summary>
/// Turns on/off settings in PostProcessingProfiles.
/// </summary>
/// <remarks>
/// Warning: Settings that are disabled at start will be enabled again when checked. Only submit effects when you want them to be used.
/// (no inactive overrides)
/// </remarks>

public class PostProcesSettingsManager : MonoBehaviour
{
	[SerializeField] private List<PostProcessProfile> profiles = default;
	[SerializeField] private List<PostProcessProfile> profileOriginals = default;
	private IGameSettingsMediator gameSettingsMediator;
	
	private Dictionary<SettingOfGame, Action<PostProcessProfile, bool>> lookUpActionBySetting = 
		new Dictionary<SettingOfGame, Action<PostProcessProfile, bool>>
		{
			{SettingOfGame.GraphicsSSAO, SetSettingActive<AmbientOcclusion>},
			{SettingOfGame.GraphicsCA, SetSettingActive<ChromaticAberration>},
			{SettingOfGame.GraphicsVignette, SetSettingActive<Vignette>},
			{SettingOfGame.GraphicsSSR, SetSettingActive<ScreenSpaceReflections>},
			{SettingOfGame.GraphicsBloom, SetSettingActive<Bloom>}
		};
	
	protected virtual void Awake()
	{
		CloneProfiles();
		gameSettingsMediator = FindObjectOfType<GameSettingsMediator>();
	}

	private void CloneProfiles() //Used to store boot settings. Unused.
	{
		profileOriginals = new List<PostProcessProfile>();
		foreach (var profile in profiles)
		{
			profileOriginals.Add(Instantiate(profile));
		}
	}

	protected void OnEnable()
	{
		gameSettingsMediator.OnSettingBoolStateChanged += GameSettingsMediator_OnSettingBoolStateChanged;
		SetProfileSetting(SettingOfGame.GraphicsSSAO);
	}

	private static void SetSettingActive<T>(PostProcessProfile profile, bool state) where T : PostProcessEffectSettings
	{
		if (profile.HasSettings<T>())
			profile.GetSetting<T>().active = state;
	}

	private void SetProfileSetting(SettingOfGame setting)
	{
		GameSettingsMediator_OnSettingBoolStateChanged(setting,
			gameSettingsMediator.GetBoolState(setting));
	}

	private void GameSettingsMediator_OnSettingBoolStateChanged(SettingOfGame setting, bool value)
	{
		if (profiles == null || profiles.Count == 0)
			return;
		
		foreach (var profile in profiles)
		{
			if (profile == null)
				continue;

			if (lookUpActionBySetting.ContainsKey(setting))
			{
				lookUpActionBySetting[setting].Invoke(profile,value);
			}
		}
	}

	protected void OnDisable()
	{
		gameSettingsMediator.OnSettingBoolStateChanged -= GameSettingsMediator_OnSettingBoolStateChanged;
	}
}
