﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

public class AccessArea : MonoBehaviour, IAccessArea
{
    [Tooltip("Leave null to assign this GameObject")]
    [SerializeField] private GameObject Area = default;
    private const UnitOfLayer layer = UnitOfLayer.Raycast;
    private IInteractAreaRay interactAreaRay;
    private string access;

    public bool IsWithin { get; private set; }

    void Awake()
    {
        if (Area == null)
            Area = this.gameObject;

        interactAreaRay = InteractAreaRay.Instance;
    }

    // Start is called before the first frame update

    void Start()
    {
        this.gameObject.layer = LayerMaskManager.GetLayer(layer);
        if (Area.GetComponent<Collider>() ==  null)
            Debug.LogError("Area Collider required");
    }

    // Update is called once per frame

    void Update()
    {
        IsWithin = interactAreaRay.IsHit(Area.gameObject);
        access = (IsWithin ? AccessOfInteractable.Available : AccessOfInteractable.Unavailable).ToString();
    }
}
