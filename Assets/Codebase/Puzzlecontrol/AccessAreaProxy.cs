﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

public class AccessAreaProxy : MonoBehaviour,IAccessArea
{
    [SerializeField] private AccessArea accessArea = default;

    private void Awake()
    {
        InitializationChecker.CheckImmediate(accessArea);
    }

    public bool IsWithin
    {
        get
        {
            if (accessArea == null) 
                return false;
            return accessArea.IsWithin;
        }
    }
}
