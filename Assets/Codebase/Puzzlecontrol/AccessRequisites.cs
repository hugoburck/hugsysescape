﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

public class AccessRequisites : MonoBehaviour, IAccessRequisites
{
    [SerializeField] 
    private PuzzleToState unlockOn = default;
    [SerializeField]
    private PuzzleToState relockOn = default;

    [SerializeField]
    private AccessOfInteractable access;
    

    public bool IsMeeting 
    {
        get
        {
            bool meeting = (unlockOn == null || unlockOn.State == StateOfPuzzlePiece.Correct)  && 
                           (relockOn == null || relockOn.State == StateOfPuzzlePiece.Incorrect);
            access = meeting ? AccessOfInteractable.Available : AccessOfInteractable.Unavailable;
            return meeting;
        }
    }
}
