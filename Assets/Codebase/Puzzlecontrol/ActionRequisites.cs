﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

public class ActionRequisites : MonoBehaviour,IActionRequisites
{
    [SerializeField] 
    private PuzzleToState unlockOn = default;


    public bool IsMeeting
    {
        get
        {
            if (unlockOn != null) 
                return unlockOn.State == StateOfPuzzlePiece.Correct;
            
            Debug.Log("Unlock has no reference",this);
            return true;
        }
    }
}
