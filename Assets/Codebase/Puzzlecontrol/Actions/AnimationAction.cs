﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class AnimationAction : GameAction, IActionAsync
{
    [SerializeField] private AnimationFunction animationFunction = default;
    public CommandOfTimer command;
    public bool Reverse;

    public override void Action()
    {
        switch (command)
        {
            case CommandOfTimer.RunFromStart:
                animationFunction.Play(true, Reverse);
                break;
            case CommandOfTimer.Run:
                animationFunction.Play(false, Reverse);
                break;
            case CommandOfTimer.Stop:
                animationFunction.Stop();
                break;
            case CommandOfTimer.Pause:
                animationFunction.Pause();
                break;
        }
    }

    public IEnumerator ActionAsync()
    {
        Action();
        while (animationFunction.IsInProgress)
            yield return null;
    }
}
