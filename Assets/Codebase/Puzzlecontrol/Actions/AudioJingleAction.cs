﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Audio;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class AudioJingleAction : GameAction
{
	[FormerlySerializedAs("trackOfMusic")] [SerializeField] private TrackOfJingle trackOfJingle = default;
	private AudioMusicPlayer audioMusicPlayer;

	protected virtual void Awake()
	{
		audioMusicPlayer = FindObjectOfType<AudioMusicPlayer>();
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    public override void Action()
    {
	    audioMusicPlayer.Play(trackOfJingle);
    }
}
