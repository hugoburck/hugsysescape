﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class ButtonInteractivityAction : GameAction
{
	[SerializeField] private UguiButtonInteractivity buttonInteractivity = default;
	[SerializeField] private bool interactable = default;
	
	protected virtual void Awake()
	{
		InitializationChecker.CheckImmediate(this, buttonInteractivity);
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    public override void Action()
    {
	    if (interactable)
			buttonInteractivity.Enable();
	    else buttonInteractivity.Disable();
    }
}
