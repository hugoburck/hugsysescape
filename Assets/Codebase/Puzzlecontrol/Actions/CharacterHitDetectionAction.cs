﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class CharacterHitDetectionAction : GameAction
{
	[SerializeField] private CharacterHitDetection characterHitDetection = default;
	[SerializeField] private StateOfActivity stateOfActivity = default;
	
	protected virtual void Awake()
	{
		if (characterHitDetection == null)
			characterHitDetection = FindObjectOfType<CharacterHitDetection>();
		
		InitializationChecker.CheckImmediate(this, characterHitDetection);
	}

	public override void Action()
    {
	    if (stateOfActivity == StateOfActivity.Enable)
		    characterHitDetection.Activate();
	    
	    if (stateOfActivity == StateOfActivity.Disable)
		    characterHitDetection.Deactivate();
    }
}