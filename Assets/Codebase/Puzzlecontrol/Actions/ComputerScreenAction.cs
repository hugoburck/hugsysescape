﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class ComputerScreenAction : GameAction
{
    [SerializeField] private ComputerScreenFunction computerScreenFunction = default;
    [SerializeField] private uint index = default;

    private void Awake()
    {
        InitializationChecker.CheckImmediate(this, computerScreenFunction);
    }

    public override void Action()
    {
	    computerScreenFunction.SetScreen(index);
    }
}


