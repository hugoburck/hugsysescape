﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class DebugLogAction : GameAction
{
    public string Message = "Hello World!";

    public override void Action()
    {
        if (Message != string.Empty)
            Debug.Log(Message,this);
        else Debug.Log("Hello World!", this);
    }
}
