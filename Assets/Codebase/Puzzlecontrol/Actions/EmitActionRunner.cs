﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Runs a GameAction when an IEmitAction emits its event.
/// The IEmitAction is cast from an injectable MonoBehaviour.
/// </summary>
public class EmitActionRunner : MonoBehaviour
{
    [SerializeField] private MonoBehaviour emitter = default;
    [SerializeField] private GameAction action = default;
    private IEmitAction emitAction;

    private bool HasAction => emitAction != null;

    private void Awake()
    {
        if (emitter is IEmitAction emitAction)
            this.emitAction = emitAction;
        else Debug.LogWarning("Monobehaviour didn't implement Interface",this);
    }

    private void OnEnable()
    {
        if (HasAction)
            emitAction.OnAction += EmitActionOnOnAction;
    }
    
    private void OnDisable()
    {
        if (HasAction)
            emitAction.OnAction -= EmitActionOnOnAction;
    }
    private void EmitActionOnOnAction()
    {
        action.Action();
    }
}
