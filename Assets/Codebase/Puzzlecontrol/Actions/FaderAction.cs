﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class FaderAction : GameAction
{
	[SerializeField] private IImageFader imageFader = default;
	[SerializeField] private CommandOfFader commandOfFader = default;
	public float seconds;

    protected virtual void Start()
    {
	    imageFader = ServiceLocator.Instance.Resolve(imageFader);
    }

    public override void Action()
    {
	    if (commandOfFader == CommandOfFader.FadeIn)
		    imageFader.FadeIn(seconds);
	    
	    if (commandOfFader == CommandOfFader.FadeOut)
		    imageFader.FadeOut(seconds);
    }
}