﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class FaderColorAction : GameAction
{
	[SerializeField] private IImageFader imageFader = default;
	[SerializeField] private Color color = default;

    protected virtual void Start()
    {
	    imageFader = ServiceLocator.Instance.Resolve(imageFader);
    }

    public override void Action()
    {
	    imageFader.SetColorForNextFadeIn(color);
    }
}
