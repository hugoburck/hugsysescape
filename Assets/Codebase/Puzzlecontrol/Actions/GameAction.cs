﻿using UnityEngine;

namespace Codebase.PuzzleControl.Actions
{
    public abstract class GameAction : MonoBehaviour
    {
        public abstract void Action();
    }
}