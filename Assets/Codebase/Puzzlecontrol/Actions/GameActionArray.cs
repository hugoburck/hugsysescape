﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class GameActionArray : GameAction
{
    [SerializeField] private GameAction[] actions = default;
    [SerializeField] private bool AwaitNodes = default;

    public override void Action()
    {
        StartCoroutine(LoopActions());
    }

    private IEnumerator LoopActions()
    {
        foreach (var action in actions)
        {
            if (action == null)
                continue;
            
            if (AwaitNodes && action is IActionAsync actionAsync)
                yield return StartCoroutine(actionAsync.ActionAsync());
            else action.Action();
        }
    }
}
