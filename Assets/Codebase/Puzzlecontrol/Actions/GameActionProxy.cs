﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class GameActionProxy : GameAction
{
    [SerializeField] 
    private GameAction gameAction = default;

    public override void Action()
    {
        gameAction.Action();
    }
}
