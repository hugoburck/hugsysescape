﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class GameNewAction : GameAction
{
	[SerializeField] private TransitionNewGame transitionNewGame = default;
	
	protected virtual void Awake()
	{
		if (transitionNewGame == null)
			transitionNewGame = FindObjectOfType<TransitionNewGame>();
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    public override void Action()
    {
	    transitionNewGame.Load();
    }
}
