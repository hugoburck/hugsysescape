﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class GameObjectAction : GameAction
{
    [SerializeField] private GameObject target = default;
    [SerializeField] private VisibilityAction.ModeOfVisibility modeOfVisibility = VisibilityAction.ModeOfVisibility.Hidden;
    
    // Start is called before the first frame update
    private void Start()
    {
        //ActOnChanged(CurrentState);
    }
    
    public override void Action()
    {
        target.SetActive(modeOfVisibility == VisibilityAction.ModeOfVisibility.Visible);
    }
}
