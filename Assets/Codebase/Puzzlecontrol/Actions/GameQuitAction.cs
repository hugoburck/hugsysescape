﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class GameQuitAction : GameAction
{
	[SerializeField] private TransitionQuitDesktop transitionQuitDesktop = default;
	
	protected virtual void Awake()
	{
		InitializationChecker.CheckImmediate(this, transitionQuitDesktop);
	}

    public override void Action()
    {
	    if (transitionQuitDesktop != null)
		    transitionQuitDesktop.Quit();
    }
}
