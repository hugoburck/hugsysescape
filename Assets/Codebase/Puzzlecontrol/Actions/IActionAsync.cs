﻿using System.Collections;

namespace Codebase.PuzzleControl.Actions
{
    public interface IActionAsync
    {
        IEnumerator ActionAsync();
    }
}