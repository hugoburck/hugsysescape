﻿using System;

namespace Codebase.PuzzleControl.Actions
{
    public interface IEmitAction
    {
        event Action OnAction;
        void InvokeAction();
    }
}