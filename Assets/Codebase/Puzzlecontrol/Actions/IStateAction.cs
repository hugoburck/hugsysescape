﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStateAction
{
    void ActOnChanged(StateOfPuzzlePiece stateEnum);
    void ActOnSet(StateOfPuzzlePiece stateEnum);
}
