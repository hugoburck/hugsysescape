﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class InstructionFlushAction : GameAction
{
	private InstructionViewManager instructionViewManager;
	
	protected virtual void Awake()
	{
		instructionViewManager = FindObjectOfType<InstructionViewManager>();
	}

    public override void Action()
    {
	    if (instructionViewManager != null)
	    {
			instructionViewManager.Flush();	    
	    }
    }
}
