﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class IntroAction : GameAction
{
    [SerializeField] private int sequence = default;
    private RoomIntroManager introManager;

    private void Awake()
    {
        introManager = FindObjectOfType<RoomIntroManager>();
        InitializationChecker.CheckImmediate(this, introManager);
    }
    public override void Action()
    {
        introManager.PlayFromStart(sequence);
    }
}
