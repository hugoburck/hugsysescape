﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class InventoryClearAction : GameAction
{
    private InventoryRepoControl inventoryRepoControl;
    
    public override void Action()
    {
        inventoryRepoControl.ClearItems();
    }

    private void Awake()
    {
        inventoryRepoControl = GameAction.FindObjectOfType<InventoryRepoControl>();
        if (inventoryRepoControl == null)
            Debug.LogWarning("InventoryRepoControl not found",this);
    }
}
