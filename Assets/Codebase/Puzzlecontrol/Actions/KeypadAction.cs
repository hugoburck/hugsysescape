﻿using Codebase.PuzzleControl.Actions;
using UnityEngine;

public abstract class KeypadAction : GameAction
{
    [SerializeField]
    protected KeypadFunction keypadFunction = default;

    protected virtual void Awake()
    {
        InitializationChecker.CheckImmediate(this, keypadFunction);
    }
}
