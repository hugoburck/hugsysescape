﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadCheckSequenceAction : KeypadAction
{
    public override void Action()
    {
            keypadFunction.CheckSequence();
    }
}
