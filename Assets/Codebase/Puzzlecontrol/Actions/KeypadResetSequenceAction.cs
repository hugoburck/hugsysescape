﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadResetSequenceAction : KeypadAction
{
    public override void Action()
    {
            keypadFunction.ResetSequence();
    }
}
