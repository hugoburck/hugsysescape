﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadSendInputAction : KeypadAction
{
    [SerializeField] 
    private string input = default;
    
    protected override void Awake()
    {
        base.Awake();
        if (input == default)
            Debug.Log("Input is empty",this);
    }

    public override void Action()
    {
        keypadFunction.SetInput(input);
    }
}
