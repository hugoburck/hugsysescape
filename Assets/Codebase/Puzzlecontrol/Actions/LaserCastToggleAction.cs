﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCastToggleAction : LaserCastAction
{
    public override void Action()
    {
        laserCast.Toggle();
    }
}
