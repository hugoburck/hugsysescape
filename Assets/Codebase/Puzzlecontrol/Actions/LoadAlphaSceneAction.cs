﻿using System;
using Codebase.SceneLogic;
using UnityEngine;

namespace Codebase.PuzzleControl.Actions
{
    public class LoadAlphaSceneAction : GameAction
    {
        [SerializeField] private CommandOfLoadAlphaScene command = default;
        private ILoadAlphaRoom loadAlphaRoom = default;

        public override void Action()
        {
            if (command == CommandOfLoadAlphaScene.Next)
                loadAlphaRoom.LoadNext();

            if (command == CommandOfLoadAlphaScene.FromBegin)
                loadAlphaRoom.LoadFromBegin();
        }

        private void Start()
        {
            loadAlphaRoom = ServiceLocator.Instance.Resolve(loadAlphaRoom);
        }
    }
}