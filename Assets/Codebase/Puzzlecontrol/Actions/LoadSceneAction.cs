﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using Codebase.SceneLogic;
using UnityEngine;
using UnityEngine.Serialization;

public class LoadSceneAction : GameAction, IActionAsync
{
	
	[SerializeField] private SceneLoader sceneloader = default;
	[SerializeField] private UnitOfScene unitOfScene = default;
	[SerializeField] private OperationOfSceneLoad operationOfSceneLoad = default;
	//[SerializeField] private SceneLoadSyncMode sceneLoadSyncMode = default;

	public override void Action()
	{
		if (operationOfSceneLoad == OperationOfSceneLoad.Load)
			sceneloader.Load(unitOfScene);
		
		if (operationOfSceneLoad == OperationOfSceneLoad.Unload)
			sceneloader.Unload(unitOfScene);
	}

	public IEnumerator ActionAsync()
	{
		Action();

		while (sceneloader.IsInProgress)
			yield return null;
	}
	

	protected virtual void Awake()
	{
		InitializationChecker.CheckImmediate(this, sceneloader);
	}
}