﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public abstract class PointLightAction : GameAction
{
    [SerializeField] protected PointLightFunction pointLightFunction = default;
    
}
