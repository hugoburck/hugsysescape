﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointLightOffAction : PointLightAction
{
    public override void Action()
    {
            pointLightFunction.TurnOff();
    }
}
