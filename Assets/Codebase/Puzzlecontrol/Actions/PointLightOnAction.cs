﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointLightOnAction : PointLightAction
{
    [Tooltip("Optional; Leave black to turn on to previous color.")]
    public Color color;
    
    public override void Action()
    {
        if (color == Color.black)
            pointLightFunction.TurnOn();
        else
            pointLightFunction.TurnOn(color);
    }
}
