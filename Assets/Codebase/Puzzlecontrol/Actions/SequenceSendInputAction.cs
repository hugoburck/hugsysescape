﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class SequenceSendInputAction : GameAction
{
    [SerializeField] private SequenceLockFunction sequenceLockFunction = default;
    [SerializeField] private string input = default;

    public override void Action()
    {
        sequenceLockFunction.SetInput(input);
    }
}
