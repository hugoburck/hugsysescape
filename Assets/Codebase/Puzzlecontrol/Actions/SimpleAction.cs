﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class SimpleAction : GameAction
{
    [SerializeField] private SimpleFunction simpleFunction = default;
    [SerializeField] private StateOfPuzzlePiece stateEnum = default;
    
    // public override void ActOnSet(PieceState state)
    // {
    //     //throw new System.NotImplementedException();
    // }
    //
    // public override void ActOnChanged(PieceState state)
    // {
    //     if (state == PieceState.Correct)
    //         simpleFunction.SetCorrect();
    // }

    public override void Action()
    {
        if (stateEnum == StateOfPuzzlePiece.Correct)
            simpleFunction.SetCorrect();
        if (stateEnum == StateOfPuzzlePiece.Incorrect)
            simpleFunction.SetIncorrect();
    }
}
