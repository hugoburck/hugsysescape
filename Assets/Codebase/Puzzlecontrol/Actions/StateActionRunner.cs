﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Codebase.PuzzleControl.Actions
{
    /// <summary>
    /// State Actions run actions based on the state and how the state was achieved
    /// </summary>
    public class StateActionRunner : MonoBehaviour, IStateAction
    {
        [SerializeField] private PuzzleToState puzzleState = default;
        [SerializeField] private StateOfPuzzlePiece stateOfPuzzle = default;
        [SerializeField] private ModeOfTrigger modeOfTrigger = default;
        [SerializeField] private GameAction gameAction = default;

        public enum ModeOfTrigger { Set, Changed }
        
        public void ActOnSet(StateOfPuzzlePiece stateEnum)
        {
            if (stateEnum == stateOfPuzzle && modeOfTrigger == ModeOfTrigger.Set)
                gameAction.Action();
        }

        public void ActOnChanged(StateOfPuzzlePiece stateEnum)
        {
            if (stateEnum == stateOfPuzzle && modeOfTrigger == ModeOfTrigger.Changed)
                gameAction.Action();
        }

        protected StateOfPuzzlePiece Current
        {
            get
            {
                if (HasPuzzleState)
                    return puzzleState.State;
                
                return StateOfPuzzlePiece.Stateless;
            }
        }

        private bool HasPuzzleState => puzzleState != null;

        private void OnEnable()
        {
            if (!HasPuzzleState)
                return;
        
            puzzleState.OnStateChanged += ActOnChanged;
            puzzleState.OnStateSet += ActOnSet;
        }

        private void OnDisable()
        {
            if (!HasPuzzleState)
                return;
        
            puzzleState.OnStateChanged -= ActOnChanged;
            puzzleState.OnStateSet -= ActOnSet;
        }


        protected virtual void Awake()
        {
            if (!HasPuzzleState)
                puzzleState = GetComponent<PuzzleToState>();
        }
    }
}
