﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using Codebase.PuzzleControl.Functions;
using UnityEngine;
using UnityEngine.Serialization;

public class StreakLightAction : GameAction, IActionAsync
{
    [SerializeField] private StreakLightFunction streakLightFunction = default;
    [SerializeField] private Material material = default;
    
    public enum OperationOfStreakLight {Flash, Set, FlashSet}
    
    public OperationOfStreakLight operation;

    public override void Action()
    {
        switch (operation)
        {
            case OperationOfStreakLight.Flash:
                streakLightFunction.FlashColor(material);
                break;
            case OperationOfStreakLight.Set:
                streakLightFunction.SetColor(material);
                break;
            case OperationOfStreakLight.FlashSet:
                streakLightFunction.FlashSetColor(material);
                break;
            default:
                Debug.LogWarning("Missing operation handler",this);
                return;
        }
    }

    public IEnumerator ActionAsync()
    {
        Action();
        while (streakLightFunction.IsInProgress)
            yield return null;
    }
}
