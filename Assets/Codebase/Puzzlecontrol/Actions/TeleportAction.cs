﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class TeleportAction : GameAction
{
	[SerializeField] private TeleportSceneTarget teleportSceneTarget = default;
	private TeleportPlayer teleportPlayer;
	
	protected virtual void Awake()
	{
		teleportPlayer = FindObjectOfType<TeleportPlayer>();
		InitializationChecker.CheckImmediate(this, teleportPlayer, teleportSceneTarget);
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    public override void Action()
    {
	    teleportPlayer.Teleport(teleportSceneTarget.transform);
    }
}
