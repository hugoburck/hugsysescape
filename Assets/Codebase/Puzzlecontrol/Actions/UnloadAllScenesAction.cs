﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using Codebase.SceneLogic;
using UnityEngine;

public class UnloadAllScenesAction : GameAction, IActionAsync
{
	[SerializeField] private SceneLoader sceneloader = default;
	
	public override void Action()
	{
		sceneloader.UnloadAllInDictionaryButActive();
	}

	public IEnumerator ActionAsync()
	{
		Action();

		while (sceneloader.IsInProgress)
			yield return null;
	}

	protected virtual void Awake()
	{
		InitializationChecker.CheckImmediate(this, sceneloader);
	}
}
