﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;



public class VisibilityAction : GameAction
{
    [SerializeField] private ModeOfVisibility modeOfVisibility = default;
    [SerializeField] private GameObject root = null;

    private Collider[] colliders;
    private Renderer[] renderers;
    
    public enum ModeOfVisibility {Visible,Hidden}

    public override void Action()
    {
        SetComponents(modeOfVisibility == ModeOfVisibility.Visible);
    }

    private void SetComponents(bool state)
    {
        if (root == null)
            return;

        foreach (var collider in colliders)
        {
            collider.enabled = state;
        }

        foreach (var renderer in renderers)
        {
            renderer.enabled = state;
        }
    }

    protected void Awake()
    {
        if (root != null)
        {
            colliders = root.GetComponentsInChildren<Collider>();
            renderers = root.GetComponentsInChildren<Renderer>();
        }
        else
        {
            Debug.LogError("No root",this);
        }
    }
}
