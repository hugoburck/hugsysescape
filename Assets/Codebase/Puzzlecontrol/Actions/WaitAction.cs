﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class WaitAction : GameAction,IActionAsync
{
    public float seconds;

    public override void Action()
    {
        //
    }

    public IEnumerator ActionAsync()
    {
        yield return new WaitForSeconds(seconds);
    }
}
