﻿using System;
using System.Collections;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;
using UnityEngine.Serialization;

namespace Codebase.PuzzleControl.Animation
{
    public abstract class AnimationInputDevice : MonoBehaviour
    {
        public event Action OnPlayerAction;
        public event Action OnExitPlayerAction;
        public event Action<float> OnKeyFetch;
        [SerializeField] private ActionOfPlayer actionOfPlayer = default;
        public float Multiplier = 1f;
        public bool Inverse;
        protected float key;
        private float axisCache;
        private IInputStepping inputStepping;
        private IDeviceInput deviceInput;

        public float CurrentKey 
        {
            get
            {
                float key = inputStepping?.GetStepKey(CalculateKey(currentAxis)) ?? CalculateKey(currentAxis);
                OnKeyFetch?.Invoke(key);
                return key;
            }
        }

        protected abstract float CalculateKey(float axis);
    
        protected float currentAxis
        {
            get
            {
                float axis = axisCache;
                axisCache = 0;
                return axis;
            }
        }

        private float AxisDirected(float axis) => Inverse ? -axis : axis;

        private void Awake()
        {
            inputStepping = GetComponent<IInputStepping>();
        }

        private void Start()
        {
            deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
        }

        private void OnEnable()
        {
            StartCoroutine(SubServices());
        }

        private IEnumerator SubServices()
        {
            yield return null;
            deviceInput.OnInputEvent += DeviceInput_OnInputEvent;
            deviceInput.OnExitInputEvent += DeviceInput_OnExitInputEvent;
        }

        private void OnDisable()
        {
            deviceInput.OnInputEvent -= DeviceInput_OnInputEvent;
            deviceInput.OnExitInputEvent -= DeviceInput_OnExitInputEvent;
        }

        private void DeviceInput_OnExitInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
        {
            if (actionOfPlayer == this.actionOfPlayer)
            {
                OnExitPlayerAction?.Invoke();
            }
        }

        private void DeviceInput_OnInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
        {
            if (actionOfPlayer == this.actionOfPlayer)
            {
                axisCache = AxisDirected(intensity);
                OnPlayerAction?.Invoke();
            }
        }
    }
}
