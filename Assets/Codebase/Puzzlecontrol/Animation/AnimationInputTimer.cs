﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationInputTimer : MonoBehaviour
{
    private const float Begin = 0, End = 1; 
    public float SecondsPerUnit = 1;
    public BehaviourOfTimer Behaviour;
    private CommandOfTimer Command = CommandOfTimer.Stop;
    public StateOfTimer State { get; private set; }
    public bool Reverse,PingPong;
    [Range(0,1)]
    private float key;
    public float CurrentKey { get; private set; }

    public bool IsKeyStoppedAtEnd 
    { 
        get => (CurrentKey == End) && (State == StateOfTimer.Stopped);
    }

    public bool IsKeyStoppedAtBegin
    {
        get => (CurrentKey == Begin) && (State == StateOfTimer.Stopped);
    }

    public void Play(bool FromStart = false)
    {
        if (FromStart)
            Command = CommandOfTimer.RunFromStart;
        else
            Command = CommandOfTimer.Run;
    }

    public void PlayForwards()
    {
        Reverse = false;
        Play(false);
    }

    public void PlayInReverse()
    {
        Reverse = true;
        Play(false);
    }

    public void Pause()
    {
        Command = CommandOfTimer.Pause;
    }

    public void Stop()
    {
        Command = CommandOfTimer.Stop;
    }

    // Update is called once per frame
    void Update()
    {
        State = StateFromCommand(Command);
        Command = CommandOfTimer.Idle;

        if (State == StateOfTimer.Stopped)
            return;

        key = AdvanceKey(key);
        key = ClampKey(key);
        CurrentKey = UpdateKeyOut(key);
    }

    private float ClampKey(float key)
    {
        if (key > End)
            switch (Behaviour)
            {
                case BehaviourOfTimer.Single:
                    key = End;
                    Command = CommandOfTimer.Pause;
                    break;
                case BehaviourOfTimer.Repeat:
                    key -= (End - Begin);
                    break;
                default:
                    break;
            }

        if (key < Begin)
            switch (Behaviour)
            {
                case BehaviourOfTimer.Single:
                    key = Begin;
                    Command = CommandOfTimer.Pause;
                    break;
                case BehaviourOfTimer.Repeat:
                    key += (End - Begin);
                    break;
                default:
                    break;
            }
        return key;
    }

    private float UpdateKeyOut(float key)
    {
        key = PingPong ? PingPongKey(key) : key;
        return key;
    }

    private float AdvanceKey(float key)
    {
        if (Reverse)
            return key -= Time.deltaTime / SecondsPerUnit;
        else
            return key += Time.deltaTime / SecondsPerUnit;
    }

    private float PingPongKey(float key)
    {
        float half = (End - Begin) * 0.5f;
        if (key < half)
            return key * 2;
        else return (half - (key - half)) * 2;

    }

    private StateOfTimer StateFromCommand(CommandOfTimer command)
    {
        switch (command)
        {
            case CommandOfTimer.Idle:
                return State;
            case CommandOfTimer.Run:
                return StateOfTimer.Running;
            case CommandOfTimer.RunFromStart:
                if (State == StateOfTimer.Running)
                    return State;
                key = Begin;
                return StateOfTimer.Running;
            case CommandOfTimer.Pause:
                return StateOfTimer.Stopped;
            case CommandOfTimer.Stop:
                key = Begin;
                return StateOfTimer.Stopped;
            default:
                return State;
        }
    }
}
