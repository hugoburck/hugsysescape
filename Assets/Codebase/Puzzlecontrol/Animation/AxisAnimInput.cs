﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;

public class AxisAnimInput : AnimationInputDevice
{
    protected override float CalculateKey(float axis)
    {
        return  axis * Time.deltaTime * Multiplier;
    }
}
