﻿namespace Codebase.PuzzleControl.Animation
{
    public interface IInputStepping
    {
        float GetStepKey(float keyIn);
    }
}