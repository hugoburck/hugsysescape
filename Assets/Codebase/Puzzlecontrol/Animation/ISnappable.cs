﻿namespace Codebase.PuzzleControl.Animation
{
    public interface ISnappable
    {
        void Snap();
    }
}