﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;

public class InputStepping : MonoBehaviour, IInputStepping
{
    [SerializeField] private AudioEffectPlayer _audioEffectPlayer = default;
    public float KeyStep = 0.01f;
    public event Action<float> OnStep;
    public event Action OnStepUp, OnStepDown;
    private int steps;
    private float stepSize, remainder, curKey;
    private IAudioEffectPlayer audioEffectPlayer;
    
    public float GetStepKey(float keyIn)
    {
        if (KeyStep == 0)
            return keyIn;

        curKey += keyIn;
        if (IsBiggerThanPositiveStep || IsSmallerThanNegativeStep)
        {
            steps = (int)(curKey / KeyStep);
            remainder = IsBiggerThanPositiveStep ? curKey - KeyStep : curKey + KeyStep;
            curKey = remainder;
            stepSize = steps * KeyStep;
            OnStep?.Invoke(stepSize);
            if (IsBiggerThanPositiveStep)
                OnStepUp?.Invoke();
            else OnStepDown?.Invoke();
            audioEffectPlayer?.Play();
            return stepSize;
        }

        return 0f;
    }

    private bool IsSmallerThanNegativeStep => curKey < -KeyStep;
    private bool IsBiggerThanPositiveStep => curKey > KeyStep;

    private void Awake()
    {
        if (_audioEffectPlayer != null)
            audioEffectPlayer = _audioEffectPlayer;
    }
}


