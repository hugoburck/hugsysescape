﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using Codebase.PuzzleControl;
using Codebase.PuzzleControl.Animation;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class LerpAnim : TransformAnimation
{
    public Vector3 origin;
    public Vector3 target;
    public float startKey = 0.5f;
    public bool originFromStart = true;
    public bool relativeTarget;
    public ScopeOfTransform scope = ScopeOfTransform.Local;
    public event Action<StateOfLerp> OnKeyClamped;
    private TriggerComparable<bool> trigClamp = new TriggerComparable<bool>();

    public bool KeyAtEnd => CurrentKey.IsEqualTo(1f);
    public bool KeyAtBegin => CurrentKey.IsEqualTo(0f);
    public bool KeyInBetween => !KeyAtBegin && !KeyAtEnd;
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        target = relativeTarget ? origin + target : target;
        AnimateAbsolute(startKey);
        UpdateSnap();
    }

    protected float ClampKey(float key)
    {
        float clamped = Mathf.Clamp01(key);
        if (trigClamp.OnUp(clamped.IsEqualTo(0f) || clamped.IsEqualTo(1f)))
        {
            OnKeyClamped?.Invoke(KeyAtBegin ? StateOfLerp.AtBegin : StateOfLerp.AtEnd);
        }
        
        return clamped;
    }
}
