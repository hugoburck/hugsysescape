﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

public class LerpAnimAudio : TransformAnimationAudio
{
	private LerpAnim lerpAnim;
	[SerializeField] protected AudioEffectPlayer clampEffect = default;
	
	protected override void Awake()
	{
		base.Awake();
		if (transformAnimation is LerpAnim lerpAnim)
			this.lerpAnim = lerpAnim;
	}

    protected override void OnEnable()
    {
	    base.OnEnable();
	    if (lerpAnim != null)
		    lerpAnim.OnKeyClamped += LerpAnim_OnKeyClamped;
    }

    private void LerpAnim_OnKeyClamped(StateOfLerp state)
    {
	    switch (state)
	    {
		    case StateOfLerp.AtBegin:
			    clampEffect.Play();
			    break;
		    case StateOfLerp.AtEnd:
			    clampEffect.Play();
			    break;
		    default:
			    break;
	    }
    }
    
    protected override void OnDisable()
    {
	    base.OnDisable();
	    if (lerpAnim != null)
		    lerpAnim.OnKeyClamped -= LerpAnim_OnKeyClamped;
    }
}
