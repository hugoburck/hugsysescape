﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;

public class PositionLerpAnim : LerpAnim
{
    // Start is called before the first frame update
    protected override void Start()
    {
        if (originFromStart)
        {
            if (scope == ScopeOfTransform.Local)
                origin = transform.localPosition;
            if (scope == ScopeOfTransform.World)
                origin = transform.position;
        }
        
        base.Start();
    }

    public override void AnimateRelative(float key)
    {
        CurrentKey += key;
        AnimateAbsolute(CurrentKey);
    }

    public override void AnimateAbsolute(float key)
    {
        CurrentKey = ClampKey(key);
        if (scope == ScopeOfTransform.Local)
            transTarget.localPosition = Vector3.Lerp(origin, target, CurrentKey);
        if (scope == ScopeOfTransform.World)
            transTarget.position = Vector3.Lerp(origin, target, CurrentKey);
        //UpdateSnap();
        SetDirection();
    }
}
