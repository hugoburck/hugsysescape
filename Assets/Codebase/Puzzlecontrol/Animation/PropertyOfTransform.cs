﻿public enum PropertyOfTransform
{
    Position,
    Rotation,
    Scale
}
