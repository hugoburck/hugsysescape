﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;

public class RangedAnimInput : AnimationInputDevice
{

    protected override float CalculateKey(float axis)
    {
        key += axis * Time.deltaTime * Multiplier;
        return key = Mathf.Clamp(key, 0, 1);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
