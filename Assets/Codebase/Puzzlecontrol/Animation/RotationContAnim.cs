﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationContAnim : ContinueousAnim
{


    public override void AnimateAbsolute(float key)
    {
        throw new System.NotImplementedException();
    }

    public override void AnimateRelative(float key)
    {
        var euler = ToEulerAngles(key);
        transform.Rotate(euler, Space.Self);
        //transform.localRotation = Quaternion.Euler(euler);
        //UpdateSnap();
    }
    

    protected override void Start()
    {
        base.Start();
        Origin = transform.localEulerAngles;
    }

    private Vector3 ToEulerAngles(float key)
    {
        Vector3 euler = Vector3.zero;
        if (axisOfTransform == AxisOfTransform.X)
            euler.x = key;
        else if (axisOfTransform == AxisOfTransform.Y)
            euler.y = key;
        else
            euler.z = key;
        return euler;
    }

    private float GetCurrentEulerAxis()
    {
        Vector3 euler = transform.localEulerAngles;
        if (axisOfTransform == AxisOfTransform.X)
            return euler.x;
        else if (axisOfTransform == AxisOfTransform.Y)
            return euler.y;
        else
            return euler.z;
    }

    private void SetEulerAnglesAxis(float axis)
    {
        Vector3 euler = transform.localEulerAngles;
        if (axisOfTransform == AxisOfTransform.X)
            euler.x = axis;
        else if (axisOfTransform == AxisOfTransform.Y)
            euler.y = axis;
        else
            euler.z = axis;
        transform.localEulerAngles = euler;
    }

    protected override void Snap()
    {
        float eulerAngle = ValueSnap.ClosestSnapOrOld(GetCurrentEulerAxis());
        SetEulerAnglesAxis(eulerAngle);
    }
}
