﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;


public class RotationLerpAnim : LerpAnim
{
    // Start is called before the first frame update
    protected override void Start()
    {
        if (originFromStart)
        {
            if (scope == ScopeOfTransform.Local)
                origin = transform.localRotation.eulerAngles;
            if (scope == ScopeOfTransform.World)
                origin = transform.rotation.eulerAngles;
        }
        
        base.Start();
    }

    public override void AnimateRelative(float key)
    {
        CurrentKey += key;
        AnimateAbsolute(CurrentKey);
    }

    public override void AnimateAbsolute(float key)
    {
        key = ClampKey(key);
        CurrentKey = key;
        if (scope == ScopeOfTransform.Local)
            transTarget.localRotation = Quaternion.Slerp(Quaternion.Euler(origin), Quaternion.Euler(target), key);
        if (scope == ScopeOfTransform.World)
            transTarget.rotation = Quaternion.Slerp(Quaternion.Euler(origin), Quaternion.Euler(target), key);

        //UpdateSnap();
        SetDirection();
    }
}
