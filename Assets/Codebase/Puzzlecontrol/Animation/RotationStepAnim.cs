﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StepAnim : TransformAnimation
{
	[SerializeField] protected AxisOfTransform axisOfTransform = default;
	[SerializeField] protected float[] steps = default;
	[SerializeField] protected int startIndex = default;
	public event EventHandler<StepAnimEventArgs> OnStep;
	public int Index { get; protected set; }


	protected void InvokeOnStep(int index, float value, MovementOfAnimation movement)
	{
		OnStep?.Invoke(this, new StepAnimEventArgs(index, value, movement));
	}
	
	public class StepAnimEventArgs : EventArgs
	{
		public StepAnimEventArgs(int index, float value, MovementOfAnimation movement)
		{
			this.index = index;
			this.value = value;
			this.movement = movement;
		}
	
		public readonly int index;
		public readonly float value;
		public readonly MovementOfAnimation movement;
	}
}



[RequireComponent(typeof(InputStepping))]
public class RotationStepAnim : StepAnim
{
	private Vector3 startRotation;
	
	public override void AnimateRelative(float key)
	{
		AnimateAbsolute(key);
	}

	public override void AnimateAbsolute(float key)
	{
		MovementOfAnimation movement = MovementOfAnimation.None;
		if (key == 0f) 
			return;

		if (key > 0)
		{
			Index++;
			movement = MovementOfAnimation.Up;
		}

		if (key < 0)
		{
			Index--;
			movement = MovementOfAnimation.Down;
		}

		if (Index < 0)
			Index = steps.Length - 1;

		if (Index >= steps.Length)
			Index = 0;
		
		MoveToIndex(Index);
		InvokeOnStep(Index,steps[Index],movement);
	}

	private void MoveToIndex(int index)
	{
		if (index >= steps.Length || index < 0)
			return;
		
		float value = steps[index];
		Vector3 rotation = startRotation;

		switch (axisOfTransform)
		{
			case AxisOfTransform.X:
				rotation.x = value;
				break;
			case AxisOfTransform.Y:
				rotation.y = value;
				break;
			case AxisOfTransform.Z:
				rotation.z = value;
				break;
		}

		transform.localRotation = Quaternion.Euler(rotation);
	}

	protected override void Start()
    {
        base.Awake();
        startRotation = transform.localRotation.eulerAngles;
        Index = startIndex;
        MoveToIndex(Index);
    }
}
