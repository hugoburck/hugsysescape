﻿namespace Codebase.PuzzleControl.Animation
{
    public enum ScopeOfTransform
    {
        World,
        Local
    }
}