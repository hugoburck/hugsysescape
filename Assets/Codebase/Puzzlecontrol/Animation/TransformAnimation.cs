﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Codebase.Helpers.Extensions;
using UnityEngine;

///<summary>
/// Use Absolute to animate to the supplied key.
/// Use Relative to animate from the current key with the supplied key.
/// </summary>
public abstract class TransformAnimation : MonoBehaviour
{
    [SerializeField] protected Transform transTarget = default;
    protected IValueSnap ValueSnap;
    private TriggerComparable<int> triggerMove = new TriggerComparable<int>();
    private float oldKey;
    public event Action<MovementOfAnimation> OnMovementChanged;

    /// <summary>
    /// Use with Timers.
    /// </summary>
    /// <param name="key"></param>
    public abstract void AnimateAbsolute(float key);

    /// <summary>
    /// Use with device input.
    /// </summary>
    /// <param name="key"></param>
    public abstract void AnimateRelative(float key);

    public float CurrentKey { get; protected set; }

    public void Release()
    {
        if (HasSnap)
        {
            Snap();
        }
    }

    protected bool HasSnap => ValueSnap != null;

    protected virtual void Snap()
    {
        AnimateAbsolute(ValueSnap.ClosestSnapOrOld(CurrentKey));
    }

    protected void SetDirection()
    {
        var movement = MovementOfAnimation.None;
        if (CurrentKeyDelta > 0)
            movement = MovementOfAnimation.Up;
        else if (CurrentKeyDelta < 0)
            movement = MovementOfAnimation.Down;
        
        if (triggerMove.OnChange((int) movement))
            OnMovementChanged?.Invoke(movement);
        
        //StopCoroutine(nameof(countdownToFreeze)); //werkt niet?
        StopAllCoroutines();
        StartCoroutine(countdownToFreeze());
    }

    private float CurrentKeyDelta => CurrentKey - oldKey;

    private IEnumerator countdownToFreeze()
    {
        yield return new WaitForSeconds(0.2f);

        OnMovementChanged?.Invoke(MovementOfAnimation.None);
    }

    protected void UpdateSnap() //is this needed? I forgot the usage of this. Remove any usage of it
    {
        if (HasSnap)
        {
            ValueSnap.ClosestSnapOrOld(CurrentKey);
        }
    }

    protected virtual void Awake()
    {
        ValueSnap = GetComponent<ValueSnap>();
        if (transTarget == null)
            transTarget = this.transform;
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {

    }

    private void Update()
    {
        //SetDirection();
    }

    private void LateUpdate()
    {
        oldKey = CurrentKey;
    }
}