﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Serialization;

public class TransformAnimationAudio : MonoBehaviour
{
	[SerializeField] protected TransformAnimation transformAnimation = default;
	[SerializeField] private AudioEffectPlayer moveEffect = default;
	
	protected virtual void Awake()
	{
        
    }
    
    protected virtual void OnEnable()
    {
	    if (transformAnimation != null)
			transformAnimation.OnMovementChanged += TransformAnimation_OnMovementChanged;
    }

    private void TransformAnimation_OnMovementChanged(MovementOfAnimation movement)
    {
	    if (moveEffect == null)
		    return;
		    
	    switch (movement)
	    {
		    case MovementOfAnimation.Up :
			    moveEffect.Play();
			    break;
		    case MovementOfAnimation.Down:
			    moveEffect.Play();
			    break;
		    default:
			    moveEffect.Stop();
			    break;
	    }
    }

    protected virtual void OnDisable()
    {
	    if (transformAnimation != null)
			transformAnimation.OnMovementChanged -= TransformAnimation_OnMovementChanged;
    }
}
