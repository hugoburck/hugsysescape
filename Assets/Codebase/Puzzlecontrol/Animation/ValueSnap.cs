﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IValueSnap
{
    int SnappedAtIndex { get; }
    bool IsSnapped { get; }
    float ValueAtIndex(int index);
    float ClosestSnapOrOld(float key);
    event Action<int> OnSnap;
}

public class ValueSnap : MonoBehaviour, IValueSnap
{
    [SerializeField] private float[] snaps = default;
    [SerializeField] private float snapRange = 0.01f;
    public event Action<int> OnSnap;

    public int SnappedAtIndex { get; private set; }
    public bool IsSnapped => SnappedAtIndex != -1;

    public float ValueAtIndex(int index)
    {
        if (index > 0 && index < snaps.Length - 1)
            return snaps[index];
        
        return Mathf.Infinity;
    }

    public float ClosestSnapOrOld(float key)
    {
        float closestDistance = Mathf.Infinity;
        int counter = -1;
        foreach (var snap in snaps)
        {
            counter++;
            float distance = Mathf.Abs(key - snap);
            if (distance > snapRange || distance > closestDistance)
                continue;
            closestDistance = distance;
            key = snap;
            SnappedAtIndex = counter;
        }

        if (IsSnapped)
        {
            OnSnap?.Invoke(SnappedAtIndex);
        }
        
        return key;
    }
}
