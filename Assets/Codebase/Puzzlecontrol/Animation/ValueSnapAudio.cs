﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueSnapAudio : MonoBehaviour
{
	[SerializeField] private ValueSnap valueSnap = default;
	[SerializeField] private AudioEffectPlayer audioEffectPlayer = default;

	protected virtual void OnEnable()
	{
		if (valueSnap != null)
			valueSnap.OnSnap += ValueSnap_OnSnap;
	}

	private void ValueSnap_OnSnap(int index)
	{
		if (audioEffectPlayer != null)
			audioEffectPlayer.Play();
	}

	protected virtual void OnDisable()
	{
		if (valueSnap != null)
			valueSnap.OnSnap -= ValueSnap_OnSnap;
	}
}
