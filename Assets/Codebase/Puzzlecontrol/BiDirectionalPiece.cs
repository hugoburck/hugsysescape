﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class BiDirectionalPiece : PuzzlePiece
{
    public AxisAnimInput HorizontalInput, VerticalInput;
    public LerpAnim HorizontalLerp, VerticalLerp;
    public bool ReleaseResetAnim;
    private bool mouseHolding, goingHorizontal;
    private float horInput, verInput, leewayKey = 0.01f, leewaySwap = 0.1f;

    protected override void OnEnable()
    {
        base.OnEnable();
        HorizontalInput.OnPlayerAction += InputKeyOnHorizontalPlayerAction;
        VerticalInput.OnPlayerAction += InputKeyOnVerticalPlayerAction;
    }


    protected override void OnDisable()
    {
        base.OnDisable();
        HorizontalInput.OnPlayerAction -= InputKeyOnHorizontalPlayerAction;
        VerticalInput.OnPlayerAction -= InputKeyOnVerticalPlayerAction;
    }

    private void InputKeyOnHorizontalPlayerAction()
    {
        if (!mouseHolding)
            return;

        horInput = HorizontalInput.CurrentKey;

        if (GoHorizontal) 
            goingHorizontal = true;
        
        if (goingHorizontal) 
            HorizontalLerp.AnimateRelative(horInput);
        
        InvokeStateCheck();

    }
    
    private void InputKeyOnVerticalPlayerAction()
    {
        if (!mouseHolding)
            return;

        verInput = VerticalInput.CurrentKey;
        
        if (GoVertical) 
            goingHorizontal = false;
        
        if (!goingHorizontal)
            VerticalLerp.AnimateRelative(verInput);
        
        InvokeStateCheck();
        
    }

    private bool GoHorizontal => Math.Abs(horInput) > Math.Abs(verInput) && 
                                 Math.Abs(horInput) > leewayKey && 
                                 Math.Abs(VerticalLerp.CurrentKey)-0.5f < leewaySwap;

    private bool GoVertical => Math.Abs(verInput) > Math.Abs(horInput) && 
                               Math.Abs(verInput) > leewayKey && 
                               Math.Abs(HorizontalLerp.CurrentKey)-0.5f < leewaySwap;

    protected override void OnClickOverAction()
    {
        mouseHolding = true;
    }

    protected override void OnUnclick()
    {
        base.OnUnclick();
        mouseHolding = false;
        HorizontalLerp.Release();
        VerticalLerp.Release();
        InvokeStateCheck();
        if (ReleaseResetAnim)
        {
            HorizontalLerp.AnimateAbsolute(0.5f);
            VerticalLerp.AnimateAbsolute(0.5f);
        }
        
    }
}


