﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickablePiece : PuzzlePiece
{
    public TransformAnimation Animatable;
    public AnimationInputTimer Timer;
    public AudioEffectPlayer AudioEffectPlayer;

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        if (Animatable != null)
            Animatable.AnimateAbsolute(Timer.CurrentKey);
    }

    protected override void OnClickOverAction()
    {
        if (Timer != null)
        {
            if (AudioEffectPlayer != null)
                AudioEffectPlayer.Play();
            Timer.Play(true);
        }
        InvokeStateCheck();
    }
}
