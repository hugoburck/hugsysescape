﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;

public class ControllablePiece : PuzzlePiece
{
    public TransformAnimation Animatable;
    public AnimationInputDevice InputKey;
    private bool mouseholding;

    protected override void OnEnable()
    {
        base.OnEnable();
        InputKey.OnPlayerAction += InputKeyOnPlayerAction;
    }

    private void InputKeyOnPlayerAction()
    {
        if (mouseholding)
        {
            Animatable.AnimateRelative(InputKey.CurrentKey);
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        InputKey.OnPlayerAction -= InputKeyOnPlayerAction;
    }

    protected override void OnClickOverAction()
    {
        mouseholding = true;
    }

    protected override void OnUnclick()
    {
        base.OnUnclick();
        mouseholding = false;
        Animatable.Release();
        InvokeStateCheck();
    }
}
