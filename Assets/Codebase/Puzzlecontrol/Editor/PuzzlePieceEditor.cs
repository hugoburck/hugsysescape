﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PuzzlePiece),true)]
[CanEditMultipleObjects]
public class PuzzlePieceEditor : Editor
{
    private PuzzlePiece[] pieces;
    private Vector2 boxDimension = new Vector2(100,40);
    private Color colorBoxSelected, colorBox;

    private struct FloatingInfoBox
    {
        public readonly string Text;
        public readonly Vector2 Position;
        public readonly Vector2 Dimension;
        public readonly bool Visible;
        public readonly bool Selected;

        public FloatingInfoBox(string text, Vector2 position, Vector2 dimension, bool visible, bool selected)
        {
            Text = text;
            Position = position;
            Dimension = dimension;
            Visible = visible;
            Selected = selected;
        }
        
        public Rect Rect => new Rect(Position,Dimension);
    }

    private void Awake()
    {
        if (pieces == null || pieces.Length == 0)
        {
            pieces = GameObject.FindObjectsOfType<PuzzlePiece>();
        }
        colorBox = Color.white;
        colorBox.a = 0.5f;
        colorBoxSelected = Color.white;
    }

    private void OnSceneGUI()
    {
        if (PuzzlePiece.EnableCustomEditor)
            RenderGUI(GetBoxes(pieces));
    }

    private List<FloatingInfoBox> GetBoxes(PuzzlePiece[] pieces)
    {
        var boxes = new List<FloatingInfoBox>();
        foreach (var piece in pieces)
        {
            var screenPosition = GetScreenPosition(GetWorldPosition(piece));
            boxes.Add(new FloatingInfoBox(
                GetText(piece),
                screenPosition,
                boxDimension,
                GetVisible(screenPosition),
                IsSelected(piece)));
        }

        return boxes;
    }
    
    private Vector3 GetWorldPosition(PuzzlePiece piece)
    {
        return piece.transform.position;
    }

    private Vector3 GetScreenPosition(Vector3 worldPosition)
    {
        return HandleUtility.WorldToGUIPointWithDepth(worldPosition);
    }

    private void RenderGUI(List<FloatingInfoBox> infoBoxes)
    {
        Handles.BeginGUI();

        foreach (var box in infoBoxes)
        {
            if (!box.Visible)
                continue;
            GUI.color = box.Selected ? colorBoxSelected : colorBox;
            GUI.Box(box.Rect, box.Text);
        }

        Handles.EndGUI();
    }

    private bool IsSelected(PuzzlePiece piece)
    {
        return piece.transform == Selection.activeTransform;
    }

    private string GetText(PuzzlePiece piece)
    {
        StringBuilder sb = new StringBuilder(piece.gameObject.name);
        sb.AppendLine();
        sb.AppendLine("no state");
        return sb.ToString();
    }

    private bool GetVisible(Vector3 screenPosition)
    {
        return screenPosition.z > 0;
    }
}
