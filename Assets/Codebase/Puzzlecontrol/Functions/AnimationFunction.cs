﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Functions;
using UnityEngine;

public class AnimationFunction : PuzzleFunction, IRunAsync
{
    [SerializeField] private TransformAnimation transformAnimation = default;
    [SerializeField] private AnimationInputTimer timer = default;
    [SerializeField] private AudioEffectPlayer _audioEffectPlayer = default;
    private IAudioEffectPlayer audioEffectPlayer;

    public bool IsInProgress { get; private set; }

    public void Play(bool fromStart, bool reverse)
    {
        StopCoroutine(Animate(fromStart, reverse));
        StartCoroutine(Animate(fromStart, reverse));
    }

    public void Stop()
    {
        timer.Stop();
    }

    public void Pause()
    {
        timer.Pause();
    }

    private IEnumerator Animate(bool fromStart, bool reverse)
    {
        timer.Reverse = reverse;
        timer.Play(fromStart);
        IsInProgress = true;
        audioEffectPlayer?.Play();
        while (timer.State != StateOfTimer.Running)
        {
            yield return null;
        }

        while (timer.State == StateOfTimer.Running)
        {
            transformAnimation.AnimateAbsolute(timer.CurrentKey);
            yield return null;
        }

        InvokeStateCheck();
        IsInProgress = false;
    }

    private void Awake()
    {
        if (_audioEffectPlayer != null)
            audioEffectPlayer = _audioEffectPlayer;
    }
}
