﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerScreenFunction : PuzzleFunction
{
	[SerializeField] private Material[] screens = default;
	[SerializeField] private MeshRenderer meshRenderer = default;
	[SerializeField] private int materialIndex = default;

	private Material[] mats;

	public void SetScreen(uint index)
	{
		mats[materialIndex] = screens[index];
		meshRenderer.materials = mats;
	}

	public void SetDefaultScreen()
	{
		SetScreen(0);
	}
	
	protected virtual void Awake()
	{
        
    }

    protected virtual void Start()
    {
	    mats = meshRenderer.materials;
    }

    protected virtual void Update()
    {
        
    }
}
