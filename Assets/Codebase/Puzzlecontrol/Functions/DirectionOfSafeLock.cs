﻿namespace Codebase.PuzzleControl.Functions
{
    public enum DirectionOfSafeLock
    {
        Idle,
        Left,
        Right
    }
}