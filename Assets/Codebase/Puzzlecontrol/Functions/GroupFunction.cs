﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class GroupFunction : PuzzleFunction
{
    [SerializeField] private List<PuzzleToState> puzzleStates = null;

    protected void Awake()
    {
        if (puzzleStates == null || puzzleStates.Count == 0)
            Debug.LogError("Piecegroup contains no pieces");
    }

    private bool DoesPiecesContain(PuzzleToState toState)
    {
        return puzzleStates.Contains(toState);
    }


    public bool ArePiecesCorrect
    {
        get
        {
            if (puzzleStates == null)
                return false;

            foreach (var puzzle in puzzleStates)
            {
                if (puzzle.State == StateOfPuzzlePiece.Incorrect)
                    return false;
            }

            return true;
        }
    }
}
