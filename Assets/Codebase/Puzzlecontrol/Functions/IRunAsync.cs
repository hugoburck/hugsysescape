﻿namespace Codebase.PuzzleControl.Functions
{
    public interface IRunAsync
    {
        bool IsInProgress { get; }
    }
}