﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Text;
using UnityEngine;

public class KeypadFunction : PuzzleFunction, ISendMessage
{
    public event Action<object, string> OnMessage;

    [SerializeField]
    private string Passcode = default;

    public int CharacterLimit = 4;

    private string sequence;// { get; private set; }
    public bool CheckAfterInput;
    public bool ResetAfterCheck = true;
    public bool IsMatching => sequence == Passcode;


    public void SetInput(string input)
    {
        if (sequence.Length >= CharacterLimit)
            return;
        
        sequence += input;
        OnMessage?.Invoke(this,sequence);
        if (CheckAfterInput)
            CheckSequence();
    }

    public void ResetSequence()
    {
        sequence = string.Empty;
        OnMessage?.Invoke(this,sequence);
    }

    public void CheckSequence()
    {
        InvokeStateCheck();
        if (ResetAfterCheck)
            ResetSequence();
    }

    private void Start()
    {
        sequence = string.Empty;
    }
}
