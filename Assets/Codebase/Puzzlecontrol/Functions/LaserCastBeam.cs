﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Codebase.Helpers.Extensions;
using UnityEngine;

public class LaserCastBeam
{
    public Transform HitTransform { get; private set; }
    public Vector3 HitPoint { get; private set; }
    public Vector3 HitNormal { get; private set; }
    public Vector3 StartPos { get; }
    public Vector3 StartDir { get; }

    private readonly GameObject beamGObj, hitGObj;
    private const float halfway = 0.5f, maxLength = 100f;

    public LaserCastBeam(GameObject beamGObj, GameObject hitGObj, Vector3 startPos, Vector3 startDir)
    {
        this.beamGObj = Object.Instantiate(beamGObj,beamGObj.transform.parent.transform);
        this.hitGObj = Object.Instantiate(hitGObj,hitGObj.transform.parent.transform);
        this.StartPos = startPos;
        this.StartDir = startDir;
    }

    public void ShootRay()
    {
        Physics.Raycast(StartPos, StartDir, out RaycastHit hit, maxLength,
            LayerMaskManager.GetMask(UnitOfLayer.Default));

        HitTransform = hit.transform;
        HitPoint = hit.point;
        HitNormal = hit.normal;
        Render(StartPos,HitPoint);
    }

    public void DestroyGObjs()
    {
        Object.Destroy(beamGObj);
        Object.Destroy(hitGObj);
    }

    public bool IsHitReflectable => HitTransform != null && HitTransform.TryGetComponent(typeof(ReflectableSurface), out _);

    private void Render(Vector3 startPos, Vector3 endPos)
    {
        hitGObj.transform.position = endPos;
        beamGObj.transform.position = Vector3.Lerp(startPos, endPos, halfway);
        beamGObj.transform.LookAt(endPos);
        beamGObj.transform.localScale = beamGObj.transform.localScale.CopyWithZ(Vector3.Distance(startPos, endPos));

    }
}
