﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCastBeamFactory
{
    private readonly GameObject beamModel, hitModel;

    public LaserCastBeamFactory(GameObject beamModel, GameObject hitModel)
    {
        this.beamModel = beamModel;
        this.hitModel = hitModel;
    }
    
    public LaserCastBeam Create(Transform transform)
    {
        return new LaserCastBeam(beamModel, hitModel, transform.position, transform.TransformDirection(Vector3.forward));
    }

    public LaserCastBeam Create(LaserCastBeam previous)
    {
        Vector3 reflectDir = Vector3.Reflect(previous.StartDir, previous.HitNormal);
        return new LaserCastBeam(beamModel,hitModel,previous.HitPoint,reflectDir);
    }
}
