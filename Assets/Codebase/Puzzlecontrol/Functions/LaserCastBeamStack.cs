﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCastBeamStack
{
    private const int MaxBeams = 9;
    private Stack<LaserCastBeam> laserBeams = new Stack<LaserCastBeam>();
    private readonly LaserCastBeamFactory beamFactory;

    public LaserCastBeamStack(LaserCastBeamFactory factory)
    {
        this.beamFactory = factory;
    }
    
    public void Stack(Transform startTransform)
    {
        EmptyStack();
        laserBeams.Push(beamFactory.Create(startTransform));
        laserBeams.Peek().ShootRay();

        int counter = 0;
        while (counter <= MaxBeams && laserBeams.Peek().IsHitReflectable)
        {
            laserBeams.Push(beamFactory.Create(laserBeams.Peek()));
            laserBeams.Peek().ShootRay();
            counter++;
        }
    }

    public void EmptyStack()
    {
        foreach (var beam in laserBeams)
        {
            beam.DestroyGObjs();
        }
        laserBeams = new Stack<LaserCastBeam>();
    }

    public bool IsTransformInFinalHit(Transform target) => laserBeams.Count > 0 && 
                                                           target == GetFinalHit;

    public Transform GetFinalHit => laserBeams.Count > 0 ? laserBeams.Peek().HitTransform : null;
}
