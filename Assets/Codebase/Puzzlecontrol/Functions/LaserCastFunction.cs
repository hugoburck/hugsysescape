﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LaserCastFunction : PuzzleFunction
{
    [SerializeField] private GameObject beamModel = default;
    [SerializeField] private GameObject hitModel = default;
    [SerializeField] private GameObject targetObj = default;
    public event Action<Transform> OnHitChanged;
    public static event Action<LaserCastFunction, Transform> OnAnyHitChanged;
    private bool isEnabled = true;
    private readonly TriggerComparable<bool> triggerHitTarget = new TriggerComparable<bool>();
    private LaserCastBeamStack beamStack;

    public bool HitTarget { get; private set; }

    public void TurnOn() => isEnabled = true;

    public void TurnOff() => isEnabled = false;
    
    public void Toggle() => isEnabled = !isEnabled;

    public Transform CurrentFinalHit => beamStack.GetFinalHit;

    private Transform previousHit;

    private void Start()
    {
        var beamFactory = new LaserCastBeamFactory(beamModel,hitModel);
        beamStack = new LaserCastBeamStack(beamFactory);
        previousHit = CurrentFinalHit;
    }

    // Update is called once per frame
    private void Update()
    {
        if (isEnabled)
        {
            beamStack.Stack(transform);   
        }
        else beamStack.EmptyStack();

        if (CurrentFinalHit != previousHit)
        {
            OnHitChanged?.Invoke(CurrentFinalHit);
            OnAnyHitChanged?.Invoke(this,CurrentFinalHit);
            previousHit = CurrentFinalHit;
        }
        
        if (triggerHitTarget.OnChange(HitTarget = beamStack.IsTransformInFinalHit(targetObj.transform)))
            InvokeStateCheck();
    }


}
