﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTargetFunction : PuzzleFunction
{
	[Tooltip("If null, will check any laser event")]
	[SerializeField] private LaserCastFunction laserCastFunction = default;

	[SerializeField] private Collider targetCollider = default;
	private readonly TriggerComparable<bool> triggerHit = new TriggerComparable<bool>();

	public bool IsHit { get; private set; }

	private void OnEnable()
	{
		LaserCastFunction.OnAnyHitChanged += LaserCastFunction_OnAnyHitChanged;
	}

	private void LaserCastFunction_OnAnyHitChanged(LaserCastFunction laser, Transform trans)
	{
		if (laserCastFunction == null || laser == laserCastFunction) 
			GotHit(trans == targetCollider.transform);
	}

	private void OnDisable()
	{
		LaserCastFunction.OnAnyHitChanged -= LaserCastFunction_OnAnyHitChanged;
	}

	private void GotHit(bool state)
	{
		if (triggerHit.OnChange(state))
		{
			IsHit = state;
			InvokeStateCheck();
		}
	}

	private void Awake()
	{
		InitializationChecker.CheckImmediate(this, targetCollider);
	}
}
