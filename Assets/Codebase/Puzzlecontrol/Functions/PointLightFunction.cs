﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PointLightFunction : PuzzleFunction
{
    public Light pointLight;

    public void TurnOn()
    {
        SetLightState(true);
    }

    public void TurnOn(Color color)
    {
        pointLight.color = color;
        TurnOn();
    }

    public void TurnOff()
    {
        SetLightState(false);
    }

    public bool IsOn => pointLight.enabled;

    private void SetLightState(bool state)
    {
        pointLight.enabled = state;
        InvokeStateCheck();
    }

    private void Awake()
    {
        if (pointLight == null)
            GetComponent<Light>();
        if (pointLight == null)
            Debug.LogError("Light missing",this);
    }
}
