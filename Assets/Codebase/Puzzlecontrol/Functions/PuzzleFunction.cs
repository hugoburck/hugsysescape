﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;
using Codebase.PuzzleControl;
using Codebase.PuzzleControl.State;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class PuzzleFunction : MonoBehaviour
{
    public event Action OnStateCheck;
    protected void InvokeStateCheck() => OnStateCheck?.Invoke();
}
