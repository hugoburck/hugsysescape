﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using Codebase.PuzzleControl.Animation;
using Codebase.PuzzleControl.Functions;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

//TODO safelock does not reset when turning on ResetSnap, only when resting when sequence has failed.
//need to check when turning before resolving sequence

public class SafeLockFunction : PuzzleFunction 
{
    [SerializeField] private StepAnim stepAnim = default;
    [SerializeField] private int stepAnimResetIndex = default;
    [SerializeField] private int[] stepValues = default;
    [SerializeField] private DirectionOfSafeLock start = default;
    private bool[] stepStates;
    private DirectionOfSafeLock[] stepDirs;
    private int stepCounter, stepOld;
    private DirectionOfSafeLock direction, directionOld;
    private bool sequenceHasFailed;

    // private TriggerComparable<float> triggerAxisDown = new TriggerComparable<float>(),
    //     triggerAxisUp = new TriggerComparable<float>(); //trigger can't publish old value

    public bool IsSequenceCorrect => stepStates[stepStates.Length - 1];

    public void Reset()
    {
        for (int i = 0; i < stepStates.Length; i++)
            stepStates[i] = false;
        
        sequenceHasFailed = true;
        stepCounter = stepOld = 0;
        direction = directionOld = DirectionOfSafeLock.Idle;
    }

    void Start()
    {
        stepStates = new bool[stepValues.Length];
        stepDirs = new DirectionOfSafeLock[stepValues.Length];
        SetupStepDirections();
        sequenceHasFailed = stepAnim.Index != stepAnimResetIndex;
    }

    private void SetupStepDirections()
    {
        for (int i = 0; i < stepDirs.Length; i++)
        {
            if (i == 0)
            {
                stepDirs[i] = start == DirectionOfSafeLock.Idle ? DirectionOfSafeLock.Left : start;
                continue;
            }

            stepDirs[i] = stepDirs[i - 1] == DirectionOfSafeLock.Left ? DirectionOfSafeLock.Right : DirectionOfSafeLock.Left;
        }
    }

    private void OnEnable()
    {
        stepAnim.OnStep += StepAnim_OnStep;
    }

    private void StepAnim_OnStep(object sender, StepAnim.StepAnimEventArgs e)
    {
        if (sequenceHasFailed)
        {
            Reset();
            if (e.index == stepAnimResetIndex)
                sequenceHasFailed = false; //reset override

            return;
        }

        switch (e.movement)
        {
            case MovementOfAnimation.Down:
                stepCounter--;
                break;
            case MovementOfAnimation.Up:
                stepCounter++;
                break;
            default:
                return;
        }

        ResolveDirections();
    }

    private void OnDisable()
    {
        stepAnim.OnStep -= StepAnim_OnStep;
    }

    private void ResolveDirections()
    {
        if (stepCounter < stepOld)
        {
            direction = DirectionOfSafeLock.Left;
            if (directionOld != DirectionOfSafeLock.Left && 
                directionOld != DirectionOfSafeLock.Idle)
            {
                CheckValueAgainstStep();
                stepCounter = -1;
            }
        }

        if (stepCounter > stepOld)
        {
            direction = DirectionOfSafeLock.Right;
            if (directionOld != DirectionOfSafeLock.Right &&
                directionOld != DirectionOfSafeLock.Idle)
            {
                CheckValueAgainstStep();
                stepCounter = 1;
            }
        }

        sequenceHasFailed = direction != stepDirs[CurrentStepIndex] ||
                            IsExceedingCurrent(stepCounter);

        stepOld = stepCounter;
        directionOld = direction;
    }

    private int CurrentStepIndex
    {
        get
        {
            for (int i = 0; i < stepValues.Length; i++)
            {
                if (!stepStates[i])
                    return i;
            }

            return stepValues.Length - 1;
        }
    }

    private void CheckValueAgainstStep()
    {
        if (IsSequenceCorrect)
            return;

        if (IsCorrectStepDirection(directionOld) && IsStepCorrect(stepOld))
            stepStates[CurrentStepIndex] = true;
        else
            Reset();
            
        InvokeStateCheck();
    }

    private bool IsCorrectStepDirection(DirectionOfSafeLock dir)
    {
        return stepDirs[CurrentStepIndex] == dir;
    }

    private bool IsStepCorrect(int step)
    {
        return stepValues[CurrentStepIndex] == Mathf.Abs(step);
    }

    private bool IsExceedingCurrent(int step)
    {
        return stepValues[CurrentStepIndex] < Mathf.Abs(step);
    }
}
