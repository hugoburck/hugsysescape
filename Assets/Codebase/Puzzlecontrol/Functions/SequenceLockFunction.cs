﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceLockFunction : PuzzleFunction
{
    [SerializeField]
    private string Passcode = default;

    public string Sequence;// { get; private set; }
    public bool IsMatching => Sequence == Passcode;
    //Debug
    private bool _isStartingCorrect;

    private bool IsStartingCorrect
    {
        get
        {
            if (Sequence.Length > Passcode.Length)
                return false;
                
            for (int i = 0; i < Sequence.Length; i++)
            {
                if (Sequence[i] != Passcode[i])
                    return false;
            }

            return true;
        }
    }

    public void SetInput(string input)
    {
        //Debug.Log("input! "+input);
        Sequence += input;
        _isStartingCorrect = IsStartingCorrect;
        if (!IsStartingCorrect)
            Sequence = string.Empty;
        InvokeStateCheck();
    }
}
