﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFunction : PuzzleFunction
{
    public StateOfPuzzlePiece StateEnum { get; private set; }
    
    public void SetCorrect()
    {
        StateEnum = StateOfPuzzlePiece.Correct;
        InvokeStateCheck();
    }
    
    public void SetIncorrect()
    {
        StateEnum = StateOfPuzzlePiece.Incorrect;
        InvokeStateCheck();
    }

    public void Toggle()
    {
        if (StateEnum == StateOfPuzzlePiece.Stateless)
            StateEnum = StateOfPuzzlePiece.Incorrect;
        
        if (StateEnum == StateOfPuzzlePiece.Correct)
            StateEnum = StateOfPuzzlePiece.Incorrect;
        else StateEnum = StateOfPuzzlePiece.Correct;
        
        InvokeStateCheck();
    }
}
