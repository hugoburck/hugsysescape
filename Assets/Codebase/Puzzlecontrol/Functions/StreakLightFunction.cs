﻿using System.Collections;
using UnityEngine;

namespace Codebase.PuzzleControl.Functions
{
    public class StreakLightFunction : PuzzleFunction
    {
        [SerializeField] private Material target = default;
        [SerializeField] private MeshRenderer mrenderer = default;
        public int Flashes = 3;
        public bool IsInProgress { get; private set; }
        private Material[] sourceMats;

        public void SetColor(Material mat)
        {
            mrenderer.sharedMaterials = Adjusted(mat);
        }

        public void FlashColor(Material mat)
        {
            StartCoroutine(Flash(Adjusted(mat)));
        }

        public void FlashSetColor(Material mat)
        {
            StartCoroutine(FlashSet(mat));
        }
        
        public IEnumerator FlashColorAsync(Material mat)
        {
            yield return StartCoroutine(Flash(Adjusted(mat)));
        }

        public IEnumerator FlashSetColorAsync(Material mat)
        {
            yield return StartCoroutine(FlashSet(mat));
        }

        private void Start()
        {
            if (mrenderer == null)
                mrenderer = GetComponent<MeshRenderer>();
            if (mrenderer != null)
            {
                sourceMats = mrenderer.sharedMaterials;
            }
        }

        private Material[] Adjusted(Material replacement)
        {
            if (sourceMats == null)
                Start();
            
            Material[] adjusted = new Material[sourceMats.Length];
            for (int i = 0; i < adjusted.Length; i++)
            {
                adjusted[i] = sourceMats[i];
                if (adjusted[i] == target)
                    adjusted[i] = replacement;
            }

            return adjusted;
        }

        private IEnumerator Flash(Material[] adjusted)
        {
            IsInProgress = true;
            int counter = 0;
            while (counter < Flashes)
            {
                mrenderer.sharedMaterials = adjusted;
                yield return new WaitForSeconds(0.1f);
                mrenderer.sharedMaterials = sourceMats;
                yield return new WaitForSeconds(0.1f);
                counter++;
            }
            IsInProgress = false;
        }

        private IEnumerator FlashSet(Material mat)
        {
            yield return StartCoroutine(Flash(Adjusted(mat)));
            SetColor(mat);
        }
    }
}