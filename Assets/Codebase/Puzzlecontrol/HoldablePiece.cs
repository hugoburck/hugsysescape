﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldablePiece : PuzzlePiece
{
    public PositionLerpAnim Animatable;
    public AnimationInputTimer Timer;
    public bool IsHolding { get; private set; }
    
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        //SetState(Timer.IsKeyStoppedAtEnd ? PieceState.Correct : PieceState.Incorrect);
        Animatable.AnimateAbsolute(Timer.CurrentKey);
    }

    protected override void OnClickOverAction()
    {
        //base.Action();
        Timer.PlayForwards();
        IsHolding = true;
        InvokeStateCheck();
    }

    protected override void OnUnclick()
    {
        base.OnUnclick();
        Timer.PlayInReverse();
        IsHolding = false;
        InvokeStateCheck();
    }
}
