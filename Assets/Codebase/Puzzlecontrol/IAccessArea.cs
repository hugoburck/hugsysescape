﻿namespace Codebase.PuzzleControl
{
    public interface IAccessArea
    {
        bool IsWithin { get; }
    }
}