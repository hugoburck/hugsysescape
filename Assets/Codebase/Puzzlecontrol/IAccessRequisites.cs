﻿namespace Codebase.PuzzleControl
{
    public interface IAccessRequisites
    {
        bool IsMeeting { get; }
    }
}