﻿namespace Codebase.PuzzleControl
{
    public interface IActionRequisites
    {
        bool IsMeeting { get; }
    }
}