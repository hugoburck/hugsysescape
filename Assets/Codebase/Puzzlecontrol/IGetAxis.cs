﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGetAxis
{
    float GetAxis { get; }
}
