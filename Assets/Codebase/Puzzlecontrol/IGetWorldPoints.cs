﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGetWorldPoints
{
    Vector3[] Points { get; }
    void Recalculate();
}
