﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Supplies a key to an animator
/// </summary>
public interface IKeySupplier
{
    float CurrentKey { get; }
}
