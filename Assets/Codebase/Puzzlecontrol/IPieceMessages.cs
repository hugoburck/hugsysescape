﻿using System;

namespace Codebase.PuzzleControl
{
    public interface IPieceMessages
    {
        void PushBlock(PuzzlePiece piece);
        void PushAction(PuzzlePiece piece);
        void PushDisplay(PuzzlePiece piece);
        void PullBlock(PuzzlePiece piece);
        void PullAction(PuzzlePiece piece);
        
    }
}