﻿namespace Codebase.PuzzleControl
{
    public interface IReceiveItems
    {
        PuzzlePieceData Data { get; }
        bool IsCorrectItem(InventoryItemData itemData);
        bool HasReward { get; }
        InventoryItemData GetReward { get; }
    }
}