﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRequisiteVisibility
{
    void SetVisibility(bool isAccessible);
}
