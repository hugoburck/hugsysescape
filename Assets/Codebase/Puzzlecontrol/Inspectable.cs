﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(InspectionView))]
public class Inspectable : Interactable
{
    public InspectionView InspectionView { get; private set; }

    [SerializeField]
    private Inspectable parent = null;
    
    public static event Action<Inspectable> OnInspectionChangedEvent;
    public static bool IsFirstInspection;
    private static LinkedList<Inspectable> inspectionStack = new LinkedList<Inspectable>();
    private IInspectableConstraints constraints;

    public static bool IsInspectionChangedThisFrame { get; private set; }

    public static bool BeingInspected(Inspectable Inspectable) => CurrentInspection == Inspectable;

    public bool IsBeingInspected => CurrentInspection == this;

    public static void RemoveCurrentInspection()
    {
        if (IsNotInspecting)
            return;
        
        PopInspection();
    }

    public static bool IsNotInspecting => inspectionStack.Count == 0;

    private static Inspectable CurrentInspection => IsNotInspecting ? null : inspectionStack.Last.Value;

    private bool HasConstraints => constraints != null;

    private bool InStack(Inspectable inspectable) => inspectable != null && inspectionStack.Contains(inspectable);
    
    private bool IsInStack => inspectionStack.Contains(this);

    private static void AddInspection(Inspectable inspectable)
    {
        if (inspectable == null)
        {
            Debug.LogError("Attempted to push null to inspectable stack");
            return;
        }

        // if (inspectable.parent != null && !inspectable.parent.IsBeingInspected)
        // {
        //     Debug.Log("parent not being inspected, returning");
        //     return;
        // }
        
        IsFirstInspection = IsNotInspecting;

        if (!IsFirstInspection)
            RemoveIfDisabled(inspectionStack.Last);

        var node = inspectionStack.Find(inspectable);
        if (node == null)
        {
            inspectionStack.AddLast(inspectable);
        }
        else
        {
            inspectionStack.Remove(node);
            inspectionStack.AddLast(node);
        }
        
        inspectable.SetCollision(false);
        RaiseInspectionChanged();
    }

    private static void RemoveInspection(Inspectable inspectable)
    {
        if (inspectable == null)
        {
            Debug.LogError("Attempted to pull null to inspectable stack");
            return;
        }

        if (IsNotInspecting)
            return;

        IsFirstInspection = false;
        
        if (inspectionStack.Last.Value != inspectable)
            throw new System.Exception("This Inspectable was not being inspected!");
        
        inspectionStack.RemoveLast();
        inspectable.SetCollision(true);
        RaiseInspectionChanged();
    }

    private static void PopInspection()
    {
        RemoveInspection(inspectionStack.Last.Value);
    }

    protected override void Awake()
    {
        base.Awake();
        if (InspectionView == null)
            InspectionView = GetComponent<InspectionView>();
        constraints = GetComponent<IInspectableConstraints>();
    }

    private bool IsCurrentInspection => CurrentInspection == this;

    private static void RemoveIfDisabled(LinkedListNode<Inspectable> node)
    {
        if (!node.Value.gameObject.activeInHierarchy)
            inspectionStack.Remove(node);
    }

    protected override void OnClickOver()
    {
        base.OnClickOver();
        AddInspection(this);
    }

    private bool IsAvailable
    {
        get
        {
            if (IsBeingInspected)
                return false;
            if (HasParent && !IsParentInStack)
                return false;
            if (HasConstraints)
            {
                if (!constraints.HasAccessorAndIsInspecting)
                    return false;
                if (constraints.HasBlockerAndIsInspecting)
                    return false;
            }
            return true;
        }
    }

    public override bool IsClickable => base.IsClickable && IsAvailable;

    protected override void OnRayEnter()
    {
        base.OnRayEnter();
    }

    protected override void OnRayExit()
    {
        base.OnRayExit();
    }

    private void SetCollision(bool state)
    {
        this.Collider.enabled = state;
    }

    private void LateUpdate()
    {
        IsInspectionChangedThisFrame = false;
    }

    private static void RaiseInspectionChanged()
    {
        OnInspectionChangedEvent?.Invoke(CurrentInspection);
        IsInspectionChangedThisFrame = true;
    }

    private bool IsParentBeingInspected => parent != null && parent.IsBeingInspected;
    
    private bool IsParentInStack => HasParent && InStack(parent);

    private bool HasParent => parent != null;
    
}
