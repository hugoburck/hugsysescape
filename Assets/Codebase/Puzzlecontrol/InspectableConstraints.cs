﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public interface IInspectableConstraints
{
	bool HasAccessorAndIsInspecting { get; }
	bool HasBlockerAndIsInspecting { get; }
}

public class InspectableConstraints : MonoBehaviour, IInspectableConstraints
{
	[SerializeField] private Inspectable[] accessors = default;
	[SerializeField] private Inspectable[] blockers = default;

	public bool HasAccessorAndIsInspecting => IsInspectableInspecting(accessors, true);
	public bool HasBlockerAndIsInspecting => IsInspectableInspecting(blockers, false);

	private bool IsInspectableInspecting(Inspectable[] array, bool stateOnEmpty)
	{
		if (array == null || array.Length == 0)
		{
			return stateOnEmpty;
		}

		foreach (var inspectable in array)
		{
			if (inspectable.IsBeingInspected)
				return true;
		}

		return false;
	}
}
