﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

public class InspectablePop : MonoBehaviour
{
    private IDeviceInput deviceInput;
    private IPlayerStateMachine playerStateMachine;

    private void Start()
    {
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
        playerStateMachine = playerStateMachine ?? ServiceLocator.Instance.Resolve(playerStateMachine);
    }

    protected virtual void OnEnable()
    {
        StartCoroutine(SubServices());
    }

    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnEnterInputEvent += DeviceInput_OnEnterInputEvent;
    }

    protected virtual void OnDisable()
    {
        deviceInput.OnEnterInputEvent -= DeviceInput_OnEnterInputEvent;
    }

    private void DeviceInput_OnEnterInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (playerStateMachine.State == StateOfPlayer.InspectingPuzzle && actionOfPlayer == ActionOfPlayer.Cancel)
        {
            Inspectable.RemoveCurrentInspection();
        }
    }
}
