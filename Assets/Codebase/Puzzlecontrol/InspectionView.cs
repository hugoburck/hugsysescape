﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[System.Serializable]
public class InspectionView : MonoBehaviour
{
    public Vector3 CameraPosition { get; private set; }
    public Quaternion CameraRotation { get; private set; }
    public float CameraFOV { get; private set; }
    [SerializeField]
    private GameObject Origin = null,Target = null;
    private Camera cam = null;

    public void ReOrientView()
    {
        GetFromOrigin();
    }

    private void Awake()
    {
        if (Origin == null)
            return;
        cam = Origin.GetComponent<Camera>();
    }

    private void Start()
    {
        if (Application.isPlaying)
            cam.enabled = false;
        GetFromOrigin();
    }

    private void Update()
    {
        if (!Application.isPlaying)
            GetFromOrigin();
    }

    private void GetFromOrigin()
    {
        CameraPosition = Origin.transform.position;
        Origin.transform.LookAt(Target.transform);
        CameraRotation = Origin.transform.rotation;
        CameraFOV = cam.fieldOfView;
    }
}
