﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IInteractAreaRay
{
    bool IsHit(GameObject gameobject);
}

public class InteractAreaRay : MonoBehaviourSingleton<InteractAreaRay>, IInteractAreaRay
{
    private RaycastHit[] hits;
    private Transform camTrans;
    private int layerMask;
    private InitializationChecker init;

    public bool IsHit(GameObject gameobject)
    {
        if (hits == null)
            return false;
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].transform == gameobject.transform)
                return true;
        }
        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        camTrans = PlayerCamera.Instance.Camera.transform;
        layerMask = LayerMaskManager.GetMask(UnitOfLayer.Raycast);
        init = InitializationChecker.Factory(this, camTrans);
    }

    // Update is called once per frame
    void Update()
    {
        if (init.CheckFail)
            return;
        hits = Physics.RaycastAll(camTrans.position, Vector3.down, Mathf.Infinity, layerMask);
    }
}
