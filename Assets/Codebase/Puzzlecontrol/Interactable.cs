﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Cursor;
using Codebase.PlayerControl;
using Codebase.PlayerControl.DeviceInput;
using Codebase.PuzzleControl;
using UnityEngine;

//[RequireComponent(typeof(Collider))]
public abstract class Interactable : MonoBehaviour
{
    public static readonly List<Interactable> Instances = new List<Interactable>();
    public static Interactable CurrentClickableMouseOver { get; private set; }
    public Collider Collider;
    public IGetWorldPoints GetWorldPoints { get; private set; } //old journal pointer code
    private IAccessRequisites accessRequisites;
    private IAccessArea accessArea;
    private IRequisiteVisibility requisiteVisibility;
    private ICursorRaycast cursorRaycast;
    private IDeviceInput deviceInput;
    private IPlayerStateMachine playerStateMachine;
    private ICursorHider cursorHider;
    private bool rayOver;
    private bool clickHold;

    public Interactable Inject(IAccessRequisites requisites, IAccessArea area, ICursorRaycast cursorRaycast)
    {
        this.accessRequisites = requisites;
        this.accessArea = area;
        this.cursorRaycast = cursorRaycast;
        return this;
    }

    public bool HasWorldPoints { get; private set; }

    public static bool HasClickableMouseOver => CurrentClickableMouseOver != null;

    public bool IsInArea => accessArea == null || accessArea.IsWithin;

    public bool HasReadableMeshCollider {
        get
        {
            if (Collider != null && Collider is MeshCollider)
            {
                var mcol = Collider as MeshCollider;
                return mcol.sharedMesh.isReadable;
            }

            return false;
        }
    }

    public MeshCollider GetColliderAsMeshCollider
    {
        get
        {
            if (!HasReadableMeshCollider)
                return null;
            return Collider as MeshCollider;
        }
    }

    public MeshFilter GetMeshFilterFromMeshCollider => !HasReadableMeshCollider ? null : Collider.GetComponent<MeshFilter>();

    public void Click()
    {
        OnClickOver();
    }

    public void Cancel()
    {
        OnCancel();
    }

    public void HandleHitChanged(Transform trans, Collider col)
    {
        rayOver = this.Collider == col;
        if (rayOver && IsClickable)
            OnRayEnter();
        else
            OnRayExit();
        SetVisibility();
    }

    private void SetVisibility()
    {
        if (requisiteVisibility != null)
        {
            requisiteVisibility.SetVisibility(MeetsRequisites);
        }
    }

    public void HandleInput(ActionOfPlayer actionOfPlayer)
    {
        if (actionOfPlayer == ActionOfPlayer.Use && rayOver && IsClickable)
            OnClickOver();

        if (actionOfPlayer == ActionOfPlayer.Cancel)
            OnCancel();
    }

    public virtual bool IsClickable => MeetsRequisites && IsInArea && IsCursorOverUI && !IsLayerDisabled;

    private bool IsLayerDisabled => this.gameObject.layer == LayerMaskManager.GetLayer(UnitOfLayer.Disabled);

    private bool IsCursorOverUI => !InventoryItemView.HasMouseOver;

    private bool MeetsRequisites => accessRequisites?.IsMeeting ?? true;

    protected virtual void Awake()
    {
        accessRequisites = GetComponent<IAccessRequisites>();
        accessArea = GetComponent<IAccessArea>();
        GetWorldPoints = GetComponent<IGetWorldPoints>();
        requisiteVisibility = GetComponent<IRequisiteVisibility>();
        if (Collider == null)
        {
            if ((Collider = GetComponent<Collider>()) == null)
                Debug.Log("This Puzzle has no collider and can't be clicked", this);    
        }
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        cursorRaycast = cursorRaycast ?? ServiceLocator.Instance.Resolve(cursorRaycast);
        deviceInput = deviceInput ?? ServiceLocator.Instance.Resolve(deviceInput);
        playerStateMachine = ServiceLocator.Instance.Resolve(playerStateMachine);
        cursorHider = ServiceLocator.Instance.Resolve(cursorHider);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (Collider != null)
            Collider.enabled = IsClickable;
    }

    protected virtual void OnEnable()
    {
        Instances.Add(this);
        StartCoroutine(SubServices());
    }

    private IEnumerator SubServices()
    {
        yield return null;
        deviceInput.OnEnterInputEvent += DeviceInput_EnterInputEvent;
        deviceInput.OnExitInputEvent += DeviceInput_ExitInputEvent;
        cursorRaycast.OnHitChanged += CursorRaycast_OnHitChanged;
        CursorRaycast_OnHitChanged(cursorRaycast.CurrentHitTransform, cursorRaycast.CurrentHitCollider);
    }

    private void CursorRaycast_OnHitChanged(Transform transform,Collider collider)
    {
        HandleHitChanged(transform, collider);
    }


    protected virtual void OnDisable()
    {
        Instances.Remove(this);
        deviceInput.OnEnterInputEvent -= DeviceInput_EnterInputEvent;
        deviceInput.OnExitInputEvent -= DeviceInput_ExitInputEvent;
        cursorRaycast.OnHitChanged -= CursorRaycast_OnHitChanged;
    }

    private void DeviceInput_EnterInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (IsNotAllowedInputHandling || cursorHider.IsHiding)
            return;
        
        if (!Inspectable.IsInspectionChangedThisFrame)
            HandleInput(actionOfPlayer);

    }

    private void DeviceInput_ExitInputEvent(ActionOfPlayer actionOfPlayer, float intensity)
    {
        if (IsNotAllowedInputHandling || cursorHider.IsHiding)
            return;
        
        if (clickHold && actionOfPlayer == ActionOfPlayer.Use)
        {
            OnUnclick();
            if (rayOver)
                OnUnclickOver();
            clickHold = false;
        }
    }

    private bool IsNotAllowedInputHandling => playerStateMachine.State == StateOfPlayer.Animating ||
                                              playerStateMachine.State == StateOfPlayer.Idle ||
                                              playerStateMachine.State == StateOfPlayer.OpenMenu;

    protected virtual void OnCancel()
    {

    }

    protected virtual void OnClickOver()
    {
        clickHold = true;
    }

    protected virtual void OnUnclick()
    {

    }

    protected virtual void OnUnclickOver()
    {
        
    }

    protected virtual void OnRayEnter()
    {
        CurrentClickableMouseOver = this;
    }

    protected virtual void OnRayExit()
    {
        if (CurrentClickableMouseOver == this)
            CurrentClickableMouseOver = null;
    }
}
