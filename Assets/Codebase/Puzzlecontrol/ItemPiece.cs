﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPiece : PuzzlePiece
{
    public InventoryItemData ItemData;
    public delegate void CollectedPieceHandler(InventoryItemData ItemData);
    public static event CollectedPieceHandler OnCollectedEvent; //Todo static event?

    public bool PickedUp;
    //Reset event?

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        InitializationChecker.CheckImmediate(this, ItemData);
    }

    public void ResetState()
    {
        EnablePiece(true);
    }

    protected override void OnClickOverAction()
    {
        EnablePiece(false);
        InvokeStateCheck();
        OnCollectedEvent?.Invoke(ItemData);
    }

    private void EnablePiece(bool state)
    {
        gameObject.SetActive(state);
        PickedUp = !state;
    }
}
