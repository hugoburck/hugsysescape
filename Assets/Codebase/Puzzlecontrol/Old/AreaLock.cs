﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaLock : IGatedAccess
{
    private readonly AccessArea accessArea;

    public AreaLock(AccessArea accessArea)
    {
        this.accessArea = accessArea;
    }

    public bool Unlocked => InteractAreaRay.Instance.IsHit(accessArea.gameObject);

    public bool Locked => !Unlocked;


   
}
