﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGatedAccess
{
    bool Unlocked { get; }
    bool Locked { get; }
}
