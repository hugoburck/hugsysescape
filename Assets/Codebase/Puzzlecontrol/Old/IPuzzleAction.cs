﻿using System;

public interface IPuzzleAction
{

}

public interface IPuzzleActionCorrect : IPuzzleAction { void Action(); }

public interface IPuzzleActionIncorrect : IPuzzleAction { void Action(); }
