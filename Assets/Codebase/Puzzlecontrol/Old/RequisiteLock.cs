﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequisiteLock : IGatedAccess
{
    private List<PuzzlePieceData> unlock = null;
    private List<PuzzlePieceData> relock = null;

    public RequisiteLock(AccessRequisites accessRequisites)
    {
        // unlock = accessRequisites.UnlockOn;
        // relock = accessRequisites.RelockOn;
    }

    public RequisiteLock Inject(List<PuzzlePieceData> datas)
    {
        this.unlock = datas;
        return this;
    }

    public bool Unlocked
    {
        get { return checkUnlockPieces(unlock) & !checkRelockPieces(relock); }
    }

    private bool checkUnlockPieces(List<PuzzlePieceData> datas)
    {
        if (datas == null || datas.Count == 0)
            return true;
        
        return CheckPieces(datas);
    }
    
    private bool checkRelockPieces(List<PuzzlePieceData> datas)
    {
        if (datas == null || datas.Count == 0)
            return false;

        return CheckPieces(datas);
    }

    private bool CheckPieces(List<PuzzlePieceData> datas)
    {
        foreach (var piece in datas)
        {
            if (piece == null)
                continue;
            if (piece.stateEnum != StateOfPuzzlePiece.Correct)
                return false;
        }

        return true;
    }

    public bool Locked => !Unlocked;

    public AccessOfInteractable Resolved
    {
        get => Unlocked ? AccessOfInteractable.Available : AccessOfInteractable.Unavailable;
    }
}
