﻿using System;
using UnityEngine;

namespace Codebase.PuzzleControl
{
    public class PieceMessages : MonoBehaviour,IPieceMessages
    {
        [SerializeField] private string block = default, action = default, display;

        public static event Action<PuzzlePiece, string> OnMessagePush;
        public static event Action<PuzzlePiece, string> OnMessagePull;

        public void PushBlock(PuzzlePiece piece)
        {
            OnMessagePush?.Invoke(piece,block);
        }

        public void PushAction(PuzzlePiece piece)
        {
            OnMessagePush?.Invoke(piece,action);
        }

        public void PushDisplay(PuzzlePiece piece)
        {
            Debug.LogError("not implemented");
        }

        public void PullBlock(PuzzlePiece piece)
        {
            OnMessagePull?.Invoke(piece,block);
        }

        public void PullAction(PuzzlePiece piece)
        {
            OnMessagePull?.Invoke(piece,action);
        }
    }
}