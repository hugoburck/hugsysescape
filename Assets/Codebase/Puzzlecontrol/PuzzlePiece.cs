﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.CompilerServices;
using Codebase.PuzzleControl;
using Codebase.PuzzleControl.State;
using UnityEngine.Serialization;

public abstract class PuzzlePiece : Interactable
{
    [SerializeField] private Inspectable inspectable;
    public static bool EnableCustomEditor;
    public event Action OnStateCheck;
    protected IActionRequisites ActionRequisites;
    protected IPieceMessages PieceMessages;

    private static List<PuzzlePiece> PuzzlePieces = new List<PuzzlePiece>();

    private bool HasActionRequisites => this.ActionRequisites != null;
    protected bool HasPieceMessages => this.PieceMessages != null;

    protected void InvokeStateCheck() => OnStateCheck?.Invoke();

    public PuzzlePiece Inject(Inspectable inspectable)
    {
        this.inspectable = inspectable;
        return this;
    }

    public PuzzlePiece Inject(IActionRequisites actionRequisites)
    {
        this.ActionRequisites = actionRequisites;
        return this;
    }

    public override bool IsClickable => base.IsClickable && Inspectable.BeingInspected(inspectable);

    protected override void Awake()
    {
        base.Awake();
        PuzzlePieces.Add(this);
        ActionRequisites = GetComponent<IActionRequisites>();
        PieceMessages = GetComponent<IPieceMessages>();
    }

    protected virtual void OnDestroy()
    {
        PuzzlePieces.Remove(this);
    }

    protected override void OnClickOver()
    {
        base.OnClickOver();

        if (!HasActionRequisites || (HasActionRequisites && ActionRequisites.IsMeeting))
        {
            OnClickOverAction();
            if (HasPieceMessages)
                PieceMessages.PushAction(this);
        }
        else if (HasPieceMessages)
        {
            PieceMessages.PushBlock(this);
        }
    }

    protected abstract void OnClickOverAction();

    [ContextMenu("Toggle Custom Editor")]
    private void ToggleCustomEditor()
    {
        EnableCustomEditor = !EnableCustomEditor;
    }
}
