﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


[CreateAssetMenu(fileName = "PuzzlePieceData", menuName = "ScriptableObjects/PuzzlePieceData_", order = 1)]
public class PuzzlePieceData : ScriptableObject
{
    [SerializeField]
    private string Name;
    public StateOfPuzzlePiece stateEnum;
    [SerializeField]
    private string _blockMessage = default;
    [SerializeField]
    private string _actionMessage = default;
    [SerializeField]
    private PuzzlePieceData[] ActionRequisites = new PuzzlePieceData[0];

    public bool HasRequisitesMet
    {
        get
        {
            if (ActionRequisites == null || ActionRequisites.Length == 0) 
                return true;
            for (int i = 0; i < ActionRequisites.Length; i++)
            {
                if(ActionRequisites[i] == null)
                {
                    Debug.Log("Missing requisite");
                    continue;
                }
                if (ActionRequisites[i].stateEnum == StateOfPuzzlePiece.Incorrect)
                    return false;
            }
            return true;
        }
    }

    public void AddRequisite(PuzzlePieceData pieceData)
    {
        if (pieceData == null)
            return;
        int newLength = ActionRequisites.Length + 1;
        PuzzlePieceData[] tmpArray = new PuzzlePieceData[newLength];
        for (int i = 0; i < ActionRequisites.Length; i++)
        {
            tmpArray[i] = ActionRequisites[i];
        }
        tmpArray[ActionRequisites.Length] = pieceData;
        ActionRequisites = tmpArray;
    }

    public string BlockMessage => _blockMessage;
    public string ActionMessage => _actionMessage;
}
