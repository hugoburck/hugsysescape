﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleWorldPoints : MonoBehaviour,IGetWorldPoints
{
    public MeshFilter MeshFilter;
    private Vector3[] points;

    public Vector3[] Points
    {
        get
        {
            if (points == null || points.Length == 0)
                Recalculate();
            return points;
        }
    }

    public void Recalculate()
    {
        Mesh mesh = MeshFilter.sharedMesh;
        Vector3[] vertices = mesh.vertices;
        int vertexCount = mesh.vertexCount;
        
        points = new Vector3[vertexCount];
        for (int i = 0; i < vertexCount; i++)
        {
            points[i] = transform.TransformPoint(vertices[i]);
        }   
    }

    void Awake()
    {
        if (MeshFilter == null)
            MeshFilter = GetComponent<MeshFilter>();
        if (MeshFilter == null)
            Debug.LogError("MeshFilter not found",this);
        
    }

    void Start()
    {
        if (MeshFilter.mesh != null && !MeshFilter.mesh.isReadable)
            Debug.LogError("Mesh not readable",this);
    }
}
