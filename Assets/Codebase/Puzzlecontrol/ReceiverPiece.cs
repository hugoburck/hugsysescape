﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ReceiverPiece : PuzzlePiece, IReceiveItems
{
    [SerializeField] private InventoryItemData itemData = default;
    [SerializeField] private InventoryItemData rewardData = default;
    [SerializeField] private PuzzlePieceData pieceData = default;
    public bool ReceivedCorrectItem { get; private set; }
    
    public override bool IsClickable => base.IsClickable && !ReceivedCorrectItem;

    public PuzzlePieceData Data => pieceData;

    public bool IsCorrectItem(InventoryItemData itemData)
    {
        bool state = itemData == this.itemData;
        
        if (state)
            SetCorrectState();

        return state;
    }

    public bool HasReward => rewardData != null;
    public InventoryItemData GetReward => rewardData;

    protected override void Awake()
    {
        base.Awake();
        InitializationChecker.CheckImmediate(this,pieceData);
    }

    protected override void OnClickOverAction()
    {

    }

    protected override void OnEnable()
    {
        base.OnEnable();
        SolutionChecker.OnSolutionPiece += SolutionChecker_OnSolutionPiece;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        SolutionChecker.OnSolutionPiece -= SolutionChecker_OnSolutionPiece;
    }

    protected virtual void SolutionChecker_OnSolutionPiece(PuzzlePieceData data)
    {
        if (data != pieceData)
            return;

        SetCorrectState();
    }

    private void SetCorrectState()
    {
        ReceivedCorrectItem = true;
        InvokeStateCheck();

        if (HasPieceMessages)
        {
            PieceMessages.PullBlock(this);
            PieceMessages.PullAction(this);  
        }
    }
}
