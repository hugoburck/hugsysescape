﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequisiteVisibility : MonoBehaviour,IRequisiteVisibility
{
    [SerializeField]
    private GameObject root = default;

    private Renderer[] renderers;

    public void SetVisibility(bool isAccessible)
    {
        if (renderers != null)
            foreach (var r in renderers)
            {
                r.enabled = isAccessible;
            }
    }

    protected virtual void Awake()
    {
        if (root == null)
        {
            Debug.LogError("No renderers supplied",this);
            return;
            
        }

        renderers = root.GetComponentsInChildren<Renderer>();
    }
}
