﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl;
using UnityEngine;
using UnityEngine.Serialization;

public class BiDirectionalToState : PuzzleToState
{
    [SerializeField] private BiDirectionalPiece biDirectional = default;
    [SerializeField] private LerpAnim lerpAnim = default;
    [SerializeField] private StateOfLerp stateOfLerpState = default;
    

    private void OnEnable()
    {
        biDirectional.OnStateCheck += BiDirectionalOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        biDirectional.OnStateCheck -= BiDirectionalOnOnStateCheck;
    }

    private void BiDirectionalOnOnStateCheck()
    {
        StateOfPuzzlePiece local = StateOfPuzzlePiece.Incorrect;

        if (stateOfLerpState == StateOfLerp.InBetween && lerpAnim.KeyInBetween)
            local = StateOfPuzzlePiece.Correct;

        if (stateOfLerpState == StateOfLerp.AtBegin && lerpAnim.KeyAtBegin)
            local = StateOfPuzzlePiece.Correct;

        if (stateOfLerpState == StateOfLerp.AtEnd && lerpAnim.KeyAtEnd)
            local = StateOfPuzzlePiece.Correct;

        State = local;
    }
}
