﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ClickableToState : PuzzleToState
{
    [SerializeField] private ClickablePiece clickable = default;

    private void OnEnable()
    {
        clickable.OnStateCheck += PuzzlePieceOnCLickAction;
    }
    
    private void OnDisable()
    {
        clickable.OnStateCheck -= PuzzlePieceOnCLickAction;
    }

    private void PuzzlePieceOnCLickAction()
    {
        StartCoroutine(OneTimeCorrect());
    }

    private IEnumerator OneTimeCorrect()
    {
        State = StateOfPuzzlePiece.Correct;
        yield return null;
        State = StateOfPuzzlePiece.Incorrect;
    }
}
