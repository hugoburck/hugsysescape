﻿namespace Codebase.PuzzleControl.State
{
    public interface IHaveState
    {
        StateOfPuzzlePiece State { get; }
    }
}