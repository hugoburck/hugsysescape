﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemToState : PuzzleToState
{
    [SerializeField] private ItemPiece itemPiece = default;
    
    private void OnEnable()
    {
        itemPiece.OnStateCheck += ItemPieceOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        //itemPiece.OnStateCheck -= ItemPieceOnOnStateCheck; //this stateholder will handle events even when disabled
    }

    private void ItemPieceOnOnStateCheck()
    {
        Debug.Log(itemPiece.PickedUp);
        State = itemPiece.PickedUp ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
