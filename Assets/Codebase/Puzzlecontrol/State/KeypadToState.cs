﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeypadToState : PuzzleToState
{
    [SerializeField] private KeypadFunction keypadFunction = default;

    private void OnEnable()
    {
        keypadFunction.OnStateCheck += KeypadFunctionOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        keypadFunction.OnStateCheck -= KeypadFunctionOnOnStateCheck;
    }

    private void KeypadFunctionOnOnStateCheck()
    {
        State = keypadFunction.IsMatching ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
