﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserCastToState : PuzzleToState
{
    [SerializeField] private LaserCastFunction laserCastFunction = default;

    private void OnEnable()
    {
        laserCastFunction.OnStateCheck += LaserCastFunctionOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        laserCastFunction.OnStateCheck -= LaserCastFunctionOnOnStateCheck;
    }

    private void LaserCastFunctionOnOnStateCheck()
    {
        State = laserCastFunction.HitTarget ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
