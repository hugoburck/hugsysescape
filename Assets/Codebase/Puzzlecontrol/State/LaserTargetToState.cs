﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserTargetToState : PuzzleToState
{
	[SerializeField] private LaserTargetFunction laserTargetFunction = default;
	
	protected virtual void Awake()
	{
		InitializationChecker.CheckImmediate(this, laserTargetFunction);
	}

	private void OnEnable()
	{
		laserTargetFunction.OnStateCheck += LaserTargetFunction_OnStateCheck;
	}

	private void LaserTargetFunction_OnStateCheck()
	{
		State = laserTargetFunction.IsHit ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
	}

	private void OnDisable()
	{
		laserTargetFunction.OnStateCheck -= LaserTargetFunction_OnStateCheck;
	}
}
