﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiToState : PuzzleToState
{
    [SerializeField] private PuzzleToState[] States = default;
    
    private void OnEnable()
    {
        OnAnyStateChanged += PuzzleStateOnOnAnyStateChanged;
    }
    
    private void OnDisable()
    {
        OnAnyStateChanged -= PuzzleStateOnOnAnyStateChanged;
    }

    private void PuzzleStateOnOnAnyStateChanged(PuzzleToState toState)
    {
        if (toState == this)
            return;
        
        State = ArePiecesCorrect ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
    
    private bool ArePiecesCorrect
    {
        get
        {
            if (States.Length == 0)
                return true;
            
            foreach (var state in States)
            {
                if (state.State == StateOfPuzzlePiece.Incorrect || state.State == StateOfPuzzlePiece.Stateless)
                    return false;
            }

            return true;
        }
    }
}
