﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Codebase.PuzzleControl.State;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class PuzzleToState : MonoBehaviour, IHaveState
{
    [SerializeField] private StateOfPuzzlePiece state;

    public static event Action<PuzzleToState> OnAnyStateChanged; 
    public event Action<StateOfPuzzlePiece> OnStateChanged;
    public event Action<StateOfPuzzlePiece> OnStateSet;
    
    public StateOfPuzzlePiece State 
    { 
        get => state;
        protected set
        {
            if (state != value)
            {
                state = value;
                OnStateChanged?.Invoke(state);
                OnAnyStateChanged?.Invoke(this);
            }
            OnStateSet?.Invoke(state);
        }
    }
}
