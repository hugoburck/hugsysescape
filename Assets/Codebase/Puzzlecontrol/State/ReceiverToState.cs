﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.State;
using UnityEngine;

public class ReceiverToState : PuzzleToState
{
    [SerializeField] private ReceiverPiece receiver = default;

    private void OnEnable()
    {
        receiver.OnStateCheck += ReceiverOnOnStateCheck;
    }

    private void OnDisable()
    {
        receiver.OnStateCheck -= ReceiverOnOnStateCheck;
    }

    private void ReceiverOnOnStateCheck()
    {
        State = receiver.ReceivedCorrectItem ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
    
}
