﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeLockToState : PuzzleToState
{
    [SerializeField] private SafeLockFunction safeLock = default;

    private void OnEnable()
    {
        safeLock.OnStateCheck += SafeLockOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        safeLock.OnStateCheck -= SafeLockOnOnStateCheck;
    }
    
    private void SafeLockOnOnStateCheck()
    {
        State = safeLock.IsSequenceCorrect ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
