﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class SequenceLockToState : PuzzleToState
{
    [SerializeField] private SequenceLockFunction sequenceLockFunction = default;

    private void OnEnable()
    {
        sequenceLockFunction.OnStateCheck += SequenceLockFunctionOnOnStateCheck;
    }
    private void OnDisable()
    {
        sequenceLockFunction.OnStateCheck -= SequenceLockFunctionOnOnStateCheck;
    }

    private void SequenceLockFunctionOnOnStateCheck()
    {
        State = sequenceLockFunction.IsMatching ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
