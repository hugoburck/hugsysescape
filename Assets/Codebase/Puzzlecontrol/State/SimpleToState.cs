﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleToState : PuzzleToState
{
    [SerializeField] private SimpleFunction simpleFunction = default;

    private void OnEnable()
    {
        simpleFunction.OnStateCheck += SimpleFunctionOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        simpleFunction.OnStateCheck -= SimpleFunctionOnOnStateCheck;
    }

    private void SimpleFunctionOnOnStateCheck()
    {
        State = simpleFunction.StateEnum;
    }
}
