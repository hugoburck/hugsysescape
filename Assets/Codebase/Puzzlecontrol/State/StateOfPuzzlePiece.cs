﻿public enum StateOfPuzzlePiece
{
    Stateless,
    Incorrect,
    Correct
}
