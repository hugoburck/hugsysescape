﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerToState : PuzzleToState
{
    [SerializeField] private AnimationInputTimer timer = default;

    protected void Awake()
    {
        InitializationChecker.CheckImmediate(timer);
    }
    
    // Start is called before the first frame update
    public bool IsCorrect 
    {
        get
        {
            return timer.IsKeyStoppedAtEnd;
        } 
    }

    // Update is called once per frame
    private void Update() //upgrade to eventlistener
    {
        State = IsCorrect ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
