﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleableToState : PuzzleToState
{
    [SerializeField] private ToggleablePiece toggleablePiece = default;
    public bool Reverse;
    
    private void OnEnable()
    {
        toggleablePiece.OnStateCheck += ToggleablePieceOnOnStateCheck;
    }
    
    private void OnDisable()
    {
        toggleablePiece.OnStateCheck -= ToggleablePieceOnOnStateCheck;
    }

    private void ToggleablePieceOnOnStateCheck()
    {
        if (Reverse)
            State = toggleablePiece.IsGoingToEnd ? StateOfPuzzlePiece.Incorrect : StateOfPuzzlePiece.Correct;
        else State = toggleablePiece.IsGoingToEnd ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }

    private void Start()
    {
        ToggleablePieceOnOnStateCheck();
    }
}
