﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Animation;
using UnityEngine;

public class TransformPropertyToState : PuzzleToState
{
    public PropertyOfTransform Property;
    public ScopeOfTransform Scope;
    public Vector3 Value;
    public float Leeway = 0.01f;

    public bool IsCorrect => IsWithinLeeway(GetProperty());

    private Vector3 GetProperty()
    {
        if (Property == PropertyOfTransform.Position)
        {
            if (Scope == ScopeOfTransform.Local)
                return transform.localPosition;
            if (Scope == ScopeOfTransform.World)
                return transform.position;
        }
        
        if (Property == PropertyOfTransform.Rotation)
        {
            if (Scope == ScopeOfTransform.Local)
                return transform.localEulerAngles;
            if (Scope == ScopeOfTransform.World)
                return transform.eulerAngles;
        }

        if (Property == PropertyOfTransform.Scale)
        {
            if (Scope == ScopeOfTransform.Local)
                return transform.localScale;
            if (Scope == ScopeOfTransform.World)
                return transform.lossyScale;
        }

        return Vector3.zero;
    }

    private bool IsWithinLeeway(Vector3 prop)
    {
        return Vector3.Distance(prop, Value) <= Leeway;
    }

    private void Update()
    {
        State = IsCorrect ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
