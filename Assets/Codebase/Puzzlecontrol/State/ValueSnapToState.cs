﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValueSnapToState : PuzzleToState
{
    [SerializeField] private ValueSnap valueSnap = default;
    public int index;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        State = valueSnap.IsSnapped && valueSnap.SnappedAtIndex == index ? StateOfPuzzlePiece.Correct : StateOfPuzzlePiece.Incorrect;
    }
}
