﻿namespace Codebase.PuzzleControl
{
    public enum StateOfLerp
    {
        InBetween, 
        AtBegin, 
        AtEnd
    }
}