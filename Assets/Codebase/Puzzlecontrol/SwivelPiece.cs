﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwivelPiece : PuzzlePiece
{
    public AxisAnimInput HorizontalInput, VerticalInput;
    public RotationLerpAnim HorizontalLerp, VerticalLerp;
    private bool mouseHolding;

    protected override void OnEnable()
    {
        base.OnEnable();
        HorizontalInput.OnPlayerAction += InputKeyOnHorizontalPlayerAction;
        VerticalInput.OnPlayerAction += InputKeyOnVerticalPlayerAction;
    }


    protected override void OnDisable()
    {
        base.OnDisable();
        HorizontalInput.OnPlayerAction -= InputKeyOnHorizontalPlayerAction;
        VerticalInput.OnPlayerAction -= InputKeyOnVerticalPlayerAction;
    }

    private void InputKeyOnHorizontalPlayerAction()
    {
        if (!mouseHolding)
            return;
        
        HorizontalLerp.AnimateRelative(HorizontalInput.CurrentKey);
    }

    private void InputKeyOnVerticalPlayerAction()
    {
        if (!mouseHolding)
            return;
        
        VerticalLerp.AnimateRelative(VerticalInput.CurrentKey);
    }
    
    protected override void OnClickOverAction()
    {
        mouseHolding = true;
    }

    protected override void OnUnclick()
    {
        base.OnUnclick();
        mouseHolding = false;
        HorizontalLerp.Release();
        VerticalLerp.Release();
        InvokeStateCheck();
    }
}
