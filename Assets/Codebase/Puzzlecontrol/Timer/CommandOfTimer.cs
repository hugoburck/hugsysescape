﻿public enum CommandOfTimer
{
    Idle,
    Run,
    RunFromStart,
    Pause,
    Stop
}
