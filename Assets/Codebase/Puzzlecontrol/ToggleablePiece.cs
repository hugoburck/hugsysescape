﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleablePiece : PuzzlePiece
{
    public TransformAnimation Animatable;
    public AnimationInputTimer Timer;
    public bool IsGoingToEnd { get; private set; }
    
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        Animatable.AnimateAbsolute(Timer.CurrentKey);
    }

    protected override void OnClickOverAction()
    {
        if (Timer.IsKeyStoppedAtBegin)
        {
            Timer.PlayForwards();
            IsGoingToEnd = true;
        }
        else
        {
            Timer.PlayInReverse();
            IsGoingToEnd = false;
        }
        InvokeStateCheck();

    }
}
