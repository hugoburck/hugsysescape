﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class ActiveOnGameState : MonoBehaviour
{
    [SerializeField] protected StateOfGame stateOfGame = default;
    private IGameStateMachine gameStateMachine;

    protected virtual void Awake()
    {
        gameStateMachine = FindObjectOfType<GameStateMachine>();
    }

    protected virtual void Start()
    {
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
        GameStateMachine_OnStateChanged(gameStateMachine.State);
    }

    private void GameStateMachine_OnStateChanged(StateOfGame state)
    {
        SetActive(state == this.stateOfGame);
    }

    protected abstract void SetActive(bool state);

}
