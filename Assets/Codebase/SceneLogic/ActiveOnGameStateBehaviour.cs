﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using UnityEngine;

public class ActiveOnGameStateBehaviour : ActiveOnGameState
{
	[SerializeField] private Behaviour behaviour = default;
	
	protected override void Awake()
	{
		base.Awake();
		InitializationChecker.CheckImmediate(this, behaviour);
	}

    protected override void SetActive(bool state)
    {
	    behaviour.enabled = state;
    }
}
