﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using UnityEngine;

public class ActiveOnGameStateGameObject : ActiveOnGameState
{
	[SerializeField] private GameObject gameObjectRoot = default;
	
	protected override void Awake()
	{
		base.Awake();
		InitializationChecker.CheckImmediate(this, gameObjectRoot);
	}

    protected override void SetActive(bool state)
    {
	    gameObjectRoot.SetActive(state);
    }
}
