﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.PlayerControl;
using UnityEngine;

public abstract class ActiveOnPlayerState : MonoBehaviour
{
	[SerializeField] protected StateOfPlayer stateOfPlayer = default;
	private IPlayerStateMachine playerStateMachine;

	protected virtual void Awake()
	{
		playerStateMachine = FindObjectOfType<PlayerStateMachine>();
	}

	protected virtual void Start()
	{
		playerStateMachine.OnChanged += GameStateMachine_OnStateChanged;
		GameStateMachine_OnStateChanged(playerStateMachine.State);
	}

	private void GameStateMachine_OnStateChanged(StateOfPlayer state)
	{
		SetActive(state == this.stateOfPlayer);
	}

	protected abstract void SetActive(bool state);
}
