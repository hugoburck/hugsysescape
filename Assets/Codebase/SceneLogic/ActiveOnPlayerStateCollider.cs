﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ActiveOnPlayerStateCollider : ActiveOnPlayerState
{
	[SerializeField] private Collider col = default;
	
	protected override void Awake()
	{
		base.Awake();
		InitializationChecker.CheckImmediate(this, col);
	}

	protected override void SetActive(bool state)
	{
		col.enabled = state;
	}
}
