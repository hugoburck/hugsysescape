﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class CharacterHitDetection : MonoBehaviour, IEmitAction
{
    public static event Action<GameObject> OnHit;
    public event Action OnAction;
    
    private bool active = false;
    private PlayerCharacter playerCharacter;
    private Collider characterController;
    private IImageFader imageFader;

    public bool IsHit { get; private set; }

    public void InvokeAction()
    {
        OnAction?.Invoke();
    }

    public void Activate()
    {
        active = true;
    }
    
    public void Deactivate()
    {
        active = false;
    }

    private void Awake()
    {
        playerCharacter = PlayerCharacter.Instance;
    }

    private void Start()
    {
        characterController = playerCharacter.CharacterController;
        imageFader = ServiceLocator.Instance.Resolve(imageFader);
    }

    private IEnumerator CountDownTillActive()
    {
        yield return new WaitForSeconds(3);
        active = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other != characterController)
            return;

        if (!active || imageFader.IsInTransition)
            return;

        IsHit = true;
        InvokeAction();
        OnHit?.Invoke(this.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other != characterController)
            return;

        IsHit = false;
    }
}
