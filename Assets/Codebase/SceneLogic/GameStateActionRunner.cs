﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class GameStateActionRunner : MonoBehaviour
{
    [SerializeField] private StateOfGame stateMode = default;
    [SerializeField] private GameAction action = default;
    private IGameStateMachine gameStateMachine;

    protected void Awake()
    {
        gameStateMachine = FindObjectOfType<GameStateMachine>();
    }

    protected virtual void OnEnable()
    {
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
    }

    private void GameStateMachine_OnStateChanged(StateOfGame stateMode)
    {
        if (stateMode == this.stateMode && action != null)
            action.Action();
    }

    protected virtual void OnDisable()
    {
        gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
    }
}
