﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using UnityEngine;

namespace Codebase.SceneLogic
{
    public interface ILoadAlphaRoom
    {
        int CurrentSceneIndex { get; } //TODO should be deprecated when proper scenetitles are available
        void LoadNext();
        void LoadFromBegin();
        bool IsLoading { get; }
        event Action<int> OnLoaded;
    }

    public class LoadAlphaRoom : MonoBehaviour, ILoadAlphaRoom
    {
        
        [SerializeField] private SceneListerData sceneListerData = default;
        private ISceneLoader sceneLoader;
        private IImageFader imageFader;

        public event Action<int> OnLoaded;
        public bool IsLoading { get; private set; }
        public int CurrentSceneIndex { get; private set; } = -1; //TODO should be deprecated when proper scenetitles are available

        public void LoadNext()
        {
            if (IsLoading)
                return;
            
            if (IsOnLastNode)
                return;

            StartCoroutine(UnloadThenLoadNext());
        }

        public void LoadFromBegin()
        {
            if (IsLoading)
                return;

            CurrentSceneIndex = -1;
            LoadNext();
        }

        private IEnumerator UnloadThenLoadNext()
        {
            imageFader.FadeIn();
            while (imageFader.IsInTransition)
                yield return null;
            
            IsLoading = true;
            if (CurrentSceneIndex >= 0)
            {
                sceneLoader.Unload(sceneListerData.SceneNameByIndex(CurrentSceneIndex));
                while (sceneLoader.IsInProgress)
                    yield return null;
            }
            
            sceneLoader.Load(sceneListerData.SceneNameByIndex(++CurrentSceneIndex));
            while (sceneLoader.IsInProgress)
                yield return null;

            IsLoading = false;
            OnLoaded?.Invoke(CurrentSceneIndex);
            imageFader.FadeOut(1,true);
        }

        private void Awake()
        {
            ServiceLocator.Instance.Register<ILoadAlphaRoom>(this);
        }

        private void Start()
        {
            sceneLoader = ServiceLocator.Instance.Resolve(sceneLoader);
            imageFader = ServiceLocator.Instance.Resolve(imageFader);
        }

        private bool IsOnFirstNode => CurrentSceneIndex == 0;
        private bool IsOnLastNode => CurrentSceneIndex == sceneListerData.SceneCount - 1;
    }
}