﻿using System.Collections;
using System.Collections.Generic;
using Codebase.SceneLogic;
using UnityEngine;

public class LoadAlphaRoomProxy : MonoBehaviour
{
    private LoadAlphaRoom loadAlphaRoom;
    
    // Start is called before the first frame update
    private void Awake()
    {
        Find();
    }

    private void Find()
    {
        loadAlphaRoom = FindObjectOfType<LoadAlphaRoom>();
    }

    public void Register(LoadAlphaRoom loadAlphaRoom)
    {
        this.loadAlphaRoom = loadAlphaRoom;
    }

    public void LoadNext()
    {
        if (HasLoader) loadAlphaRoom.LoadNext();
    }

    public void LoadFromBegin()
    {
        if (HasLoader) loadAlphaRoom.LoadFromBegin();
    }

    private bool HasLoader
    {
        get
        {
            if (loadAlphaRoom == null)
                Awake();
            
            if (loadAlphaRoom != null) 
                return true;
            
            Debug.LogError("loader wasn't found", this);
            return false;

        }
    }
    
}
