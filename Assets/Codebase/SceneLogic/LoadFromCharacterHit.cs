﻿using System.Collections;
using System.Collections.Generic;
using Codebase.SceneLogic;
using UnityEngine;

public class LoadFromCharacterHit : MonoBehaviour
{
    [SerializeField] private LoadAlphaRoom loadAlphaRoom = default;
    
    // Start is called before the first frame update
    void OnEnable()
    {
        CharacterHitDetection.OnHit += CharacterHitDetectionOnOnHit;
    }
    
    void OnDisable()
    {
        CharacterHitDetection.OnHit -= CharacterHitDetectionOnOnHit;
    }

    private void CharacterHitDetectionOnOnHit(GameObject obj)
    {
        loadAlphaRoom.LoadNext();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
