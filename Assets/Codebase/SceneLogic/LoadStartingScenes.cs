﻿using System.Collections;
using System.Collections.Generic;
using Codebase.SceneLogic;
using UnityEngine;

public class LoadStartingScenes : MonoBehaviour
{
	[SerializeField] private SceneLoader sceneloader = default;
	[SerializeField] private UnitOfScene unitOfScene = default;

	public void Load()
	{
		sceneloader.Load(unitOfScene);
	}
	
	protected virtual void Awake()
	{
		InitializationChecker.CheckImmediate(this, sceneloader);
	}

    protected virtual void Start()
    {
	    Load();
    }

    protected virtual void Update()
    {
        
    }
}
