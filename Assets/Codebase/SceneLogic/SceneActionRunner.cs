﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

public class SceneActionRunner : MonoBehaviour
{
    [SerializeField] private MethodOfMonoBehaviourEvent method = MethodOfMonoBehaviourEvent.Start;
    [SerializeField] private GameAction action = default;

    private void Awake()
    {
        if (method == MethodOfMonoBehaviourEvent.Awake)
            action.Action();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        if (method == MethodOfMonoBehaviourEvent.Start)
            action.Action();
    }
}
