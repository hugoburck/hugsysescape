﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Codebase.SceneLogic
{
	public static class SceneDictionary
	{
		private static Dictionary<UnitOfScene, string> dictionary = new Dictionary<UnitOfScene, string>
		{
			{UnitOfScene.Alpha, "Alpha"},
			{UnitOfScene.AR_01_button, "AR_01_button"},
			{UnitOfScene.AR_02_box, "AR_02_box"},
			{UnitOfScene.AR_03_locked, "AR_03_locked"},
			{UnitOfScene.AR_04_button_combo, "AR_04_button_combo"},
			{UnitOfScene.AR_05_button_broken, "AR_05_button_broken"},
			{UnitOfScene.AR_06_diallock_drawers, "AR_06_diallock_drawers"},
			{UnitOfScene.AR_07_sequencelock, "AR_07_sequencelock"},
			{UnitOfScene.AR_08_safelock, "AR_08_safelock"},
			{UnitOfScene.AR_09_keypad, "AR_09_keypad"},
			{UnitOfScene.AR_99_ending, "AR_99_ending"}
		};

		public static string GetName(UnitOfScene unitOfScene)
		{
			if (IsValid(unitOfScene))
				return dictionary[unitOfScene];
			
			return String.Empty;
		}

		public static bool IsValid(UnitOfScene unitOfScene)
		{
			if (dictionary.ContainsKey(unitOfScene))
				return true;

			Debug.LogWarning("SceneName was not found in dictionary");
			return false;
		}
	}
}