﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[ExecuteInEditMode]
public class SceneListerBehaviour : MonoBehaviour
{
    [SerializeField] private SceneListerData[] sceneListerDatas = default;
    [SerializeField] private bool update;

    private void InitializeAll()
    {
        if (sceneListerDatas == null || sceneListerDatas.Length == 0)
            Debug.LogWarning("SceneListerData missing");
        else
            foreach (var sceneListerData in sceneListerDatas)
            {
                sceneListerData.Initialize();
            }
    }

    void Update()
    {
        if (!update)
            return;

        update = false;
        InitializeAll();
        Save();
        Debug.Log("scenelister updated",this);
    }

    private void Save()
    {
#if UNITY_EDITOR
        UnityEditor.Undo.RecordObjects(sceneListerDatas, "sceneListerUpdate");
#endif
    }
}
