﻿using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.IO;
using System.Linq;

[CreateAssetMenu(fileName = "SceneListerData", menuName = "ScriptableObjects/SceneListerData", order = 1)]
public class SceneListerData : ScriptableObject
{
    [HideInInspector]
    [SerializeField] private string[] scenesInBuild;

    [HideInInspector]
    [SerializeField] private string[] sceneNames = default;

    //These hidden fields should be available outside of the editor
    
#if UNITY_EDITOR
    [SerializeField] private SceneAsset[] sceneAssets = default;
#endif

    public void Initialize()
    {
        if (Application.isPlaying)
            return;
#if UNITY_EDITOR
        CopyNames();
        CheckScenesInBuild();
#endif
    }

#if UNITY_EDITOR
    private void CopyNames()
    {
        sceneNames = new string[sceneAssets.Length];
        for (int i = 0; i < sceneAssets.Length; i++)
        {
            if (sceneAssets[i] == null)
                continue;
            
            sceneNames[i] = sceneAssets[i].name;
        }
    }

    private void CheckScenesInBuild()
    {
        scenesInBuild = Array.ConvertAll(EditorBuildSettings.scenes, s => Path.GetFileNameWithoutExtension(s.path));

        string names = string.Empty;
        int nameCount = 0;
        foreach (var sceneName in sceneNames)
        {
            if (!scenesInBuild.Contains(sceneName))
            {
                names += sceneName + ", ";
                nameCount++;
            }
        }

        if (nameCount > 0)
        {
            names = names.Remove(names.Length - 2);
            string text = nameCount > 1 ? "scenes are" : "scene is";
            Debug.LogWarning($"{nameCount} {text} not in build: {names}");
        }
    }
#endif

    public int SceneCount => sceneNames.Length;

    public string SceneNameByIndex(int index)
    {
        if (index >= 0 && index < sceneNames.Length)
            return sceneNames[index];
        
        return string.Empty;
    }
}
