﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Codebase.SceneLogic
{
	public interface ISceneLoader
	{
		event Action OnLoadingStart;
		event Action OnLoadingDone;
		event Action OnUnloadingStart;
		event Action OnUnloadingDone;
		float Progress { get; }
		bool IsInProgress { get; }
		void Load(UnitOfScene unitOfScene);
		void Load(string sceneName);
		void Unload(UnitOfScene unitOfScene);
		void Unload(string sceneName);
		void UnloadAllInDictionaryButActive();
	}

	public class SceneLoader : MonoBehaviour, ISceneLoader
	{
		public static SceneLoader Instance;
		public event Action OnLoadingStart;
		public event Action OnLoadingDone;
		public event Action OnUnloadingStart;
		public event Action OnUnloadingDone;
		public float Progress { get; private set; }
		public bool IsInProgress { get; private set; }

		private Scene Active;
	
		//TODO should work with multiple scenes (params)
	
		public void Load(UnitOfScene unitOfScene)
		{
			if (SceneDictionary.IsValid(unitOfScene))
				StartCoroutine(LoadRoutine(SceneDictionary.GetName(unitOfScene)));
		}

		public void Load(string sceneName)
		{
			StartCoroutine(LoadRoutine(sceneName));
		}
	
		public void Unload(UnitOfScene unitOfScene)
		{
			if (SceneDictionary.IsValid(unitOfScene))
				StartCoroutine(UnloadRoutine(SceneDictionary.GetName(unitOfScene)));
		}

		public void Unload(string sceneName)
		{
			StartCoroutine(UnloadRoutine(sceneName));
		}

		public void UnloadAllInDictionaryButActive()
		{
			foreach (UnitOfScene sceneEnum in Enum.GetValues(typeof(UnitOfScene)))
			{
				Scene scene = SceneManager.GetSceneByName(SceneDictionary.GetName(sceneEnum));
				if (scene == Active )
					continue;
				
				if (scene.IsValid() && scene.isLoaded)
					StartCoroutine(UnloadRoutine(scene.name));
			}
		}

		private IEnumerator LoadRoutine(string levelName)
		{
			OnLoadingStart?.Invoke();
			if (SceneManager.GetSceneByName(levelName).isLoaded)
			{
				Debug.Log("Scene was already loaded",this);
				OnLoadingDone?.Invoke();
				yield break;
			}
		
			IsInProgress = true;
			var op = SceneManager.LoadSceneAsync(levelName,LoadSceneMode.Additive);
			while (!op.isDone)
			{
				Progress = op.progress;
				yield return null;
			}

			IsInProgress = false;
			OnLoadingDone?.Invoke();
		}

		private IEnumerator UnloadRoutine(string levelName)
		{
			OnUnloadingStart?.Invoke();
			if (!SceneManager.GetSceneByName(levelName).isLoaded)
			{
				Debug.Log("Scene wasn't load",this);
				OnUnloadingDone?.Invoke();
				yield break;
			}
		
			IsInProgress = true;
			var op = SceneManager.UnloadSceneAsync(levelName);
			while (!op.isDone)
			{
				Progress = op.progress;
				yield return null;
			}

			IsInProgress = false;
			OnUnloadingDone?.Invoke();
		}

		private void Awake()
		{
			if (Instance == null)
				Instance = this;
			else 
				Debug.Log("Instance already exists");
			
			ServiceLocator.Instance.Register<ISceneLoader>(this);
		}

		private void Start()
		{
			Active = SceneManager.GetActiveScene();
		}
	}
}
