﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using Codebase.SceneLogic;
using UnityEngine;
using UnityEngine.Serialization;

public class SceneLoaderActionRunner : MonoBehaviour
{
	[Tooltip("Leave null to find")]
	[SerializeField] private SceneLoader sceneloader = default;
	[SerializeField] private GameAction onLoadingDone = default;
	[SerializeField] private GameAction onLoadingStart = default;
	[SerializeField] private GameAction onUnloadingDone = default;
	[SerializeField] private GameAction onUnloadingStart = default;

	private ISceneLoader SceneLoaderApi => sceneloader;
	
	protected virtual void Awake()
	{
		if (sceneloader == null)
			sceneloader = FindObjectOfType<SceneLoader>();
	}

	private void OnEnable()
	{
		SceneLoaderApi.OnLoadingDone += SceneLoaderApi_OnLoadingDone;
		SceneLoaderApi.OnLoadingStart += SceneLoaderApi_OnLoadingStart;
		SceneLoaderApi.OnUnloadingDone += SceneLoaderApi_OnUnloadingDone;
		SceneLoaderApi.OnUnloadingStart += SceneLoaderApi_OnUnloadingStart;
	}

	private void SceneLoaderApi_OnUnloadingStart()
	{
		if (onUnloadingStart != null)
			onUnloadingStart.Action();
	}

	private void SceneLoaderApi_OnUnloadingDone()
	{
		if (onUnloadingDone != null)
			onUnloadingDone.Action();
	}

	private void SceneLoaderApi_OnLoadingStart()
	{
		if (onLoadingStart != null)
			onLoadingStart.Action();
	}

	private void SceneLoaderApi_OnLoadingDone()
	{
		if (onLoadingDone != null)
			onLoadingDone.Action();
	}
	
	private void OnDisable()
	{
		SceneLoaderApi.OnLoadingDone -= SceneLoaderApi_OnLoadingDone;
		SceneLoaderApi.OnLoadingStart -= SceneLoaderApi_OnLoadingStart;
		SceneLoaderApi.OnUnloadingDone -= SceneLoaderApi_OnUnloadingDone;
		SceneLoaderApi.OnUnloadingStart -= SceneLoaderApi_OnUnloadingStart;
	}
}
