﻿public enum SynchronityOfSceneLoad
{
    Synchronized,
    Asynchronized
}