﻿namespace Codebase.SceneLogic
{
    public enum UnitOfScene
    {
        AR_01_button,
        AR_02_box,
        AR_03_locked,
        AR_04_button_combo,
        AR_05_button_broken,
        AR_06_diallock_drawers,
        AR_07_sequencelock,
        AR_08_safelock,
        AR_09_keypad,
        AR_99_ending,
        Alpha
    }
}