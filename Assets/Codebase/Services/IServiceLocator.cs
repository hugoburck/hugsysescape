﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IServiceLocator
{
    void Register<T>(T service);
    T Resolve<T>(T service) where T : class;
    event Action<Type> OnServiceAdded;
    string[] RegisteredServices { get; }
}
