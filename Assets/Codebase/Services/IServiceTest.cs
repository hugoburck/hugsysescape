﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IServiceTest
{
    void DisplayDebugMessage(string message);
}
