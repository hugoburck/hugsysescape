﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceConsumerTest : MonoBehaviour
{
	[SerializeField] private string message = "Hello world";
	private IServiceTest serviceTest;

	public void Inject(IServiceTest serviceTest)
	{
		this.serviceTest = serviceTest;
	}
	
	protected virtual void Awake()
	{
	}

    protected virtual void Start()
    {
	    Inject(ServiceLocator.Instance.Resolve(serviceTest));
        serviceTest.DisplayDebugMessage(message);
    }

    protected virtual void Update()
    {
        
    }
}
