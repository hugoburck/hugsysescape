﻿using System.Collections;
using System.Collections.Generic;
using Codebase.PlayerControl.DeviceInput;
using UnityEngine;

/// <summary>
/// Tries to find a service to register before any monobehaviour is awoken.
/// Has execution priority.
/// </summary>
public sealed class ServiceFinder : MonoBehaviour
{
	private IServiceLocator serviceLocator;
	
	private void Awake()
	{
        serviceLocator = ServiceLocator.Instance;
        Register<DeviceInput,IDeviceInput>();
		//serviceLocator.Register<IDeviceInput>(FindObjectOfType<DeviceInput>());
    }

	private void Register<T, I>() where T : MonoBehaviour, I
	{
		I service = FindObjectOfType<T>();
		if (service != null)
			serviceLocator.Register(service);
		else Debug.LogWarning($"Service of {nameof(T)} as {nameof(I)} not found",this);
	}
}
