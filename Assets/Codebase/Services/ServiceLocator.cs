﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Register services on Awake. Resolve services on Start. Remove services on OnDestroy. Delay usage of services in OnEnable by one frame.
/// </summary>
public class ServiceLocator : IServiceLocator
{
    public event Action<Type> OnServiceAdded;
    private static ServiceLocator _instance;
    private Dictionary<string, object> services;
    
    public static IServiceLocator Instance => _instance ?? (_instance = new ServiceLocator());

    private ServiceLocator()
    {
        services = new Dictionary<string, object>();
    }

    public void Register<T>(T service)
    {
        string name = typeof(T).Name;
        if (services.ContainsKey(name))
        {
            Debug.LogWarning($"Tried to register Service {name} twice");
            return;
        }
        
        services.Add(name,service);
        OnServiceAdded?.Invoke(typeof(T));
    }

    public T Resolve<T>(T service = null) where T : class
    {
        string name = typeof(T).Name;
        if (!services.ContainsKey(name))
        {
            Debug.Log($"Requested Service {name} isn't registered (yet)");
            return null;
        }

        return services[name] as T;
    }
    
    public void Remove<T>(T service = null) where T : class
    {
        string name = typeof(T).Name;
        if (!services.ContainsKey(name))
        {
            Debug.LogWarning($"Removal request of Service {name} which isn't registered");
            return;
        }

        services.Remove(name);
    }

    public string[] RegisteredServices => services.Keys.ToArray();

    private bool IsServiceRegistered<T>(T service) => services.ContainsKey(typeof(T).Name);

}
