﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceLocatorBehaviour : MonoBehaviour
{
	private IServiceLocator serviceLocator;
	[SerializeField] private string[] services = default;
	
	protected virtual void OnEnable()
	{
		serviceLocator.OnServiceAdded += ServiceLocator_OnServiceAdded;
    }

	private void ServiceLocator_OnServiceAdded(Type type)
	{
		services = serviceLocator.RegisteredServices;
	}

	protected virtual void OnDisable()
	{
		serviceLocator.OnServiceAdded -= ServiceLocator_OnServiceAdded;
	}

	protected virtual void Awake()
	{
		serviceLocator = ServiceLocator.Instance;
	}
}
