﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceTest : MonoBehaviour, IServiceTest
{
	protected virtual void Awake()
	{
        ServiceLocator.Instance.Register<IServiceTest>(this);
    }

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }

    public void DisplayDebugMessage(string message)
    {
	    Debug.Log(message,this);
    }
}
