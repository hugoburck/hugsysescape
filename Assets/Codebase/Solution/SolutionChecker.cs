﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Codebase.PuzzleControl;

public class SolutionChecker : MonoBehaviourSingleton<SolutionChecker> //TODO extract interface, remove static events
{
    public SolutionListData SolutionList;
    public static event Action<InventoryItemData> OnSolutionItem;
    public static event Action<PuzzlePieceData> OnSolutionPiece;
    public static event Action<InventoryItemData> OnSolutionReward;
    public static event Action<bool> OnSolutionCorrect;
    public bool DebugInfo;

    private IEnumerable<SolutionItemData> solutionItemList;
    private IEnumerable<SolutionPieceData> solutionPieceList;

    public SolutionChecker Inject(List<SolutionData> solutions)
    {
        SolutionList.Solutions = solutions.ToArray();
        return this;
    }

    private void OnEnable()
    {
        InventoryItemView.OnItemDrop += InventoryItemView_OnItemDrop;
    }

    private void OnDisable()
    {
        InventoryItemView.OnItemDrop -= InventoryItemView_OnItemDrop;
    }

    private void InventoryItemView_OnItemDrop(InventoryItemData itemData)
    {
        if (InventoryItemView.HasMouseOver)
        {
            RaiseIfItemOnItemSolution(itemData, InventoryItemView.CurrentMouseOver.ItemData);
            return;
        }
        
        if (InventoryItemInspection.HasMouseOver)
        {
            RaiseIfItemOnItemSolution(itemData, InventoryItemInspection.CurrentMouseOver.ItemData);
            return;
        }

        if (Interactable.HasClickableMouseOver && Interactable.CurrentClickableMouseOver is IReceiveItems receiver)
        {
            RaiseIfReceiverConfirms(receiver, itemData);
            return;
        }
        
        OnSolutionCorrect?.Invoke(false);
    }

    public void RaiseIfReceiverConfirms(IReceiveItems receiver, InventoryItemData itemData)
    {
        bool correct = false;
        if (receiver.IsCorrectItem(itemData))
        {
            OnSolutionItem?.Invoke(itemData);
            correct = true;
            if (receiver.HasReward)
                OnSolutionReward?.Invoke(receiver.GetReward);
        }
        
        OnSolutionCorrect?.Invoke(correct);
    }

    public void RaiseIfItemOnItemSolution(InventoryItemData itemData, InventoryItemData otherItemData)
    {
        bool correct = false;
        foreach (SolutionItemData solution in solutionItemList)
        {
            if ((solution.ItemData == itemData && solution.OtherItemData == otherItemData) ||
                (solution.OtherItemData == itemData && solution.ItemData == otherItemData))
            {
                RaiseSolutionEvent(solution);
                correct = true;
            }
        }
        
        OnSolutionCorrect?.Invoke(correct);
    }

    public void RaiseIfItemOnPieceSolution(InventoryItemData itemData, PuzzlePieceData data)
    {
        bool correct = false;
        foreach (SolutionPieceData solution in solutionPieceList)
            if (solution.ItemData == itemData && solution.PuzzlePieceData == data)
            {
                RaiseSolutionEvent(solution);
                correct = true;
            }
        
        OnSolutionCorrect?.Invoke(correct);
    }

    private void RaiseSolutionEvent(SolutionItemData solution)
    {
        if (DebugInfo)
            Debug.Log("Item on Item solved",solution);
        
        OnSolutionItem?.Invoke(solution.OtherItemData);
        RaiseBaseItemAndRewardEvent(solution);
    }

    private void RaiseSolutionEvent(SolutionPieceData solution)
    {
        if (DebugInfo)
            Debug.Log("Item on Piece solved",solution);
        
        OnSolutionPiece?.Invoke(solution.PuzzlePieceData);
        RaiseBaseItemAndRewardEvent(solution);
    }

    private void RaiseBaseItemAndRewardEvent(SolutionData solution)
    {
        OnSolutionItem?.Invoke(solution.ItemData);
        if (solution.RewardItemData != null)
        {
            if (DebugInfo)
                Debug.Log("Reward added",solution);
            
            OnSolutionReward?.Invoke(solution.RewardItemData);
        }
    }

    private void Awake()
    {
        solutionItemList = SolutionList.Solutions.Where((s) => s is SolutionItemData).Cast<SolutionItemData>();
        solutionPieceList = SolutionList.Solutions.Where((s) => s is SolutionPieceData).Cast<SolutionPieceData>();
    }

    // Start is called before the first frame update
    void Start()
    {
        CheckSolutionsForErrors();
    }

    private void CheckSolutionsForErrors()
    {
        foreach (SolutionItemData solution in solutionItemList)
        {
            if (solution.ItemData == null || solution.OtherItemData == null)
                Debug.Log("Solution is incomplete", solution);
        }

        foreach (SolutionPieceData solution in solutionPieceList)
        {
            if (solution.ItemData == null || solution.PuzzlePieceData == null)
                Debug.Log("Solution is incomplete", solution);
        }
    }
}
