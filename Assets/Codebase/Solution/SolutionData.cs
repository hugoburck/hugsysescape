﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SolutionData : ScriptableObject
{
    public InventoryItemData ItemData;
    public InventoryItemData RewardItemData;
}
