﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SolutionItemData", menuName = "ScriptableObjects/SolutionItemData", order = 2)]
public class SolutionItemData : SolutionData
{
    public InventoryItemData OtherItemData;
}
