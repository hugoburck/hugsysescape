﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SolutionListData", menuName = "ScriptableObjects/SolutionListData", order = 2)]
public class SolutionListData : ScriptableObject
{
    public SolutionData[] Solutions;
}
