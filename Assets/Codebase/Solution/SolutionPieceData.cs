﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SolutionPieceData", menuName = "ScriptableObjects/SolutionPieceData", order = 2)]
public class SolutionPieceData : SolutionData
{
    public PuzzlePieceData PuzzlePieceData;
}
