﻿using System;
using Codebase.Application;

public interface ISplashNodeRunnerManager
{
    event Action<StateOfGame> OnStart;
    event Action<StateOfGame> OnEnd;
    void Register(SplashNodeRunner runner);
    void Unregister(SplashNodeRunner runner);
}