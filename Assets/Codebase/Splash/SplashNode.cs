﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Fade;
using UnityEngine;

public class SplashNode : MonoBehaviour
{
    [SerializeField] private GameObject gobj = default;
    public int seconds = 3;
    private IImageFader imageFader = default;

    public bool IsDone { get; private set; }
    
    public void Play()
    {
        StartCoroutine(PlayRoutine());
    }

    private IEnumerator PlayRoutine()
    {
        Setup();
        while (imageFader.IsInTransition)
            yield return null;
        
        yield return new WaitForSeconds(seconds);
        
        imageFader.FadeIn();
        while (imageFader.IsInTransition)
            yield return null;
        
        Teardown();
    }

    private void Setup()
    {
        SetActive(true);
        IsDone = false;
        imageFader.FadeOut();
    }

    private void Teardown()
    {
        SetActive(false);
        IsDone = true;
    }

    private void Awake()
    {

    }

    private void Start()
    {
        imageFader = ServiceLocator.Instance.Resolve(imageFader);
        Teardown();
    }

    private void SetActive(bool state)
    {
        gobj.SetActive(state);
    }
    
}
