﻿using System;
using System.Collections;
using System.Threading;
using Codebase.Application;
using Codebase.Fade;
using Codebase.PuzzleControl.Actions;
using UnityEngine;
using UnityEngine.Serialization;

public class SplashNodeRunner : MonoBehaviour
{
    [SerializeField] private GameObject background = default;
    [SerializeField] private SplashNode[] splashNodes = default;
    [SerializeField] private StateOfGame stateOfGame = default;
    public event Action<StateOfGame> OnStart, OnEnd;
    private IImageFader imageFader;
    private IGameStateMachine gameStateMachine;
    private ISplashNodeRunnerManager manager;

    public void Run(int index = 0)
    {
        Setup();
        StartCoroutine(RunRoutine(index));
    }

    public bool IsDone;// { get; private set; }

    private void Awake()
    {
        if (splashNodes == null || NodeCount == 0)
            splashNodes = GetComponentsInChildren<SplashNode>();

        gameStateMachine = FindObjectOfType<GameStateMachine>();
        manager = SplashNodeRunnerManager.Instance;

        //InitializationChecker.CheckImmediate(this, background, splashNodes); //TODO: Checker can't read array of monobehaviours
    }

    private void OnEnable()
    {
        manager.Register(this);
        gameStateMachine.OnStateChanged += GameStateMachine_OnStateChanged;
    }

    private void GameStateMachine_OnStateChanged(StateOfGame stateMode)
    {
        if (stateMode == stateOfGame)
            Run();
    }

    private void OnDisable()
    {
        manager.Unregister(this);
        gameStateMachine.OnStateChanged -= GameStateMachine_OnStateChanged;
    }

    private int NodeCount => splashNodes?.Length ?? 0;

    private IEnumerator RunRoutine(int i)
    {
        splashNodes[i].Play();
        while (!splashNodes[i].IsDone)
            yield return null;

        if (i + 1 < splashNodes.Length)
            Run(++i);
        else Teardown();
    }

    private void Setup()
    {
        background.SetActive(true);
        IsDone = false;
        imageFader.FadeIn(0);
        OnStart?.Invoke(stateOfGame);
    }

    private void Teardown()
    {
        background.SetActive(false);
        IsDone = true;
        OnEnd?.Invoke(stateOfGame);
    }
    
    // Start is called before the first frame update
    private void Start()
    {
        imageFader = ServiceLocator.Instance.Resolve(imageFader);
    }
}
