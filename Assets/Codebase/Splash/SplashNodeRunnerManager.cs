﻿using System;
using System.Collections.Generic;
using Codebase.Application;
using Codebase.Helpers;

public class SplashNodeRunnerManager : Singleton<SplashNodeRunnerManager>, ISplashNodeRunnerManager
{
    public event Action<StateOfGame> OnStart;
    public event Action<StateOfGame> OnEnd;
    private List<SplashNodeRunner> runners;

    public SplashNodeRunnerManager()
    {
        runners = new List<SplashNodeRunner>();
    }
    
    public void Register(SplashNodeRunner runner)
    {
        if (!runners.Contains(runner))
        {
            runners.Add(runner);
            runner.OnStart += Runner_OnStart;
            runner.OnEnd += Runner_OnEnd;
        }   
    }

    public void Unregister(SplashNodeRunner runner)
    {
        if (runners.Contains(runner))
        {
            runners.Remove(runner);
            runner.OnStart -= Runner_OnStart;
            runner.OnEnd -= Runner_OnEnd;
        }
    }

    private void Runner_OnStart(StateOfGame state)
    {
        OnStart?.Invoke(state);
    }
    
    private void Runner_OnEnd(StateOfGame state)
    {
        OnEnd?.Invoke(state);
    }
}