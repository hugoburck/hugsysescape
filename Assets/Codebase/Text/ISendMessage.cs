﻿using System;

namespace Codebase.Text
{
    public interface ISendMessage
    {
        event Action<object, string> OnMessage;
    }
}