﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Text;
using UnityEngine;

public class MessageFromEvent : MonoBehaviour
{
    [SerializeField] private MessageToText messageToText = default;
    [SerializeField] private MonoBehaviour iSendMessage = default;
    private ISendMessage sendMessage;

    private bool IsSendMessageValid => sendMessage != null;

    private void Awake()
    {
        if (iSendMessage is ISendMessage sendMessage)
            this.sendMessage = sendMessage;
        
        if (!IsSendMessageValid)
            Debug.LogWarning("Interface not found",this);
        
        if (messageToText == null)
            Debug.LogWarning("Missing",this);
    }

    private void OnEnable()
    {
        if (IsSendMessageValid)
            sendMessage.OnMessage += SendMessageOnOnMessage;
    }
    
    private void OnDisable()
    {
        if (IsSendMessageValid)
            sendMessage.OnMessage -= SendMessageOnOnMessage;
    }

    private void SendMessageOnOnMessage(object source, string message)
    {
        messageToText.SetText(message);
    }
}
