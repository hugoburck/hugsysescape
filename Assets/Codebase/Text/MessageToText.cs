﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MessageToText : MonoBehaviour
{
    public abstract void SetText(string message);
}