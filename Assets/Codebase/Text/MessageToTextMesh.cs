﻿using UnityEngine;

public class MessageToTextMesh : MessageToText
{
    [SerializeField] private TextMesh textMesh = default;
    
    public override void SetText(string message)
    {
        if (textMesh == null)
            return;

        textMesh.text = message;
    }
}