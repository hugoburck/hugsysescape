﻿public enum EventOfPointer
{
    None = 0,
    Enter,
    Exit,
    Down,
    Up,
    Click
}