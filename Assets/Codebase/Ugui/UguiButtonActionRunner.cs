﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.PuzzleControl.Actions;
using UnityEngine;

[RequireComponent(typeof(UguiButtonEventHandler))]
public class UguiButtonActionRunner : MonoBehaviour
{
	[SerializeField] private GameAction action = default;
	private UguiButtonEventHandler buttonEventHandler;
	
	protected virtual void Awake()
	{
		buttonEventHandler = GetComponent<UguiButtonEventHandler>();
		buttonEventHandler.OnButtonClick += ButtonEventHandler_OnButtonClick;
	}

	private void ButtonEventHandler_OnButtonClick()
	{
		if (action != null)
			action.Action();
	}
}
