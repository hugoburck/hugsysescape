﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UguiButtonActivity : MonoBehaviour //TODO Wat doet dit?
{
	[SerializeField] private bool active;
	public event Action<bool> OnChanged;
	private readonly TriggerComparable<bool> triggerActive = new TriggerComparable<bool>();

	public void Activate()
	{
		active = true;
	}

	public void Deactivate()
	{
		active = false;
	}

	public void SetActive(bool state)
	{
		active = state;
	}
	
	protected virtual void Awake()
	{
        
    }

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
	    if (triggerActive.OnChange(active))
	    {
		    OnChanged?.Invoke(active);
			Debug.Log(active);
	    }
    }
}
