﻿using System;
using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using UnityEngine;

[RequireComponent(typeof(UguiButtonEventHandler))]
[RequireComponent(typeof(RectTransform))]
public class UguiButtonAnimation : MonoBehaviour
{
	private UguiButtonEventHandler buttonEventHandler;
	private RectTransform rectTransform;
	private Vector3 originScale;
	private Quaternion orgRotation;
	private const float DownScaleMod = 0.8f, EnterScaleMod = 1.2f, TransitionTime = 0.1f;
	private bool isDown, isEnter;
	private IEnumerator transitionAsync;

	protected virtual void Awake()
	{
		buttonEventHandler = GetComponent<UguiButtonEventHandler>();
		rectTransform = GetComponent<RectTransform>();
		originScale = transform.localScale;
		orgRotation = transform.localRotation;
		transitionAsync = Transition(Vector3.zero, Vector3.zero);
	}

	private void OnEnable()
	{
		buttonEventHandler.OnButtonDown += ButtonEventHandler_OnButtonDown;
		buttonEventHandler.OnButtonUp += ButtonEventHandler_OnButtonUp;
		buttonEventHandler.OnButtonEnter += ButtonEventHandler_OnButtonEnter;
		buttonEventHandler.OnButtonExit += ButtonEventHandler_OnButtonExit;
		StopStartTransition(Transition(Vector3.zero, originScale,1f));
	}

	private void ButtonEventHandler_OnButtonExit()
	{
		isEnter = false;
		if (!isDown)
		{
			StopStartTransition(Transition(transform.localScale, originScale));
		}
	}

	private void ButtonEventHandler_OnButtonEnter()
	{
		isEnter = true;
		if (!isDown)
		{
			StopStartTransition(Transition(transform.localScale, Vector3.one * EnterScaleMod));
		}
	}

	private void ButtonEventHandler_OnButtonUp()
	{
		isDown = false;
		StopStartTransition(Transition(transform.localScale, originScale));
	}

	private void ButtonEventHandler_OnButtonDown()
	{
		isDown = true;
		StopStartTransition(Transition(transform.localScale, Vector3.one * DownScaleMod));
	}

	private void OnDisable()
	{
		buttonEventHandler.OnButtonDown -= ButtonEventHandler_OnButtonDown;
		buttonEventHandler.OnButtonUp -= ButtonEventHandler_OnButtonUp;
		buttonEventHandler.OnButtonEnter -= ButtonEventHandler_OnButtonEnter;
		buttonEventHandler.OnButtonExit -= ButtonEventHandler_OnButtonExit;
		//StopStartTransition(Transition(transform.localScale, Vector3.zero));
	}

	private void StopStartTransition(IEnumerator routine)
	{
		StopCoroutine(transitionAsync);
		transitionAsync = routine;
		StartCoroutine(transitionAsync);
	}

	private IEnumerator Transition(Vector3 from, Vector3 to, float time = TransitionTime)
	{
		float counter = 0;
		while (counter < 1)
		{
			counter = Mathf.Clamp01(counter + Time.deltaTime / time);
			transform.localScale = Vector3.Lerp(from, to, counter.CosineDamper());
			yield return null;
		}
	}

}
