﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

/// <summary>
/// This scripts detects mouseclicks on the attached gameobject
/// </summary>
[RequireComponent(typeof(Button))]
public class UguiButtonEventHandler : MonoBehaviour, 
    IPointerDownHandler, 
    IPointerClickHandler, 
    IPointerUpHandler, 
    IPointerEnterHandler, 
    IPointerExitHandler
{
    public event Action OnButtonClick, OnButtonDown, OnButtonUp, OnButtonEnter, OnButtonExit;
    private Button button;

    /// <summary>
    /// Detects if the GameObject was clicked down and up in succession
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (button.interactable)
            OnButtonClick?.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (button.interactable)
            OnButtonDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (button.interactable)
            OnButtonUp?.Invoke();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (button.interactable)
            OnButtonEnter?.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (button.interactable)
            OnButtonExit?.Invoke();
    }

    private void Awake()
    {
        button = GetComponent<Button>();
    }
}