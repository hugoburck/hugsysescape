﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UguiButtonInteractivity : MonoBehaviour
{
	[SerializeField] private Button[] buttons = default;

	public void Enable()
	{
		SetButtons(true);
	}

	public void Disable()
	{
		SetButtons(false);
	}

	private void SetButtons(bool state)
	{
		if (buttons == null || buttons.Length == 0)
		{
			Debug.LogWarning("No buttons found here");
			return;
		}
		
		foreach (var button in buttons)
		{
			button.interactable = state;
		}
	}
	
	protected virtual void Awake()
	{
		buttons = GetComponentsInChildren<Button>();
	}

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {
        
    }
}
