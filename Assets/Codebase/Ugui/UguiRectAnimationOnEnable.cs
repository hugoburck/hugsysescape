﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UguiRectAnimationOnEnable : MonoBehaviour
{
	private Vector3 originScale;
	private const float seconds = 0.2f;
	private const int waitFrames = 1;
	
	protected virtual void Awake()
	{
		originScale = transform.localScale;
	}

    protected virtual void OnEnable()
    {
	    StartCoroutine(ScaleUp(seconds));
    }

    private IEnumerator ScaleUp(float seconds)
    {
	    for (int i = 0; i < waitFrames; i++)
	    {
		    yield return null;
	    }
	    
	    float timer = 0f;
	    while (timer < 1f)
	    {
		    timer += Time.deltaTime / seconds;
		    transform.localScale = Vector3.Lerp(Vector3.zero, originScale, timer.CosineDamper());
		    yield return null;
	    }
    }

    protected virtual void Update()
    {
        
    }
}
