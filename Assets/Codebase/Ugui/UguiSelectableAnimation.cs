﻿using System.Collections;
using System.Collections.Generic;
using Codebase.Helpers.Extensions;
using Codebase.Ugui;
using UnityEngine;

[RequireComponent(typeof(UguiSelectableEventHandler))]
[RequireComponent(typeof(RectTransform))]
public class UguiSelectableAnimation : MonoBehaviour
{
	[SerializeField] private RectTransform rectTransform = default;
	private UguiSelectableEventHandler selectableEventHandler;
	private Vector3 originScale;
	private Quaternion orgRotation;
	private const float DownScaleMod = 0.8f, EnterScaleMod = 1.2f, TransitionTime = 0.1f, EnableTime = 0.4f;
	private bool isDown, isEnter;
	private IEnumerator transitionAsync;

	protected virtual void Awake()
	{
		if (rectTransform == null)
			rectTransform = GetComponent<RectTransform>();
		
		selectableEventHandler = GetComponent<UguiSelectableEventHandler>();
		originScale = rectTransform.localScale;
		orgRotation = rectTransform.localRotation;
		transitionAsync = Transition(Vector3.zero, Vector3.zero);
	}

	private void OnEnable()
	{
		selectableEventHandler.OnDown += EventHandler_OnDown;
		selectableEventHandler.OnUp += EventHandler_OnUp;
		selectableEventHandler.OnEnter += EventHandler_OnEnter;
		selectableEventHandler.OnExit += EventHandler_OnExit;
		//StopStartTransition(Transition(Vector3.zero, originScale,EnableTime));
	}

	private void EventHandler_OnDown()
	{
		isDown = true;
		StopStartTransition(Transition(rectTransform.localScale, Vector3.one * DownScaleMod));
	}

	private void EventHandler_OnUp()
	{
		isDown = false;
		if (!isEnter)
			StopStartTransition(Transition(rectTransform.localScale, originScale));
		else StopStartTransition(Transition(rectTransform.localScale, Vector3.one * EnterScaleMod));
	}

	private void EventHandler_OnEnter()
	{
		isEnter = true;
		if (!isDown)
		{
			StopStartTransition(Transition(rectTransform.localScale, Vector3.one * EnterScaleMod));
		}
	}

	private void EventHandler_OnExit()
	{
		isEnter = false;
		if (!isDown)
		{
			StopStartTransition(Transition(rectTransform.localScale, originScale));
		}
	}

	private void OnDisable()
	{
		selectableEventHandler.OnDown -= EventHandler_OnDown;
		selectableEventHandler.OnUp -= EventHandler_OnUp;
		selectableEventHandler.OnEnter -= EventHandler_OnEnter;
		selectableEventHandler.OnExit -= EventHandler_OnExit;
	}

	private void StopStartTransition(IEnumerator routine)
	{
		StopCoroutine(transitionAsync);
		transitionAsync = routine;
		StartCoroutine(transitionAsync);
	}

	private IEnumerator Transition(Vector3 from, Vector3 to, float time = TransitionTime)
	{
		float counter = 0;
		while (counter < 1)
		{
			counter = Mathf.Clamp01(counter + Time.deltaTime / time);
			rectTransform.localScale = Vector3.Lerp(from, to, counter.CosineDamper());
			yield return null;
		}
	}
}
