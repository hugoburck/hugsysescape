﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Codebase.Ugui
{
	//[RequireComponent(typeof(Selectable))]
	public class UguiSelectableEventHandler : MonoBehaviour, IPointerDownHandler, 
		IPointerClickHandler, 
		IPointerUpHandler, 
		IPointerEnterHandler, 
		IPointerExitHandler
	{
		public event Action OnClick, OnDown, OnUp, OnEnter, OnExit;
		private Selectable selectable;
		protected virtual void Awake()
		{
			selectable = GetComponent<Selectable>();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			OnClick?.Invoke();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			OnDown?.Invoke();
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			OnUp?.Invoke();
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			OnEnter?.Invoke();
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			OnExit?.Invoke();
		}
	}
}
