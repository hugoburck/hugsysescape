﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UguiSelectableOnClick : MonoBehaviour
{
    private Selectable selectable;
    private EventSystem eventSystem;
    protected virtual void Awake()
    {
        selectable = GetComponent<Selectable>();
        eventSystem = EventSystem.current;
    }
    
    public void OnClick()
    {
        Debug.Log("Hello");
        EventSystem.current.SetSelectedGameObject(null);
    }
}
