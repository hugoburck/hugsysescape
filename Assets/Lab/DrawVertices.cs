﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawVertices : MonoBehaviour
{
    public bool Render;
    public float radius = 0.01f;
    private Vector3[] vertices;

    // Start is called before the first frame update
    void Awake()
    {
        vertices = GetComponent<MeshFilter>().mesh.vertices;
        //verticesWorld = new Vector3[vertices.Length];
        //CalcVertices();
    }

    // Update is called once per frame
    void Update()
    {
        if (Render)
        {
            //CalcVertices();
        }
    }

    // private void CalcVertices()
    // {
    //     for (int i = 0; i < vertices.Length; i++)
    //     {
    //         verticesWorld[i] = transform.TransformPoint(vertices[i]);
    //     }
    //     
    // }

    private void OnDrawGizmosSelected()
    {
        if (Render)
        {
            for (int i = 0; i < vertices.Length; i++)
            {
                Gizmos.DrawSphere(transform.TransformPoint(vertices[i]),radius);
            }    
        }
    }
}
