﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


namespace Lab
{
    [ExecuteInEditMode]
    public class GetTransforms : MonoBehaviour
    {
        public bool Trigger;
        public List<Transform> Transforms;
        //public List<GameObject> Games;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!Trigger)
                return;

            FillList();

            Trigger = false;
        }

        private void FillList()
        {
            Transforms = new List<Transform>();
            //Games = new List<GameObject>();
            //foreach (Transform trans in transform) //only direct children
            //{
            //    Transforms.Add(trans);
            //}
            Transforms = GetComponentsInChildren<Transform>().ToList<Transform>(); //recursive children and self
            //Games = GetComponentsInChildren<GameObject>().ToList<GameObject>(); //werkt niet
        }
    }
}