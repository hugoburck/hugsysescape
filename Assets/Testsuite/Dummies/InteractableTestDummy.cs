﻿
public class InteractableTestDummy : Interactable
{
    public bool ClickOverState;

    protected override void OnClickOver()
    {
        base.OnClickOver();
        ClickOverState = true;
    }
}

