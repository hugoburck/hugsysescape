﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourTestDummy : MonoBehaviour
{
    public bool State { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        State = true;
    }

    // Update is called once per frame
    void Update()
    {
        State = true;
    }
}
