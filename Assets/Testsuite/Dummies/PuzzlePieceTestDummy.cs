﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePieceTestDummy : PuzzlePiece
{
    public bool ClickOverActionState;

    protected override void OnClickOverAction()
    {
        ClickOverActionState = true;
    }

    
}
