﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;

namespace Helper
{
    public class CallMonoBehaviourReflectionMethodTests
    {
        private MonoBehaviourTestDummy monoDummy;

        [SetUp]
        public void Setup()
        {
            monoDummy = new GameObject().AddComponent<MonoBehaviourTestDummy>();
        }

        [Test]
        public void Update_IsCalled_SetsStateOnMonoTest()
        {
            CallMonoBehaviourReflectionMethod.Update<MonoBehaviourTestDummy>(monoDummy);
            Assert.IsTrue(monoDummy.State);
        }

        [Test]
        public void Start_IsCalled_SetsStateOnMonoTest()
        {
            CallMonoBehaviourReflectionMethod.Start<MonoBehaviourTestDummy>(monoDummy);
            Assert.IsTrue(monoDummy.State);
        }

        [TearDown]
        public void TearDown()
        {
            if (monoDummy != null)
                UnityEngine.Object.DestroyImmediate(monoDummy.gameObject);
        }
    }
}
