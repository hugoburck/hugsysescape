﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.TestTools;

namespace PuzzleControl
{
    class ClickablePieceTests
    {
        private ClickablePiece clickablePiece;
        private Inspectable inspectable;
        private PuzzlePieceData pieceData;
        private AnimationInputTimer timer;

        [SetUp]
        public void Setup()
        {
            clickablePiece = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<ClickablePiece>();
            inspectable = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<Inspectable>();
            timer = new GameObject().AddComponent<AnimationInputTimer>();
            pieceData = ScriptableObject.CreateInstance<PuzzlePieceData>();
            clickablePiece.Timer = timer;
        }

        [Ignore("needs to be rewritten")]
        [Test]
        public void OnClickAction_Action_StateIsCorrect()
        {
            inspectable.Click();
            clickablePiece.Click();

            //Assert.That(clickablePiece.State, Is.EqualTo(PieceState.Correct));
        }
    }
}
