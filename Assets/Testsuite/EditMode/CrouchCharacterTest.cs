﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PlayerControl
{
    class CrouchCharacterTest
    {
        private CrouchCharacter crouchCharacter;
        private ICeilingCheck ceilingCheck;
        private readonly EventHandler<CrouchEventArgs> crouchHandler;
        private CrouchEventArgs crouchEventArgs;
        private CharacterController controller;

        public CrouchCharacterTest()
        {
            crouchHandler = delegate (object sender, CrouchEventArgs e)
            {
                crouchEventArgs = e;
            };
        }

        [SetUp]
        public void Setup() //volgorde van initialisatie maakt uit
        {
            ceilingCheck = Substitute.For<ICeilingCheck>();
            controller = new GameObject().AddComponent<CharacterController>();
            crouchCharacter = new GameObject().AddComponent<CrouchCharacter>().Inject(ceilingCheck, controller); 
            crouchCharacter.OnCrouchChanged += crouchHandler;
        }

        [TearDown]
        public void Teardown()
        {
            crouchCharacter.OnCrouchChanged -= crouchHandler;
        }

        [Test]
        public void DeviceInputOnInputEvent_CrouchIsPressed_EventArgsHasCrouchHeight()
        {
            ceilingCheck.HasEnoughHeadroom(default, default).ReturnsForAnyArgs(true);
            crouchCharacter.DeviceInput_OnEnterInputEvent(ActionOfPlayer.Crouch, 1f);

            Assert.That(crouchEventArgs.IsCrouching, Is.True);
            Assert.That(crouchEventArgs.ControllerHeight, Is.EqualTo(crouchCharacter.CrouchHeight));

        }

        [Test]
        public void DeviceInputOnInputEvent_CrouchReleased_EventArgsHasWalkHeight()
        {
            ceilingCheck.HasEnoughHeadroom(default, default).ReturnsForAnyArgs(true);

            crouchCharacter.DeviceInput_OnEnterInputEvent(ActionOfPlayer.Crouch, 1f);
            crouchCharacter.DeviceInput_OnEnterInputEvent(ActionOfPlayer.Crouch, 0f);

            Assert.That(crouchEventArgs.IsCrouching, Is.False);
            Assert.That(crouchEventArgs.ControllerHeight, Is.EqualTo(crouchCharacter.WalkHeight));
        }

        [Test]
        public void DeviceInputOnInputEvent_CrouchReleasedNoHeadRoom_EventArgsIsStillCrouching()
        {
            ceilingCheck.HasEnoughHeadroom(default, default).ReturnsForAnyArgs(false);

            crouchCharacter.DeviceInput_OnEnterInputEvent(ActionOfPlayer.Crouch, 1f);
            crouchCharacter.DeviceInput_OnEnterInputEvent(ActionOfPlayer.Crouch, 0f);

            Assert.That(crouchEventArgs.IsCrouching, Is.True);
            Assert.That(crouchEventArgs.ControllerHeight, Is.EqualTo(crouchCharacter.CrouchHeight));
        }

            
    }
}
