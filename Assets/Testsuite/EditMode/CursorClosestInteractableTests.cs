﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PlayerControl
{
    class CursorClosestInteractableTests
    {
        private CursorClosestInteractable cursorClosestInteractable;
        private Interactable interA, interB, interC;
        private List<Interactable> inters;
        private ICursorPosition cursorPosition;
        private Camera cam;

        [SetUp]
        public void Setup()
        {
            cursorClosestInteractable = new CursorClosestInteractable();
            inters = new List<Interactable>();
        }

        [TearDown]
        public void Teardown()
        {
            UnityEngine.Object.DestroyImmediate(interA);
            UnityEngine.Object.DestroyImmediate(interB);
            UnityEngine.Object.DestroyImmediate(interC);
        }

        [Test]
        public void GetClosest_ItemInFront_ItemIsClosest()
        {
            interA = UnityEngine.Object.Instantiate(new GameObject(), new Vector3(0,0,-1), Quaternion.identity).AddComponent<BoxCollider>().gameObject.AddComponent<PuzzlePieceTestDummy>();
            interB = UnityEngine.Object.Instantiate(new GameObject(), new Vector3(1, 0, -1), Quaternion.identity).AddComponent<BoxCollider>().gameObject.AddComponent<PuzzlePieceTestDummy>();
            interC = UnityEngine.Object.Instantiate(new GameObject(), new Vector3(0, 1, -1), Quaternion.identity).AddComponent<BoxCollider>().gameObject.AddComponent<PuzzlePieceTestDummy>();
            inters.Add(interA);
            inters.Add(interB);
            inters.Add(interC);
            cam = new GameObject().AddComponent<Camera>();
            cursorPosition = Substitute.For<ICursorPosition>();
            cursorPosition.Position.Returns(new Vector2(cam.pixelWidth/2,cam.pixelHeight/2));

            var interactable = cursorClosestInteractable.GetClosest(inters,cursorPosition, cam, 64);

            Assert.That(interactable, Is.EqualTo(interA));

        }
    }
}
