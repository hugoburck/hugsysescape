﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PlayerControl
{
    class CursorRaycastTests
    {
        private CursorRaycast cursorRaycast;
        private Camera cam;
        private ICursorPosition cursorPos;
        private IRaycast raycast;
        private GameObject target;

        [SetUp]
        public void Setup()
        {
            target = UnityEngine.Object.Instantiate(new GameObject(), new Vector3(0, 0, -1), Quaternion.identity);
            target.AddComponent<BoxCollider>();
            cam = new GameObject().AddComponent<Camera>();
            cursorPos = Substitute.For<ICursorPosition>();
            cursorPos.Position.Returns(new Vector2(cam.pixelWidth/2, cam.pixelHeight/2));
            raycast = Substitute.For<IRaycast>();
            raycast.GetTransform(default,default,default).ReturnsForAnyArgs<Transform>(target.transform);
            cursorRaycast = new GameObject().AddComponent<CursorRaycast>().Inject(cam,cursorPos,raycast);
        }

        [Test]
        public void IsHit_RayShootsAtObject_ObjectIsHit()
        {
            cursorRaycast.HandleFrame();

            Assert.That(cursorRaycast.IsHit(target.transform), Is.True);
        }

    }
}
