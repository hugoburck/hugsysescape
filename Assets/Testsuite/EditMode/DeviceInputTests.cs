﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PlayerControl
{
    public class DeviceInputTests
    {
        private DeviceInput deviceInput;
        private IAxisFromInput axisFromMock;
        private ActionOfPlayer actionOfPlayer;
        private int eventRaises;

        [SetUp]
        public void Setup() //MS advises against using setup and teardown
        {
            deviceInput = new GameObject().AddComponent<DeviceInput>();
            axisFromMock = Substitute.For<IAxisFromInput>();
            deviceInput.Inject(axisFromMock);
            actionOfPlayer = default;
            eventRaises = 0;
        }

        [Test]
        public void InputEvent_UseHasInput_EventContainsUse()
        {       
            deviceInput.OnInputEvent += Handler;
            axisFromMock.GetAxis(deviceInput.ActionInputName(ActionOfPlayer.Use)).Returns<float>(1f);

            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);

            Assert.AreEqual(1, eventRaises);
            Assert.AreEqual(actionOfPlayer, ActionOfPlayer.Use);
        }

        [Test]
        public void EnterInputEvent_UseHasNewInput_EventContainsUse()
        {
            deviceInput.OnEnterInputEvent += Handler;

            axisFromMock.GetAxis(deviceInput.ActionInputName(ActionOfPlayer.Sprint)).Returns<float>(1f);
         
            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);
         
            axisFromMock.GetAxis(deviceInput.ActionInputName(ActionOfPlayer.Use)).Returns<float>(1f);
         
            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);

            Assert.AreEqual(2, eventRaises);
            Assert.AreEqual(actionOfPlayer, ActionOfPlayer.Use);
        }

        [Test]
        public void EnterInputEvent_UseHasSameInput_EventIsNotRaised()
        {
            deviceInput.OnEnterInputEvent += Handler;
            axisFromMock.GetAxis(deviceInput.ActionInputName(ActionOfPlayer.Use)).Returns<float>(1f);

            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);
            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);

            Assert.AreEqual(1, eventRaises);
        }

        [Test]
        public void ExitInputEvent_UseIsNowUnused_EventIsRaised()
        {
            deviceInput.OnExitInputEvent += Handler;
            axisFromMock.GetAxis(deviceInput.ActionInputName(ActionOfPlayer.Use)).Returns<float>(1f);

            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);
 
            axisFromMock.GetAxis(deviceInput.ActionInputName(ActionOfPlayer.Use)).Returns<float>(0f);
 
            CallMonoBehaviourReflectionMethod.Update<DeviceInput>(deviceInput);

            Assert.AreEqual(1, eventRaises);
            Assert.AreEqual(actionOfPlayer, ActionOfPlayer.Use);
        }

        [TearDown]
        public void TearDown()
        {
            UnityEngine.Object.DestroyImmediate(deviceInput.gameObject);
            deviceInput.OnInputEvent -= Handler;
            deviceInput.OnEnterInputEvent -= Handler;
            deviceInput.OnExitInputEvent -= Handler;
        }

        private void Handler(ActionOfPlayer pa, float i)
        {
            eventRaises++;
            actionOfPlayer = pa;
        }
    }
}
