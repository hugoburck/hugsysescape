﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.TestTools;
using NSubstitute;

namespace Journal
{
    class EntryMonoTests
    {
        private EntryMono entryMono;
        private MessageUnit messageUnit;
        private ICheckInitialized checkInitialized;
        private UguiButtonEventHandler uguiButtonEventHandler;
        private IGuiText guiText;

        [SetUp]
        public void Setup()
        {
            uguiButtonEventHandler = new GameObject().AddComponent<UguiButtonEventHandler>();
            checkInitialized = Substitute.For<ICheckInitialized>();
            checkInitialized.CheckSuccess.Returns(true);
            checkInitialized.CheckFail.Returns(false);
            guiText = Substitute.For<IGuiText>();
            entryMono = new GameObject().AddComponent<EntryMono>().Inject(checkInitialized, uguiButtonEventHandler, guiText);
            messageUnit = new MessageUnit { Message = "hello" };

        }

        [Test]
        public void Remove_RemoveMessage_EntriesDoesNotContainMessage()
        {
            Assert.That(EntryManager.EntriesCount, Is.EqualTo(0));
            entryMono.Initialize(messageUnit);

            Assert.That(EntryManager.EntriesCount, Is.EqualTo(1));
            EntryManager.Remove(messageUnit);

            Assert.That(EntryManager.EntriesCount, Is.EqualTo(0));
        }
    }
}
