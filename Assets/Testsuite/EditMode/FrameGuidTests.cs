﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;

namespace Helper
{
    class FrameGuidTests
    {
        private FrameGuid frameGuid;
        private Guid guid;
        private Action handler;

        [SetUp]
        public void Setup()
        {
            frameGuid = new GameObject().AddComponent<FrameGuid>();
        }

        [Test]
        public void IsDifferent_CalledNextFrame_IsTrue()
        {
            guid = FrameGuid.Current;
            CallMonoBehaviourReflectionMethod.Update<FrameGuid>(frameGuid);
            Assert.IsTrue(FrameGuid.IsDifferentTo(guid));
        }

        [Test]
        public void IsDifferent_CalledSameFrame_IsFalse()
        {
            guid = FrameGuid.Current;
            Assert.IsFalse(FrameGuid.IsDifferentTo(guid));
        }

        [Test]
        public void OnNewFrame_CalledOnUpdate_IsRaised()
        {
            bool raised = false;
            FrameGuid.OnNewFrame += handler = () => { raised = true; };

            CallMonoBehaviourReflectionMethod.Update<FrameGuid>(frameGuid);

            Assert.IsTrue(raised);
        }

        [TearDown]
        public void TearDown()
        {
            UnityEngine.Object.DestroyImmediate(frameGuid.gameObject);
            FrameGuid.OnNewFrame -= handler;
        }
    }
}
