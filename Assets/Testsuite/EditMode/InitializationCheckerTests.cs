﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;

namespace Helper
{
    public class InitializationCheckerTests
    {
        public MonoBehaviourTestDummy owner;
        public MonoBehaviourTestDummy argA;
        public MonoBehaviourTestDummy argB;
        public MonoBehaviourTestDummy argNull;

        [SetUp]
        public void Setup()
        {
            owner = new GameObject().AddComponent<MonoBehaviourTestDummy>();
            argA = new GameObject().AddComponent<MonoBehaviourTestDummy>();
            argB = new GameObject().AddComponent<MonoBehaviourTestDummy>();
        }

        [Test]
        public void CheckImmediate_ComponentsAreNotNull_CheckIsTrue()
        {
            Assert.IsTrue(InitializationChecker.CheckImmediate(owner, argA));
            Assert.IsTrue(InitializationChecker.CheckImmediate(owner, argA, argB));
            Assert.IsTrue(InitializationChecker.CheckImmediate(null, argA, argB));
        }

        [Test]
        public void CheckImmediate_ComponentsArNull_CheckIsFalse()
        {
            
            Assert.That(InitializationChecker.CheckImmediate(owner, argNull), Is.False);
            LogAssert.Expect(LogType.Error, InitializationChecker.ErrorMessage);
            Assert.That(InitializationChecker.CheckImmediate(owner, argNull, null), Is.False);
            LogAssert.Expect(LogType.Error, InitializationChecker.ErrorMessage);
        }

        //[Test]
        public void CheckImmediate_ComponentsAreNull_ThrowsException() //no longer throws, setup attribute commented. Leaving code as example.
        {
            Assert.That(() => InitializationChecker.CheckImmediate(owner, argNull),Throws.Exception.TypeOf<MissingComponentException>());
            Assert.That(() => InitializationChecker.CheckImmediate(owner, argNull, null), Throws.Exception.TypeOf<MissingComponentException>());
        }
    }
}
