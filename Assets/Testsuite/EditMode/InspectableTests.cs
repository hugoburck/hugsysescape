﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PuzzleControl
{
    class InspectableTests
    {
        private Inspectable inspectable, inspectable2;

        [SetUp]
        public void Setup()
        {
            inspectable = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<Inspectable>();
            inspectable2 = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<Inspectable>();
        }

        [Test]
        public void OnClickOver_AddedToInspectionList_IsBeingInspected()
        {
            inspectable.Click();

            Assert.That(inspectable.IsBeingInspected, Is.True);
        }

        [Test]
        public void OnClickOver_AddedToTwoToInspectionList_SecondIsBeingInspected()
        {
            inspectable.Click();
            inspectable2.Click();

            Assert.That(inspectable.IsBeingInspected, Is.False);
            Assert.That(inspectable2.IsBeingInspected, Is.True);
        }

        [Test]
        public void OnCancel_RemoveSecondInspection_FirstIsBeingInspected()
        {
            inspectable.Click();
            inspectable2.Click();
            inspectable2.Cancel();

            Assert.That(inspectable.IsBeingInspected, Is.True);
            Assert.That(inspectable2.IsBeingInspected, Is.False);
        }
    }
}
