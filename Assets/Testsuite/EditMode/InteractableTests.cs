﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using Codebase.PuzzleControl;
using NSubstitute;

namespace PuzzleControl
{
    class InteractableTests
    {
        private Interactable interactable;
        private InteractableTestDummy dummy;
        private IAccessRequisites accessRequisites;
        private IAccessArea accessArea;
        private ICursorRaycast cursorRaycast;

        [SetUp]
        public void Setup()
        {
            accessRequisites = Substitute.For<IAccessRequisites>();
            accessArea = Substitute.For<IAccessArea>();
            cursorRaycast = Substitute.For<ICursorRaycast>();
            interactable = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<InteractableTestDummy>().Inject(accessRequisites, accessArea, cursorRaycast);
            dummy = interactable as InteractableTestDummy;
        }

        [TearDown]
        public void TearDown()
        {
            UnityEngine.Object.DestroyImmediate(interactable.gameObject);
        }

        [Test]
        public void HandleInput_RayOverPieceIsClicked_StateIsClicked()
        {
            accessRequisites.IsMeeting.Returns(true);
            accessArea.IsWithin.Returns(true);

            interactable.HandleHitChanged(interactable.transform,interactable.Collider);
            interactable.HandleInput(ActionOfPlayer.Use);

            Assert.IsTrue(dummy.ClickOverState);
        }
    }
}
