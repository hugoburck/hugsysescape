﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Helper
{
    public class LayerMaskManagerTests
    {
        private const string defaultText = "Default", 
            transparentFXText = "TransparentFX",
            ignoreRaycastText = "Ignore Raycast",
            incorrectText = "Ipsem Lorum";

        // A Test behaves as an ordinary method
        [Test]
        public void GetLayer_FromLayerEnum_IsCorrectLayerNumber()
        {
            int layerFromManager = LayerMaskManager.GetLayer(UnitOfLayer.Default);
            int layerFromUnity = LayerMask.NameToLayer(defaultText);

            Assert.AreEqual(layerFromUnity, layerFromManager);
        }

        [Test]
        public void GetMask_FromLayerEnums_IsCorrectMask()
        {
            LayerMask managerMask = LayerMaskManager.GetMask(UnitOfLayer.Default, UnitOfLayer.TransparentFX);
            LayerMask unityMask = LayerMask.GetMask(defaultText, transparentFXText);

            Assert.AreEqual(unityMask, managerMask);
        }

        [Test]
        public void GetLayer_FromLayerEnumWithUnderscore_IsStillCorrectLayer()
        {
            int layerFromManager = LayerMaskManager.GetLayer(UnitOfLayer.Ignore_Raycast);
            int layerFromUnity = LayerMask.NameToLayer(ignoreRaycastText);

            Assert.AreEqual(layerFromUnity, layerFromManager);
        }
    }
}
