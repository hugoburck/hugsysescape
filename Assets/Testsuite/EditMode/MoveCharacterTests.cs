﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PlayerControl
{
    class MoveCharacterTests
    {
        private MoveCharacter moveCharacter;

        [SetUp]
        public void Setup()
        {
            moveCharacter = new GameObject().AddComponent<MoveCharacter>();
        }

        [TearDown]
        public void TearDown()
        {
            UnityEngine.Object.DestroyImmediate(moveCharacter.gameObject);
        }

        [Test]
        public void AddSpeedModifier_AddNewModifier_ReturnsTrue()
        {
            IModifySpeed speed = Substitute.For<IModifySpeed>();

            bool added = MoveCharacter.AddSpeedModifier(speed);

            Assert.IsTrue(added);
        }

        [Test]
        public void AddSpeedModifier_AddsSameModifier_ReturnsFalse()
        {
            IModifySpeed speed = Substitute.For<IModifySpeed>();

            bool added = MoveCharacter.AddSpeedModifier(speed);
            added = MoveCharacter.AddSpeedModifier(speed);

            Assert.IsFalse(added);
        }

        [Test]
        public void GetInput_InputWalksForwardWhileExploring_MovementIsForward()
        {
            Vector3 movement = moveCharacter.Move(ActionOfPlayer.WalkDepth, 1f,StateOfPlayer.Exploring);

            Assert.AreEqual(Vector3.forward, moveCharacter.Movement);
        }

        [Test]
        public void GetInput_InputWalksForwardWhileInspecting_MovementIsZero()
        {
            Vector3 movement = moveCharacter.Move(ActionOfPlayer.WalkDepth, 1f, StateOfPlayer.InspectingPuzzle);

            Assert.AreEqual(Vector3.zero, moveCharacter.Movement);
        }
    }
}
