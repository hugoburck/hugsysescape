﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codebase.PuzzleControl;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;

namespace PuzzleControl
{
    class PuzzlePieceTests
    {
        private PuzzlePiece puzzlePiece;
        private PuzzlePieceTestDummy dummy;
        private PuzzlePieceData pieceData, pieceData2;
        private MessageRepoControl messageRepoControl;
        private MessageDisplay messageDisplay;
        private Inspectable inspectable;
        private IActionRequisites actionRequisites;

        public PuzzlePieceTests()
        {

        }

        [SetUp]
        public void Setup()
        {
            actionRequisites = Substitute.For<IActionRequisites>();
            messageRepoControl = new GameObject().AddComponent<MessageRepoControl>();
            messageDisplay = new GameObject().AddComponent<MessageDisplay>();
            pieceData = ScriptableObject.CreateInstance<PuzzlePieceData>();
            pieceData2 = ScriptableObject.CreateInstance<PuzzlePieceData>();
            puzzlePiece = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<PuzzlePieceTestDummy>();
            dummy = puzzlePiece as PuzzlePieceTestDummy;
            inspectable = new GameObject().AddComponent<BoxCollider>().gameObject.AddComponent<Inspectable>();
        }

        [TearDown]
        public void Teardown()
        {
            UnityEngine.Object.DestroyImmediate(puzzlePiece);
            UnityEngine.Object.DestroyImmediate(pieceData);
            UnityEngine.Object.DestroyImmediate(pieceData2);
            UnityEngine.Object.DestroyImmediate(dummy);
        }

        [Test]
        public void IsClicked_HasNoRequisites_CallsAction()
        {
            puzzlePiece.Click();

            Assert.That(dummy.ClickOverActionState, Is.True);
        }

        [Test]
        public void IsClicked_HasRequisitesAndCorrect_CallsAction()
        {
            puzzlePiece.Inject(actionRequisites);
            actionRequisites.IsMeeting.Returns(true);

            puzzlePiece.Click();

            Assert.That(dummy.ClickOverActionState, Is.True);
        }

        [Test]
        public void IsClicked_HasRequisitesAndInCorrect_CallsNoAction()
        {
            puzzlePiece.Inject(actionRequisites);
            actionRequisites.IsMeeting.Returns(false);

            puzzlePiece.Click();
            Assert.That(dummy.ClickOverActionState, Is.False);
        }

        [Test]
        public void IsClicked_InspectionInspected_IsClicked()
        {
            puzzlePiece.Inject(inspectable);

            inspectable.Click();

            Assert.That(dummy.IsClickable, Is.True);
        }

        [Test]
        public void IsClickable_InspectionNotInspected_IsNotClickable()
        {
            puzzlePiece.Inject(inspectable);

            Assert.That(dummy.IsClickable, Is.False);
        }

    }
}
