﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System;
using System.Reflection;
using NSubstitute;

namespace PlayerControl
{
    class RotateCameraTests
    {
        private CameraCharacterRotation cameraCharacterRotation;

        [SetUp]
        public void Setup()
        {
            cameraCharacterRotation = new GameObject().AddComponent<CameraCharacterRotation>();
        }

        [TearDown]
        public void TearDown()
        {
            UnityEngine.Object.DestroyImmediate(cameraCharacterRotation.gameObject);
        }

        [Test]
        public void Rotate_LooksRightExploring_RotatedRight()
        {
            cameraCharacterRotation.SetEulerRotation(Vector3.zero);
            Vector3 result = Vector3.up;

            cameraCharacterRotation.Rotate(ActionOfPlayer.LookHeading, 1f, StateOfPlayer.Exploring,false);

            Assert.AreEqual(result, cameraCharacterRotation.EulerRotation);
        }

        [Test]
        public void Rotate_LooksRightNotExploring_NotRotated()
        {
            Vector3 current = Vector3.left;
            cameraCharacterRotation.SetEulerRotation(current);

            cameraCharacterRotation.Rotate(ActionOfPlayer.LookHeading, 1f, StateOfPlayer.InspectingItem, false);

            Assert.AreEqual(current, cameraCharacterRotation.EulerRotation);
        }

        [Test]
        public void RotateWithoutPitch_LooksLeftAndUp_OnlyLeft()
        {
            Vector3 result = new Vector3(0,1,0);

            cameraCharacterRotation.Rotate(ActionOfPlayer.LookHeading, 1f, StateOfPlayer.Exploring, false);
            cameraCharacterRotation.Rotate(ActionOfPlayer.LookPitch, 1f, StateOfPlayer.Exploring, false);
            float difference = Vector3.Distance(result, cameraCharacterRotation.RotationWithoutPitch.eulerAngles);

            Assert.IsTrue(difference < 0.0001);
        }

        [Test]
        public void RotateWithoutPitch_LooksLeftAndUp_ReturnLeftUp()
        {
            Vector3 result = new Vector3(1, 1, 0);

            cameraCharacterRotation.Rotate(ActionOfPlayer.LookHeading, 1f, StateOfPlayer.Exploring, false);
            cameraCharacterRotation.Rotate(ActionOfPlayer.LookPitch, 1f, StateOfPlayer.Exploring, false);
            float difference = Vector3.Distance(result, cameraCharacterRotation.RotationWithPitch.eulerAngles);

            Assert.IsTrue(difference < 0.0001);
        }
    }
}
