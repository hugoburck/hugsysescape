﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codebase.PuzzleControl;
using UnityEngine;

namespace Solution
{
    class SolutionCheckerTests
    {
        private SolutionChecker solutionChecker;
        private List<SolutionData> solutions;
        private InventoryItemData item, itemOther, itemReward;
        private List<InventoryItemData> itemsRaised;
        private PuzzlePieceData piece, pieceRaised;
        private SolutionItemData solutionItem;
        private SolutionPieceData solutionPiece;
        private Action<InventoryItemData> handleItem, handleReward;
        private Action<PuzzlePieceData> handlePiece;
        private int raisedCount;

        public SolutionCheckerTests()
        {
            solutions = new List<SolutionData>();
            itemsRaised = new List<InventoryItemData>();
            item = ScriptableObject.CreateInstance<InventoryItemData>();
            itemOther = ScriptableObject.CreateInstance<InventoryItemData>();
            itemReward = ScriptableObject.CreateInstance<InventoryItemData>();
            piece = ScriptableObject.CreateInstance<PuzzlePieceData>();
            solutionItem = ScriptableObject.CreateInstance<SolutionItemData>();
            solutionItem.ItemData = item;
            solutionItem.OtherItemData = itemOther;
            solutionItem.RewardItemData = itemReward;
            solutionPiece = ScriptableObject.CreateInstance<SolutionPieceData>();
            solutionPiece.ItemData = item;
            solutionPiece.PuzzlePieceData = piece;
            solutionPiece.RewardItemData = itemReward;
            solutions.Add(solutionItem);
            solutions.Add(solutionPiece);
            handleReward = handleItem = i => 
            { 
                itemsRaised.Add(i);
                raisedCount++;
            };
            handlePiece = p =>
            {
                pieceRaised = p;
                raisedCount++;
            };

        }

        [SetUp]
        public void Setup()
        {
            solutionChecker = new GameObject().AddComponent<SolutionChecker>().Inject(solutions);
            SolutionChecker.OnSolutionItem += handleItem;
            SolutionChecker.OnSolutionReward += handleReward;
            SolutionChecker.OnSolutionPiece += handlePiece;
            itemsRaised.Clear();
            pieceRaised = null;
            raisedCount = 0;
        }

        [TearDown]
        public void Teardown()
        {
            SolutionChecker.OnSolutionItem -= handleItem;
            SolutionChecker.OnSolutionReward -= handleReward;
            SolutionChecker.OnSolutionPiece -= handlePiece;
        }

        [Test]
        public void RaiseIfItemOnItemSolution_MatchFoundWithReward_RaisesThreeItemEvents()
        {
            solutionChecker.RaiseIfItemOnItemSolution(item, itemOther);

            Assert.That(itemsRaised.Contains(item), Is.True);
            Assert.That(itemsRaised.Contains(itemOther), Is.True);
            Assert.That(itemsRaised.Contains(itemReward), Is.True);
        }

        [Test]
        public void RaiseIfItemOnItemSolution_NoMatchFound_NoEvents()
        {
            var itemNew = ScriptableObject.CreateInstance<InventoryItemData>();
            var itemOtherNew = ScriptableObject.CreateInstance<InventoryItemData>();

            solutionChecker.RaiseIfItemOnItemSolution(itemNew, itemOtherNew);

            Assert.That(itemsRaised.Count == 0);
            Assert.That(raisedCount == 0);
        }

        [Test]
        public void RaiseIfItemOnPieceSolution_MatchFound_RaisesTwoItemEventAndOnePieceEvent()
        {
            solutionChecker.RaiseIfItemOnPieceSolution(item, piece);

            Assert.That(itemsRaised.Contains(item), Is.True);
            Assert.That(pieceRaised,Is.EqualTo(piece));
            Assert.That(itemsRaised.Contains(itemReward), Is.True);
        }
    }
}
