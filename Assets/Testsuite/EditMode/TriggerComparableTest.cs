﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Helper
{
    public class TriggerComparableTest
    {
        private TriggerComparable<int> trigger;

        [SetUp]
        public void Setup()
        {
            trigger = new TriggerComparable<int>();
            trigger.OnChange(0);
        }

        [TestCase(1,true)]
        [TestCase(0,false)]
        [TestCase(-1,false)]
        public void OnUp_NumberGoesIn_IsResult(int number,bool result)
        {
            Assert.IsTrue(trigger.OnUp(number) == result);
        }

        [TestCase(1, false)]
        [TestCase(0, false)]
        [TestCase(-1, true)]
        [Test]
        public void OnDown_NumberGoesIn_IsResult(int number, bool result)
        {
            Assert.IsTrue(trigger.OnDown(number) == result);
        }

        [TestCase(1, true)]
        [TestCase(0, false)]
        [TestCase(-1, true)]
        [Test]
        public void OnChange_NumberGoesIn_IsResult(int number, bool result)
        {
            Assert.IsTrue(trigger.OnChange(number) == result);
        }
    }
}
