﻿using Codebase.Helpers.Extensions;
using NUnit.Framework;
using UnityEngine;

namespace Helper
{
    public class VectorExtentionTests
    {
        [Test]
        public void SetX_XIsSupplied_VectorHasX()
        {
            Vector3 vector = new Vector3(1, 1, 1);
            
            vector.SetX(2);
            
            Assert.That(vector.Equals(new Vector3(2,1,1)));
        }
        
        [Test]
        public void SetY_YIsSupplied_VectorHasY()
        {
            Vector3 vector = new Vector3(1, 1, 1);
            
            vector.SetY(2);
            
            Assert.That(vector.Equals(new Vector3(1,2,1)));
        }
        
        [Test]
        public void SetZ_ZIsSupplied_VectorHasZ()
        {
            Vector3 vector = new Vector3(1, 1, 1);
            
            vector.SetZ(2);
            
            Assert.That(vector.Equals(new Vector3(1,1,2)));
        }
        
        [Test]
        public void CopyWithX_XIsSupplied_CopyHasX()
        {
            Vector3 vector = new Vector3(1, 1, 1);
            
            vector = vector.CopyWithX(2);
            
            Assert.That(vector.Equals(new Vector3(2,1,1)));
        }
        
        [Test]
        public void CopyWithY_YIsSupplied_CopyHasY()
        {
            Vector3 vector = new Vector3(1, 1, 1);
            
            vector = vector.CopyWithY(2);
            
            Assert.That(vector.Equals(new Vector3(1,2,1)));
        }
        
        [Test]
        public void CopyWithZ_ZIsSupplied_CopyHasZ()
        {
            Vector3 vector = new Vector3(1, 1, 1);
            
            vector = vector.CopyWithZ(2);
            
            Assert.That(vector.Equals(new Vector3(1,1,2)));
        }
    }
}