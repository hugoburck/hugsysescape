﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Helper
{
    public class MonoBehaviourSingletonTests //Singlton cannot be tested in EditMode (DontDestroyOnLoad())
    {
        private MonoBehaviourSingletonTestDummy dummy;

        [Test]
        public void Instance_ReturnsCurrent_IsNotNull()
        {
            dummy = new GameObject().AddComponent<MonoBehaviourSingletonTestDummy>();
            var instance = MonoBehaviourSingletonTestDummy.Instance;
            Assert.AreEqual(dummy, instance);
        }

        [Test]
        public void Instance_ReturnsNew_IsNotNull()
        {
            var instance = MonoBehaviourSingletonTestDummy.Instance;
            Assert.IsNotNull(instance);
        }

        [TearDown]
        public void TearDown()
        {
            if (dummy != null)
                UnityEngine.Object.DestroyImmediate(dummy.gameObject);
        }
    }
}
